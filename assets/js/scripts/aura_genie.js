
$(document).ready(function() {
    $('body').on('change', '[name="selectDistributor"]', function() {
        var url = $(this).attr('data-url');
        var distributor_id = $(this).val();
        var parent_element = $(this).parent().parent();
        console.log(parent_element);
        var parent_id = $(parent_element).attr('id');
        var user_array = parent_id.split('^^');
        var user_id = user_array[1];
        var data = {
            user_id: user_id,
            distributor_id: distributor_id
        };
        var distributorId = $(this).val();
        $.ajax({
            type: "POST",
            url: site_url + url,
            async: false,
            data: data,
            success: function(html) {
                kq = $.trim(html);
                if (kq == 1) {
                    alert("Distributor Updated Successfully");
                    $(parent_element).find('.sendEmail').show();
                } else {
                    alert("Updation Failed.");
                }
            }
        });

    });

    $("#generate-codes").button().click(function() {
        var isOK = confirm('Are you sure to generate more 1000 codes?');
        var url = $(this).attr('data-url');
        if (isOK) {
            $(this).attr('href', site_url + url + "/generateCodes?genCode=1&numOfCodes=1000");
        }
    });



});
function user_change_password(obj) {
    var form = $(obj).parents('form.user_change_password_form');
    if (form.valid()) {
        $.ajax({
            url: $(form).attr("action"),
            method: "post",
            data: form.serialize(),
            dataType: "json",
            beforeSend: function() {
                $(obj).attr('disabled', 'disabled');
                $(form).find('span.help-block-error').remove();
                $('div.load-spinner').show();
            },
            complete: function() {
                $('div.load-spinner').hide();
                $(obj).removeAttr('disabled');
            },
            success: function(response) {
                if (response['status'] == true) {
                    $(form).find('input[name="password"]').val("");
                    $(form).find('input[name="confirm_password"]').val("");
                    $("#ajax-modal").modal('hide');
                    alert("Password changed successfully");
                } else {
                    var message = typeof (response['message']) != 'undefined' ? response['message'] : "";
                    if (typeof (message.password) != 'undefined') {
                        $(form).find('input[name="password"]').parent('p').addClass('has-error');
                        $(form).find('input[name="password"]').parent('p').find('span').remove();
                        $(form).find('input[name="password"]').after('<span id="Password-error" class="help-block help-block-error">' + message.password + '</span>');
                    }
                    if (typeof (message.confirm_password) != 'undefined') {
                        $(form).find('input[name="confirm_password"]').parent('p').addClass('has-error');
                        $(form).find('input[name="confirm_password"]').parent('p').find('span').remove();
                        $(form).find('input[name="confirm_password"]').after('<span id="confirm_password-error" class="help-block help-block-error">' + message.confirm_password + '</span>');
                    }
                    if (typeof (response['server_error']) != 'undefined') {
                        $(form).find('.server_error').find('span').html(response['server_error']);
                        (form).find('.server_error').show();
                    }

                }
            }
        });
    }
}

function send_activation_key_old(id) {
  var data = {
    id: id,
  };
  $.ajax({
    url: site_url + 'API/User/send_activation_key',
    method: "post",
    data: data,
    dataType: "json",
    beforeSend: function() {
      $('#send_activation_key_' + id).attr('disabled', 'disabled')
      $('div.load-spinner').show();
    },
    complete: function() {
      $('#send_activation_key_' + id).removeAttr("disabled");
      $('div.load-spinner').hide();
    },
    success: function(response) {
      if (response['status'] == true) {
        alert("Activation key sent successfully.");
      }
    }
  });
}


function send_activation_key(id, url) {
    if(url.length <=0){
        return false;
    }
    var data = {
        id: id,
    };
    $.ajax({
        url: site_url + url,
        method: "post",
        data: data,
        dataType: "json",
        beforeSend: function() {
            $('#send_activation_key_' + id).attr('disabled', 'disabled');
            $('div.load-spinner').show();
        },
        complete: function() {
            $('div.load-spinner').hide();
            $('#send_activation_key_' + id).removeAttr('disabled');
        },
        success: function(response) {
            if (response['status'] == true) {
                alert("Activation key sent successfully.");
            }
        }
    });
}

function send_3d_activation_key(id) {
    var data = {
        id: id,
    };
    $.ajax({
        url: site_url + 'Admin/genius_insight/send_3d_activation_key',
        method: "post",
        data: data,
        dataType: "json",
        beforeSend: function() {
            $('#send_activation_key_' + id).attr('disabled', 'disabled');
            $('div.load-spinner').show();
        },
        complete: function() {
            $('div.load-spinner').hide();
        },
        success: function(response) {
            if (response['status'] == true) {
                alert("3D Activation key sent successfully.");
            }
        }
    });
}

function sendEmail(obj) {
    var distributorId = $(obj).closest('tr').find('[name="selectDistributor"]').find('option:selected').val();
    console.log(distributorId);
    var id = $($(obj).parent().parent()).attr("id").split('^^');
    console.log(id[1]);
    if (distributorId === "") {
        alert("Please Select The Distributor");
        return false;
    }
//    var html = '<div class="loading-spinner in" style="width:15%;margin-left: -164px;z-index:10051;margin-top:-1%">';
//    html += '<div class="progress progress-striped active">';
//    html += '<div class="progress-bar" style="width:100%"></div>';
//    html += '</div>';
//    html += '</div>';
//    $("body").append(html);

    $.ajax({
        type: "POST",
        url: site_url + $(obj).attr('data-url'),
        async: false,
        data: {
            "distributor_id": distributorId,
            "user_id": id[1],
        },
        beforeSend: function() {
            $('div.load-spinner').show();
        },
        complete: function() {
            $('div.load-spinner').hide();
        },
        success: function(html) {
            
//            $("body").find(".loading-spinner").remove();
            var response = $.trim(html);
            if (response == 1)
            {
                alert("Email Sent Successfully");
            }
            else {
                alert("Failed To Send Email. Please try Again..");
                console.log("response : " + response);
            }
        },
        error: function(xhr, status, code) {
//            $("body").find(".loading-spinner").remove();
        },
    });


}

function check_email(url, obj) {
    var email = $.trim($(obj).val());

    if (email === "")
        return false;
    $.ajax({
        url: site_url + url + "/check_email",
        data: {"email": email},
        type: "POST",
        asynch: false,
        success: function(html) {
            var response = $.trim(html);
            if (response == 1) {
                var result = confirm("Email You Entered Already Exist. Do you want to Create A New Row? ");
                if (!result) {
                    $(obj).val("");
                }
            }
        },
        failure: function(xhr, status, code) {
            alert("Error Occured");
            alert("parameter 1 : " + xhr);
            alert("parameter 2 : " + status);
            alert("parameter 3 : " + code);
        }
    });
    return false;
}

function delete_user(str) {
    var data_url = $(str).attr('data_url');
    console.log(str.parentNode);
    var parent_element = str.parentNode.parentElement;
    var parent_id = parent_element.id;
    console.log("data_url");
    console.log(data_url);
    console.log("parent Element");
    console.log(parent_element);
    console.log("parent_id");
    console.log(parent_id);
    var user_array = parent_id.split('^^');
    var parent_container = user_array[0];
    var user_id = user_array[1];
    var data = {
        user_id: user_id,
    };

    console.log("user_id");
    /*
     token name and hashvalue variables defined in theme.php
     for post the values with security 
     */
    data[tokenname] = hashvalue;
    var result = confirm("Are you sure you want to delete this record!");
    if (result == true)
    {
        $.ajax({
            url: site_url + data_url,
            method: "post",
            data: data,
            success: function(response) {
//                if(parent_container =="tr")
//                str.parentNode.parentElement.remove();
                window.location.reload();
            }
        });
    } else {
        return false;
    }
}

function block_device(obj) {
    /* DISABLE THE BUTTON FIRST */
    $(obj).attr("disabled","disabled");
    var id = $(obj).data('id');
    var url = $(obj).data('url');
    var data = {id: id};
    var html = '<div class="loading-spinner in" style="width:15%;margin-left: -70px;z-index:10051;margin-top:-1%">';
    html += '<div class="progress progress-striped active">';
    html += '<div class="progress-bar" style="width:100%"></div>';
    html += '</div>';
    html += '</div>';
    $("body").append(html);
    $.ajax({
        url: site_url + 'Admin/' + url + '/block_device',
        method: "post",
        data: data,
        beforeSend: function() {
            $('div.load-spinner').show();
        },
        complete: function() {
            /* ENABLE THE BUTTON AFTER PROCESSING */
            $(obj).removeAttr('disabled');
        },
        success: function(response) {
            $('div.loading-spinner').hide();
            console.log(response);
            if (response == 1) {
                alert('Device blocked successfully');
            }
            $(obj).after('<button class="unblockDevice" role="button" data-url="'+ url+'" data-id="' + id + '" onclick="unblock_device(this)">Unblock</button>');
            $(obj).remove();
        }
    });
}

function unblock_device(obj) {
    $(obj).attr("disabled","disabled");
//        var data = { id: id};
    var id = $(obj).data('id');
    var url = $(obj).data('url');
    var data = {id: id};
    var html = '<div class="loading-spinner in" style="width:15%;margin-left: -70px;z-index:10051;margin-top:-1%">';
    html += '<div class="progress progress-striped active">';
    html += '<div class="progress-bar" style="width:100%"></div>';
    html += '</div>';
    html += '</div>';
    $("body").append(html);
    $.ajax({
        url: site_url + 'Admin/' + url + '/unblock_device',
        method: "post",
        data: data,
        beforeSend: function() {
            $('div.loading-spinner').show();
        },
        complete: function() {
            /* ENABLE THE BUTTON AFTER PROCESSING */
            $(obj).removeAttr('disabled');
        },
        success: function(response) {
            $('div.loading-spinner').hide();

            console.log(response);
            if (response == 1) {
                alert('Device unblocked successfully');
            }
            $(obj).after('<button class="blockDevice" role="button" data-url="'+ url+'" data-id="' + id + '" onclick="block_device(this)">Block</button>');
            $(obj).remove();
        }

    });
}
