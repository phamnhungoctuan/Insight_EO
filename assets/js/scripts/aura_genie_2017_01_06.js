function send_activation_key(id){
    var data = {
        id : id,
    };
        $.ajax({
            url:site_url+'Admin/genius_insight/send_activation_key',
            method:"post",
            data:data,
            dataType: "json",
            beforeSend: function () {
            $('#send_activation_key_'+id).attr('disabled','disabled')
        },
            success:function(response){
                if(response['status'] == true){
                    alert("Activation key sent successfully.");
                }
            }
        });
}

function send_3d_activation_key(id){
    var data = {
        id : id,
    };
        $.ajax({
            url:site_url+'Admin/genius_insight/send_3d_activation_key',
            method:"post",
            data:data,
            dataType: "json",
            beforeSend: function () {
            $('#send_activation_key_'+id).attr('disabled','disabled')
        },
            success:function(response){
                if(response['status'] == true){
                    alert("3D Activation key sent successfully.");
                }
            }
        });
}

function sendEmail(obj){
    var distributorId= $(obj).closest('tr').find('[name="selectDistributor"]').find('option:selected').val();
    console.log(distributorId);
    var id= $($(obj).parent().parent()).attr("id").split('^^');
    console.log(id[1]);
    if(distributorId===""){
        alert("Please Select The Distributor");
        return false;
    }
    var html='<div class="loading-spinner in" style="width:15%;margin-left: -164px;z-index:10051;margin-top:-1%">';
            html+='<div class="progress progress-striped active">';
            html+='<div class="progress-bar" style="width:100%"></div>';
            html+='</div>';
            html+='</div>';
    $("body").append(html);
    $.ajax({
        type: "POST",
        url: site_url+$(obj).attr('data-url'),
        async:false,
        data:{
                "distributor_id":distributorId,
                "user_id":id[1],    
             },
        success: function(html){
            $("body").find(".loading-spinner").remove();
            var response= $.trim(html);
            if(response==1)
            {
                alert("Email Sent Successfully");
            }
            else{
                alert("Failed To Send Email. Please try Again..");
                console.log("response : "+response);
            }
        },
        error:function(xhr,status,code){
            $("body").find(".loading-spinner").remove();
        },
    });
    
    
}


$(document).ready(function(){
    $('body').on('change','[name="selectDistributor"]',function() {
        var url= $(this).attr('data-url');
        var distributor_id=$(this).val();
        var parent_element = $(this).parent().parent();
        console.log(parent_element);
        var parent_id =$(parent_element).attr('id');
        var user_array = parent_id.split('^^');
        var user_id    = user_array[1];
        var data= {
            user_id : user_id,
            distributor_id:distributor_id
        };
        var distributorId = $(this).val();
        $.ajax({
            type: "POST",
            url: site_url+url,
            async: false,
            data:data,
            success: function(html) {
                kq = $.trim(html);
                if (kq == 1) {
                    alert("Distributor Updated Successfully");
                    $(parent_element).find('.sendEmail').show();
                } else{
                    alert("Updation Failed.");
                }
            }
        });

    });
    
    $("#generate-codes").button().click(function () {
        var isOK = confirm('Are you sure to generate more 1000 codes?');
        var url = $(this).attr('data-url');
        if (isOK){
            $(this).attr('href', site_url + url + "/generateCodes?genCode=1&numOfCodes=1000");
        }
    });
    
    
});

function check_email(url,obj){
    var email=$.trim($(obj).val());
    
    if(email==="") return false;
    $.ajax({
        url:site_url+url+"/check_email",
        data: {"email" : email},
        type:"POST",
        asynch:false,
        success: function(html){
            var response= $.trim(html);
            if(response==1){
                var result = confirm("Email You Entered Already Exist. Do you want to Create A New Row? ");
                if(!result){
                    $(obj).val("");
                }
            }
        },
        failure:function(xhr,status,code){
            alert("Error Occured");
            alert("parameter 1 : "+xhr);
            alert("parameter 2 : "+status);
            alert("parameter 3 : "+code);
        }
    });
    return false;
}

function delete_user(str)
{
    var data_url= $(str).attr('data_url');
    console.log(str.parentNode);
    var parent_element = str.parentNode.parentElement;
    var parent_id = parent_element.id;
    console.log("data_url"); console.log(data_url);
    console.log("parent Element");
    console.log(parent_element);
    console.log("parent_id");
    console.log(parent_id);
    var user_array = parent_id.split('^^');
    var parent_container= user_array[0];
    var user_id    = user_array[1];
    var data= {
        user_id : user_id,
    };
    
    console.log("user_id");
    /*
            token name and hashvalue variables defined in theme.php
            for post the values with security 
    */
    data[tokenname] = hashvalue;
    var result = confirm("Are you sure you want to delete this record!");
    if(result == true) 
    {
        $.ajax({
            url:site_url+data_url,
            method:"post",
            data:data,
            success:function(response){
//                if(parent_container =="tr")
//                str.parentNode.parentElement.remove();
                window.location.reload();
            }
        });
    } else {
        return false;
    }
}
