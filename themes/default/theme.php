<?php
defined('BASEPATH') OR exit('No direct script access allowed');
echo validation_errors('<span class="error">', '</span>');
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.4
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Energy Re-Mastered</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/plugins/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/login.css" rel="stylesheet" type="text/css"/>
        <?php if($this->router->class== 'apps'){ ?>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/download-apps.css" rel="stylesheet" type="text/css"/>
        <?php } ?>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo $this->config->config['base_url'];?>/assets/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login">
        <!----->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="javascript:void(0)">
                        <img src="<?php echo $this->config->config['base_url']; ?>/assets/img/logo.png" alt=""/>
                    </a>
                </div>
            </div>
            <!-- END HEADER INNER -->
        </div>

        <div class="content download-android" style="width:70%">
            <?php echo $content; ?>
        </div>
        <div class="copyright">
            <?php echo date('Y'); ?> &copy; <?php echo "Energy Re-Mastered" ?>
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/plugins/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/plugins/bootstrap.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/scripts/layout.js" type="text/javascript"></script>
        <!--<script src="<?php echo $this->config->config['base_url']; ?>/assets/js/scripts/login.js" type="text/javascript"></script>-->
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
            });
        </script>

        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>

