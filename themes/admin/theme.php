<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo isset($pageTitle) ? $pageTitle : ''; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!--BEGIN GLOBAL SCRIPT-->
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
        <!--END GLOBAL SCRIPT-->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>

        <link href="<?php echo $this->config->config['base_url']; ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/plugins/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/plugins/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/plugins/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/plugins.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/layout.css" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="<?php echo $this->config->config['base_url']; ?>/assets/css/themes/default.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url']; ?>/assets/css/style.css" rel="stylesheet" type="text/css"/>



        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="http://biofeedbackapps.com/activation/assets/admin/layout/img/favicon2.ico"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-quick-sidebar-over-content ">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<?php echo base_url() . 'Admin/admin_user/index'; ?>">
                        <img src="<?php echo $this->config->config['base_url']; ?>/assets/img/logo.png" alt="logo" class="logo-default"/>
                    </a>
                    <div class="menu-toggler sidebar-toggler hide">
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle" src="<?php echo $this->config->config['base_url']; ?>/assets/img/default.png"/>
                                <span class="username username-hide-on-mobile">
                                    <?php echo $this->session->userdata('username'); ?>
                                </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li class="divider">
                                </li>
                                <li>
                                    <a class="ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/admin_user/change_password'; ?>"><i class="icon-lock"></i>Change password</a>
                                </li>
                                <li>
                                    <?php echo anchor('Authentication/logout', '<i class="icon-key"></i> Log Out', array('title' => 'Log Out')); ?>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <!--<div class="page-sidebar-wrapper-div page-sidebar-wrapper" >-->
            <div class="page-sidebar-wrapper" >
                <div class="page-sidebar navbar-collapse collapse">
                    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="sidebar-toggler-wrapper">
                            <div class="sidebar-toggler">
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li class="start <?php if (strtolower($this->router->class) == 'admin_user') echo 'active'; else ''; ?>">
                                <?php echo anchor('Admin/admin_user/index', '<i class="icon-home"></i><span class="title">Admin Users</span><span class="selected"></span>', array('title' => 'Admin User List')); ?>
                        </li>
                        <li class="tooltips <?php if (strtolower($this->router->class) == 'genius_insight') echo 'active'; else ''; ?>" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">
                            <?php echo anchor('Admin/genius_insight/index', '<i class="icon-briefcase"></i><span class="title">Energy Re-Mastered</span><span class="selected"></span>', array('title' => 'Users List')); ?>
                        </li>
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'holistic_prosomatics') echo 'active'; else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                            --><?php //echo anchor('Admin/holistic_prosomatics/index', '<i class="icon-briefcase"></i><span class="title">Holistic Prosomatics</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li>-->
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'insight_water_harmonizer') echo 'active'; else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                            --><?php //echo anchor('Admin/insight_water_harmonizer/index', '<i class="icon-briefcase"></i><span class="title">Insight Water Harmonizer</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li> -->
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'insight_quanta_cap') echo 'active'; else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                            --><?php //echo anchor('Admin/insight_quanta_cap/index', '<i class="icon-briefcase"></i><span class="title">Insight Quanta Cap</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li>-->
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'quantum_ilife') echo 'active'; else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                            --><?php //echo anchor('Admin/quantum_ilife/index', '<i class="icon-briefcase"></i><span class="title">Quantum iLife/iNfinity</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li>-->
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'quanta_capsule') echo 'active';
//                            else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                            --><?php //echo anchor('Admin/quanta_capsule/index', '<i class="icon-briefcase"></i><span class="title">Quanta Capsule</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li>-->
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'harmonizer') echo 'active'; else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                            --><?php //echo anchor('Admin/harmonizer/index', '<i class="icon-briefcase"></i><span class="title">Water harmonizer</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li>-->
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'royal_rife') echo 'active'; else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                            --><?php //echo anchor('Admin/royal_rife/index', '<i class="icon-briefcase"></i><span class="title">Royal Rife Machine</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li>-->
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'relationship_harmonizer') echo 'active';
//                            else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                            --><?php //echo anchor('Admin/relationship_harmonizer/index', '<i class="icon-briefcase"></i><span class="title">Relationship Harmonizer</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li>-->
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'mindNRG') echo 'active'; else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                            --><?php //echo anchor('Admin/mindNRG/index', '<i class="icon-briefcase"></i><span class="title">Mind NRG App</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li>-->
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'emotional_insight') echo 'active'; else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                            --><?php //echo anchor('Admin/emotional_insight/index', '<i class="icon-briefcase"></i><span class="title">Emotional Insight</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li>-->
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'betterguide') echo 'active';
//                            else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                            --><?php //echo anchor('Admin/betterguide/index', '<i class="icon-briefcase"></i><span class="title">Better Guide</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li>-->
                        <li class="tooltips <?php if (strtolower($this->router->class) == 'energy_mastered') echo 'active'; else ''; ?>" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">
                            <?php echo anchor('Admin/energy_mastered/index', '<i class="icon-briefcase"></i><span class="title">Energy Mastered</span><span class="selected"></span>', array('title' => 'Users List')); ?>
                        </li>
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'hoursofrest') echo 'active'; else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                            --><?php //echo anchor('Admin/hoursofrest/index', '<i class="icon-briefcase"></i><span class="title">Hours Of Rest</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li>-->
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'pacific_health_foods') echo 'active'; else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                        --><?php //echo anchor('Admin/pacific_health_foods/index', '<i class="icon-briefcase"></i><span class="title">Pacific Health Foods</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li>-->
<!--                        <li class="tooltips --><?php //if (strtolower($this->router->class) == 'rincon_brewery') echo 'active'; else ''; ?><!--" data-container="body" data-placement="right" data-html="true" data-original-title="Users List">-->
<!--                            --><?php //echo anchor('Admin/rincon_brewery/index', '<i class="icon-briefcase"></i><span class="title">Rincon Brewery</span><span class="selected"></span>', array('title' => 'Users List')); ?>
<!--                        </li>-->
                    </ul>
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- Page title -->
                    <h3 class="page-title">
<?php echo isset($pageTitle) ? $pageTitle : ''; ?> <small><?php echo isset($subPageTitle) ? $subPageTitle : ''; ?></small>
                    </h3>
                    <!-- END PAGE HEADER-->
                    <div class="row">
                <?php echo $content; ?>
                    </div>
                    <div class="load-spinner" style="z-index: 10050;bottom:0;top:0;left:0;right:0;position:fixed;display:none">
                        <div class="loading-spinner in" style="width: 200px; margin-left: -100px; z-index: 10051;"><div class="progress progress-striped active"><div class="progress-bar" style="width: 100%;"></div></div></div>
                    </div>
                    <!-- Page contents section -->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->

        <!--DOC: Apply "modal-cached" class after "modal" class to enable ajax content caching-->
        <div class="modal fade" id="ajax-modal" tab-index="-1" ></div>
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner">
<?php echo date('Y'); ?> &copy; <?php echo "Energy Re-Mastered Apps" ?>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/plugins/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/plugins/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/plugins/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/plugins/bootstrap-modal.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery.validate.min.js'); ?>"></script>
        <!-- END CORE PLUGINS -->
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/scripts/layout.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/scripts/aura_genie.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/scripts/ui-extended-modals.js" type="text/javascript"></script>
        <script  src="<?php echo base_url('assets/js/scripts/form-validation.js'); ?>" type="text/javascript"></script>

        <script type="text/javascript">
            jQuery(document).ready(function() {
                Metronic.init(); // init metronic core componets
                Layout.init(); // init layout
                UIExtendedModals.init();
                FormValidation.init(); // Form validation
                $("#draggable").draggable({
                    handle: ".modal-header"
                });

            });
            var site_url = '<?php echo site_url(); ?>';
            var tokenname = '<?php echo $this->security->get_csrf_token_name(); ?>';
            var hashvalue = '<?php echo $this->security->get_csrf_hash(); ?>';
        </script>
        <script type="text/javascript">
<?php
$url = parse_url($_SERVER['REQUEST_URI']);
if (isset($url['query'])) {
    $query = $url['query'];
    ?>
                console.log($('[data-url="<?php echo $query ?>"]'));
                $('[data-url="<?php echo $query ?>"]').addClass("active_submenu");
                $('[data-url="<?php echo $query ?>"]').parent().addClass("active_submenu");
<?php } else {
    ?>
                $('[data-url="<?php echo strtolower($this->router->class) ?>/index"]').addClass("active_submenu");
                $('[data-url="<?php echo strtolower($this->router->class) ?>/index"]').parent().addClass("active_submenu");
<?php } ?>
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>