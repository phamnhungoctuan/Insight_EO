<?php

/**
 * @Name : formatActivatedDateDisplay()
 * @Purpose : To change the format of date.
 * @Call from : Can be called from any controller file.
 * @Functionality : display "Today", "yesterday" or the actual date 
 * @Receiver params : date string
 * @Return params : Return date or (string today/yesterday ) 
 * @Created : Akshay <akshay.intersoft@gmail.com> on 16 July 2015
 * @Modified :
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once 'application/libraries/MailChimp.php';


if (!function_exists('add_to_mailchimp')) {

    function add_to_mailchimp($data = array(), $payment_type = '', $operation = '') {

        $db = & get_instance()->db;

        $MailChimp = new MailChimp(API_KEY);
        $replace_interest = TRUE;
        $user_id = $data['id'];
        $db_data = $db->get_where('users', array('id' => $user_id))->row_array();
        $email = !empty($data['email']) ? $data['email'] : $db_data['email'];
//        if (!empty($data['email'])) {
//        $email = trim($data['email']);
        if ($payment_type) {

            if ($payment_type == 'monthly') {
                $mergeVars['GROUPINGS'] = array(
                    array('name' => 'Energy Re-Mastered AppsUsers', 'groups' => array('Genius Insight App Paid Users', 'Monthly Subscribers'))
                );
            } elseif ($payment_type == 'annually') {
                $mergeVars['GROUPINGS'] = array(
                    array('name' => 'Energy Re-Mastered AppsUsers', 'groups' => array('Genius Insight App Paid Users', 'Annually Subscribers'))
                );
            } elseif ($payment_type == 'lifetime') {
                $mergeVars['GROUPINGS'] = array(
                    array('name' => 'Energy Re-Mastered AppsUsers', 'groups' => array('Genius Insight App Paid Users', 'Lifetime Subscribers'))
                );
            }
        } else {
            $replace_interest = FALSE;
            if ($operation == 'add_user_app') {
                $mergeVars['GROUPINGS'] = array(
                    array('name' => 'Energy Re-Mastered AppsProspects', 'groups' => array('Genius Leads'))
                );
                $replace_interest = TRUE;
            } elseif ($operation == 'activation_key') {
                $mergeVars['GROUPINGS'] = array(
                    array('name' => 'Energy Re-Mastered AppsUsers', 'groups' => array('Genius Insight App Paid Users'))
                );
                $replace_interest = TRUE;
            }
        }
        $first_name = !empty($data['first_name']) ? $data['first_name'] : $db_data['first_name'];
        $last_name = !empty($data['last_name']) ? $data['last_name'] : $db_data['last_name'];
        $phone_no = !empty($data['phone_no']) ? $data['phone_no'] : $db_data['phone_no'];
        $uuid = !empty($data['uuid']) ? $data['uuid'] : $db_data['uuid'];

        $mergeVars += array(
            'MERGE1' => $first_name,
            'MERGE2' => $last_name,
            'MERGE3' => $phone_no,
            'MERGE20' => !empty($db_data['date_created']) ? date('m/d/Y h:i A', strtotime($db_data['date_created'])) : '',
            'MERGE21' => !empty($db_data['date_updated']) ? date('m/d/Y h:i A', strtotime($db_data['date_updated'])) : '',
            'MERGE27' => $uuid,
        );
        //error_log($mergeVars . "<br><br>", 3, 'help.txt');
        $MCArray = array(
            'id' => LIST_ID,
            'email' => array('email' => $email),
            'merge_vars' => $mergeVars,
            'double_optin' => false,
            'update_existing' => true,
            'replace_interests' => $replace_interest,
//            'replace_interests' => TRUE,
            'send_welcome' => false,
        );
//        return $MCArray;
        $result = $MailChimp->call('lists/subscribe', $MCArray);
        if (!isset($result['status'])) {
            $db->update('users', ['synched_with_mailchimp' => '1'], ['id' => $user_id]);
            return true;
        }
//        } 
        else {
            return FALSE;
        }
    }

}

//function get_db_instance() {
//    $CI = & get_instance();
//    return $CI->db;
//}
?>
