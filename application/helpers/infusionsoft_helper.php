<?php

/**
 * @Name : formatActivatedDateDisplay()
 * @Purpose : To change the format of date.
 * @Call from : Can be called from any controller file.
 * @Functionality : display "Today", "yesterday" or the actual date 
 * @Receiver params : date string
 * @Return params : Return date or (string today/yesterday ) 
 * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 16 July 2015
 * @Modified :
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once("application/libraries/iSDK-master/conn.cfg.php");
require_once("application/libraries/iSDK-master/isdk.php");

if (!function_exists('trigger_emailconfirmation_api')) {

    /* FIRSTLY ADD THE CONTACT IN INFUSIONSOFT DB WITH, IF NOT EXISTS THERE  
     * ASSIGN THE GIVEN TAG 
     * TRIGGER THE EMAILCONFIRMATION API TASK
     */

    function trigger_emailconfirmation_api($post) {
        $infusionsoft = new iSDK;
        if ($infusionsoft->cfgCon("qo332")) {
            /* GET THE CONTACT ID OF THE GIVEN EMAIL */
            $contact_id = NULL;
            $returnFields = array('Id', 'Groups');
            $data = $infusionsoft->findByEmail($post['email'], $returnFields);
            /* IF CONTACT ALREADY EXISTS */
            if (count($data) && !empty($data[0]['Id']) && !empty($data[0]['Groups'])) {
                $contact_id = $data[0]['Id'];
                /* CHECK THE GROUP ID 236 */
                $groups = explode(',', $data[0]['Groups']);
                if (in_array(236, $groups)) { /* USER HAS SUBMITTED THE WEB FORM */
                    $data = $infusionsoft->achieveGoal('qo332', 'userEmailVerificationDone', $contact_id);
                } else {/* USER HAS NOT SUBMITTED THE WEB FORM rather downloaded the APP directly */
                    $infusionsoft->optIn($post['email'], "Application");
                    $data = $infusionsoft->achieveGoal('qo332', 'RegisteredAppDirectly', $contact_id); /* add the contact in campaign */
                }
            } else {
                /* ADD THE USER TO INFUSIONSOFT DATABASE and apply the tag ID 282 to add the contact in campaign */
                $data = array(
                    'FirstName' => isset($post['first_name']) ? trim($post['first_name']) : "",
                    'LastName' => isset($post['last_name']) ? trim($post['last_name']) : "",
                    'Email' => isset($post['email']) ? trim($post['email']) : "",
                );
                $contact_id = $infusionsoft->addWithDupCheck($data, 'Email');
                $infusionsoft->optIn($post['email'], "Application");
                $infusionsoft->achieveGoal('qo332', 'RegisteredAppDirectly', $contact_id);
            }
        }
        return;
    }

}

if (!function_exists('add_to_infusion')) {

    function add_to_infusion($post, $tagId = array()) {

        if (empty($post['email'])) {
            return FALSE;
        }
        $infusionsoft = new iSDK();
        if ($infusionsoft->cfgCon("qo332")) {
            $first_name = isset($post['first_name']) ? trim($post['first_name']) : "";
            $last_name = isset($post['last_name']) ? trim($post['last_name']) : "";
            $email = trim($post['email']);
            $data = array('FirstName' => $first_name,
                'LastName' => $last_name,
                'Email' => $email
            );
            $contactId = $infusionsoft->addWithDupCheck($data, 'Email');
            /* UPDATE THE TAG */
            if (count($tagId)) {
                foreach ($tagId as $tag){
                    $infusionsoft->grpAssign($contactId, $tag);
                }
                
            }
            
            return $contactId;
        }
        return FALSE;
    }

}

function test_infusion($email) {
    $infusionsoft = new iSDK();
    if ($infusionsoft->cfgCon("qo332")) {
        $data = array('Email' => $email);
        $contactId = $infusionsoft->addWithDupCheck($data, 'Email');
        /* UPDATE THE TAG */
        echo "<pre>";
        print_r($contactId);
        echo "</pre>";
//        $returnFields = array('Id', 'FirstName', 'LastName','Groups');
//        $data = $infusionsoft->findByEmail($email, $returnFields);
//        
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        
//        $returnFields = array('Email', 'FirstName', 'LastName');
//        $conDat = $infusionsoft->loadCon($contactId, $returnFields);
//        echo "<pre>";
//        print_r($conDat);
//        echo "</pre>";
    }
}

if (!function_exists('infusion_trigger_campaign')) {

    function infusion_trigger_campaign($post) {
        $infusionsoft = new iSDK;
        if ($infusionsoft->cfgCon("qo332")) {
            /* GET THE CONTACT ID OF THE GIVEN EMAIL */
            /* tag id of tag "imported->iLife/iNfinity Downloads" in which to search the email */
            $tag_id = 104;
            $contact_id = NULL;
            $returnFields = array('Id', 'Groups');
            $data = $infusionsoft->findByEmail($post['email'], $returnFields);
            if (count($data) && isset($data[0]['Id'])) {
                $contact_id = $data[0]['Id'];
            } else {
                /* ADD THE USER TO INFUSIONSOFT DATABASE WITH TAG-ID 104 */
                $data = array('FirstName' => isset($post['first_name']) ? $post['first_name'] : "",
                    'LastName' => isset($post['last_name']) ? $post['last_name'] : "",
                    'Email' => isset($post['email']) ? $post['email'] : "",
                );
                $contact_id = $infusionsoft->addWithDupCheck($data, 'Email');
            }
            /* IF CONTACT ID IS NOT NULL THEN TRIGGER THE EMAIL CAMPAIGN SEQUENCE */
            if (!is_null($contact_id)) {
                /* UPDATE THE TAG */
                $infusionsoft->grpAssign($contact_id, $tag_id);
                $integration = 'qo332';
                $callName = 'userRegistered';
                $data = $infusionsoft->achieveGoal($integration, $callName, $contact_id);
            }
        }
        return;
    }

}
?>