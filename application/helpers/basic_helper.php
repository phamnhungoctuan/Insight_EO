<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('verifyIosReceipt')) {

    function verifyIosReceipt($receipt, $build = 'test') {
        if ($build == 'test') {
            $ch = curl_init(IOS_TEST_URL);
        } else {
            $ch = curl_init(IOS_LIVE_URL);
        }
        $data_string = json_encode(array(
            'receipt-data' => $receipt,
            'password' => IOS_SHARED_KEY,
            'exclude-old-transactions' => 1
        ));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output, TRUE);
    }

}
if (!function_exists('verifyAndroidReceipt')) {

    function verifyAndroidReceipt($receipt, $payment_type = 'monthly') {

        $access_token = '';
        $ch = curl_init('https://accounts.google.com/o/oauth2/token');
        $data_string = array(
            'grant_type' => 'refresh_token',
            'client_id' => CLIENT_ID,
            'client_secret' => CLIENT_SECRET,
            'redirect_uri' => REDIRECT_URI,
            'refresh_token' => ANDROID_REFRESH_TOKEN
        );
//                    return $data_string;
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $output = curl_exec($ch);
        curl_close($ch);
        $decoded = json_decode($output, TRUE);
        $access_token = $decoded['access_token'];

        $get_url = "https://www.googleapis.com/androidpublisher/v2/applications/insighthealthapps.geniusinsight/purchases/subscriptions/insighthealthapps.geniusinsight.subscribemonthly/tokens/" . $receipt;
//        $get_url = "https://www.googleapis.com/androidpublisher/v2/applications/insighthealthapps.geniusinsight/purchases/subscriptions/insighthealthapps.geniusinsight.productmonthly/tokens/" . $receipt;


        $ch1 = curl_init($get_url);
        curl_setopt($ch1, CURLOPT_URL, $get_url);
        curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch1, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $access_token)
        );

        $output1 = curl_exec($ch1);
        curl_close($ch1);

        return json_decode($output1, TRUE);
    }

}