<?php
/**
* @Name : formatActivatedDateDisplay()
* @Purpose : To change the format of date.
* @Call from : Can be called from any controller file.
* @Functionality : display "Today", "yesterday" or the actual date 
* @Receiver params : date string
* @Return params : Return date or (string today/yesterday ) 
* @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 16 July 2015
* @Modified :
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('formatActivatedDateDisplay'))
{
    function formatActivatedDateDisplay($date)
    {
        if(empty($date) || $date=="0000-00-00 00:00:00") return "";
        $Dateobj= date_format(date_create($date),'Y-m-d');
        if($Dateobj==date('Y-m-d')) return "<b>Today<b>";
        if($Dateobj==date('Y-m-d',strtotime("-1 days"))) return "<b>Yesterday</b>";
        return date_format(date_create($date), 'F d, Y');
    }  
}


if(!function_exists('format_date')){
    function format_date($date_string, $format = 'Y-m-d'){
        if(strtotime($date_string) > 0){
            return date_format(date_create($date_string), $format);
        }
        return "";
    }
}

?>