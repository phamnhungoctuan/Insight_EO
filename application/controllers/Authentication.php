<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @description	  Allows you login and logout from the site
 * @author	  Karan <karan.intersoft@gmail.com>
 * @ModifiedBy    Karan <karan.intersoft@gmail.com> on 11 June 2015
 * */
class Authentication extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }

    /**
     * @Name : index()
     * @Purpose : Define why function is created
     * @Call from : Define from where function is called
     * @Functionality : Define how function works. 
       Explain step by step.
     * @Receiver params : Define recevier parameters one by one.
     * @Return params : Return variables.
     * @Created : Developer name <email address> on date
     * @Modified : Developer name <email address> on date
     */
    public function index() {
        $data = array();
        $data['pageTitle'] = 'Login';
        $data['subPageTitle'] = 'Admin login';
        if (!$this->session->has_userdata('UserId')) {
            $this->load->view('login/login',$data);
        } else {
            redirect('Admin/genius_insight/index');
        }
    }

    /**
     * @Name : login()
     * @Purpose : Define why function is created
     * @Call from : Define from where function is called
     * @Functionality : Define how function works. 
     * @Receiver params : Define recevier parameters one by one.
     * @Return params : Return variables.
     * @Created : Developer name <email address> on date
     * @Modified : Developer name <email address> on date
     */
    public function login() {
        if ($this->input->post()) {
            
            $this->load->model('Admin/authentications_model');
            if ($this->authentications_model->validates() == FALSE) {
                redirect('/');
            } else {
                $where = array();
                $where['username'] = $this->input->post('username');
                $where['password'] = $this->input->post('password');
                // check username and password is correct or not
                $data = $this->authentications_model->check_login($where);
                if ($data != false) {
                    $session_array = $data->result_array();
                    // set session of user who has logged in
                    $this->session->set_userdata($session_array[0]);
                    redirect('Admin/genius_insight/index');
                    
                } else {
                    $this->session->set_flashdata('error', 'Username or password is incorrect');
                   
                     $this->load->view('login/login');
                }
            }
        } else {
            redirect('Authentication/index');
        }
    }

    /**
     * @Name : logout()
     * @Purpose : To destroy the current session to logout the user.
     * @Call from : On click of lougout link.
     * @Functionality : Simply destroy the session and redirects to the login page.
     * @Receiver params : Only post data is accepted.
     * @Return params : No returning parameters.
     * @Created : Karan <karan.intersoft@gmail.com> on 11 June 2015
     * @Modified : Karan <karan.intersoft@gmail.com> on 11 June 2015
     */
    public function logout() {
        if($this->session->flashdata("message")){
            $message=$this->session->flashdata("message");
        }
        if($this->session->flashdata("type")){
            $type=$this->session->flashdata("type");
        }
        $this->session->sess_destroy();
//        $this->session->sess_create();
        if(isset($message)){$this->session->set_flashdata('message',$message);}
        if(isset($type)){$this->session->set_flashdata('type',$type);}
        redirect('Authentication/index',"refresh");                   
    }

}
