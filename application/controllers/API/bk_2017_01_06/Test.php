<?php

require(APPPATH . 'libraries/REST_Controller.php');
require(APPPATH . 'libraries/Custom_file.php');

class Test extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->model = 'user_model';
        $this->load->library('api_manager');
        $this->load->library('form_validation');
        $this->load->library('custom_file');
        $this->load->model($this->model);
        $this->load->helper('date');
        $this->data = array();
    }

    public function index_get() {
        $result = $this->db->get('user_records');
        echo "<pre>";
        print_r($result->num_rows());
        echo "</pre>";

        if ($result->num_rows()) {
            $records = $result->result_array();
        } else {
            $records = FALSE;
        }
        $arr = array();
        if ($records) {
            foreach ($records as $key => $record) {
                $unserialized_data = unserialize($record['suffer_from']);
                $json_data = $unserialized_data ? json_encode($unserialized_data) : NULL;
                $this->db->update('user_records', array('json_suffer_from' => $json_data), array('id' => $record['id']));
                $arr[] = unserialize($record['suffer_from']);
            }
        } else {
            echo "<pre>";
            print_r('No records');
            echo "</pre>";
        }
        echo "<pre>";
        print_r(count($arr));
        echo "</pre>";
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
    }

    public function index2_get() {
        $this->db->where("id", '54');
        $result = $this->db->get('user_records');
        echo "<pre>";
        print_r($result->num_rows());
        echo "</pre>";

        $row = $result->row_array();
        echo "<pre>";
        print_r($row);
        echo "</pre>";
        $row['suffer_from'] = $row['suffer_from'] ? $row['suffer_from'] : array();

        echo json_encode($row);
    }

    public function list_library_get() {
        $record_id = $this->input->get('id');
        echo "<pre>";
        print_r($record_id);
        echo "</pre>";
        $this->load->model('user_library_model');
        $library_data = $this->user_library_model->get_all_library_data($record_id);
//        echo "<pre>";
//        print_r($library_data);
//        echo "</pre>";
    }

    public function list_library_1_get() {
        $record_id = $this->input->get('id');
        echo "<pre>";
        print_r($record_id);
        echo "</pre>";
        $sql = $this->db->query('SELECT l.id, l.name as library_name , l.description as library_description , l.image ,li.name as item_name , li.frequency
FROM libraries l LEFT JOIN library_items li ON li.library_id = l.id 
WHERE l.id IN (SELECT library_id FROM record_library rl WHERE rl.record_id = 6197)');

        echo "<pre>";
        print_r($sql->num_rows());
        echo "</pre>";
        echo "<pre>";
        print_r($sql->result_array());
        echo "</pre>";
//        echo "<pre>";
//        print_r($library_data);
//        echo "</pre>";
    }

    public function list_library_2_get() {
        echo "<pre>";
        echo date('Y-m-d H:i:s ') . round(microtime(true) * 1000);
        echo "</pre>";
        $record_id = $this->input->get('id');
        echo "<pre>";
        print_r($record_id);
        echo "</pre>";
        $this->db->select('l.id, l.name as library_name , l.description as library_description , l.image');
        $this->db->from('record_library rl');
        $this->db->join('libraries l', ' l.id = rl.library_id', 'INNER');
        $this->db->where('rl.record_id', $record_id);
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            $library_data = $getdata->result_array();
            if (!empty($library_data)) {
            $response_data = array();
            foreach ($library_data as $key => $libarary) {
                $response_data[$key]['library_id'] = $libarary['id'];
                $response_data[$key]['name'] = $libarary['library_name'];
                $response_data[$key]['description'] = $libarary['library_description'];
                $response_data[$key]['image'] = $libarary['image'];
                
                $this->db->select('name , frequency');
                $query  = $this->db->get_where('library_items', array('library_id'=>$libarary['id']));
                $items_array = array();
                if($query->num_rows()){
                    $items_array = $query->result_array();
                    foreach ($items_array as $item_key => $value){
                        $items_array[$item_key]['frequency'] =  json_decode($value['frequency'], TRUE);
                    }
                }
                $response_data[$key]['items'] = $items_array;
            }

//            echo "<pre>";
//            print_r($response_data);
//            echo "</pre>";
                $this->response(array(
                    'status' => true,
                    'message' => "Success",
                    'data' => $this->api_manager->format_output($response_data),
                    'time' =>date('Y-m-d H:i:s ') . round(microtime(true) * 1000)
//                    'data' => $this->api_manager->format_output($library_data)
                        ), REST_Controller::HTTP_OK);
            }
        } else {
            echo "NO results";
        }
        echo "<pre>";
        echo date('Y-m-d H:i:s ') . round(microtime(true) * 1000);
        echo "</pre>";
    }

}
