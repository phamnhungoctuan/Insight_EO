<?php

require(APPPATH . 'libraries/REST_Controller.php');
require(APPPATH . 'libraries/Custom_file.php');

class User extends REST_Controller {

    public $model = 'user_model';

    public function __construct() {
        parent::__construct();
        $this->model = 'user_model';
        $this->load->library('api_manager');
        $this->load->library('form_validation');
        $this->load->library('custom_file');
        $this->load->model($this->model);
        $this->load->helper('date');
        $this->data = array();
    }

    // Register API
    function register_post() {
        $this->api_manager->handle_request();
        $postdata = file_get_contents("php://input");
        $data = (array) json_decode($postdata, true);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim');
        $this->form_validation->set_rules('phone_no', 'Contact Number', 'trim');
        $this->form_validation->set_rules('uuid', 'UUID', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors'), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $user = $this->user_model->get_user(array("email" => $data['email']));
            if (!empty($user)) {
                $this->response(array('status' => FALSE, 'message' => 'User registered already'), REST_Controller::HTTP_OK);
            } else {
                $data['otp'] = rand('123456', '987654');
                $data['otp'] = $this->user_model->generate_random_string(10);
                $data['user_type'] = 'app';
                $data['status'] = '0';
                $data['dropbox_implemented'] = '0';
                $user_id = $this->user_model->register($data);
                if ($user_id > 0) {
                    /* EMAIL ENCRYPTED OTP and user_id TO THE USER */
                    $data['otp'] = md5($data['otp'] . $user_id . 'Qg');
                    $data['user_id'] = base64_encode($user_id);
                    $mail_params['ContentType'] = "text/html";
                    $mail_params['To'] = $data['email'];
                    $mail_params['Subject'] = 'Energy ReMastered Registration Link';
                    $mail_params['Body'] = $this->load->view('email_format/otp_email', $data, true);

                    if ($this->{$this->model}->sendMail($mail_params)) {
                        $this->response(array(
                            'status' => TRUE, 'message' => "User created successfully", 'data' => array('email' => $data['email'], 'dropbox_implemented' => '0'),), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array(
                            'status' => TRUE, 'message' => "User created but confirmation mail not send",
                            'data' => array('email' => $data['email'], 'dropbox_implemented' => '0'),
                                ), REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response(array(
                        'status' => FALSE, 'message' => 'Error in registration process. Please try again.'), REST_Controller::HTTP_OK);
                }
            }
        }
    }

    function login_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $postdata = file_get_contents("php://input");

        $this->api_manager->handle_request();
        $data = json_decode($postdata, true);

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('uuid', 'UUID', 'trim|required');
        if ($this->form_validation->run() == false) {
            $message = 'Validation errors';
        } else {
            $user = $this->user_model->check_login($data['email'], $data['password']);
            if (!empty($user)) {
                /* CHECK WHETHER THIS USER IS DELETED BY THE ADMIN */
                if ($user['is_deleted'] == '1') {
                    $status = FALSE;
                    $message = 'Account deleted by admin.Please contact support.';
                } else if ($user['status'] == '0') { /* CHECK STATUS OF THE USER */
                    $status = TRUE;
                    $message = 'Account confirmation pending.';
                    $response_data = $user;
                }
                else {
                    if (!strlen($user['uuid'])) {
                        $this->{$this->model}->update_user(array('uuid' => $data['uuid']), $user['id']);
                    }
                    /* if user exist */
                    $device_token = isset($data['uuid']) ? $data['uuid'] : '';
                    $platform = isset($data['platform']) ? $data['platform'] : 'other';
                    $user['token'] = $this->api_manager->createAccessToken($user['id'], $device_token, $platform);
                    $user['trial_days_left'] = $this->calculate_trial_days_left($user['freetrial_start_date']);
                    $status = TRUE;
                    $message = 'success';
                    if ($user['image'] != '') {
                        $user['image'] = base_url('assets/uploads/users/') . '/' . $user['image'];
                    }
                    if (isset($user['3d_activation_status']) && $user['3d_activation_status'] != 'activated') {
                        $user['3d_activation_status'] = 1;
                    } else {
                        $user['3d_activation_status'] = 0;
                    }
                    $response_data = $user;
                }
            } else {
                //if user does not exist
                $message = 'Email or password is not correct.';
            }
        }


        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $this->api_manager->format_output($response_data)
                ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
    }

    /* FACEBOOK LOGIN */
    public function facebook_login_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        //check if form is not validated then return all errors
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim');
        $this->form_validation->set_rules('uuid', 'Device Id', 'trim|required');
        if ($this->form_validation->run() == false) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors'), REST_Controller::HTTP_OK);
            die();
        }
        $status = FALSE;
        $message = '';
        $response_data = array();

        $userDetails = $this->user_model->get_user(array('email' => $data['email']));
        $uuid = isset($data['uuid']) ? $data['uuid'] : "";
        $platform = isset($data['platform']) ? $data['platform'] : 'other';
        if (!empty($userDetails)) {
//            if($userDetails['uuid'] != $uuid){
//                $status = FALSE;
//                $message = 'Please login from your registered device.';
//            }else
//            {
            if (!strlen($userDetails['uuid'])) {
                $this->{$this->model}->update_user(array('uuid' => $data['uuid']), $userDetails['id']);
            }
            $token = $this->api_manager->createAccessToken($userDetails['id'], $uuid, $platform);
            $status = TRUE;
            $message = 'Congratulations! you are successfully logged in.';
            $response_data['user_id'] = $userDetails['id'];
            $response_data['email'] = $userDetails['email'];
            $response_data['first_name'] = $userDetails['first_name'];
            $response_data['last_name'] = $userDetails['last_name'];
            $response_data['status'] = $userDetails['status'];
            $response_data['token'] = $token;
            $response_data['dropbox_implemented'] = $userDetails['dropbox_implemented'];
            $response_data['trial_days_left'] = $this->calculate_trial_days_left($userDetails['freetrial_start_date']);
//            }
        } else {
            $user = array();
            $user['first_name'] = $data['first_name'];
            $user['last_name'] = $data['last_name'];
            $user['email'] = $data['email'];
            $user['user_type'] = 'facebook';
            $user['status'] = '1';
            $user['dropbox_implemented'] = '0';
            $user['freetrial_start_date'] = date('Y-m-d');
            $user_id = $this->user_model->register($user);
            if ($user_id) {
                //if user type exist and it is business or client
                $token = $this->api_manager->createAccessToken($user_id, $uuid, $platform);
                $status = TRUE;
                $message = 'Congratulations! you are successfully logged in.';
                $response_data['user_id'] = $user_id;
                $response_data['email'] = $user['email'];
                $response_data['first_name'] = $user['first_name'];
                $response_data['last_name'] = $user['last_name'];
                $response_data['status'] = $user['status'];
                $response_data['token'] = $token;
                $response_data['dropbox_implemented'] = $user['dropbox_implemented'];
                $response_data['trial_days_left'] = 0;
            } else {
                //if user does not exist
                $message = 'Unable to login.';
            }
        }
        $this->response(array('status' => $status, 'message' => $message, 'data' => $this->api_manager->format_output($response_data)), REST_Controller::HTTP_OK);
    }

    /* NOT USED */

    public function confirm_otp_post() {
        $this->api_manager->handle_request();
        $status = FALSE;
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('otp', 'OTP', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $userDetails = $this->{$this->model}->get_user(array('email' => $data['email']));
            if (!empty($userDetails) && $userDetails['status'] == '0') {
//                if($userDetails['uuid'] != $data['uuid']){
//                    $message = 'Please login from your registered device';
//                }else{
                $diff_time = (strtotime(date('Y-m-d H:m:i')) - strtotime($userDetails['otp_date_created'])) / 60;
                if ($diff_time >= 10) {
                    $message = 'OTP expired.';
                } else if ($data['otp'] == $userDetails['otp']) {
                    $user['otp'] = '';
                    $user['status'] = '1';
                    $user['otp_date_confirmed'] = date('Y-m-d H:m:i');
                    $this->{$this->model}->update_user($user, $userDetails['id']);

                    $device_token = isset($data['uuid']) ? $data['uuid'] : '';
                    $platform = isset($data['platform']) ? $data['platform'] : 'other';
                    $this->data['token'] = $this->api_manager->createAccessToken($userDetails['id'], $device_token, $platform);
                    $this->data['trial_days_left'] = $this->calculate_trial_days_left($user['otp_date_confirmed']);
                    //$device_token = NULL;
                    //$token = $this->api_manager->createAccessToken($userDetails['id'], $device_token);
                    $this->data['user_id'] = $userDetails['id'];
                    $this->data['email'] = $userDetails['email'];
                    $this->data['first_name'] = $userDetails['first_name'];
                    $this->data['last_name'] = $userDetails['last_name'];
                    $this->data['status'] = $user['status'];
                    if ($userDetails['3d_activation_status'] != 'activated') {
                        $this->data['3d_activation_status'] = 0;
                    } else {
                        $this->data['3d_activation_status'] = 1;
                    }


                    $status = TRUE;
                    $message = 'Your account is activated.';
                } else {
                    $message = 'OTP does not match';
                }
//                }
            } else {
                //if user does not exist
                $message = 'Email is not correct.';
            }
            $this->response(array(
                'status' => $status,
                'message' => $message,
                'data' => $this->api_manager->format_output($this->data)
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function forgot_password_post() {
        $this->api_manager->handle_request();
        $status = FALSE;
        $message = '';
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* IF VALID REQUEST */
        /* CHECK EMAIL IS REGISTERED OR NOT WITH STATUS = 1 */
        $userDetails = $this->user_model->get_user(array('email' => $data['email']));
        if ($userDetails) {
            /* CHECK STATUS */
            if ($userDetails['status'] == 1 || $userDetails['status'] == 2) {
                /* GENERATE A RANDOM PASSWORD AND SAVE IT IN USERS RECORD */
                $user['password'] = $this->{$this->model}->generate_random_string(10);
                $this->{$this->model}->update_user($user, $userDetails['id']);
                /* EMAIL PASSWORD TO USER */
                /* EMAIL OTP TO THE USER */
                $mail_params['ContentType'] = "text/html";
                $mail_params['To'] = $userDetails['email'];
                $mail_params['Subject'] = 'Energy ReMastered Password Notification';
                $mail_params['Body'] = $this->load->view('email_format/forgot_password_email', array('user' => $userDetails, 'password' => $user['password']), true);

                if ($this->{$this->model}->sendMail($mail_params)) {
                    $status = TRUE;
                    $message = 'Please check your email inbox';
                } else {
                    $status = TRUE;
                    $message = 'Email sending Failed';
                }
            } else {
                $message = 'Confirmation Pending';
            }
        } else {
            $message = 'Your email is not registered. Please check and try again';
        }
        $this->response(array(
            'status' => $status,
            'message' => $message,
                ), REST_Controller::HTTP_OK);
    }

    public function change_password_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        $data = is_array($data) ? $data : array();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required');
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors',), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        }
        /* PROCESS THE REQUEST */
        if ($this->user_model->matchOldPassword($user_details['user_id'], $data['old_password'])) {
            $this->{$this->model}->update_user(array('password' => $data['new_password']), $user_details['user_id']);
            $this->response(array('status' => TRUE, 'message' => 'Password Reset Successfully'), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $this->response(array('status' => FALSE, 'message' => 'Old password is not correct'), REST_Controller::HTTP_OK); // OK (200) being the HTTP response code 
        }
    }

    public function logout_post() {
        $this->api_manager->handle_request();
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'User logged out already'), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->api_manager->destroyAccessToken($user_details['user_id'])) {
            $this->response(array('status' => TRUE, 'message' => 'Logout successfully'), REST_Controller::HTTP_OK);
        } else {
            $this->response(array('status' => TRUE, 'message' => 'Database error. Please try again'), REST_Controller::HTTP_OK);
        }
    }

    private function calculate_trial_days_left($date_created) {
        /* Calculate trial Days left */
        $current_date = date('Y-m-d H:i:s');
        $days = floor(abs(strtotime($current_date) - strtotime($date_created)) / (60 * 60 * 24));
        return $result = (TRIAL_PERIOD - $days) <= 0 ? 0 : (TRIAL_PERIOD - $days);
    }

    public function trial_days_left_post() {
        $this->api_manager->handle_request();
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
//        $trial_date_check = $user_details['user_type'] == 'app' ? 'otp_date_confirmed' : 'date_created';
        $days_remaining = self::calculate_trial_days_left($user_details['freetrial_start_date']);
        $this->response(array(
            'status' => True,
            'message' => 'You have ' . $days_remaining . ' days left',
            'data' => $this->api_manager->format_output(array('trial_days_left' => $days_remaining, 'dropbox_implemented' => $user_details['dropbox_implemented']))
                ), REST_Controller::HTTP_OK);
        die();
    }

    public function account_activation_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('activation_key', 'Activation Code', 'trim|required');
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        }
        /* PROCESS THE REQUEST */
        if ($user_details['activation_key'] == $data['activation_key']) {
            /* ACTIVATE THE USER */
            $this->{$this->model}->update_user(array('status' => "2", 'date_updated' => date('Y-m-d H:i:s')), $user_details['user_id']);
            $this->response(array(
                'status' => TRUE,
                'message' => 'User Activated Successfully.'
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Please check your activation key again or contact support for assistance'
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function resent_otp_post() {
        $this->api_manager->handle_request();
        $status = FALSE;
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');


        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        } else {
            $userDetails = $this->{$this->model}->get_user(array('email' => $data['email']));
            if (!empty($userDetails)) {
                if ($userDetails['status'] == '0') {
                    $data['otp'] = $this->user_model->generate_random_string(10);

                    $data['otp_date_created'] = date('Y-m-d H:i:s');
                    $result = $this->user_model->update_user($data, $userDetails['id']);
                    if ($result > 0) {
                        /* EMAIL OTP TO THE USER */
                        $data['otp'] = md5($data['otp'] . $userDetails['id'] . 'Qg');
                        $data['user_id'] = base64_encode($userDetails['id']);
                        $mail_params['ContentType'] = "text/html";
                        $mail_params['To'] = $data['email'];
                        $mail_params['Subject'] = 'Energy ReMastered Verification';
                        $mail_params['Body'] = $this->load->view('email_format/otp_email', $data, true);

                        if ($this->{$this->model}->sendMail($mail_params)) {
                            $this->response(array(
                                'status' => TRUE,
                                'message' => "Resent OTP successfully",
                                'data' => array('email' => $data['email']),
                                    ), REST_Controller::HTTP_OK);
                            die();
                        } else {
                            $this->response(array(
                                'status' => FALSE,
                                'message' => "Resending OTP failed",
                                'data' => array('email' => $data['email']),
                                    ), REST_Controller::HTTP_OK);
                            die();
                        }
                    } else {
                        $this->response(array(
                            'status' => FALSE,
                            'message' => 'Error in registration process. Please try again.'
                                ), REST_Controller::HTTP_OK);
                        die();
                    }
                } else {
                    $this->response(array(
                        'status' => FALSE,
                        'message' => 'Account already confirmed'
                            ), REST_Controller::HTTP_OK);
                    die();
                }
            } else {
                //if user does not exist
                $message = 'Email is not correct.';
                $this->response(array(
                    'status' => $status,
                    'message' => $message,
                    'data' => $this->api_manager->format_output($this->data)
                        ), REST_Controller::HTTP_OK);
            }
        }
    }

    public function update_user_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        /* write file parameters */
        $fp = fopen("assets/logfiles/update_profile.txt", "a");
        fwrite($fp, "\n\n*****************************************upload_file*******    " . date("Y-m-d H:i:s") . "\n");
        fwrite($fp, json_encode($_REQUEST));
        fwrite($fp, json_encode($_FILES));
        fclose($fp);
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */

        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* USER AUTHENTICATED SUCCESSFULLY */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('first_name', 'first_name', 'trim');
        $this->form_validation->set_rules('last_name', 'last_name', 'trim');
        $this->form_validation->set_rules('dob', 'DOB', 'trim');
        $this->form_validation->set_rules('image', 'image', 'trim');
        $this->form_validation->set_rules('phone_no', 'Phone Number', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* PROCESS THE REQUEST */
        $user_record['id'] = $user_details['user_id'];
        $requests_keys = array('first_name', 'last_name', 'dob', 'phone_no');
        foreach ($requests_keys as $key) {
            if (isset($data[$key])) {
                if ($key == "dob") {
                    $user_record[$key] = date('Y-m-d', strtotime($user_record[$key]));
                } else {
                    $user_record[$key] = $data[$key];
                }
            }
        }
        $user_record['date_updated'] = date('Y-m-d H:i:s');
        $image_result = isset($data['image']) && strlen($data['image']) ? $this->move_file($data['image'], $user_details['user_id'], 'assets/uploads/users/') : array();
        if (isset($image_result['filename'])) {
            $user_record['image'] = $image_result['filename'];
            $response_data['image'] = $image_result;
        }
        if ($this->{$this->model}->update_user($user_record, $user_details['user_id'])) {
            $message = 'Record updated successfully';
            $status = TRUE;
        } else {
            $message = 'Error updating the record';
            $status = TRUE;
        }
        $response_data = array_merge($response_data, $user_record);
        /* Filter the output */

        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $this->api_manager->format_output($response_data)
                ), REST_Controller::HTTP_OK);
    }

    // user image upload library
    public function update_user_image_post() {
        $this->api_manager->handle_request();
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $max_size = '1000000';
        $allowedExt = array('jpg', 'png', 'jpeg', 'gif');
        $min_size = NULL;
        $fileObj = new Custom_file();
        $validationError = $fileObj->validateFile('file', $max_size, $min_size, $allowedExt);
        if (strlen($validationError)) {
            $this->response(array(
                'status' => FALSE,
                'message' => $validationError
                    ), REST_Controller::HTTP_OK);
            die();
        }
        // process the request
        $uploaddir = FCPATH . 'assets/uploads/users/';
        $url = base_url('assets/uploads/users/');
        $fileObj->mkdir_r($uploaddir);
        if ($user_details['image'] != '') {
            if (file_exists($uploaddir . $user_details['image'])) {
                $fileObj->unlink_files($uploaddir . $user_details['image']);
            }
        }

        $filename = $user_details['user_id'] . '.' . pathinfo(basename($_FILES['file']['name']), PATHINFO_EXTENSION);
        if (move_uploaded_file($_FILES['file']['tmp_name'], $uploaddir . $filename)) {
            $this->{$this->model}->update_user(array('image' => $filename, 'date_updated' => date('Y-m-d H:i:s')), $user_details['user_id']);
            $this->response(array(
                'status' => true,
                'message' => "your image is uploded",
                'data' => $this->api_manager->format_output(array('url' => $url . '/' . $filename))
                    ), REST_Controller::HTTP_OK);
            die();
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'your image can not be uploded'
                    ), REST_Controller::HTTP_OK);
            die();
        }
    }

    /* USER RECORD RELATED API'S */

    public function list_user_record_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* USER AUTHENTICATED SUCCESSFULLY */
        $records = $this->{$this->model}->get_user_records($user_details['user_id']);
        if ($records) {
            foreach ($records as $key => $record) {
                $records[$key]['suffer_from'] = $record['suffer_from'] ? json_decode($record['suffer_from'], TRUE) : array();
                $records[$key]['record_id'] = $record['id'];
                $records[$key]['record_image'] = strlen($record['record_image']) ? base_url('assets/uploads/user_records/profile/' . $record['record_image']) : "";
                $records[$key]['audio'] = strlen($record['audio']) ? base_url('assets/uploads/user_records/audio/' . $record['audio']) : "";
                $records[$key]['dob'] = strtotime($record['dob']) > 0 ? mdate("%j%S %M, %Y", strtotime($record['dob'])) : "";
                $records[$key]['dob_org'] = strtotime($record['dob']) ? date("Y-m-d", strtotime($record['dob'])) : "";
            }
            $this->response(array(
                'status' => TRUE,
                'message' => 'success',
                'data' => $this->api_manager->format_output($records)
//                'data' => $records
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'NO Records Found',
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function delete_user_record_post() {
        $this->api_manager->handle_request();
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        /* USER AUTHENTICATED SUCCESSFULLY */
        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('record_id', 'Record_id', 'required|trim|numeric');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors'), REST_Controller::HTTP_OK);
        }
        /* VALIDATE THE RECORD */
        $this->load->model('user_record_model');
        $result = $this->user_record_model->get_user_record(array('id' => $data['record_id'], 'user_id' => $user_details['user_id']));
        if ($result) {
            if ($this->user_record_model->delete(array('id' => $data['record_id']))) {
                $this->response(array('status' => TRUE, 'message' => 'success'), REST_Controller::HTTP_OK);
            } else {
                $this->response(array('status' => FALSE, 'message' => 'Error while deleting Record'), REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(array('status' => FALSE, 'message' => 'Record does not exist'), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
        }
    }

    public function verify_3d_activation_key_post() {
        $this->api_manager->handle_request();
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);

        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('key', 'Avtivation Key', 'trim|required');


        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }

        if ($user_details['3d_activation_key'] == $data['key']) {
            $this->{$this->model}->update_user(array('3d_activation_status' => 'activated', 'date_updated' => date('Y-m-d H:i:s')), $user_details['user_id']);
            $this->response(array(
                'status' => true,
                'message' => 'Your videos are activated'
                    ), REST_Controller::HTTP_OK);
            die();
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Your key does not match'
                    ), REST_Controller::HTTP_OK);
            die();
        }
    }

    public function add_user_record_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }

        $data = json_decode(file_get_contents("php://input"), true);
        /* USER AUTHENTICATED SUCCESSFULLY */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('first_name', 'first_name', 'required|trim');
        $this->form_validation->set_rules('gender', 'Gender', 'in_list[M,F]');
        $this->form_validation->set_rules('last_name', 'last_name', 'trim');
        $this->form_validation->set_rules('dob', 'DOB', 'trim');
        $this->form_validation->set_rules('symptoms_detail', 'Symptoms Detail', 'trim');
        $this->form_validation->set_rules('suffer_from', 'Suffer From', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }


        /* PROCESS THE REQUEST */
        $user_record['user_id'] = $user_details['user_id'];
        /* PROCESS THE REQUEST */
        $requests_keys = array('first_name', 'last_name', 'dob', 'gender', 'symptoms_detail', 'suffer_from');
        foreach ($requests_keys as $key) {
            if (isset($data[$key])) {
                if ($key == "suffer_from") {
                    $data[$key] = is_array($data[$key]) && count($data[$key]) ? $data[$key] : array();
                    $user_record[$key] = json_encode($data[$key]);
                } else if ($key == "dob") {
                    $user_record[$key] = date('Y-m-d', strtotime($data[$key]));
                } else {
                    $user_record[$key] = $data[$key];
                }
            }
        }
        $user_record['gender'] = isset($data['gender']) ? $data['gender'] : "M";
        $this->load->model('user_record_model');
        $user_record['record_id'] = $this->user_record_model->add_user_record($user_record);
        $message = $user_record['record_id'] > 0 ? 'You have added a new client.' : 'error inserting the record';
        $status = $user_record['record_id'] > 0 ? TRUE : FALSE;
        if (isset($user_record['suffer_from'])) {
            unset($user_record['suffer_from']);
        }
        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $this->api_manager->format_output($user_record)
                ), REST_Controller::HTTP_OK);
    }

    public function update_user_record_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        /* write file parameters */
        $fp = fopen("assets/logfiles/user_record_update.txt", "a");
        fwrite($fp, "\n\n*****************************************upload_file*******    " . date("Y-m-d H:i:s") . "\n");
        fwrite($fp, json_encode($_REQUEST));
        fwrite($fp, json_encode($_FILES));
        fclose($fp);
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */

        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* USER AUTHENTICATED SUCCESSFULLY */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('record_id', 'Record Id', 'required|trim');
        $this->form_validation->set_rules('first_name', 'first_name', 'trim');
        $this->form_validation->set_rules('last_name', 'last_name', 'trim');
        $this->form_validation->set_rules('dob', 'DOB', 'trim');
        $this->form_validation->set_rules('gender', 'Gender', 'in_list[M,F]');
        $this->form_validation->set_rules('symptoms_detail', 'Symptoms Detail', 'trim');
        $this->form_validation->set_rules('suffer_from', 'Suffer From', 'trim');
        $this->form_validation->set_rules('image', 'image', 'trim');
        $this->form_validation->set_rules('audio', 'audio', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }

        /* VALIDATE THE RECORD */
        $this->load->model('user_record_model');
        $result = $this->user_record_model->get_user_record(array('id' => $data['record_id'], 'user_id' => $user_details['user_id']));
        if (!$result) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Record does not exist'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }
        /* PROCESS THE REQUEST */
        $requests_keys = array('first_name', 'last_name', 'dob', 'gender', 'symptoms_detail', 'suffer_from');
        foreach ($requests_keys as $key) {
            if (isset($data[$key])) {
                if ($key == "suffer_from") {
                    $data[$key] = is_array($data[$key]) && count($data[$key]) ? $data[$key] : array();
                    $user_record[$key] = json_encode($data[$key]);
                } else if ($key == "dob") {
                    $user_record[$key] = date('Y-m-d', strtotime($data[$key]));
                } else {
                    $user_record[$key] = $data[$key];
                }
            }
        }
        $user_record['gender'] = isset($data['gender']) ? $data['gender'] : "M";
        $user_record['date_updated'] = date('Y-m-d H:i:s');
//        $fileObj = new Custom_file();
//        $image_result = isset($data['image']) && strlen($data['image']) ? $fileObj->move_file($data['image'], $data['record_id'], 'assets/uploads/user_records/profile/') : array();
//        if (isset($image_result['filename'])) {
//            $user_record['record_image'] = $image_result['filename'];
//            $response_data['image'] = $image_result;
//        }
//        $audio_result = isset($data['audio']) && strlen($data['audio']) ? $fileObj->move_file($data['audio'], $data['record_id'], 'assets/uploads/user_records/audio/') : array();
//        if (isset($audio_result['filename'])) {
//            $user_record['audio'] = $audio_result['filename'];
//            $response_data['audio'] = $audio_result;
//        }
        $this->load->model('user_record_model');
        if ($this->user_record_model->update_user_record($user_record, $data['record_id'])) {
            $message = 'Record updated successfully';
            $status = TRUE;
        } else {
            $message = 'Error updating the record';
            $status = TRUE;
        }
        $response_data = array_merge($response_data, $user_record);
        if (isset($response_data['suffer_from'])) {
            unset($response_data['suffer_from']);
        }
        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $this->api_manager->format_output($response_data)
                ), REST_Controller::HTTP_OK);
    }

    public function upload_record_file_post() {
        ini_set('max_execution_time', 18000);
        $status = FALSE;
        $message = '';
        $response_data = array();

        $this->api_manager->handle_request();
        /* write file parameters */
        $fp = fopen("assets/logfiles/user_record_update.txt", "a");
        fwrite($fp, "\n\n*****************************************upload_file*******    " . date("Y-m-d H:i:s") . "\n");
        fwrite($fp, json_encode($_REQUEST));
        fwrite($fp, "\n" . json_encode($_FILES));
        fclose($fp);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* USER AUTHENTICATED SUCCESSFULLY */
        $data = $this->input->post();
        /* VALIDATE THE PARAMETERS */
        $this->form_validation->set_rules('record_id', 'Record Id', 'trim|required');
        $this->form_validation->set_rules('type', 'Type', 'required|trim|in_list[image,audio]');
        /* VALIDATE THE FILE */
        $max_size = isset($data['type']) && $data['type'] == "image" ? '1000000' : NULL;
        $allowedExt = isset($data['type']) && $data['type'] == "image" ? array('jpg', 'png', 'jpeg', 'gif') : array('mp3', 'wav', 'pcm', 'raw', 'm4a');
        $min_size = NULL;
        $fileObj = new Custom_file();
        $validationError = $fileObj->validateFile('file', $max_size, $min_size, $allowedExt);
        if ($this->form_validation->run() == FALSE || strlen($validationError)) {
            $error_message = 'Validation errors';
            //$error_message['file'] = $validationError;
            $this->response(array(
                'status' => FALSE,
                'message' => $error_message
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* VALIDATE THE RECORD */
        $this->load->model('user_record_model');
        $client_record = $this->user_record_model->get_user_record(array('id' => $data['record_id'], 'user_id' => $user_details['user_id']));
        if (!$client_record) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Record does not exist'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }
        /* PROCESS THE REQUEST */
        switch ($data['type']) {
            case 'image' :
                $uploaddir = FCPATH . 'assets/uploads/user_records/profile/';
                $url = base_url('assets/uploads/user_records/profile/');
                break;
            case 'audio' :
                $uploaddir = FCPATH . 'assets/uploads/user_records/audio/';
                $url = base_url('assets/uploads/user_records/audio/');
                break;
            default:
                $uploaddir = FCPATH . 'assets/uploads/temp_uploads/';
                $url = base_url('assets/uploads/temp_uploads/');
                break;
        }
        $fileObj->mkdir_r($uploaddir);
        $filename = $data['record_id'] . '.' . pathinfo(basename($_FILES['file']['name']), PATHINFO_EXTENSION);
        if (!is_uploaded_file($_FILES['file']['tmp_name'])) {
            $message = 'File not uploaded';
        } else if (move_uploaded_file($_FILES['file']['tmp_name'], $uploaddir . $filename)) {
            $this->load->model('API_V1/user_record_model');
            if ($this->user_record_model->update_user_record(array($data['type'] => $filename), $data['record_id'])) {
                $message = 'Record updated successfully';
                $status = TRUE;
                $response_data['filename'] = $filename;
                $url = substr($url, strlen($url) - 1) == '/' ? $url : $url . '/';
                $response_data['url'] = $url . $filename;
                if ($data['type'] == 'audio') {
                    /* SEND THE EMAIL IN CASE OF AUDIO */
                    $user_details['url'] = $response_data['url'];
                    $user_details['client_record'] = $client_record;
                    $this->notify_audio_upload($user_details, $uploaddir . $filename);
                }
            } else {
                $message = 'Error updating the record';
            }
        } else {
            $message = 'Error in uploading File';
        }


        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $response_data
                ), REST_Controller::HTTP_OK);
    }

    private function notify_audio_upload($user_details, $path) {
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $user_details['email'];
        $mail_params['Subject'] = 'Energy ReMastered Notification';
        $mail_params['Body'] = $this->load->view('email_format/audio_notification', $user_details, true);
        $mail_params['attachment'] = array('path' => $path, 'name' => 'Audio File');
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        }
        return FALSE;
    }

    /* UPLOAD THE FILE IN TEMP_UPLOADS FOLDER AND RETURNS THE FILE NAME */

    public function upload_file_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        /* write file parameters */
        $fp = fopen("assets/logfiles/user_record_update.txt", "a");
        fwrite($fp, "\n\n*****************************************upload_file*******    " . date("Y-m-d H:i:s") . "\n");
        foreach ($_REQUEST as $key => $val) {
            fwrite($fp, $key . '=>' . $val . "\n");
        }
        fwrite($fp, json_encode($_FILES));
        fclose($fp);


        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* USER AUTHENTICATED SUCCESSFULLY */
        $data = $this->input->post();
        /* VALIDATE THE PARAMETERS */
        $this->form_validation->set_rules('record_id', 'Record Id', 'trim');
        $this->form_validation->set_rules('type', 'Type', 'required|trim|in_list[image,audio]');
        /* VALIDATE THE FILE */
        $max_size = isset($data['type']) && $data['type'] == "image" ? '1000000' : NULL;
        $allowedExt = isset($data['type']) && $data['type'] == "image" ? array('jpg', 'png', 'jpeg', 'gif') : array('mp3', 'wav', 'pcm', 'raw', 'm4a');
        $min_size = NULL;
        $fileObj = new Custom_file();
        $validationError = $fileObj->validateFile('file', $max_size, $min_size, $allowedExt);
        if ($this->form_validation->run() == FALSE || strlen($validationError)) {
            $error_message = 'Validation errors';
            $error_message['file'] = $validationError;
            $this->response(array(
                'status' => FALSE,
                'message' => $error_message
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $uploaddir = FCPATH . 'assets/uploads/temp_uploads/';
        $fileObj->mkdir_r($uploaddir);
        // remove special characters
        $file = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', strtolower(basename($_FILES['file']['name'])));
        //create a new name
        $randomNumber = time() - 3600 * 24 * 365 * 44;
        $filename = $randomNumber . rand(100, 1000) . rand(1, 100) . $file;
        $newName = $uploaddir . $filename;
        if (!is_uploaded_file($_FILES['file']['tmp_name'])) {
            $message = 'File not uploaded';
        } else if (move_uploaded_file($_FILES['file']['tmp_name'], $newName)) {
            $status = TRUE;
            $message = 'success';
            $response_data = array('filename' => $filename);
        } else {
            $message = 'Error in uploading File';
        }
        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $response_data
                ), REST_Controller::HTTP_OK);
    }

    /* ANALYSIS RELATED APIS */

    public function add_analysis_record_post() {
        $this->api_manager->handle_request();

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $data = json_decode(file_get_contents("php://input"), true);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('record_id', 'Record ID', 'trim|required');
        $this->form_validation->set_rules('tag', 'Record ID', 'trim|required');
        $this->form_validation->set_rules('result', 'Result', 'trim');
        if ($this->form_validation->run() == false) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        } else {
            /* VALIDATE THE RECORD */
            $this->load->model('user_record_model');
            $result = $this->user_record_model->get_user_record(array('id' => $data['record_id'], 'user_id' => $user_details['user_id']));
            if (!$result) {
                $this->response(array(
                    'status' => FALSE,
                    'message' => 'Record does not exist'
                        ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
                die();
            }
            /* Process the request */
            $this->load->model('analysis_records_model');
            $record_list['tag'] = $data['tag'];
            if (isset($data['analysis_id']) && $data['analysis_id'] != '') {
                $record_list['analysis_id'] = $data['analysis_id'];
                $record_list['parent_id'] = 1;
            } else {
                $record_list['analysis_id'] = uniqid();
                $record_list['parent_id'] = 0;
            }



            $record_list['record_id'] = $data['record_id'];
            $record_list['analysis'] = $data['result'] ? json_encode($data['result']) : "";
            $record_list['date_created'] = date('Y-m-d H:i:s');

            $data['analysis_id'] = $this->analysis_records_model->save($record_list);
            $this->response(array(
                'status' => $data['analysis_id'] > 0 ? TRUE : FALSE,
                'message' => $data['analysis_id'] > 0 ? "success" : "error inserting the record",
//                'data'=> $this->api_manager->format_output($data)
                'data' => $data['analysis_id'] > 0 ? array('analysis_id' => $record_list['analysis_id']) : array(),
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }
    }

    public function list_analysis_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* VALIDATE THE PARAMETERS */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('record_id', 'Record ID', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }

        /* VALIDATE THE RECORD */
        $this->load->model('user_record_model');
        $result = $this->user_record_model->get_user_record(array('id' => $data['record_id'], 'user_id' => $user_details['user_id']));
        if (!$result) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Record does not exist'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }

        /* PROCESS THE REQUEST */
        $this->load->model('analysis_records_model');
        $records = $this->analysis_records_model->get_latest_analysis(array('record_id' => $data['record_id']));

        if (!empty($records)) {
            $result = array();
            foreach ($records as $key => $record) {
                $result[$key]['analysis_id'] = $record['analysis_id'];
                //  $result['analysis'] = $record['analysis'];
                $record_date = date('M d, Y', strtotime($record['date_created']));
                $record_time = date('h:i a ', strtotime($record['date_created']));
                $result[$key]['date'] = $record_date;
                $result[$key]['time'] = $record_time;
            }


            $this->response(array(
                'status' => TRUE,
                'message' => 'success',
                'data' => $this->api_manager->format_output($result)
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'NO Records Found',
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function analysis_detail_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* VALIDATE THE PARAMETERS */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('analysis_id', 'Analysis ID', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* PROCESS THE REQUEST */
        $this->load->model('analysis_records_model');
        $records = $this->analysis_records_model->get_analysis_detail(array('analysis_id' => $data['analysis_id'], 'user_id' => $user_details['user_id']));
        if ($records) {
            $result = array();
            foreach ($records as $key => $record) {
                $result[$key]['date'] = date('M d, Y', strtotime($record['date_created']));
                $result[$key]['analysis'] = json_decode($record['analysis'], TRUE);
                $result[$key]['time'] = date('h:i a', strtotime($record['date_created']));
                $result[$key]['tag'] = $record['tag'];
            }
//              
            $this->response(array(
                'status' => TRUE,
                'message' => 'success',
                'data' => $this->api_manager->format_output($result)
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'NO Records Found',
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function import_records_post() {
        $post_data = (array) json_decode(file_get_contents("php://input"), true);
        $this->api_manager->handle_request();

        $base_path = FCPATH . 'assets/uploads/';
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array('status' => FALSE,'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        /* VALIDATE THE PARAMETERS */
        $this->form_validation->set_data($post_data);
        if (!isset($post_data['data']) || !count($post_data['data'])) {
            $this->response(array('status' => FALSE,'message' => 'Validation errors',), REST_Controller::HTTP_OK);
            die();
        }

        $data = $post_data['data'];
        /* PROCESS THE REQUEST */
        if (count($data)) {
            /* DELETE THE PREVIOUS DROPBOX DATA OF THIS USER */
            if ($user_details['dropbox_implemented'] == "1") {
                $this->{$this->model}->delete_dropbox_data($user_details['user_id']);
            }
            foreach ($data as $records) {
                $record_data['user_id'] = $user_details['user_id'];
                $record_data['inserted_from'] = "dropbox";
                $record_data['first_name'] = isset($records['first_name']) ? $records['first_name'] : '';
                $record_data['last_name'] = isset($records['last_name']) ? $records['last_name'] : "";

                $record_data['dob'] =  strtotime($records['dob']) ? date('Y-m-d', strtotime($records['dob'])) : '';
                $record_data['date_created'] = date('Y-m-d H:i:s');
                /* SAVE THE USER's RECORD IN THR USER RECORD TABLE */
                $this->load->model('user_record_model');
                $record_id = $this->user_record_model->save($record_data);

                if ($record_id) {
                    if (isset($records['USER_IMAGE']) && @getimagesize($records['USER_IMAGE'])) {
                        $content = file_get_contents($records['USER_IMAGE']);
                        $ext = pathinfo($records['USER_IMAGE'], PATHINFO_EXTENSION);
                        $file_name = $record_id . '.' . $ext;
                        file_put_contents($base_path . 'user_records/profile/' . $file_name, $content);
                        $this->user_record_model->update_user_record(array("record_image" => $file_name), $record_id);
                    }
                    $user_record_library_data = array();
                    /* GET THE LIBRARY DATA */
                    if (isset($records['categories']) && count($records['categories'])) {
                        foreach ($records['categories'] as $category) {
                            $library_data['uuid'] = $user_details['uuid'];
                            $library_data['name'] = $category['name'];
                            $library_data['user_id'] = $user_details['user_id'];
                            $library_data['description'] = '';
                            $library_data['record_id'] = $record_id;
                            $library_data['lib_inserted_from'] = "dropbox";
                            $library_data['is_shared'] = isset($category['is_shared'])? $category['is_shared'] : '0';
                            $this->load->model('user_library_model');
                            $library_id = $this->user_library_model->add_library($library_data);
                            if ($library_id) {
                                $user_record_library_data[] = array('record_id' => $record_id, 'library_id' => $library_id);
                                if (isset($category['CATEGORY_IMAGE']) && @getimagesize($category['CATEGORY_IMAGE'])) {
                                    $content = file_get_contents($category['CATEGORY_IMAGE']);
                                    $ext = pathinfo($category['CATEGORY_IMAGE'], PATHINFO_EXTENSION);
                                    $file_name = $library_id . '.' . $ext;
                                    file_put_contents($base_path . 'libraries' . $file_name, $content);
                                    $this->user_library_model->update_library(array("image" => $file_name), $library_id);
                                }
                                if (isset($category['subCategory']) && count($category['subCategory'])) {
                                    foreach ($category['subCategory'] as $items) {
                                        $item_data['library_id'] = $library_id;
                                        $item_data['name'] = $items['name'];
                                        if (count($items['frequencies'])) {
                                            $item_data['frequency'] = json_encode($items['frequencies']);
                                        } else {
                                            $item_data['frequency'] = '';
                                        }
                                        $item_data['date_created'] = date('Y-m-d H:i:s');
                                        $this->load->model('library_items_model');
                                        $this->library_items_model->save_items($item_data);
                                    }
                                }
                            }
                        }
                    }

                    /* ASSOCIATE THE LIBRARY WITH USER */
                    if (isset($user_record_library_data) && count($user_record_library_data)) {
                        while ($rows = array_splice($user_record_library_data, 0, 50)) {
                            $this->db->insert_batch('record_library', $rows);
                        }
                    }
                }
            }
            /* UPDATE THE IMPORT_IMPLEMENTED PARAMETER */
            $this->{$this->model}->update_user(array('dropbox_implemented' => '1'), $user_details['user_id']);
            $this->response(array(
                'status' => TRUE,
                'message' => "Records imported successfully"
                    ), REST_Controller::HTTP_OK);
            die();
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => "No records to import"
                    ), REST_Controller::HTTP_OK);
            die();
        }
    }

    function send_activation_key_post() {
        $id = $this->input->post('id');
        $this->load->model('user_model');
        $user_details = $this->user_model->get_user(array('id' => $id));
        if ($user_details) {
            /* EMAIL OTP TO THE USER */
            $mail_params['ContentType'] = "text/html";
            $mail_params['To'] = $user_details['email'];
            $mail_params['Subject'] = 'Your Energy ReMastered Activation Code';
            $mail_params['Body'] = $this->load->view('email_format/acivation_key_email', $user_details, true);

            if ($this->{$this->model}->sendMail($mail_params)) {
                $response = array('status' => TRUE, 'message' => "Key Sent sucessfully",);
            } else {
                $response = array('status' => false, 'message' => "key can not be sent",);
            }
        } else {
            $response = array('status' => false, 'message' => "user not found",);
        }
        echo json_encode($response);
        exit;
    }

    function send_3d_activation_key_post() {
        $id = $this->input->post('id');
        $this->load->model('user_model');
        $user_details = $this->user_model->get_user(array('id' => $id));
        if ($user_details) {
            /* EMAIL OTP TO THE USER */
            $mail_params['ContentType'] = "text/html";
            $mail_params['To'] = $user_details['email'];
            $mail_params['Subject'] = 'Your Energy ReMastered 3D Graphics Activation Code';
            $mail_params['Body'] = $this->load->view('email_format/3d_acivation_key_email', array('first_name' => $user_details['first_name'], 'video_activation_key' => $user_details['3d_activation_key']), true);

            if ($this->{$this->model}->sendMail($mail_params)) {
                $response = array('status' => TRUE, 'message' => "Key Sent sucessfully",);
            } else {
                $response = array('status' => false, 'message' => "key can not be sent",);
            }
        } else {
            $response = array('status' => false, 'message' => "user not found",);
        }
        echo json_encode($response);
        exit;
    }

    function add_neuro_remedy_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('tag', 'Tag', 'trim|required');
        $this->load->helper('validation_helper');
        if ($this->form_validation->run() == FALSE || !isset($data['frequency']) || !validate_array($data['frequency'])) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* save the item */
        $item_record['date_created'] = date('Y-m-d H:i:s');
        $item_record['tag'] = $data['tag'];
        $item_record['user_id'] = $user_details['user_id'];
        $item_record['title'] = $data['title'];
        $item_record['frequency'] = json_encode($data['frequency']);
        $this->load->model('neuro_remedy_items_model');
        $item_id = $this->neuro_remedy_items_model->save($item_record);
        $message = $item_id > 0 ? 'You have added a new item.' : 'Error inserting the item';
        $status = $item_id > 0 ? TRUE : FALSE;
        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $this->api_manager->format_output(array('item_id' => $item_id))
                ), REST_Controller::HTTP_OK);
    }

    function list_neuro_remedy_post() {
        $this->api_manager->handle_request();
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* list the item */
        $this->load->model('neuro_remedy_items_model');
        $items_array = $this->neuro_remedy_items_model->list_all(array('user_id' => $user_details['user_id']));
        if ($items_array) {
            foreach ($items_array as $key => $item) {
                $items_array[$key]['frequency'] = json_decode($item['frequency']);
                unset($items_array[$key]['date_created']);
                unset($items_array[$key]['user_id']);
            }
            $this->response(array(
                'status' => TRUE,
                'message' => count($items_array) . ' items found',
                'data' => $this->api_manager->format_output($items_array)
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => "No records found",
                    ), REST_Controller::HTTP_OK);
        }
    }

    function delete_neuro_remedy_post() {
        $this->api_manager->handle_request();
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $data = json_decode(file_get_contents("php://input"), true);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('neuro_remedy_id', 'Item Id', 'trim|numeric|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* list the item */
        $this->load->model('neuro_remedy_items_model');
        if ($this->neuro_remedy_items_model->get_item(array('neuro_remedy_id' => $data['neuro_remedy_id']))) {
            $result = $this->neuro_remedy_items_model->delete(array('neuro_remedy_id' => $data['neuro_remedy_id']));
            if ($result) {
                $status = TRUE;
                $message = 'Item deleted successfully';
            } else {
                $status = FALSE;
                $message = 'Error while deleting the record';
            }
        } else {
            $status = FALSE;
            $message = 'No such record exist';
        }

        $this->response(array(
            'status' => $status,
            'message' => $message,
                ), REST_Controller::HTTP_OK);
    }

}
