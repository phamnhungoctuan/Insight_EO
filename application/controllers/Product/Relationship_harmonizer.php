<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH . 'libraries/REST_Controller.php');


class Relationship_Harmonizer extends REST_Controller {

    public $model = "relationship_harmonizer_model";

    public function __construct() {
        parent::__construct();
        $this->model = 'relationship_harmonizer_model';
    }

    /**
    * @Name : index()
    * @Purpose : To show the listing of all Quantum iLife/iNfinity App users to admin.
    * @Call from : When admin click on the Quantum iLife/iNfinity App Tab. 
    * @Functionality : Simply fetch the records from database, and set the data to the view.
    * @Receiver params : No receiver parameters passed.
    * @Return params : No parameters returned
    */
    
    public function purchase_post($where=NULL) {
        $fp=fopen("assets/logfiles/payment/relationship_harmonizer.txt", "a");
        fwrite($fp, "**************************payment/Relationship Harmonizer*****************".date("Y-m-d H:i:s"));
        fwrite($fp,"\n");
        foreach ($_REQUEST as $key=>$val){
            fwrite($fp, "$key => $val \n");
        }
        fwrite($fp,"\n\n");
        if(count($_REQUEST)){
            $validateArray = array(
                array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email',
                'message'=>array(
                    'required'=>'Required',
                    'valid_email'=>'Must Be A Valid Email',
                    ),
                ),
                array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim|required',
                'message'=>array(
                    'required'=>'Required',
                    ),
                ),
                array(
                'field' => 'amount',
                'label' => 'Amount',
                'rules' => 'trim',
                'message'=>array(
                    'required'=>'Required',
                    ),
                ),
            );
            $this->load->library("form_validation");
            $this->form_validation->set_data($_REQUEST);
            $this->load->model($this->model);
            if ($this->{$this->model}->validates($validateArray) == FALSE) {
                fwrite($fp,"\nVALIDATION ERROR : ".  json_encode($this->form_validation->error_array())."\n");
                $responseArray['message']='failed';
                $responseArray['validation_errors'] = $this->form_validation->error_array();
                echo json_encode($responseArray);
                fwrite($fp,"\nResponse : " .  json_encode($responseArray));
                die();
            } 
            $data = $_REQUEST;
            $version = isset($data['version'])?$data['version']:NULL;
            if(!is_null($version)){
                $version = $version=="Android"? "Android":$version;
                $version = $version=="iOS"? "iOS":$version;
            }
            $data['version'] = $version;
            $userIdArray = $this->{$this->model}->doPayment($data);
            /* SEND NOTIFICATION TO USERS */
            if($userIdArray){
                foreach ($userIdArray as $id){
                    $this->send_userEmail($id,$version);
                }
            }
            /* SEND NOTIFICATION TO ADMIN */
            $this->notifyAdmin($data);
            $responseArray['message'] = "success";
            echo json_encode($responseArray);
            fwrite($fp,"\nResponse : " .  json_encode($responseArray));
        }
        else{ 
            $responseArray['message'] = "No Data";
            fwrite($fp,"\nResponse : " .  json_encode($responseArray));
        }
        fclose($fp);
    }
    
    public function generateKey($col){
        $randomString = $this->generate_random_string();
        while(!$this->checkRandomStringUniqueness($randomString,$col)){
            $randomString = $this->generate_random_string();
        }
        return $randomString;
    }
    
    
    function notifyAdmin($data){
        
        $versionText = is_null($data['version'])?"":$data['version']." Version";
        $fp1=  fopen("assets/logfiles/payment/relationship_harmonizer.txt", "a");
        fwrite($fp1,"\n\n**********ADMIN EMAIL********".date('m/d/Y H:i:s'));
        //---Get the details of the product----
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = "ryan@quantum-life.com,admin@quantum-life.com";
//        $mail_params['To'] = "ankit.intersoft@gmail.com";
        $mail_params['From'] = "support@quantum-life.com";
        $mail_params['Subject'] = "Relationship Harmonizer Activity Notification";
        $message= 'Dear Admin, '."<br/>Following are the details of the user that has purchased the 'Relationship Harmonizer App' ".$versionText;
        $message.="<br/><br/>Details :<br/>";
        $message.="<br/>------------------------------------------";
        $message.="<br/>Name : ".$data['first_name'];
        $message.="<br/>Email : ".$data['email'];
        $message.="<br/><br/>".'Thank you, '."<br/>"
                   . 'Quantum Life Support '."<br/>";
        $mail_params['Body'] = $message;
        $mail_params['BCC'] = "lifequantum34@gmail.com";
        $mail_result =  $this->sendMail($mail_params);
        fwrite($fp1,"\nMail Params : ".  json_encode($mail_params)."\nMailResult".$mail_result);
        fclose($fp1);
    }
}/* End of file Relationship_harmonizer.php */
/* Location: ./application/controllers/Relationship_harmonizer.php */