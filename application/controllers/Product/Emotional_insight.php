<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emotional_Insight extends MY_Controller {
    
    public $model="emotional_insight_model";
    
    public function __construct(){
        parent::__construct();
        $this->model='emotional_insight_model';
    }
    
    /**
    * @Name : index()
    * @Purpose : To show the listing of all Emotional Insight App users to admin.
    * @Call from : When admin click on the Emotional Insight App Tab. 
    * @Functionality : Simply fetch the records from database, and set the data to the view.
    * @Receiver params : No receiver parameters passed.
    * @Return params : No parameters returned 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified :
    */
    
    public function index($where=NULL) {
        $fp=fopen("assets/logfiles/payment/emotional_insight.txt", "a");
        fwrite($fp, "**************************payment/index(Emotional_insight)*****************".date("Y-m-d H:i:s"));
        fwrite($fp,"\n");
        foreach ($_REQUEST as $key=>$val){
            fwrite($fp, "$key => $val \n");
        }
        fwrite($fp,"\n\n");
        if(count($_REQUEST)){
            $validateArray = array(
                array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email',
                'message'=>array(
                    'required'=>'Required',
                    'valid_email'=>'Must Be A Valid Email',
                    ),
                ),
                array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim|required',
                'message'=>array(
                    'required'=>'Required',
                    ),
                ),
                array(
                'field' => 'amount',
                'label' => 'Amount',
                'rules' => 'trim',
                'message'=>array(
                    'required'=>'Required',
                    ),
                ),
            );
            $this->load->library("form_validation");
            $this->form_validation->set_data($_REQUEST);
            $this->load->model($this->model);
            if ($this->{$this->model}->validates($validateArray) == FALSE) {
                fwrite($fp,"\nVALIDATION ERROR : ".  json_encode($this->form_validation->error_array())."\n");
                $responseArray['message']='failed';
                $responseArray['validation_errors'] = $this->form_validation->error_array();
                echo json_encode($responseArray);
                fwrite($fp,"\nResponse : " .  json_encode($responseArray));
                die();
            } 
            $data = $_REQUEST;
            $version = isset($data['version'])?$data['version']:NULL;
            if(!is_null($version)){
                $version = $version=="Android"? "Android":$version;
                $version = $version=="iOS"? "iOS":$version;
            }
            $data['version'] = $version;
            $userIdArray = $this->{$this->model}->doPayment($data);
            /* SEND NOTIFICATION TO USERS */
            if($userIdArray){
                foreach ($userIdArray as $id){
                    $this->send_userEmail($id,$version);
                }
            }
            /* SEND NOTIFICATION TO ADMIN */
            $this->notifyAdmin($data);
            $responseArray['message'] = "success";
            echo json_encode($responseArray);
            fwrite($fp,"\nResponse : " .  json_encode($responseArray));
        }
        else{ 
            $responseArray['message'] = "No Data";
            fwrite($fp,"\nResponse : " .  json_encode($responseArray));
        }
        fclose($fp);
    }
    
    /* TO NOTIFY ADMIN ABOUT THE PURCHASE*/
    function notifyAdmin($data){
        $versionText = is_null($data['version'])?"":$data['version']." Version";
        $fp1=  fopen("assets/logfiles/payment/emotional_insight.txt", "a");
        fwrite($fp1,"\n\n**********ADMIN EMAIL********".date('m/d/Y H:i:s'));
        //---Get the details of the product----
        $mail_params['ContentType'] = "text/html";
//        $mail_params['To'] = "rw@possibilitywave.com,admin@quantum-life.com";
        $mail_params['To'] = "ankit.intersoft@gmail.com";
        $mail_params['From'] = "support@quantum-life.com";
        $mail_params['Subject'] = "Emotional Insight Notification";
        $message= 'Dear Admin, '."<br/>Following are the details of the user that has purchased the 'Emotional Insight App' ".$versionText;
        $message.="<br/><br/>Details :<br/>";
        $message.="<br/>------------------------------------------";
        $message.="<br/>Name : ".$data['first_name'];
        $message.="<br/>Email : ".$data['email'];
        $message.="<br/><br/>".'Thank you, '."<br/>"
                   . 'Possibility Wave LLC '."<br/>";
        $mail_params['Body'] = $message;
        $mail_params['BCC'] = "lifequantum34@gmail.com";
        $mail_result =  $this->sendMail($mail_params);
        fwrite($fp1,"\nMail Params : ".  json_encode($mail_params)."\nMailResult".$mail_result);
        fclose($fp1);
    }
    
    /**
     * @Name : send_userEmail()
     * @Purpose : To send email to the user.
     * @Call from : Can be called from any controller file.
     * @Functionality : to send email to user with the download link.
     * @Receiver params : one parameter: user id of the user whose details need to be send to admin
     * @Return params : return 1 on success.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 6 July 2015
     * @Modified :
     */
    public function send_userEmail($userid) {
        // ---Get the details of user----
        $this->load->model($this->model);
        $app_id = $this->{$this->model}->app_id;
        //---get the details of app----
        $app_data = $this->fetch_single_record('apps_model', array('id' => $app_id));
        $data = $this->{$this->model}->get_user_details_toEmail($userid);
        if (count($data) > 0) {
            $mail_params['ContentType'] = "text/html";
//        $mail_params['To'] = "rw@possibilitywave.com,admin@quantum-life.com";
            $mail_params['To'] = $data['email'];
            $mail_params['From'] = $app_data['from'];
            $mail_params['Subject'] = "Your " . $app_data['app_name'] . " Download Link ";
            $message = 'Dear ' . ucfirst($data['name']) . ", <br/><br/>" . 'Congratulations on your purchase of the "' . $app_data['app_name'] . '" App.' . "<br/>"
                        . 'Your activation code is:  ' . $data['Key'] . "<br/><br/>"
                        . 'To download the app please click here :' . "<br/>";
            $message.=!empty($app_data['ios_link']) && !is_null($app_data['ios_link']) ? 'iOS Download Link: ' . $app_data['ios_link'] . "<br/>" : '';
            $message.=!empty($app_data['android_link']) && !is_null($app_data['android_link']) ? 'Android Download Link: ' . $app_data['android_link'] . "<br/>" : '';
            $message.=  "<br/>"."Learn more about Emotional Insight and Possibility Wave here:"."<br/><br/>"."www.emotionalinsightapp.com"."<br/><br/>"."www.PossibilityWave.com"."<br/><br/>";
            $message.= 'Questions? Contact Possibility Wave on +1-805-284-5213'."<br/><br/>"
                        . 'Thank you, ' . "<br/>"
                        . 'Possibility Wave LLC ' . "<br/>";
            $mail_params['Body'] = $message;
            $mail_params['BCC'] = $app_data['admin'] . ",lifequantum34@gmail.com";
            $mail_result =  $this->sendMail($mail_params);
        }
    }
    
}/* End of file emotional_insight.php */
/* Location: ./application/controllers/emotional_insight.php */