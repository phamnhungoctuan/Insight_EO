<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require(APPPATH . 'libraries/REST_Controller.php');

class Royal_rife extends CI_Controller {

    public $model = "rife_model";

    public function __construct() {
        parent::__construct();
        $this->model = 'rife_model';
        $this->load->library('api_manager');
        $this->load->model('Admin/rife_model');
        $this->data = array();
    }

    /**
     * @Name : index()
     * @Purpose : Generate the activation keys and email the user about the keys
     * @Call from : Infusionsoft CRM when user purchases some product
     * @Functionality : Generate the unique activation keys and save them in DB and email it to the user who purchased those keys
     * @Receiver params : first_name, email, quantity, amount paid
     * @Return params : Success or failure
     */
    public function purchase() {
        $this->api_manager->handle_request(TRUE, 'payment');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        if ($this->form_validation->run() == FALSE) {
            $this->data['status'] = FALSE;
            $this->data['message'] = 'Missing parameters';
        } else {
            $quantity = $this->api_manager->parse_request('quantity');
            $quantity = $quantity >= 1 ? $quantity : 1;

            $user_details['email'] = $this->api_manager->parse_request('email');
            $user_details['Name'] = $this->api_manager->parse_request('first_name');
            $user_details['last_name'] = $this->api_manager->parse_request('last_name');
            $user_details['address'] = $this->api_manager->parse_request('address');
            $user_details['city'] = $this->api_manager->parse_request('city');
            $user_details['state'] = $this->api_manager->parse_request('state');
            $user_details['Country'] = $this->api_manager->parse_request('country');
            $user_details['zip_code'] = $this->api_manager->parse_request('zip_code');
            $user_details['has_upgrade_app'] = "1";

            $activation_keys = $this->{$this->model}->do_payment($user_details, $quantity);
            /* SEND NOTIFICATION TO USERS */
            if (count($activation_keys)) {
                if ($this->notify_user($user_details, $activation_keys)) {
                    $this->data['status'] = TRUE;
                    $this->data['message'] = 'Email sent to user';
                } else {
                    $this->data['status'] = FALSE;
                    $this->data['message'] = 'Email not sent to user';
                }
                $this->notify_admin($user_details, $activation_keys);
            } else {
                $this->data['status'] = FALSE;
                $this->data['message'] = 'No Licenses Generated';
            }
        }
        $this->api_manager->set_output($this->data, TRUE, 'payment');
    }
    
    
    
    private function notify_admin($user_details, $activation_keys = array()) {
        if (empty($user_details['email']) || count($activation_keys) < 0) {
            return FALSE;
        }
        $user_details['activation_keys'] = $activation_keys;
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        $user_details['activation_keys'] = $activation_keys;
        $user_details['license_count'] = count($activation_keys);
        /* SEND EMAIL TO ADMIN */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = admin_email;
        $mail_params['Subject'] = 'Energy ReMastered Royal Rife New User Notification';
        $user_details['name'] = trim($user_details['Name']. ' '. $user_details['last_name']);
        $mail_params['Body'] = $this->load->view('royal_rife/admin_email_format/purchase_email', $user_details, true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function notify_user($user_details, $activation_keys = array()) {
        if (empty($user_details['email']) || count($activation_keys) < 0) {
            return FALSE;
        }
        $user_details['activation_keys'] = $activation_keys;
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        /* SEND EMAIL TO User */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $user_details['email'];
        $mail_params['Subject'] = 'Your Energy ReMastered Royal Rife App Download Link';
        $user_details['name'] = trim($user_details['Name']. ' '. $user_details['last_name']);
        foreach ($activation_keys as $key) {
            $user_details['Key'] = $key;
            $mail_params['Body'] = $this->load->view('royal_rife/user_email_format/purchase_email', $user_details, true);
            $this->{$this->model}->sendMail($mail_params);
        }
        return TRUE;
    }

}

/* End of file aura.php */
/* Location: ./application/controllers/aura.php */