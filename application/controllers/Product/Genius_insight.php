<?php

require(APPPATH . 'libraries/REST_Controller.php');

class Genius_insight extends REST_Controller {

    public $model = 'genius_insight_model';

    public function __construct() {
        parent::__construct();
        $this->model = 'genius_insight_model';
        $this->load->library('api_manager');
        $this->load->model('Products/genius_insight_model');
        $this->data = array();
    }

    /**
     * @Name : purchase()
     * @Purpose : Generate the activation keys and save the entries in activation_keys table
     * @Call from : Infusionsoft CRM when user purchases some product
     * @Functionality : Generate the unique activation keys and save them in DB and email it to the user who purchased those keys
     * @Receiver params : Email, quantity, amount paid
     * @Return params : Success or failure
     */
    public function purchase_post() {
        $this->api_manager->handle_request(TRUE, 'payment');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|numeric');
        if ($this->form_validation->run() == FALSE) {
            $this->data['status'] = FALSE;
            $this->data['message'] = 'Missing parameters';
        } else {
            $insert_data = array();
            $quantity_purchased = $this->input->post('quantity');
            $quantity_purchased = !empty($quantity_purchased) ? $quantity_purchased : 1;
            $post_data = $this->input->post();
            for ($i = 1; $i <= $quantity_purchased; $i++) {
                $insert_data[] = array(
                    'activation_key' => $this->{$this->model}->generateKey("activation_key"),
                    'email' => $post_data['email'],
                    'date_created' => date('Y-m-d H:i:s')
                );
            }
            $activation_keys = array_column($insert_data, 'activation_key');
            if (count($insert_data)) {
//                $this->load->model('Products/genius_insight_model');
                while ($rows = array_splice($insert_data, 0, 5)) {
                    $this->{$this->model}->add_activation_keys($rows);
                }
                if ($this->notify_user($post_data, $activation_keys)) {
                    $this->data['status'] = TRUE;
                    $this->data['message'] = 'Email sent to user';
                } else {
                    $this->data['status'] = FALSE;
                    $this->data['message'] = 'Email not sent to user';
                }
                $this->notify_admin($post_data, $activation_keys);
            } else {
                $this->data['status'] = FALSE;
                $this->data['message'] = 'No data to process';
            }
        }
        $this->response($this->data, REST_Controller::HTTP_OK);
    }

    private function notify_admin($user_details, $activation_keys = array()) {
        if (empty($user_details['email']) || count($activation_keys) < 0) {
            return FALSE;
        }
        $user_details['activation_keys'] = $activation_keys;
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        /* SEND EMAIL TO ADMIN */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = admin_email;
        $mail_params['Subject'] = 'Energy ReMastered Purchase Notification';
        $mail_params['Body'] = $this->load->view('email_format/admin/app_purchase_email', $user_details, true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function notify_user($user_details, $activation_keys = array()) {
        if (empty($user_details['email']) || count($activation_keys) < 0) {
            return FALSE;
        }
        $user_details['activation_keys'] = $activation_keys;
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        /* SEND EMAIL TO User */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $user_details['email'];
        $mail_params['Subject'] = 'Your Energy ReMastered Activation Code' . (count($activation_keys) > 1 ? 's' : '');
        $mail_params['Body'] = $this->load->view('email_format/app_purchase_email', $user_details, true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
