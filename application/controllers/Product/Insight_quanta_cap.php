<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Insight_quanta_cap extends MY_Controller {

    public $model = "quanta_capsule_model";

    public function __construct() {
        parent::__construct();
        $this->model = 'insight_quanta_cap_model';
        $this->load->library('api_manager');
        $this->load->model('Admin/insight_quanta_cap_model');
        $this->data = array();
    }

    /**
     * @Name : index()
     * @Purpose : Generate the activation keys and email the user about the keys
     * @Call from : Infusionsoft CRM when user purchases some product
     * @Functionality : Generate the unique activation keys and save them in DB and email it to the user who purchased those keys
     * @Receiver params : first_name, email, quantity, amount paid
     * @Return params : Success or failure
     */
    public function purchase() {
        $this->api_manager->handle_request(TRUE, 'payment');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        if ($this->form_validation->run() == FALSE) {
            $this->data['status'] = FALSE;
            $this->data['message'] = 'Missing parameters';
        } else {
            $quantity = $this->api_manager->parse_request('quantity');
            $quantity = $quantity >= 1 ? $quantity : 1;

            $key_details['email'] = $this->api_manager->parse_request('email');
            $key_details['first_name'] = $this->api_manager->parse_request('first_name');
            $key_details['last_name'] = $this->api_manager->parse_request('last_name');
            $key_details['address'] = $this->api_manager->parse_request('address');
            $key_details['city'] = $this->api_manager->parse_request('city');
            $key_details['state'] = $this->api_manager->parse_request('state');
            $key_details['country'] = $this->api_manager->parse_request('country');
            $key_details['zip_code'] = $this->api_manager->parse_request('zip_code');

            $activation_keys = $this->{$this->model}->do_payment($key_details, $quantity);
            /* SEND NOTIFICATION TO USERS */
            if (count($activation_keys)) {
                if ($this->notify_user($key_details, $activation_keys)) {
                    $this->data['status'] = TRUE;
                    $this->data['message'] = 'Email sent to user';
                } else {
                    $this->data['status'] = FALSE;
                    $this->data['message'] = 'Email not sent to user';
                }
                $this->notify_admin($key_details, $activation_keys);
            } else {
                $this->data['status'] = FALSE;
                $this->data['message'] = 'No Licenses Generated';
            }
        }
        $this->api_manager->set_output($this->data, TRUE, 'payment');
    }

    private function notify_admin($key_details, $activation_keys = array()) {
        if (empty($key_details['email']) || count($activation_keys) < 0) {
            return FALSE;
        }
        $key_details['activation_keys'] = $activation_keys;
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        $key_details['activation_keys'] = $activation_keys;
        $key_details['license_count'] = count($activation_keys);
        /* SEND EMAIL TO ADMIN */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = admin_email;
        $mail_params['Subject'] = 'Insight Quanta Cap New User Notification';
        $key_details['name'] = trim($key_details['first_name'] . ' ' . $key_details['last_name']);
        $mail_params['Body'] = $this->load->view('insight_quanta_cap/admin_email_format/purchase_email', $key_details, true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function notify_user($key_details, $activation_keys = array()) {
        if (empty($key_details['email']) || count($activation_keys) < 0) {
            return FALSE;
        }
        $key_details['activation_keys'] = $activation_keys;
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        /* SEND EMAIL TO User */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $key_details['email'];
        $mail_params['Subject'] = 'Your Insight Quanta Cap App Download Link';
        $key_details['name'] = trim($key_details['first_name'] . ' ' . $key_details['last_name']);
        foreach ($activation_keys as $key) {
            $key_details['activation_key'] = $key;
            $mail_params['Body'] = $this->load->view('insight_quanta_cap/user_email_format/purchase_email', $key_details, true);
            $this->{$this->model}->sendMail($mail_params);
        }
        return TRUE;
    }

}

/* End of file quanta_capsule.php */
/* Location: ./application/controllers/quanta_capsule.php */