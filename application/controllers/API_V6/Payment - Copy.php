<?php

require(APPPATH . 'libraries/REST_Controller.php');
require_once(APPPATH . 'libraries/Custom_file.php');

class Payment extends REST_Controller {

    var $data = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('api_manager_V1');
        $this->load->library('form_validation');
        $this->load->model('API_V6/payment_model', 'payment_model');
    }

    public function updatePaymentStatus_post() {
        $response_data = array();
        $status = FALSE;
        $this->api_manager_v1->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('payment_type', 'Payment Type', 'trim|required|in_list[monthly,annually]');
        $this->form_validation->set_rules('receipt', 'Receipt', 'trim|required');
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager_v1->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        if ($this->form_validation->run() == false) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors',), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        }
        $payment_type = $data['payment_type'];
        $receipt = $data['receipt'];
        $paymentdetail = $this->payment_model->fetch(array('user_id' => $user_details['user_id'])); //FETCH PAYMENT DETAILS REGARDING PARTICULAR USER
        if (count($paymentdetail) > 1) {

            // IF USER IS ALREADY SUBSCRIBED
            if ($paymentdetail['payment_type'] == 'lifetime') {
                $status = FALSE;
                $message = 'You have already subscribed for lifetime';
            } elseif ((($paymentdetail['payment_type'] == 'monthly') || ($paymentdetail['payment_type'] == 'annually')) && $paymentdetail['payment_mode'] == 'app') {
                $expiry_date = date('Y-m-d', strtotime($paymentdetail['expiry_date']));
                if ($expiry_date > date('Y-m-d')) {
                    if ($payment_type == 'monthly') {
                        $update['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 month', strtotime($paymentdetail['expiry_date'])));
                    } elseif ($payment_type == 'annually') {
                        $update['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($paymentdetail['expiry_date'])));
                    }
                } else {
                    if ($payment_type == 'monthly') {
                        $update['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 month'));
                    } elseif ($payment_type == 'annually') {
                        $update['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 year'));
                    }
                }
                $update['payment_type'] = $payment_type;
                $update['payment_date'] = date('Y-m-d H:i:s');
                $update['old_receipt'] = $paymentdetail['new_receipt'];
                $update['new_receipt'] = $receipt;
                $update['platform'] = $user_details['platform'];
                $update['app_mod'] = $user_details['build'];
                $this->payment_model->updateSubscription($update, array('id' => $paymentdetail['id']));
                $status = TRUE;
                $message = 'Subscription updated Successfully';
                //VERIFY RECEIPT FROM ITUNES OR GOOGLE CONSOLE
                $receivedData = verifyReceipt($receipt, $user_details['platform'], $user_details['build']);
                if ($receivedData && !empty($receivedData['latest_receipt_info'])) {
//                    $update['expiry_date'] = date('Y-m-d H:i:s', ($receivedData['latest_receipt_info'][0]['expires_date_ms']) / 1000);
                    $update['payment_date'] = date('Y-m-d H:i:s', ($receivedData['latest_receipt_info'][0]['purchase_date_ms']) / 1000);
                    $update['last_verification_date'] = date('Y-m-d');
                    $update['no_of_attempts'] = 1;
                    $this->payment_model->updateSubscription($update, array('id' => $paymentdetail['id']));
                }
                $response_data['subscription_type'] = $payment_type;
                $response_data['subscription_days_left'] = $this->calculate_subscription_days_left(['payment_type' => $payment_type, 'expiry_date' => $update['expiry_date'] ? $update['expiry_date'] : '']);
            }
            // IF USER IS NOT SUBSCRIBED
        } else {
            if ($payment_type == 'monthly') {
                $insert['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 month'));
            } elseif ($payment_type == 'annually') {
                $insert['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 year'));
            }

            $insert['new_receipt'] = $receipt;
            $insert['payment_type'] = $payment_type;
            $insert['payment_date'] = date('Y-m-d H:i:s');
            $insert['user_id'] = $user_details['user_id'];
            $insert['platform'] = $user_details['platform'];
            $insert['app_mod'] = $user_details['build'];
            $this->payment_model->savePayment($insert);
            $status = TRUE;
            $message = 'Subscribed Successfully';
            //VERIFY RECEIPT FROM ITUNES OR GOOGLE CONSOLE
            $receivedData = verifyReceipt($receipt, $user_details['platform'], $user_details['build']);
            if ($receivedData && !empty($receivedData['latest_receipt_info'])) {
                $update = array();
//                $update['expiry_date'] = date('Y-m-d H:i:s', ($receivedData['latest_receipt_info'][0]['expires_date_ms']) / 1000);
                $update['payment_date'] = date('Y-m-d H:i:s', ($receivedData['latest_receipt_info'][0]['purchase_date_ms']) / 1000);
                $update['last_verification_date'] = date('Y-m-d');
                $update['no_of_attempts'] = 1;
                $paymentdetail = $this->payment_model->fetch(array('user_id' => $user_details['user_id']));
                $this->payment_model->updateSubscription($update, array('id' => $paymentdetail['id']));
            }
            print_r($insert);exit;
            $expiry_date = $update['expiry_date'] ? $update['expiry_date'] : $insert['expiry_date'];
            echo $expiry_date;
            exit;

            $response_data['subscription_type'] = $payment_type;
            $response_data['subscription_days_left'] = $this->calculate_subscription_days_left(['payment_type' => $payment_type, 'expiry_date' => $expiry_date]);
        }
        $this->response(array('status' => $status, 'message' => $message, 'data' => $this->api_manager_v1->format_output($response_data)), REST_Controller::HTTP_OK);
    }

    public function checkSubscriptionStatus_post() {
        $response_data = array();
        $status = FALSE;
        $this->api_manager_v1->handle_request();
        if (!($user_details = $this->api_manager_v1->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        $paymentdetail = $this->payment_model->fetch(array('user_id' => $user_details['user_id']));
        //CHECK WHETHER USER IS SUBSCRIBED OR NOT
        if (empty($paymentdetail)) {
            $status = TRUE;
            $message = 'User is not Subscribed';
            $response_data['is_subscribed'] = FALSE;
        } else {
            if (($paymentdetail['payment_type'] == 'lifetime') || (($paymentdetail['payment_type'] == 'monthly' || $paymentdetail['payment_type'] == 'annually') && (date('Y-m-d', strtotime($paymentdetail['expiry_date'])) > date('Y-m-d')))) {
                $status = TRUE;
                $message = 'Subscription not expired';
                $response_data['is_subscribed'] = TRUE;
            } else {
                $status = TRUE;
                $message = 'Subscription expired';
                $response_data['is_subscribed'] = FALSE;
            }
            $response_data['subscription_type'] = $paymentdetail['payment_type'];
            $response_data['subscription_days_left'] = $this->calculate_subscription_days_left($paymentdetail);
        }
        $this->response(array('status' => $status, 'message' => $message, 'data' => $this->api_manager_v1->format_output($response_data)), REST_Controller::HTTP_OK);
    }

    private function calculate_subscription_days_left($data) {
        if ($data['payment_type'] == 'lifetime') {
            return $result = 0;
        }
        /* Calculate subscription Days left */
        $current_date = date('Y-m-d H:i:s');
        $days = floor(abs(strtotime($data['expiry_date']) - strtotime($current_date)) / (60 * 60 * 24));
        return $result = $days <= 0 ? 0 : $days;
    }

}
