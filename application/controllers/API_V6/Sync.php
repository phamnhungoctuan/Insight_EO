<?php

require(APPPATH . 'libraries/REST_Controller.php');
require_once(APPPATH . 'libraries/Custom_file.php');

class Sync extends REST_Controller {

    public $model = 'user_model';

    public function __construct() {
        parent::__construct();
        $this->load->library('api_manager_V1');
        $this->load->library('form_validation');
        $this->load->library('custom_file');
        $this->load->helper('date');
        $this->data = array();
    }

    /* USER RECORD RELATED API'S */

    public function get_user_records_post() {
        $this->api_manager_v1->handle_request();
        $data = (array) json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager_v1->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK); die();
        }
        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('page', 'Page', 'required|trim|numeric');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors'), REST_Controller::HTTP_OK);
        }
        $data['page'] = $data['page'] > 0 ? $data['page'] : 1;
        $where = array('user_id' => $user_details['user_id']);

        /* GET TOTAL CLIENT RECORDS */
        $this->load->model('API_V6/user_record_model');
        $total_clients = $this->user_record_model->count_user_records($user_details['user_id']);
        if ($total_clients) {
            $client_analysis_records = array();
            $offset = (($data['page'] - 1) * SYNC_RECORDS_PER_PAGE );
            $records = $this->user_record_model->get_user_records($where, SYNC_RECORDS_PER_PAGE, $offset, 'first_name', 'ASC');
            if ($records) {
                /* FETCH THE NEW ANALYSIS DETAILS OF RECORDS */
                $record_id_ary = array_column($records, "id");
                $this->load->model('API_V6/client_analysis_details_model');
                $client_analysis_records = $this->client_analysis_details_model->get_records(' record_id IN (' . implode(',', $record_id_ary) . ')');
            }
            $response_data['total_pages'] = $this->api_manager_v1->get_page_from_count($total_clients, SYNC_RECORDS_PER_PAGE);
            $response_data['records'] = self::process_records_data($records, $client_analysis_records);
            $this->response(array('status' => TRUE, 'message' => 'success', 'data' => $this->api_manager_v1->format_output($response_data)), REST_Controller::HTTP_OK);
        } else {
            $this->response(array('status' => FALSE, 'message' => 'No Records Found'), REST_Controller::HTTP_OK);
        }
    }

    private function process_records_data($records, $client_analysis_records = array()) {
        $return_array = array();
		if($records){
			foreach ($records as $key => $record) {
				$client_analysis_detail = array();
				if(isset($client_analysis_records[$record['id']])){
					$client_analysis_detail = $client_analysis_records[$record['id']];
				}
				$return_array[] = array(
					'user_id' => $record['user_id'],
					'record_id' => $record['id'],
					'first_name' => $record['first_name'],
					'last_name' => $record['last_name'],
					'dob' => strtotime($record['dob']) > 0 ? mdate("%j%S %M, %Y", strtotime($record['dob'])) : "",
					'dob_org' => strtotime($record['dob']) ? date("Y-m-d", strtotime($record['dob'])) : "",
					'gender' => $record['gender'],
					'symptoms_detail' => $record['symptoms_detail'],
					'suffer_from' => $record['suffer_from'] ? json_decode($record['suffer_from'], TRUE) : array(),
					'inserted_from' => $record['inserted_from'],
					'client_analysis_detail' => $client_analysis_detail,
					'date_created' => $record['date_created'],
					'date_updated' => $record['date_updated'],
				);
			}
			
		}
		return $return_array;
    }

    public function sync_user_libraries_post() {
        $this->load->model('API_V6/User_library_model', 'user_library_model');
        $this->load->model('API_V6/Library_items_model', 'library_items_model');
        $this->api_manager_v1->handle_request();
        $data = (array) json_decode(file_get_contents("php://input"), true);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager_v1->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }

        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('page', 'Page', 'required|trim|numeric');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors'), REST_Controller::HTTP_OK);
        }
        $page_no = $data['page'] > 0 ? $data['page'] - 1 : 0;
        $where = '((l.lib_inserted_from="dropbox" AND ul.is_shared = "0") OR '
                . '(l.lib_inserted_from="dropbox" AND ul.is_shared="1" AND ul.is_imported="1" AND ul.is_approved="1" ) OR '
                . 'ul.is_shared="0" OR '
                . '(ul.is_shared="1" AND ul.is_imported="1" AND ul.is_approved="1" AND ul.uuid = "' . $user_details['uuid'] . '"))';
        $records = $this->user_library_model->fetch_library_and_items($user_details['user_id'], $where, $page_no * SYNC_RECORDS_PER_PAGE, SYNC_RECORDS_PER_PAGE);
        $this->response(array('status' => TRUE, 'message' => 'success', 'data' => $this->api_manager_v1->format_output($records)), REST_Controller::HTTP_OK);
    }

    public function user_record_libraries_post() {
        $this->api_manager_v1->handle_request();
        $data = (array) json_decode(file_get_contents("php://input"), true);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager_v1->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }

        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('record_id', 'Record Id', 'required|trim|numeric');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors'), REST_Controller::HTTP_OK);
            die();
        }

        $this->load->model('API_V6/user_record_model', 'user_record_model');
        $response_data = array();
        $record_libraries_id = $this->user_record_model->get_record_libraries_id($data['record_id']);
        $response_data['library'] = count($record_libraries_id) ? array_column($record_libraries_id, 'id') : array();
        $record_mb_ids = $this->user_record_model->get_record_master_branch($data['record_id']);
        $response_data['master_branch'] = count($record_mb_ids) ? array_column($record_mb_ids, 'id') : array();
        $this->response(array('status' => TRUE, 'message' => 'success', 'data' => $this->api_manager_v1->format_output($response_data)), REST_Controller::HTTP_OK);
    }

    public function master_branch_post() {
        $this->api_manager_v1->handle_request();
        $data = (array) json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager_v1->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        /* USER AUTHENTICATED SUCCESSFULLY */
        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('page', 'Page', 'required|trim|numeric');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors'), REST_Controller::HTTP_OK);
        }
        $data['page'] = $data['page'] > 0 ? $data['page'] : 1;
        $where = array('user_id' => $user_details['user_id']);
        /* GET TOTAL CLIENT RECORDS */

        /* initialize response data */
        $response_data['total_pages'] = 0;
        $response_data['records'] = array();
        $this->load->model('API_V6/User_master_branch_model', 'user_master_branch_model');
        $total_master_branches = $this->user_master_branch_model->count_results($where);
        if ($total_master_branches) {
            $offset = (($data['page'] - 1) * SYNC_RECORDS_PER_PAGE );
            $records = $this->user_master_branch_model->get_master_branch_array($where, SYNC_RECORDS_PER_PAGE, $offset, 'name', 'ASC');
            if ($records) {
                foreach ($records as $key => $record) {
                    unset($records[$key]['date_created']);
                    unset($records[$key]['uuid']);
                    $mb_libraries_id = $this->user_master_branch_model->get_master_branch_libraries_id($record['master_branch_id']);
                    $records[$key]['master_branch_lib'] = $mb_libraries_id && count($mb_libraries_id) ? array_column($mb_libraries_id, 'id') : array();
                }
            }
            $response_data['total_pages'] = $this->api_manager_v1->get_page_from_count($total_master_branches, SYNC_RECORDS_PER_PAGE);
            $response_data['records'] = $records ? $records : array();
        }
        $this->response(array('status' => TRUE, 'message' => 'success', 'data' => $this->api_manager_v1->format_output($response_data)), REST_Controller::HTTP_OK);
    }

    public function upload_data_post() {
        $this->api_manager_v1->handle_request();
        $data = (array) json_decode(file_get_contents("php://input"), true);
        $fp = fopen("sync.txt", "a");
        fwrite($fp, "\n\n**********************************************" . date('Y-m-d H:i:s'));
        fwrite($fp, "\n DATA : " . file_get_contents("php://input"));
        fwrite($fp, "\n\n Headers : " . json_encode(getallheaders()));
        $data = $data['data'];
        $response_data = array();
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager_v1->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        $this->load->model('API_V6/User_record_model', 'user_record_model');
        $this->load->model('API_V6/User_library_model', 'user_library_model');
        $this->load->model('API_V6/Library_items_model', 'library_items_model');
        $this->load->model('API_V6/User_master_branch_model', 'user_master_branch_model');

        $response_data['libraries'] = array();
        $processed_library_id = array();

        if (isset($data['libraries']) && !empty($data['libraries'])) {
            $lib_response = self::process_library_and_items($data['libraries'], $user_details['user_id'], $user_details['uuid']);
            $response_data['libraries'] = isset($lib_response['response_data']) && is_array($lib_response['response_data']) && count($lib_response['response_data']) ? $lib_response['response_data'] : array();
            $processed_library_id = isset($lib_response['libraries_id']) && is_array($lib_response['libraries_id']) && count($lib_response['libraries_id']) ? $lib_response['libraries_id'] : array();
        }

        $response_data['user_master_branch'] = array();
        /* Process master branch */
        if (isset($data['user_master_branch']) && count($data['user_master_branch'])) {
            $master_branch_response = $this->process_master_branch($data['user_master_branch'], $user_details['user_id'], $user_details['uuid'], $processed_library_id);
            $response_data['user_master_branch'] = isset($master_branch_response['response_data']) && is_array($master_branch_response['response_data']) && count($master_branch_response['response_data']) ? $master_branch_response['response_data'] : array();
            $processed_master_branch_id = isset($master_branch_response['master_branch_ids']) && is_array($master_branch_response['master_branch_ids']) && count($master_branch_response['master_branch_ids']) ? $master_branch_response['master_branch_ids'] : array();
        }
        if (isset($data['deleted_user_master_branch']) && count($data['deleted_user_master_branch'])) {
            $this->user_master_branch_model->delete_master_branch_array($data['deleted_user_master_branch']);
        }

        /* PROCESS USER RECORDS */
        $response_data['user_records'] = array();
        $client_analysis_records_array = array();
        if (isset($data['user_records']) && !empty($data['user_records'])) {
            foreach ($data['user_records'] as $record_key => $user_record) {
                $user_record_data = array(
                    'user_id' => $user_details['user_id'],
                    'first_name' => $user_record['first_name'],
                    'last_name' => $user_record['last_name'],
                    'inserted_from' => $user_record['inserted_from'],
                    'symptoms_detail' => $user_record['symptoms_detail'],
                    'suffer_from' => count($user_record['suffer_from']) ? json_encode($user_record['suffer_from']) : NULL,
                    'gender' => isset($user_record['gender']) && in_array($user_record['gender'], array('M', 'F')) ? $user_record['gender'] : 'M',
                );
                if (strlen($user_record['dob'])) {
                    $user_record_data['dob'] = strtotime($user_record['dob']) ? $user_record['dob'] : NULL;
                }
                if (isset($user_record['is_from_web']) && $user_record['is_from_web'] == '1') { /* Insert */
                    $record_id = $this->user_record_model->update_user_record($user_record_data, $user_record['record_id']) ? $user_record['record_id'] : 0;
                    /* DELETE CLIENT LIBRARIES  and groups */
                    $this->user_library_model->delete_record_library($record_id);
                    $this->user_master_branch_model->delete_record_master_branch($record_id);
                } else if (isset($user_record['is_from_web']) && $user_record['is_from_web'] == '0') { /* Insert */
                    $record_id = $this->user_record_model->add_user_record($user_record_data);
                }
                if ($record_id) {
                    $response_data['user_records'][$record_key]['old_record_id'] = $user_record['record_id'];
                    $response_data['user_records'][$record_key]['new_record_id'] = $record_id;
                    /* Process Libraries */
                    $response_data['user_records'][$record_key]['client_lib'] = array();
                    if (isset($user_record['client_lib']) && count($user_record['client_lib'])) {
                        foreach ($user_record['client_lib'] as $client_lib) {
                            $client_lib = isset($processed_library_id[$client_lib]) ? (int) $processed_library_id[$client_lib] : (int) $client_lib;
                            if ($client_lib && is_numeric($client_lib))
                                $insert_data[] = array(
                                    'record_id' => $record_id,
                                    'library_id' => $client_lib,
                                    'is_group' => '0',
                                );
                        }
                        while ($rows = array_splice($insert_data, 0, 50)) {
                            $this->db->insert_batch('record_library', $rows);
                        }
                        $this->db->select('library_id');
                        $last_insert_ids = $this->db->get_where('record_library', array('record_id' => $record_id));
                        $last_insert_ids = array_column($last_insert_ids->result_array(), 'library_id');
                        $response_data['user_records'][$record_key]['client_lib'] = is_array($last_insert_ids) && count($last_insert_ids) ? $last_insert_ids : array();
                    }

                    /* SAVE CLIENT MASTER GROUPS */
                    $response_data['user_records'][$record_key]['client_master_branch'] = array();
                    if (isset($user_record['client_master_branch']) && count($user_record['client_master_branch'])) {
                        foreach ($user_record['client_master_branch'] as $client_lib) {
                            $client_lib = isset($processed_master_branch_id[$client_lib]) ? (int) $processed_master_branch_id[$client_lib] : (int) $client_lib;
                            if ($client_lib && is_numeric($client_lib))
                                $insert_data[] = array(
                                    'record_id' => $record_id,
                                    'library_id' => $client_lib,
                                    'is_group' => '1',
                                );
                        }

                        while ($rows = array_splice($insert_data, 0, 50)) {
                            $this->db->insert_batch('record_library', $rows);
                        }
                        $this->db->select('library_id');
                        $last_insert_ids = $this->db->get_where('record_library', array('record_id' => $record_id, 'is_group' => '1'));
                        $last_insert_ids = array_column($last_insert_ids->result_array(), 'library_id');
                        $response_data['user_records'][$record_key]['client_master_branch'] = is_array($last_insert_ids) && count($last_insert_ids) ? $last_insert_ids : array();
                    }
                    
                    /* GET THE CLIENT's Analysis Details and add it to Array $client_analysis_records_array  to be processed later after processing each record*/
                    if(!empty($user_record['client_analysis_records'])){
                        $client_analysis_records_array[] = array(
                            'record_id' => $record_id,
                            'data' => $user_record['client_analysis_records']
                        );
                    }
                }
            }
            $response_data['user_records'] = array_values($response_data['user_records']);
            
            /*SAVE ALL THE CLIENTS ANALYSIS DETAILS */
            if(!empty($client_analysis_records_array)){
                $this->load->model('API_V6/client_analysis_details_model');
                $this->client_analysis_details_model->save($client_analysis_records_array);
            }
        }
        if (isset($data['deleted_user_records']) && count($data['deleted_user_records'])) {
            $this->user_record_model->delete_user_records($data['deleted_user_records']);
        }

        $API_response = array('status' => TRUE, 'message' => 'success', 'data' => $this->api_manager_v1->format_output($response_data));
        fwrite($fp, "\n\n Response : " . json_encode($API_response));
        $this->response($API_response, REST_Controller::HTTP_OK);
    }

    private function match_library_id($client_libs, $libraries) {
        if (count($client_libs)) {
            foreach ($client_libs as $key => $client_lib) {
                foreach ($libraries as $lib_key => $library) {
                    if ($library['old_library_id'] == $client_lib) {
                        $client_libs[$key] = $library['new_library_id'];
                        break;
                    }
                }
            }
            return $client_libs;
        }
        return array();
    }

    /* RETURN ASSOCIATIVE ARRAY CONTAINING TWO ARRAYS (ONE TO BE SEND IN RESPONSE AND ONE LIBRARIES ARRAY TO MATCH THE OFFLINE GENERATED LIBRARYIDS WITH THE ONE SAVED IN DATABASE ) */

    private function process_library_and_items($libraries, $user_id, $uuid) {
        $this->load->model('API_V6/user_library_model');
        $this->load->model('API_V6/library_items_model');
        $deleted_lib_array = array();
        $return_array = array();
        if (count($libraries)) {
            foreach ($libraries as $lib_key => $library) {
                if (isset($library['is_deleted']) && $library['is_deleted'] == 1) {
                    $deleted_lib_array[] = $library['library_id'];
                    continue;
                }
                $library_id = 0;
                if (isset($library['is_from_web']) && $library['is_from_web'] == '0') { /* Insert */
                    $lib_data = array(
                        'name' => $library['name'],
                        'description' => $library['description'],
                        'user_id' => $user_id,
                        'uuid' => $uuid,
                    );
                    $library_id = $this->user_library_model->add_library($lib_data);
                } else if (isset($library['library_id']) && isset($library['is_from_web']) && $library['is_from_web'] == '1') {
                    /* UPDATE LIBRARY */
                    $lib_data = array(
                        'name' => $library['name'],
                        'description' => $library['description']
                    );
                    if ($this->user_library_model->update_library($lib_data, $library['library_id'])) {
                        $library_id = $library['library_id'];
                        /* DELETE THE ITEMS OF THIS LIBRARY */
                        $this->library_items_model->delete_items($library_id);
                    }
                }
                if ($library_id) {
                    $return_array['response_data'][$lib_key]['old_library_id'] = $library['library_id'];
                    $return_array['response_data'][$lib_key]['new_library_id'] = $library_id;

                    $return_array['libraries_id'][$library['library_id']] = $library_id;

                    $return_array['response_data'][$lib_key]['library_items'] = array();
                    /* Insert Items */
                    if (isset($library['library_items']) && count($library['library_items'])) {
                        $offline_items_id = array();
                        foreach ($library['library_items'] as $item_key => $items) {
                            $item_data['library_id'] = $library_id;
                            $item_data['name'] = $items['name'];
                            if (isset($items['frequency']) && count($items['frequency'])) {
                                $item_data['frequency'] = json_encode($items['frequency']);
                            } else {
                                $item_data['frequency'] = '';
                            }
                            $item_data['date_created'] = date('Y-m-d H:i:s');
                            $insert_data[] = $item_data;
                            $offline_items_id[] = $items['item_id'];
                        }
                        while ($rows = array_splice($insert_data, 0, 50)) {
                            $this->db->insert_batch('library_items', $rows);
                        }
                        $this->db->select('id');
                        $last_insert_ids = $this->db->get_where('library_items', array('library_id' => $library_id));
                        $last_insert_ids = array_column($last_insert_ids->result_array(), 'id');
                        $last_insert_ids = array_combine($offline_items_id, $last_insert_ids);
                        if ($last_insert_ids && count($last_insert_ids)) {
                            foreach ($last_insert_ids as $offline_id => $new_id) {
                                $return_array['response_data'][$lib_key]['library_items'][] = array('old_item_id' => $offline_id, 'new_item_id' => $new_id);
                            }
                        }
                    }
                }
            }/* END OF LIBRARIES FOREACH */

            $return_array['response_data'] = array_values($return_array['response_data']);
            if (count($deleted_lib_array)) {
                $this->user_library_model->delete_library($deleted_lib_array, $user_id);
            }
        } /* END OF LIBRARIES COUNT IF */
        return $return_array;
    }

    private function process_master_branch($master_branch_data, $user_id, $uuid, $processed_library_id) {
        $response_data = array();
        if (isset($master_branch_data) && !empty($master_branch_data)) {
            foreach ($master_branch_data as $mb_key => $mb_record) {
                $mb_record_data = array(
                    'user_id' => $user_id,
                    'uuid' => $uuid,
                    'name' => $mb_record['name'],
                );
                if (isset($mb_record['is_from_web']) && $mb_record['is_from_web'] == '1') { /* Update */
                    $master_branch_id = $this->user_master_branch_model->update_master_branch($mb_record_data, $mb_record['master_branch_id']) ? $mb_record['master_branch_id'] : 0;
                    if ($master_branch_id) {
                        /* DELETE Master branch LIBRARIES AND INSERT NEW ONE , SET THE PARAMETER TRUE TO DELETE LIBRARIES ONLY AND NOT THE MASTER BRANCH COMPLETELY */
                        $this->user_master_branch_model->delete_master_branch($master_branch_id, TRUE);
                    }
                } else if (isset($mb_record['is_from_web']) && $mb_record['is_from_web'] == '0') { /* Insert */
                    $master_branch_id = $this->user_master_branch_model->add_master_branch($mb_record_data);
                }
                if ($master_branch_id) {
                    $response_data['response_data'][$mb_key]['old_master_branch_id'] = $mb_record['master_branch_id'];
                    $response_data['response_data'][$mb_key]['new_master_branch_id'] = $master_branch_id;

                    $response_data['master_branch_ids'][$mb_record['master_branch_id']] = $master_branch_id;
                    /* Process Libraries */
                    $response_data['response_data'][$mb_key]['master_branch_lib'] = array();
                    if (isset($mb_record['master_branch_lib']) && count($mb_record['master_branch_lib'])) {
                        foreach ($mb_record['master_branch_lib'] as $mb_lib) {
                            $mb_lib = isset($processed_library_id[$mb_lib]) ? (int) $processed_library_id[$mb_lib] : (int) $mb_lib;
                            if ($mb_lib && is_numeric($mb_lib)) {
                                $insert_data[] = array(
                                    'master_branch_id' => $master_branch_id,
                                    'library_id' => $mb_lib
                                );
                            }
                        }
                        while ($rows = array_splice($insert_data, 0, 50)) {
                            $this->db->insert_batch('master_branch_libraries', $rows);
                        }
                        $this->db->select('library_id');
                        $last_insert_ids = $this->db->get_where('master_branch_libraries', array('master_branch_id' => $master_branch_id));
                        $last_insert_ids = array_column($last_insert_ids->result_array(), 'library_id');
                        $response_data['response_data'][$mb_key]['master_branch_lib'] = count($last_insert_ids) ? $last_insert_ids : array();
                    }
                }
            }
            $response_data['response_data'] = array_values($response_data['response_data']);
        }
        return $response_data;
    }

    /*     * *************************************NOT USED****************************** */

    public function master_branch_libraries_post() {
        $this->api_manager_v1->handle_request();
        $data = (array) json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager_v1->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('master_branch_id', 'Master Branch Id', 'required|trim|numeric');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors'), REST_Controller::HTTP_OK);
            die();
        }
        $this->load->model('API_V6/User_master_branch_model', 'user_master_branch_model');
        /* CHECK WHETHER BRANCH ID BELONGS TO THIS USER OR NOT */
        $check_mb = $this->user_master_branch_model->count_results(array('user_id' => $user_details['user_id'], 'master_branch_id' => $data['master_branch_id']));
        if ($check_mb) {
            $mb_libraries_id = $this->user_master_branch_model->get_master_branch_libraries_id($data['master_branch_id']);
            $mb_libraries_id = array_column($mb_libraries_id, 'id');
            $this->response(array('status' => TRUE, 'message' => 'success', 'data' => $this->api_manager_v1->format_output($mb_libraries_id)), REST_Controller::HTTP_OK);
        } else {
            $this->response(array('status' => FALSE, 'message' => 'You don\'t have access to this record'), REST_Controller::HTTP_OK);
        }
    }

}
