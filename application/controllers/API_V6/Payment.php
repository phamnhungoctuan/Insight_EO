<?php

require(APPPATH . 'libraries/REST_Controller.php');
require_once(APPPATH . 'libraries/Custom_file.php');

class Payment extends REST_Controller {

    var $data = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('api_manager_V1');
        $this->load->library('form_validation');
        $this->load->model('API_V6/payment_model', 'payment_model');
        $this->load->model('API_V6/user_model', 'user_model');
        $stripe = array(
            "secret_key" => config_item('secret_key_stripe'),
            "public_key" => config_item('public_key_stripe')
        );
        \Stripe\Stripe::setApiKey($stripe["secret_key"]);

    }

    public function create_charge_post() {
        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
        $this->form_validation->set_rules('source', 'Source', 'trim|required');
        $this->form_validation->set_rules('uuid', 'uuid', 'trim|required');
        if ($this->form_validation->run() == false) {
            $response = array(
                'status' => FALSE,
                'message' => 'Validation errors');
            $this->response($response);
        } else {
            $charge = \Stripe\Charge::create(array(
                "amount" => $data['amount'],
                "currency" => "usd",
                "source" => $data['source'],
                "description" => "Charge for ".$data['uuid'],
            ));
            if ($charge->paid == true) {
                // charge success

                $userDetails = $this->user_model->get_user(array("uuid" => $data['uuid']));
                $tempdetails['old_receipt'] = NULL;
                $tempdetails['new_receipt'] = NULL;
                $tempdetails['last_verification_date'] = date('Y-m-d');
                $tempdetails['no_of_attempts'] = 1;
                $tempdetails['payment_mode'] = 'app';
                $tempdetails['payment_type'] = 'lifetime';
                $tempdetails['user_id'] = $userDetails['id'];
                $tempdetails['payment_date'] = date('Y-m-d H:i:s');
                $this->payment_model->savePayment($tempdetails);
                $this->user_model->update_user(['status' => '2'], $userDetails['id']);
                $this->load->helper('mailchimp');
                add_to_mailchimp(['id'=> $userDetails['id']], 'annually'); //SYNC WITH MAILCHIMP

                $status = TRUE;
                $message = 'Subscribed Successfully';
                $response_data['subscription_type'] = 'lifetime';
                $response_data['subscription_days_left'] = $this->calculate_subscription_days_left(['payment_type' => 'lifetime', 'expiry_date' => NULL]);
                $this->response(array('status' => $status, 'message' => $message, 'data' => $this->api_manager_v1->format_output($response_data)), REST_Controller::HTTP_OK);


            } else { // Charge was not paid!
                $status = false;
                $message = 'Your payment could NOT be processed because the payment system rejected the transaction. You can try again or use another card.';
                $this->response(array('status' => $status, 'message' => $message, REST_Controller::HTTP_OK));
            }
        }
    }



    public function updatePaymentStatus_post() {
        $tempid = 0;
        $transaction_id = NULL;
        $expiry_date = NULL;
        $receivedData = NULL;
        $oldReceipt = NULL;
        $response_data = array();
        $status = FALSE;
        $message = '';
        $this->api_manager_v1->handle_request();
        $data = (array)json_decode(file_get_contents("php://input"), true);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('payment_type', 'Payment Type', 'trim|required|in_list[monthly,annually]');
        $this->form_validation->set_rules('receipt', 'Receipt', 'trim|required');
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager_v1->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        if ($user_details['platform'] == 'android') {
            $this->form_validation->set_rules('play_store_email', 'Play Store Email', 'trim|required');
        }
        if ($this->form_validation->run() == false) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors'), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            die();
        }
        $payment_type = $data['payment_type'];
        $receipt = $data['receipt'];
        $email = !empty($data['play_store_email']) ? $data['play_store_email'] : NULL;


        $paymentdetail = $this->payment_model->fetch(array('user_id' => $user_details['user_id'])); //FETCH PAYMENT DETAILS REGARDING PARTICULAR USER
        if (!empty($paymentdetail)) {
            // IF USER IS ALREADY SUBSCRIBED
            if ($paymentdetail['payment_type'] == 'lifetime') {
                $this->response(array('status' => FALSE, 'message' => 'You have already subscribed for lifetime'), REST_Controller::HTTP_OK);
                die();
            }
            $expiry_date = $paymentdetail['expiry_date'];
            $oldReceipt = $paymentdetail['new_receipt'];
            $transaction_id = $paymentdetail['transaction_id'];
        }
        /* CHECK SUBSCRIPTION HAS EXPIRED OR NOT */
        if ($expiry_date >= date('Y-m-d')) {
            if ($payment_type == 'monthly') {
                $expiry_date = date('Y-m-d H:i:s', strtotime('+1 month', strtotime($expiry_date)));
            } elseif ($payment_type == 'annually') {
                $expiry_date = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($expiry_date)));
            }
        } else {
            if ($payment_type == 'monthly') {
                $expiry_date = date('Y-m-d H:i:s', strtotime('+1 month'));
            } elseif ($payment_type == 'annually') {
                $expiry_date = date('Y-m-d H:i:s', strtotime('+1 year'));
            }
        }
        /* SAVE DETAILS TO TEMPORARY TABLE */
        $insert = array();
        $insert['user_id'] = $user_details['user_id'];
        $insert['payment_type'] = $payment_type;
        $insert['payment_date'] = date('Y-m-d H:i:s');
        $insert['expiry_date'] = $expiry_date;
        $insert['receipt'] = $receipt;
        $insert['platform'] = $user_details['platform'];
        $insert['app_mod'] = $user_details['build'];
        $tempid = $this->payment_model->saveTempDetails($insert); //GET INSERT ID 
        /* VERIFY RECEIPT */
        if ($user_details['platform'] == 'ios') {
            $receivedData = verifyIosReceipt($receipt, $user_details['build']);
        } elseif ($user_details['platform'] == 'android') {
            $receivedData = verifyAndroidReceipt($receipt, $payment_type);
        }
        if ($tempid) {
            $tempdetails = $this->payment_model->getTempDetails(array('id' => $tempid));
            if (isset($receivedData['status']) && $receivedData['status'] == '0') {
                $transaction_id_matched = $this->payment_model->fetch(array('transaction_id' => $receivedData['latest_receipt_info'][0]['transaction_id']));
                $tempdetails['expiry_date'] = date('Y-m-d H:i:s', ($receivedData['latest_receipt_info'][0]['expires_date_ms']) / 1000);
                $tempdetails['transaction_id'] = $receivedData['latest_receipt_info'][0]['transaction_id'];
            } else if (isset($receivedData['paymentState']) && $receivedData['paymentState'] == '1') {
                $transaction_id_matched = $this->payment_model->fetch(array('transaction_id' => $receipt));
                $tempdetails['expiry_date'] = date('Y-m-d H:i:s', ($receivedData['expiryTimeMillis']) / 1000);
                $tempdetails['transaction_id'] = $receipt;
                $tempdetails['play_store_email'] = $email;
            } else {
                $this->response(array('status' => FALSE, 'message' => "This transaction has been completed but not verified successfully."), REST_Controller::HTTP_OK);
                die();
            }
            if (empty($transaction_id_matched)) {
                $tempdetails['old_receipt'] = !empty($oldReceipt) ? $oldReceipt : NULL;
                $tempdetails['new_receipt'] = $tempdetails['receipt'];
                $tempdetails['last_verification_date'] = date('Y-m-d');
                $tempdetails['no_of_attempts'] = 1;
                $tempdetails['payment_mode'] = 'app';
                unset($tempdetails['id']);
                unset($tempdetails['receipt']);
                $this->payment_model->savePayment($tempdetails);
                $this->user_model->update_user(['status' => '2'], $user_details['user_id']);
                $this->load->helper('mailchimp');                
                add_to_mailchimp(['id'=>$user_details['user_id']],$payment_type); //SYNC WITH MAILCHIMP
                $status = TRUE;
                $message = 'Subscribed Successfully';
                $response_data['subscription_type'] = $payment_type;
                $response_data['subscription_days_left'] = $this->calculate_subscription_days_left(['payment_type' => $payment_type, 'expiry_date' => $expiry_date]);
            } else {
                $status = FALSE;
                $message = "This in-app purchase has already made.";
            }
        } else {
            $status = FALSE;
            $message = " This transaction can't be completed ";
        }

        $this->response(array('status' => $status, 'message' => $message, 'data' => $this->api_manager_v1->format_output($response_data)), REST_Controller::HTTP_OK);
    }

    /* CHECK WHETHER PAYMENT ALREADY MADE BY SAME GOOGLE PLAY USER */

    public function checkPayment_post() {
//        $response_data = array();
        $status = FALSE;
        $message = '';
        $this->api_manager_v1->handle_request();
        $data = (array) json_decode(file_get_contents("php://input"), true);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('play_store_email', 'Play Store Email', 'trim|required');
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager_v1->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        $email = $data['play_store_email'];
        $payment_detail = $this->payment_model->fetch(['play_store_email' => $email, 'platform' => 'android']);
        if ($payment_detail) {
            if ($payment_detail['user_id'] != $user_details['user_id']) {
                $status = FALSE;
                $message = 'Payment has already been made by "' . $email . '" Google Play Account. Please use other Google Play Account to make payment';
                $this->response(array('status' => $status, 'message' => $message), REST_Controller::HTTP_OK);
                die();
            } else {
                $status = TRUE;
                $message = 'You can proceed';
            }
        } else {
            $status = TRUE;
            $message = 'You can proceed';
        }
        $this->response(array('status' => $status, 'message' => $message), REST_Controller::HTTP_OK);
    }

    public function checkSubscriptionStatus_post() {
        $response_data = array();
        $status = FALSE;
        $this->api_manager_v1->handle_request();
        if (!($user_details = $this->api_manager_v1->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        $paymentdetail = $this->payment_model->fetch(array('user_id' => $user_details['user_id']));
        //CHECK WHETHER USER IS SUBSCRIBED OR NOT
        if (empty($paymentdetail)) {
            $status = TRUE;
            $message = 'User is not Subscribed';
            $response_data['is_subscribed'] = FALSE;
        } else {
            if (($paymentdetail['payment_type'] == 'lifetime') || (($paymentdetail['payment_type'] == 'monthly' || $paymentdetail['payment_type'] == 'annually') && (date('Y-m-d', strtotime($paymentdetail['expiry_date'])) >= date('Y-m-d')))) {
                $status = TRUE;
                $message = 'Subscription not expired';
                $response_data['is_subscribed'] = TRUE;
            } else {
                $status = TRUE;
                $message = 'Subscription expired';
                $response_data['is_subscribed'] = FALSE;
            }
            $response_data['subscription_type'] = $paymentdetail['payment_type'];
            $response_data['subscription_days_left'] = $this->calculate_subscription_days_left($paymentdetail);
        }
        $this->response(array('status' => $status, 'message' => $message, 'data' => $this->api_manager_v1->format_output($response_data)), REST_Controller::HTTP_OK);
    }

    private function calculate_subscription_days_left($data) {
        if (!empty($data)) {
            if ($data['payment_type'] == 'lifetime') {
                return $result = 0;
            }
            /* Calculate subscription Days left */
            $current_date = date('Y-m-d H:i:s');
            $days = floor((strtotime($data['expiry_date']) - strtotime($current_date)) / (60 * 60 * 24));
             if ($days == 0) {
                $days = 1;
            } elseif ($days < 0) {
                $days = 0;
            }
            return $days;
        } else {
            return FALSE;
        }
    }

}
