<?php

require(APPPATH . 'libraries/REST_Controller.php');
require(APPPATH . 'libraries/Custom_file.php');

/*
 * Master Branch refers to a group of library.
 * User can create a master nbranch and add libraries to the group
 */

class User_master_branch extends REST_Controller {

    public $model = 'user_master_branch_model';

    public function __construct() {
        parent::__construct();
        $this->model = 'user_master_branch_model';
        $this->load->library('api_manager');
        $this->load->model('API_V2/User_master_branch_model', $this->model);
        $this->data = array();
    }

    /*

     * Request parameters:
     * i) 'email'
     * ii)'master_branch_data' having following format 
     * [{"master_branch_id":1,"master_branch_name":"b1","libraries":[{"library_id":1,"library_name":"l1"},{"library_id":2,"library_name":"l2"},{"library_id":3,"library_name":"l3"},{"library_id":4,"library_name":"l4"},{"library_id":5,"library_name":"l5"}]},{"master_branch_id":2,"master_branch_name":"b2","libraries":[{"library_id":1,"library_name":"l1"},{"library_id":6,"library_name":"l6"},{"library_id":7,"library_name":"l7"},{"library_id":8,"library_name":"l8"},{"library_id":9,"library_name":"l9"}]}
     * 
     * process the array to obtain the following two arrays : 
     * 
     * 1) Process the libraries array in the request parameter as key having the library_id and value having library_name
     * [{"master_branch_id":1,"master_branch_name":"b1","libraries":{"1":"l1","2":"l2","3":"l3","4":"l4","5":"l5"}},{"master_branch_id":2,"master_branch_name":"b2","libraries":{"1":"l1","6":"l6","7":"l7","8":"l8","9":"l9"}},{"master_branch_id":3,"master_branch_name":"b3","libraries":{"1":"l1","10":"l10","11":"l11","12":"l12","13":"l13"}}]
     * 
     * 2) libraries array containing unique library ids :
     * {"0":1,"1":2,"2":3,"3":4,"4":5,"6":6,"7":7,"8":8,"9":9,"11":10,"12":11,"13":12,"14":13}
     * 
     */

    
    public function share_post() {
        $this->api_manager->handle_request();
        $data = (array) json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        if ($this->form_validation->run() == FALSE || !(isset($data['master_branch_data']) && is_array($data['master_branch_data']) && count($data['master_branch_data']) > 0)) {
            $error = isset($data['master_branch_data']) && is_array($data['master_branch_data']) && count($data['master_branch_data']) > 0 ? validation_errors() : validation_errors() . 'Master branch data required ';
            $this->response(array('status' => FALSE, 'message' => 'Validation errors', 'error' => explode("\n", $error)), REST_Controller::HTTP_OK);
            die();
        }

        /* Process the request */
        $this->load->model('user_model');
        $share_with = $this->user_model->get_user(array("email" => $data['email'], "email !=" => $user_details['email']));
        if (!$share_with) {
            $this->response(array('status' => FALSE, 'message' => 'User not found'), REST_Controller::HTTP_OK);
            die();
        }

        /* Process the Request parameters and fetch the array of unique library ids */
        $libraries_array = array();
        foreach ($data['master_branch_data'] as $key => $master_branch) {
//            $data['master_branch_data'][$key]['master_branch_share_code'] = $this->{$this->model}->generate_random_string();
            if (isset($master_branch['libraries']) && count($master_branch['libraries'])) {
                $libraries = $master_branch['libraries'];
                $data['master_branch_data'][$key]['libraries'] = array();
                foreach ($libraries as $library) {
                    $data['master_branch_data'][$key]['libraries'][$library['library_id']] = $library['library_name'];
                    $libraries_array[] = $library['library_id'];
                }
            }
        }
        /* get the unique library_ids */
        $libraries_array = array_unique($libraries_array);
        if ($libraries_array) {
            $this->load->model('API_V2/user_library_model', 'user_library_model');
            $shareable_libraries = $this->user_library_model->check_sharaeble_libraries($user_details['user_id'], $libraries_array);
            if ($shareable_libraries) {
                $already_shared_library_count = 0; 
                /* Check Already Shared Libraries */
                $already_shared_libraries = $this->user_library_model->get_already_shared_libraries($share_with['id'], $shareable_libraries);
                if ($already_shared_libraries) {
                    $already_shared_library_count = count($already_shared_libraries);
                    $already_shared_lib_ids = array_column($already_shared_libraries, 'library_id');
                    /* get the data for previously shared libararies for which user has not responded i.e. is_imported = 0 and uuid is blank or null */
                    foreach ($already_shared_libraries as $key => $shared_lib) {
                        foreach ($data['master_branch_data'] as $key => $master_branch) {
                            if (isset($master_branch['libraries'][$shared_lib['library_id']])) {
                                if (!is_null($shared_lib['bulk_share_code'])) {
                                    $email_data['already_shared']['bulk_share_libraries'][$shared_lib['bulk_share_code']][] = array('library_name' => $master_branch['libraries'][$shared_lib['library_id']], 'single_share_code' => $shared_lib['share_library_code']);
                                } else {
                                    $email_data['already_shared']['single_share_libraries'][] = array('library_name' => $master_branch['libraries'][$shared_lib['library_id']], 'single_share_code' => $shared_lib['share_library_code']);
                                }
                                break;
                            }
                        }
                    }
                }
                /* get the rest of the libraries that can be shared */
                $shareable_libraries = isset($already_shared_lib_ids) ? array_diff($shareable_libraries, $already_shared_lib_ids) : $shareable_libraries;
                if (count($shareable_libraries)) {
                    $master_branch_bulk_share_code = count($shareable_libraries) > 1 ? $this->{$this->model}->generate_random_string() : NULL;
                    foreach ($shareable_libraries as $shareable_lib_id) {
                        /* Process the master_branch_data arary to get the insert_data and email data */
                        foreach ($data['master_branch_data'] as $key => $master_branch) {
                            if (isset($master_branch['libraries']) && count($master_branch['libraries'])) {

                                if (isset($master_branch['libraries'][$shareable_lib_id])) {
                                    $single_share_code = $this->{$this->model}->generate_random_string();
                                    $email_data['new'][$master_branch['master_branch_id']]['master_branch_name'] = $master_branch['master_branch_name'];
//                                    $email_data['new'][$master_branch['master_branch_id']]['master_branch_share_code'] = $master_branch['master_branch_share_code'];
                                    $email_data['new'][$master_branch['master_branch_id']]['libraries'][] = array('library_name' => $master_branch['libraries'][$shareable_lib_id], 'single_share_code' => $single_share_code);

                                    $insert_data[] = array(
                                        'user_id' => $share_with['id'],
                                        'library_id' => $shareable_lib_id,
                                        'is_shared' => '1',
                                        'is_approved' => '1',
                                        'is_imported' => '0',
                                        'share_library_code' => $single_share_code,
                                        'bulk_share_code' => $master_branch_bulk_share_code,
//                                        'bulk_share_code' => $master_branch['master_branch_share_code'],
                                        'master_branch_bulk_share_code' => $master_branch_bulk_share_code,
                                        'date_created' => date('Y-m-d H:i:s')
                                    );
                                    break;
                                }
                            }
                        }
                    }
                }
                
                /* SEND THE EMAIL FIRST AND THEN SAVE THE ENTRIES IN DATABASE, IF NEW LIBRARIES WERE SHARED */
                if ($email_data) {
                    /* SEND EMAIL FOR SHARED AND ALREADY SHARED lIBRARIES */
                    $master_branch_bulk_share_code = !empty($master_branch_bulk_share_code) ? $master_branch_bulk_share_code : NULL;
                    if ($this->master_branch_share_email($user_details, $share_with, $email_data, $master_branch_bulk_share_code, $already_shared_library_count)) {
                        /* SAVE THE DATA IF ANY */
                        if (isset($insert_data)) {
                            $this->user_library_model->save_share_library_data($insert_data);
                            $message = isset($email_data['new']) && count($email_data['new']) > 1 ? 'Master Branches Shared' : 'Master branch Shared';
                            $this->response(array('status' => TRUE, 'message' => $message), REST_Controller::HTTP_OK);die();
                        }
                        if ($already_shared_libraries) {
                            /* ALREADY SHARED CASE : NO LIBRARIES TO SHARE */
                            $this->response(array('status' => TRUE, 'message' => 'Already Shared'), REST_Controller::HTTP_OK);die();
                        }
                    } else {
                        $this->response(array('status' => FALSE, 'message' => 'Email sending Failed. Please try again'), REST_Controller::HTTP_OK);die();
                    }
                }
            } else {
                $this->response(array('status' => FALSE, 'message' => 'You don\'t have permission to share these libraries'), REST_Controller::HTTP_OK);die();
            }
        }
        $this->response(array('status' => FALSE, 'message' => 'No libraries to share'), REST_Controller::HTTP_OK);
        die();
    }
    
    
    
    private function master_branch_share_email($user_details, $share_with, $email_data, $master_branch_bulk_share_code = NULL,$already_shared_library_count = 0) {
        $flag = TRUE;
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $share_with['email'];
        $mail_params['Subject'] = 'Energy ReMastered Shared Library Code';

        /* FIRST SEND ALREADY SHARED LIBRARIES MAIL */
        if(isset($email_data['already_shared'])){
            $mail_params['Body'] = $this->load->view('email_format/API_V2/already_shared_libraries_email', array('shared_with' => $share_with, 'shared_by' => $user_details, 'email_data' => $email_data['already_shared'], 'already_shared_library_count'=>$already_shared_library_count), true);
            $flag = $this->{$this->model}->sendMail($mail_params);
        }
        /* SEND RECENTLY SHARED LIBRARIES MAIL */
        if($flag && isset($email_data['new'])){
        $mail_params['Body'] = $this->load->view('email_format/API_V2/shared_master_branch_code_email', array('shared_with' => $share_with, 'shared_by' => $user_details, 'email_data' => $email_data, 'master_branch_bulk_share_code' => $master_branch_bulk_share_code), true);
            $flag = $this->{$this->model}->sendMail($mail_params);
        }
        return $flag;
    }

    
    
    /******************************NOT USED***************************************************/
    public function share_NOT_USED_post() {
        $this->api_manager->handle_request();
        $data = (array) json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        if ($this->form_validation->run() == FALSE || !(isset($data['master_branch_data']) && is_array($data['master_branch_data']) && count($data['master_branch_data']) > 0)) {
            $error = isset($data['master_branch_data']) && is_array($data['master_branch_data']) && count($data['master_branch_data']) > 0 ? validation_errors() : validation_errors() . 'Master branch data required ';
            $this->response(array('status' => FALSE, 'message' => 'Validation errors', 'error' => explode("\n", $error)), REST_Controller::HTTP_OK);
            die();
        }

        /* Process the request */
        $this->load->model('user_model');
        $share_with = $this->user_model->get_user(array("email" => $data['email'], "email !=" => $user_details['email']));
        if (!$share_with) {
            $this->response(array('status' => FALSE, 'message' => 'User not found'), REST_Controller::HTTP_OK);
            die();
        }

        /* Process the Request parameters and fetch the array of unique library ids */
        $libraries_array = array();
        foreach ($data['master_branch_data'] as $key => $master_branch) {
            $data['master_branch_data'][$key]['master_branch_share_code'] = $this->{$this->model}->generate_random_string();
            if (isset($master_branch['libraries']) && count($master_branch['libraries'])) {
                $libraries = $master_branch['libraries'];
                $data['master_branch_data'][$key]['libraries'] = array();
                foreach ($libraries as $library) {
                    $data['master_branch_data'][$key]['libraries'][$library['library_id']] = $library['library_name'];
                    $libraries_array[] = $library['library_id'];
                }
            }
        }
        /* get the unique library_ids */
        $libraries_array = array_unique($libraries_array);
        if ($libraries_array) {
            $this->load->model('API_V2/user_library_model', 'user_library_model');
            $shareable_libraries = $this->user_library_model->check_sharaeble_libraries($user_details['user_id'], $libraries_array);
            if ($shareable_libraries) {
                /* Check Already Shared Libraries */
                $already_shared_libraries = $this->user_library_model->get_already_shared_libraries($share_with['id'], $shareable_libraries);
                if ($already_shared_libraries) {
                    $already_shared_lib_ids = array_column($already_shared_libraries, 'library_id');
                    /* get the data for previously shared libararies for which user has not responded i.e. is_imported = 0 and uuid is blank or null */
                    foreach ($already_shared_libraries as $key => $shared_lib) {
                        foreach ($data['master_branch_data'] as $key => $master_branch) {
                            if (isset($master_branch['libraries'][$shared_lib['library_id']])) {
                                if (!is_null($shared_lib['bulk_share_code'])) {
                                    $email_data['already_shared']['bulk_share_libraries'][$shared_lib['bulk_share_code']][] = array('library_name' => $master_branch['libraries'][$shared_lib['library_id']], 'single_share_code' => $shared_lib['share_library_code']);
                                } else {
                                    $email_data['already_shared']['single_share_libraries'][] = array('library_name' => $master_branch['libraries'][$shared_lib['library_id']], 'single_share_code' => $shared_lib['share_library_code']);
                                }
                                break;
                            }
                        }
                    }
                }
                /* get the rest of the libraries that can be shared */
                $shareable_libraries = isset($already_shared_lib_ids) ? array_diff($shareable_libraries, $already_shared_lib_ids) : $shareable_libraries;
                if (count($shareable_libraries)) {
                    $master_branch_bulk_share_code = $this->{$this->model}->generate_random_string();
                    foreach ($shareable_libraries as $shareable_lib_id) {
                        /* Process the master_branch_data arary to get the insert_data and email data */
                        foreach ($data['master_branch_data'] as $key => $master_branch) {
                            if (isset($master_branch['libraries']) && count($master_branch['libraries'])) {

                                if (isset($master_branch['libraries'][$shareable_lib_id])) {
                                    $single_share_code = $this->{$this->model}->generate_random_string();
                                    $email_data['new'][$master_branch['master_branch_id']]['master_branch_name'] = $master_branch['master_branch_name'];
                                    $email_data['new'][$master_branch['master_branch_id']]['master_branch_share_code'] = $master_branch['master_branch_share_code'];
                                    $email_data['new'][$master_branch['master_branch_id']]['libraries'][] = array('library_name' => $master_branch['libraries'][$shareable_lib_id], 'single_share_code' => $single_share_code);

                                    $insert_data[] = array(
                                        'user_id' => $share_with['id'],
                                        'library_id' => $shareable_lib_id,
                                        'is_shared' => '1',
                                        'is_approved' => '1',
                                        'is_imported' => '0',
                                        'share_library_code' => $single_share_code,
                                        'bulk_share_code' => $master_branch['master_branch_share_code'],
                                        'master_branch_bulk_share_code' => $master_branch_bulk_share_code,
                                        'date_created' => date('Y-m-d H:i:s')
                                    );

                                    break;
                                }
                            }
                        }
                    }
                }
                
                /* SEND THE EMAIL FIRST AND THEN SAVE THE ENTRIES IN DATABASE, IF NEW LIBRARIES WERE SHARED */
                if ($email_data) {
                    /* SEND EMAIL FOR SHARED AND ALREADY SHARED lIBRARIES */
                    if ($this->master_branch_share_email($user_details, $share_with, $email_data)) {
                        /* SAVE THE DATA IF ANY */
                        if (isset($insert_data)) {
                            $this->user_library_model->save_share_library_data($insert_data);
                            $message = isset($email_data['new']) && count($email_data['new']) > 1 ? 'Libraries Shared' : 'Library Shared';
                            $this->response(array('status' => TRUE, 'message' => $message), REST_Controller::HTTP_OK);die();
                        }
                        if ($already_shared_libraries) {
                            /* ALREADY SHARED CASE : NO LIBRARIES TO SHARE */
                            $this->response(array('status' => TRUE, 'message' => 'Already Shared'), REST_Controller::HTTP_OK);die();
                        }
                    } else {
                        $this->response(array('status' => FALSE, 'message' => 'Email sending Failed. Please try again'), REST_Controller::HTTP_OK);die();
                    }
                }
            } else {
                $this->response(array('status' => FALSE, 'message' => 'You don\'t have permission to share these libraries'), REST_Controller::HTTP_OK);die();
            }
        }
        $this->response(array('status' => FALSE, 'message' => 'No libraries to share'), REST_Controller::HTTP_OK);
        die();
    }

    /************************************************ NOT USED *************************************************************** */

    /**
     * @Name : add()
     * @Purpose : add a Master Branch
     * @Call from : when user click on the add Master branch button from the App
     * @Functionality : Create a master branch 
     * @Receiver params : name only
     * @Return params : on successful insertion of master branch return True, False otherwise
     */
    function add() {
        $this->api_manager->handle_request();
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->data['status'] = FALSE;
            $this->data['message'] = 'Authorization Failed';
            $this->api_manager->set_output($this->data);
            die();
        }
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
//        if ($this->form_validation->run() == FALSE) {
//            $this->data['status'] = FALSE;
//            $this->data['message'] = 'Validation errors';
//            $this->api_manager->set_output($this->data);
//            die();
//        }
        $data['name'] = $this->api_manager->parse_request('name');
        /* Check if the master group with same name is already created by this user */
        $result = $this->{$this->model}->count_results(array('user_id' => $user_details['user_id'], 'name' => $data['name']));
        if ($result) {
            $this->data['status'] = FALSE;
            $this->data['message'] = 'Master Branch name already exist';
            $this->api_manager->set_output($this->data);
            die();
        }
        $data['user_id'] = $user_details['user_id'];
        $data['uuid'] = $user_details['uuid'];
        /* PROCESS THE REQUEST */
        $master_branch_id = $this->{$this->model}->add_master_branch($data);
        if ($master_branch_id) {
            $this->data['status'] = TRUE;
            $this->data['message'] = 'Master Branch added successfully';
            $this->data['data']['library_id'] = $master_branch_id;
        } else {
            $this->data['status'] = FALSE;
            $this->data['message'] = 'Error inserting the record';
        }
        $this->api_manager->set_output($this->data);
    }

    /**
     * @Name : update()
     * @Purpose : add a library 
     * @Call from : when user click on the add library button from the App
     * @Functionality : updates a library in the  user_record_library with given library_id
     * @Receiver params : array of columns and values, library_id
     * @Return params : on successful updation of library return True, False otherwise
     */
    function update() {
        $this->api_manager->handle_request();
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->data['status'] = FALSE;
            $this->data['message'] = 'Authorization Failed';
            $this->api_manager->set_output($this->data);
            die();
        }

        $this->form_validation->set_rules('name', 'Name', 'trim');
        $this->form_validation->set_rules('master_branch_id', 'Master Branch ID', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->data['status'] = FALSE;
            $this->data['message'] = 'Validation errors';
            $this->api_manager->set_output($this->data);
            die();
        }

        $data['name'] = $this->api_manager->parse_request('name');
        $data['image'] = $this->api_manager->parse_request('image');
        $data['record_id'] = $this->api_manager->parse_request('record_id');
        $data['description'] = $this->api_manager->parse_request('description');
        $data['library_id'] = $this->api_manager->parse_request('library_id');
//        $data['uuid'] = $this->api_manager->parse_request('uuid');
        // Check if library is shared or custom
        $check = $this->{$this->model}->get_library_by_id(array('library_id' => $data['library_id'], 'user_id' => $user_details['user_id']));
        if (!$check) {
            $this->data['status'] = FALSE;
            $this->data['message'] = 'You can not update this library';
            $this->api_manager->set_output($this->data);
            die();
        }
        /* VALIDATE THE RECORD */
        $result = $this->{$this->model}->get_library(array('user_id' => $user_details['user_id'], 'library_name' => $data['name'], 'ul.library_id !=' => $data['library_id']));

        if ($result) {

            $this->data['status'] = FALSE;
            $this->data['message'] = 'You can not update this library';
            $this->api_manager->set_output($this->data);
            die();
//            $this->response(array(
//                'status' => FALSE,
//                'message' => 'Library name already exist'
//                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
//            die();
        }

        $update_data['description'] = $data['description'] ? $data['description'] : "";
        $update_data['name'] = $data['name'] ? $data['name'] : "";
        //  $update_data['user_id'] = $user_details['user_id'];
        $update_data['date_updated'] = date('Y-m-d H: :s');

        /* SAVE THE IMAGE */
        if ($data['library_id'] && strlen($data['image'])) {
            $fileObj = new Custom_file();
            $image_result = isset($data['image']) && strlen($data['image']) ? $fileObj->move_file($data['image'], $data['library_id'], 'assets/uploads/libraries/') : array();
            if (isset($image_result['filename'])) {
                $update_data['image'] = $image_result['filename'];
            }
        }

        $result = $this->{$this->model}->update_library($update_data, $data['library_id']);


        $message = $result ? 'Library updated successfully' : 'Error updating the record';
        $status = $result ? TRUE : FALSE;
        $this->data['status'] = $status;
        $this->data['message'] = $message;
        $this->api_manager->set_output($this->data);
        die();

//        $this->response(array(
//            'status' => $status,
//            'message' => $message,
//            'data' => $this->api_manager->format_output($response_data)
//                ), REST_Controller::HTTP_OK);
    }

    /**
     * @Name : list()
     * @Purpose : get the list of the libraries of the given logged in user
     * @Call from : from the libraries listing page
     * @Functionality : Fetch the name , image , library_id from the user_record_libraries table
     * @Receiver params : (Headers only)
     * @Return params : return the resultset
     */
    public function get_list() {
        $this->api_manager->handle_request();
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->data['status'] = FALSE;
            $this->data['message'] = 'Authorization Failed';
            $this->api_manager->set_output($this->data);
            die();
        }
        $records = $this->{$this->model}->get_master_branch_array(array('user_id' => $user_details['user_id']));
        if ($records) {
            foreach ($records as $key => $record) {
                unset($records[$key]['date_created']);
                unset($records[$key]['uuid']);
            }
            $this->data['status'] = TRUE;
            $this->data['message'] = 'success';
            $this->api_manager->set_output($this->data);
            die();
        } else {
            $this->data['status'] = FALSE;
            $this->data['message'] = 'NO Records Found';
            $this->api_manager->set_output($this->data);
            die();
        }
    }

}
