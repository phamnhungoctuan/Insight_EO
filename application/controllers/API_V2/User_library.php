<?php

require(APPPATH . 'libraries/REST_Controller.php');
require(APPPATH . 'libraries/Custom_file.php');

class User_library extends REST_Controller {

    public $model = 'user_library_model';

    public function __construct() {
        parent::__construct();
        $this->model = 'user_library_model';
        $this->load->library('api_manager');
        $this->load->library('form_validation');
        $this->load->model('API_V2/user_library_model');
        $this->load->helper('date');
        $this->data = array();
    }

    /**
     * @Name : add()
     * @Purpose : add a library 
     * @Call from : when user click on the add library button from the App
     * @Functionality : Add a library in the  user_record_library
     * @Receiver params : array of columns and values
     * @Return params : on successful insertion of library return True, False otherwise
     */
    function add_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        /* USER AUTHENTICATED SUCCESSFULLY */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('image', 'image', 'trim');
        $this->form_validation->set_rules('record_id', 'Record ID', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');
        $this->form_validation->set_rules('uuid', 'Device ID', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors',), REST_Controller::HTTP_OK);
            die();
        }
        /* VALIDATE THE RECORD AND LIBRARY */
        $result = $this->{$this->model}->get_library_custom(array('user_id' => $user_details['user_id'], 'name' => $data['name'], 'is_shared' => '0'));
        if ($result) {
            $this->response(array('status' => FALSE, 'message' => 'Library name already exist'), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }
        $data['user_id'] = $user_details['user_id'];
        $data['uuid'] = $user_details['uuid'];
        /* PROCESS THE REQUEST */
        $response_data['library_id'] = $this->{$this->model}->add_library($data);
        /* SAVE THE IMAGE */
        if ($response_data['library_id'] && isset($data['image']) && strlen($data['image'])) {
            $fileObj = new Custom_file();
            $image_result = isset($data['image']) && strlen($data['image']) ? $fileObj->move_file($data['image'], $response_data['library_id'], 'assets/uploads/libraries/') : array();
            if (isset($image_result['filename'])) {
                $this->{$this->model}->update_library(array('image' => $image_result['filename']), $response_data['library_id']);
                $response_data['image'] = $image_result;
            }
        }
        $message = $response_data['library_id'] > 0 ? 'Library added successfully' : 'Error inserting the record';
        $status = $response_data['library_id'] > 0 ? TRUE : FALSE;
        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $this->api_manager->format_output($response_data)
                ), REST_Controller::HTTP_OK);
    }

    /**
     * @Name : update()
     * @Purpose : add a library 
     * @Call from : when user click on the add library button from the App
     * @Functionality : updates a library in the  user_record_library with given library_id
     * @Receiver params : array of columns and values, library_id
     * @Return params : on successful updation of library return True, False otherwise
     */
    function update_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* USER AUTHENTICATED SUCCESSFULLY */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('name', 'Name', 'trim');
        $this->form_validation->set_rules('image', 'image', 'trim');
        $this->form_validation->set_rules('record_id', 'Record ID', 'trim');
        $this->form_validation->set_rules('library_id', 'Library ID', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        // Check if library is shared or custom
        $check = $this->{$this->model}->get_library_custom(array('library_id' => $data['library_id'], 'user_id' => $user_details['user_id'], 'is_shared' => '0'));
        if (!$check) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'You can not update this library'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }

        if($check['lib_inserted_from'] != "dropbox"){
            /* VALIDATE THE RECORD */
            $result = $this->{$this->model}->get_library_custom(array('user_id' => $user_details['user_id'], 'name' => $data['name'], 'library_id !=' => $data['library_id']));
            if ($result) {
                $this->response(array(
                    'status' => FALSE,
                    'message' => 'Library name already exist'
                        ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
                die();
            }
        }
        /* PROCESS THE REQUEST */

        $request_keys = array('description','name');
        foreach ($request_keys as $key){
            if(isset($data[$key])){
                $update_data[$key] = $data[$key];
            }
        }
        $update_data['date_updated'] = date('Y-m-d H:i:s');

        /* SAVE THE IMAGE */
        if ($data['library_id'] && isset($data['image']) && strlen($data['image'])) {
            $fileObj = new Custom_file();
            $image_result = isset($data['image']) && strlen($data['image']) ? $fileObj->move_file($data['image'], $data['library_id'], 'assets/uploads/libraries/') : array();
            if (isset($image_result['filename'])) {
                $update_data['image'] = $image_result['filename'];
            }
        }

        $result = $this->{$this->model}->update_library($update_data, $data['library_id']);


        $message = $result ? 'Library updated successfully' : 'Error updating the record';
        $status = $result ? TRUE : FALSE;
        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $this->api_manager->format_output($response_data)
                ), REST_Controller::HTTP_OK);
    }

    /**
     * @Name : list()
     * @Purpose : get the list of the libraries of the given logged in user
     * @Call from : from the libraries listing page
     * @Functionality : Fetch the name , image , library_id from the user_record_libraries table
     * @Receiver params : (Headers only)
     * @Return params : return the resultset
     */
    public function list_post() {
        $this->api_manager->handle_request();
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $where = '(ul.is_shared="0" OR (ul.is_shared="1" AND ul.is_imported="1" AND ul.is_approved="1" AND ul.uuid = "' . $user_details['uuid'] . '"))';
        $records = $this->{$this->model}->get_user_libraries($user_details['user_id'], $where);
        if ($records) {
            $this->response(array( 'status' => TRUE, 'message' => 'success', 'data' => $this->api_manager->format_output(array_values($records)) ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array( 'status' => FALSE, 'message' => 'NO Records Found' ), REST_Controller::HTTP_OK);
        }
    }

    /**
     * @Name : detail()
     * @Purpose : return the detail of the given library Id
     * @Call from : the App, when user click on any library on the listing page
     * @Functionality : fetch the detail of the given library 
     * @Receiver params : library_id
     * @Return params : return the Detail of the library , False otherwise
     */
    public function detail_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('library_id', 'library_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $result = $this->{$this->model}->get_library_detail(array('library_id' => $data['library_id'], 'user_id' => $user_details['user_id']));

        if ($result) {
            $this->load->model('API_V2/library_items_model');
            $result['image'] = strlen($result['image']) ? base_url('assets/uploads/libraries/' . $result['image']) : '';
            $result['item_count'] = $this->library_items_model->count_items($data['library_id']);
            unset($result ['date_updated']);
            unset($result ['date_created']);
            unset($result ['user_id']);
            unset($result ['record_id']);
            unset($result ['id']);
            unset($result ['is_imported']);
            unset($result ['uuid']);
            unset($result ['share_library_code']);
            unset($result ['id']);
            $this->response(array(
                'status' => TRUE,
                'message' => 'success',
                'data' => $this->api_manager->format_output($result)
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'No Records Found',
                    ), REST_Controller::HTTP_OK);
        }
    }
    
    
    public function delete_post(){
        $this->api_manager->handle_request();
        $data = (array)json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array( 'status' => FALSE, 'message' => 'Authorization Failed' ), REST_Controller::HTTP_OK); die();
        }
        if (!(isset($data['library_id']) && is_array($data['library_id']) && count($data['library_id']) )){
            $this->response(array( 'status' => FALSE, 'message' => 'Validation errors' ), REST_Controller::HTTP_OK); die();
        }
        if ($this->{$this->model}->delete_library($data['library_id'])) {
            $this->response(array( 'status' => TRUE, 'message' => 'success' ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array( 'status' => FALSE, 'message' => 'Error while deleting the library' ), REST_Controller::HTTP_OK);
        }
    }

    public function check_item_exits_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('library_id', 'Library Id', 'trim|required');
        $this->form_validation->set_rules('name', 'Item Name', 'trim|required');
        $result = $this->{$this->model}->select_record(array('library_id' => $data['library_id'], 'name' => $data['name']), 'library_items');

        if ($result) {
            $this->response(array(
                'status' => true,
                'message' => 'Item Name  Exists'
                    ), REST_Controller::HTTP_OK);
            die();
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'Item Name Do Not Exists'
                    ), REST_Controller::HTTP_OK);
            die();
        }
    }

    public function add_item_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('library_id', 'Library Id', 'trim|required');
        $this->form_validation->set_rules('name', 'Item Name', 'trim|required');
        $this->form_validation->set_rules('frequency', 'Frequency', 'trim');
        $result = $this->{$this->model}->select_record(array('library_id' => $data['library_id'], 'name' => $data['name']), 'library_items');

        if ($result) {
            $requests_keys = array('library_id', 'name', 'frequency');
            foreach ($requests_keys as $key) {
                if (isset($data[$key])) {
                    if ($key == "frequency") {
                        $item_record[$key] = json_encode($data[$key]);
                    }
                }
            }
            $item_record['date_updated'] = date('Y-m-d H:i:s');
            $this->load->model('API_V2/library_items_model');
            $item_id = $this->library_items_model->update($item_record, array('id' => $result['id']));
            $message = $item_id > 0 ? 'You have Updated the Item ' . $result['name'] . '.' : 'Error Updating the item';
            $status = $item_id > 0 ? TRUE : FALSE;
            $this->response(array(
                'status' => $status,
                'message' => $message,
                    ), REST_Controller::HTTP_OK);
        } else {
            $requests_keys = array('library_id', 'name', 'frequency');
            foreach ($requests_keys as $key) {
                if (isset($data[$key])) {
                    if ($key == "frequency") {
                        $item_record[$key] = json_encode($data[$key]);
                    } else {
                        $item_record[$key] = $data[$key];
                    }
                }
            }
            $item_record['date_created'] = date('Y-m-d H:i:s');
            $this->load->model('API_V2/library_items_model');
            $item_id = $this->library_items_model->save($item_record);
            $message = $item_id > 0 ? 'You have added a new item.' : 'Error inserting the item';
            $status = $item_id > 0 ? TRUE : FALSE;
            $this->response(array(
                'status' => $status,
                'message' => $message,
                'data' => $this->api_manager->format_output(array('item_id' => $item_id))
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function update_item_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('library_id', 'Library Id', 'trim|required');
        $this->form_validation->set_rules('item_id', 'Item Id', 'trim|required');
        $this->form_validation->set_rules('name', 'Item Name', 'trim|required');
        $this->form_validation->set_rules('frequency', 'Frequency', 'trim');

        /* Authenticate the Library - Whether the library belongs to the current logged in user */
        $library = $this->{$this->model}->get_library_custom(array('library_id' => $data['library_id'], 'user_id' => $user_details['user_id'], 'is_shared' => '0'));
        if (!$library) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Record does not exist'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }
        /* Authenticate the Library - Whether the library belongs to the current logged in user */
        $result = $this->{$this->model}->select_record(array('library_id' => $data['library_id'], 'id !=' => $data['item_id'], 'name' => $data['name']), 'library_items');
        if ($result) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Item name is already taken'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        } else {
            $requests_keys = array('library_id', 'name', 'frequency');
            foreach ($requests_keys as $key) {
                if (isset($data[$key])) {
                    if ($key == "frequency") {
                        $item_record[$key] = json_encode($data[$key]);
                    } else {
                        $item_record[$key] = $data[$key];
                    }
                }
            }
            $item_record['date_updated'] = date('Y-m-d H:i:s');
            $this->load->model('API_V2/library_items_model');
            $item_id = $this->library_items_model->update($item_record, array('id' => $data['item_id']));
            $message = $item_id > 0 ? 'You have successfully updated an item.' : 'Error updating the item';
            $status = $item_id > 0 ? TRUE : FALSE;
            $this->response(array(
                'status' => $status,
                'message' => $message,
                'data' => $this->api_manager->format_output(array('item_id' => $item_id))
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function list_items_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('library_id', 'library_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* Authenticate the Library - Whether the library belongs to the current logged in user */
        $library = $this->{$this->model}->get_library_custom(array('library_id' => $data['library_id'], 'user_id' => $user_details['user_id']));
        if (!$library) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Library not found'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }
        $this->load->model('API_V2/library_items_model');
        $records = $this->library_items_model->fetch_records(NULL, NULL, array('library_id' => $data['library_id']));
        if ($records) {
            foreach ($records as $key => $record) {

                $records[$key]['frequency'] = json_decode($records[$key]['frequency']);
                unset($records[$key]['date_created']);
                unset($records[$key]['date_updated']);
                unset($records[$key]['library_id']);
                // unset($records[$key]['frequency']);
            }
            $this->response(array(
                'status' => TRUE,
                'message' => 'success',
                'data' => $this->api_manager->format_output($records)
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Before You Begin. It appears your selected category is empty, would you like to add some individual items now?',
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function item_detail_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }

        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('item_id', 'item_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* Process the request */
        $this->load->model('API_V2/library_items_model');
        $result = $this->library_items_model->get_item_detail(array('item_id' => $data['item_id'], 'user_id' => $user_details['user_id']));
        if ($result) {
            $result['frequency'] = json_decode($result['frequency'], true);
            unset($result ['date_created']);
            unset($result ['date_updated']);
            $this->response(array(
                'status' => TRUE,
                'message' => 'success',
                'data' => $this->api_manager->format_output($result)
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'NO Records Found',
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function share_library_old_post() {
        $status = FALSE;
        $message = 'Already shared';
//        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if (!is_array($data) || count($data) < 1)
            $data = array();
        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        if ($this->form_validation->run() == FALSE || !(isset($data['library_id']) && is_array($data['library_id']) && count($data['library_id']) > 0)) {
            $error = isset($data['library_id']) && is_array($data['library_id']) && count($data['library_id']) > 0 ? validation_errors() : validation_errors() . ' Libraries required ';
            $this->response(array('status' => FALSE, 'message' => 'Validation errors', 'error' => explode("\n", $error)), REST_Controller::HTTP_OK);
            die();
        }
        /* Process the request */
        $this->load->model('API_V2/user_model', 'user_model');
        $user = $this->user_model->get_user(array("email" => $data['email'], "email !=" => $user_details['email']));
        if (!$user) {
            $this->response(array('status' => FALSE, 'message' => 'User not found'), REST_Controller::HTTP_OK);
            die();
        }
        if (is_array($data['library_id']) && count($data['library_id']) > 1) {
            $bulk_share_code = $this->{$this->model}->generate_random_string();
        } else {
            $bulk_share_code = false;
        }
        $library_response_arr = array();
        $count = 0;
        foreach ($data['library_id'] as $library_id) {
            /* CHECK WHETHER ITS A SHARED LIBRARY OR NOT */
            $can_share = $this->{$this->model}->get_library_custom(array('user_id' => $user_details['user_id'], "is_shared" => "0", "library_id" => $library_id));
            if ($can_share) {
                $check_already_shared = $this->{$this->model}->get_library_custom(array('user_id' => $user['id'], "is_shared" => "0", "library_id" => $library_id), FALSE);
                if ($check_already_shared) {
                    $library_response_arr[] = array('library_id' => $library_id, 'message' => 'Already shared');
                    continue;
                } else {
                    $insert_data = array();
                    $insert_data['share_library_code'] = $this->{$this->model}->generate_random_string();
                    $insert_data['user_id'] = $user['id'];
                    $insert_data['library_id'] = $library_id;
                    if ($bulk_share_code) {
                        $insert_data['bulk_share_code'] = $bulk_share_code;
                    }
                    $insert_result = $this->{$this->model}->share_library($user_details['user_id'], $insert_data);
                    if ($insert_result) {
                        $library_response_arr[] = array('library_id' => $library_id, 'message' => 'Imported Successfully');
                        $count++;
                        $email_data[] = array('code' => $insert_data['share_library_code'], 'name' => $insert_result['name']);
                    } else {
                        $library_response_arr[] = array('library_id' => $library_id, 'message' => 'Library not found');
                    }
                }
            } else {
                $library_response_arr[] = array('library_id' => $library_id, 'message' => 'Cannot Share a shared library');
            }
        }
        if (isset($email_data) && count($email_data)) {
            /* EMAIL Library Code TO THE USER */
            $mail_params['ContentType'] = "text/html";
            $mail_params['To'] = $user['email'];
            $mail_params['Subject'] = 'Energy ReMastered Shared Library Code';
            $mail_params['Body'] = $this->load->view('email_format/shared_library_code_email', array('shared_with' => $user, 'shared_by' => $user_details, 'email_data' => $email_data, 'bulk_share_code' => $bulk_share_code), true);

            if ($this->{$this->model}->sendMail($mail_params)) {
                $status = TRUE;
                $message = count($data['library_id']) > 1 ? 'Libraries' : 'Library';
                $message .= strlen($message) ? ' Shared. Please check your email inbox' : '';
            } else {
                $status = TRUE;
                $message = 'Email sending Failed';
            }
        }

        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $this->api_manager->format_output(array('total_libraries' => count($data['library_id']), 'total_shared' => $count, 'result' => $library_response_arr))
                ), REST_Controller::HTTP_OK);
        die();
    }

    /* TESTING */

    public function share_library_post() {
        $this->api_manager->handle_request();
        $data = (array) json_decode(file_get_contents("php://input"), true);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        if ($this->form_validation->run() == FALSE || !(isset($data['library_id']) && is_array($data['library_id']) && count($data['library_id']) > 0)) {
            $error = isset($data['library_id']) && is_array($data['library_id']) && count($data['library_id']) > 0 ? validation_errors() : validation_errors() . ' Libraries required ';
            $this->response(array('status' => FALSE, 'message' => 'Validation errors', 'error' => explode("\n", $error)), REST_Controller::HTTP_OK);
            die();
        }
        /* Process the request */
        $this->load->model('API_V1/user_model');
        /* GET THE USER INFO WITH WHOM TO SHARE THE LIBRARIES THAT SHOULD NOT BE EQUAL TO USER SHARING THE LIBRARIES i.e. THE CURRENT LOGGED IN USER */
        $shared_with = $this->user_model->get_user(array("email" => $data['email'], "email !=" => $user_details['email']));
        if (!count($shared_with)) {
            $this->response(array('status' => FALSE, 'message' => 'User not found'), REST_Controller::HTTP_OK);
            die();
        }

        /* CHECK SHARABLE LIBRARIES (only the created libraries can be shared i.e. is_shared must be 0) */
        $where = 'user_id = "' . $user_details['user_id'] . '" AND is_shared="0" AND library_id IN (' . implode(',', $data['library_id']) . ')';
        $shareable_libraries = $this->{$this->model}->get_libraries_custom($where);

        if ($shareable_libraries) {
            $already_shared_library_count = 0;
            $sharable_lib_ids = array_column($shareable_libraries, 'library_id');
            /* CHECK ALREADY SHARED */
            $already_shared_libraries = $this->user_library_model->get_already_shared_libraries($shared_with['id'], $sharable_lib_ids);
            if ($already_shared_libraries) {
                
                $already_shared_lib_ids = array_column($already_shared_libraries, 'library_id');
                /* get the data for previously shared libararies for which user has not responded i.e. is_imported = 0 and uuid is blank or null */
                foreach ($already_shared_libraries as $key => $already_shared_lib) {
                    foreach ($shareable_libraries as $key => $library) {
                        if ($library['library_id'] == $already_shared_lib['library_id']) {
                            if (!is_null($already_shared_lib['bulk_share_code'])) {
                                $email_data['already_shared']['bulk_share_libraries'][$already_shared_lib['bulk_share_code']][] = array('library_name' => $library['name'], 'single_share_code' => $already_shared_lib['share_library_code']);
                                $already_shared_library_count++;
                            } else {
                                $email_data['already_shared']['single_share_libraries'][] = array('library_name' => $library['name'], 'single_share_code' => $already_shared_lib['share_library_code']);
                                $already_shared_library_count++;
                            }
                            break;
                        }
                    }
                }
            }
            if (!$already_shared_libraries || count($already_shared_libraries) < count($shareable_libraries)) {

                function udiffCompare($a, $b) {
                    return $a['library_id'] - $b['library_id'];
                }

                $not_shared_libraries = $already_shared_libraries?  array_udiff($shareable_libraries, $already_shared_libraries, 'udiffCompare') : $shareable_libraries;
                if (count($not_shared_libraries)) {
                    $library_insert_data = array();
                    if (is_array($not_shared_libraries) && count($not_shared_libraries) > 1) {
                        $bulk_share_code = $this->{$this->model}->generate_random_string();
                    } else {
                        $bulk_share_code = FALSE;
                    }
                    foreach ($not_shared_libraries as $key => $library) {
                        $single_share_lib_code = $this->{$this->model}->generate_random_string();
                        $library_insert_data[] = array(
                            'share_library_code' => $single_share_lib_code,
                            'user_id' => $shared_with['id'],
                            'library_id' => $library['library_id'],
                            'bulk_share_code' => $bulk_share_code ? $bulk_share_code : NULL,
                            'is_imported' => '0',
                            'is_shared' => '1',
                            'is_approved' => '1',
                            'date_created' => date('Y-m-d H:i:s'),
                        );
                        $email_data['new'][] = array('single_share_code' => $single_share_lib_code, 'library_name' => $library['name']);
                    }
                }
            }
            /* EMAIL THE CODES TO THE USER AND THEN INSERT THE ENTRIES INTO THE DATABASE */
            if ($email_data) {
                /* SEND EMAIL FOR SHARED AND ALREADY SHARED lIBRARIES */
                $bulk_share_code = !empty($bulk_share_code) ? $bulk_share_code : NULL;
                if ($this->send_libraries_share_email($user_details, $shared_with, $email_data, $bulk_share_code, $already_shared_library_count)) {
                    /* SAVE THE DATA IF ANY */
                    if (isset($library_insert_data)) {
                        $this->user_library_model->save_share_library_data($library_insert_data);
                        $message = isset($email_data['new']) && count($email_data['new']) > 1 ? 'Libraries shared.' : 'Library shared.';
                        $this->response(array('status' => TRUE, 'message' => $message), REST_Controller::HTTP_OK);
                        die();
                    }
                    if ($already_shared_libraries) {
                        /* ALREADY SHARED CASE : NO LIBRARIES TO SHARE */
                        $this->response(array('status' => TRUE, 'message' => 'Already Shared'), REST_Controller::HTTP_OK);
                        die();
                    }
                } else {
                    $this->response(array('status' => FALSE, 'message' => 'Email sending Failed. Please try again'), REST_Controller::HTTP_OK);
                    die();
                }
            }
        } else {
            $this->response(array('status' => FALSE, 'message' => 'Unable to share the Shared libraries.'), REST_Controller::HTTP_OK);
            die();
        }
    }

    private function send_libraries_share_email($user_details, $share_with, $email_data, $bulk_share_code = NULL, $already_shared_library_count = 0) {
        
        $flag = TRUE;
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $share_with['email'];
        $mail_params['Subject'] = 'Energy ReMastered Shared Library Code';

        /* FIRST SEND ALREADY SHARED LIBRARIES MAIL */
        if (isset($email_data['already_shared'])) {
            $mail_params['Body'] = $this->load->view('email_format/API_V2/already_shared_libraries_email', array('shared_with' => $share_with, 'shared_by' => $user_details, 'email_data' => $email_data['already_shared'], 'already_shared_library_count'=>$already_shared_library_count), true);
            $flag = $this->{$this->model}->sendMail($mail_params);
        }
        /* SEND RECENTLY SHARED LIBRARIES MAIL */
        if ($flag && isset($email_data['new'])) {
            $mail_params['Body'] = $this->load->view('email_format/API_V2/shared_libraries_code_email', array('shared_with' => $share_with, 'shared_by' => $user_details, 'email_data' => $email_data['new'], 'bulk_share_code' => $bulk_share_code), true);
            $flag = $this->{$this->model}->sendMail($mail_params);
        }
        return $flag;
    }

    /* Name should be import Libraries */

    public function accept_share_request_post() {
        $this->api_manager->handle_request();
        $data = (array) json_decode(file_get_contents("php://input"), true);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('code', 'Library Code', 'trim|required');
        $this->form_validation->set_rules('is_collective_import', 'Is Collective Import', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors',), REST_Controller::HTTP_OK);
            die();
        }

        /* CHECK WHETHER THE CODE IS CORRECT OR NOT BEFORE PROCESSING */
        $code_check = $this->{$this->model}->check_lib_share_code($user_details['user_id'], $data['code'], $data['is_collective_import']);
        if (!$code_check) {
            $this->response(array('status' => FALSE, 'message' => 'Your Library Code is wrong'), REST_Controller::HTTP_OK);
            die();
        }

        /* GET THE OWNER OF THE LIBRARY */
        $col_code = $data['is_collective_import'] ? "bulk_share_code" : "share_library_code";
        $owner_data = $this->{$this->model}->get_lib_owner(array($col_code => $data['code']));
        /* Single Share Library PROCESSING STARTS HERE  */
        if ($data['is_collective_import'] == 0) {
            /* CASE WHERE EITHER UUID IS BLANK OR UUID IS SAME */
            $where = "share_library_code = '" . $data['code'] . "' AND user_id = '" . $user_details['user_id'] . "' AND is_shared = '1' AND ( uuid IS NULL OR uuid = '' OR uuid ='" . $user_details['uuid'] . "' ) ";
            $library = $this->{$this->model}->get_library_custom($where);
            if ($library) { /* IF record exists */
                /* CHECK WHETHER ITS ALREADY IMPORTED */
                if ($library['is_imported'] == "1" && $library['uuid'] == $user_details['uuid'] && $library['is_approved'] == '1') { /* (is_imported = 1 and uuid = "" isn't possible case) */
                    $this->response(array('status' => FALSE, 'message' => 'Library already shared with you', 'data' => $this->api_manager->format_output(array('status' => 0))), REST_Controller::HTTP_OK);
                    die();
                } else {
                    /* import library */
                    if ($library['is_approved'] == "1") { /* isapproved = 1 & ( uuid = '' or uuid is same) in both case import */
                        $response_data = array();
                        $response_data['code'] = $data['code'];
                        $response_data['is_collective_import'] = $data['is_collective_import'];
                        $response_data['total_pages'] = 1;
                        if ($this->{$this->model}->update_user_libraries(array('uuid' => $user_details['uuid']), array('id' => $library['id']))) {
                            $this->response(array('status' => TRUE, 'message' => 'Library imported', 'data' => $this->api_manager->format_output($response_data)), REST_Controller::HTTP_OK);
                            die();
                        } else {
                            $this->response(array('status' => FALSE, 'message' => 'library cant be accepted'), REST_Controller::HTTP_OK);
                            die();
                        }
                    } else {
                        /* is_approved = 0 with uuid ="" not possible, only for same uuid */
                        $this->response(array('status' => FALSE, 'message' => 'library request already sent to the owner for approval'), REST_Controller::HTTP_OK);
                        die();
                    }
                }
            } else { /* CASE WHEN THER'S NO RECORD FOR THE CURRENT UUID i.e. USER IS IMPORTING FROM ANOTHER DEVICE */
                self::request_library($data['code'], $user_details, $owner_data);
            }
        } /* SINGLE SHARE LIBRARY PROCESSING END */
        /* BULK SHARING LIBRARY PROCESSING STARTS HERE */
        if ($data['is_collective_import'] == 1) {
            $status = FALSE;
            $message = '';
            $response_data = array();
            /* PROCESS THE RECORDS THAT HAVE EITHER BLANK UUID OR SAME UUID EXCLUSING THE RECORDS THAT HAVE IS_IMPORTED  = 1 and is_approved = 1 */
            $libraries = $this->{$this->model}->get_libraries_for_bulk_share($user_details['uuid'], $data['code']);
            $total_libraries_shared = $total_libraries_not_approved = $total_libraries_already_imported = $total_libraries_rejected = 0;
            if ($libraries) {
                foreach ($libraries as $library) {
                    if ($library['is_approved'] == '1') {
                        if ($library['is_imported'] == '0') {
                            /* IMPORT THE LIBRARIES */
                            $update_result = $this->{$this->model}->update_user_libraries(array('uuid' => $user_details['uuid']), array('id' => $library['id']));
                            if ($update_result) {
                                $total_libraries_shared++;
                            }
                        } else {
                            $total_libraries_already_imported++;
                        }
                    } else if ($library['is_approved'] == '0') {
                        $total_libraries_not_approved++;
                    } else if ($library['is_approved'] == '2') {
                        $total_libraries_rejected++;
                    }
                }
            }
            if ($total_libraries_rejected) {
                $message = 'Libraries share request for some(all) libraries has been rejected by the owner';
            } else if ($total_libraries_already_imported == count($libraries)) {
                $message = 'Libraries Already shared';
            } else if ($total_libraries_not_approved == count($libraries)) {
                $message = 'Share request for libraries already sent to the owner for approval';
            } else if ($total_libraries_already_imported || $total_libraries_not_approved) {
                $message = 'Some libraries are either already shared or waiting for the owner to approve';
            } else {
                $message = 'Library shared';
            }
            /* Process those libraries for which USER IS IMPORTING FROM ANOTHER DEVICE. SEND REQUEST TO QWNER FOR APPROVAL */
            $request_libraries = $this->{$this->model}->get_libraries_for_bulk_share_request($user_details['uuid'], $data['code']);
            if ($request_libraries) {
                $return = self::request_libraries($request_libraries, $user_details, $owner_data);
                if ($return) {
                    $message = 'Some Libraries are already shared in another device.Library share request is sent to owner';
                } else {
                    $message = 'Some Libraries are already shared in another device.Error while sending request to the owner.Please try again';
                }
            }

            if ($total_libraries_shared) {
                /* FETCH THE shared libraries data to be send in Response */
                $response_data['code'] = $data['code'];
                $response_data['is_collective_import'] = $data['is_collective_import'];
                $response_data['total_pages'] = $this->api_manager->get_page_from_count($total_libraries_shared, SYNC_RECORDS_PER_PAGE);
                $status = TRUE;
                $message = 'Library shared';
            }
            $this->response(array('status' => $status, 'message' => $message, 'data' => $this->api_manager->format_output($response_data)), REST_Controller::HTTP_OK);
            die();
        }
        /* BULK SHARING LIBRARY PROCESSING ENDS HERE */
    }

    public function sync_shared_library_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        $data = is_array($data) ? $data : array();
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('code', 'Library Code', 'trim|required');
        $this->form_validation->set_rules('total_pages', 'Total Page', 'trim|required|numeric');
        $this->form_validation->set_rules('page', 'Page', 'trim|required|numeric');
        $this->form_validation->set_rules('is_collective_import', 'Is Collective Import', 'trim|required|numeric');

        if ($this->form_validation->run() == FALSE) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors',), REST_Controller::HTTP_OK);
            die();
        }
        /* GET THE LIBRARIES  FOR THE CURRENT PAGE */
        $code_column = $data['is_collective_import'] ? "bulk_share_code" : "share_library_code";
        $where = 'ul.uuid = "' . $user_details['uuid'] . '" AND ul.is_imported ="0" AND ul.is_shared ="1" AND ul.is_approved ="1"  AND ul.' . $code_column . ' = "' . $data['code'] . '" ';
        $page_no = $data['page'] > 0 ? $data['page'] - 1 : 0;
        $records = $this->user_library_model->fetch_library_and_items($user_details['user_id'], $where, $page_no * SYNC_RECORDS_PER_PAGE, SYNC_RECORDS_PER_PAGE);
        /* UPDATE IS_IMPORTED PARAMETER IF ITS THE LAST REQUEST */
        if ($data['total_pages'] == $data['page']) {
            $this->{$this->model}->update_user_libraries(array('is_imported' => '1'), array('uuid' => $user_details['uuid'], 'is_shared' => '1', 'is_imported' => '0', 'is_approved' => '1', 'user_id' => $user_details['user_id'], $code_column => $data['code']));
        }
        $this->response(array('status' => TRUE, 'message' => 'success', 'data' => $this->api_manager->format_output($records)), REST_Controller::HTTP_OK);
    }

    private function request_library($code, $user_details, $owner_data) {
        // process the request
        $library = $this->{$this->model}->get_library_custom(array('share_library_code' => $code));
        if ($library) {
            $insert_data = array(
                'share_library_code' => $library['share_library_code'],
                'user_id' => $user_details['user_id'],
                'library_id' => $library['library_id'],
                'is_shared' => '1',
                'bulk_share_code' => $library['bulk_share_code'],
//                'is_imported' => '1',
                'uuid' => $user_details['uuid'],
                'is_approved' => '0',
                'token' => $this->{$this->model}->generate_random_string(),
            );
            /* EMAIL Library Code TO THE OWNER */
            $mail_params['ContentType'] = "text/html";
            $mail_params['To'] = $owner_data['email'];
            $mail_params['Subject'] = 'Energy ReMastered Request For Library';
            $mail_params['Body'] = $this->load->view('email_format/API_V1/request_for_library_email', array('user_id' => $user_details['user_id'], 'user_name' => trim($user_details['first_name'] . " " . $user_details['last_name']), 'library_name' => $library['name'], 'code' => $library['share_library_code'], 'token' => $insert_data['token']), true);
            if ($this->{$this->model}->sendMail($mail_params)) {
                $library_id = $this->{$this->model}->insert_user_libraries($insert_data);
                if ($library_id) {
                    $this->response(array('status' => FALSE, 'message' => 'Library is already shared in another device.Library share request is sent to owner'), REST_Controller::HTTP_OK);
                    die();
                } else {
                    $this->response(array('status' => FALSE, 'message' => 'Error inserting record'), REST_Controller::HTTP_OK);
                    die();
                }
            } else {
                $this->response(array('status' => FALSE, 'message' => 'Request cannot sent to the owner.Please Try again'), REST_Controller::HTTP_OK);
                die();
            }
        }
        $this->response(array('status' => FALSE, 'message' => 'No library found'), REST_Controller::HTTP_OK);
        die();
    }

    private function request_libraries($data, $user_details, $owner_data) {
        $token = $this->{$this->model}->generate_random_string();
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $owner_data['email'];
        $mail_params['Subject'] = 'Energy ReMastered Request For Library';
        $mail_params['Body'] = $this->load->view('email_format/API_V1/bulk_request_for_library_email', array('token' => $token, 'user_details' => $user_details, 'data' => $data, 'bulk_share_code' => $data[0]['bulk_share_code']), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            foreach ($data as $library) {
                $insert_data = array();
                $insert_data['share_library_code'] = $library['share_library_code'];
                $insert_data['user_id'] = $user_details['user_id'];
                $insert_data['library_id'] = $library['library_id'];
                $insert_data['bulk_share_code'] = $library['bulk_share_code'];
                $insert_data['is_shared'] = '1';
//                $insert_data['is_imported'] = '1';
                $insert_data['uuid'] = $user_details['uuid'];
                $insert_data['is_approved'] = '0';
                $insert_data['token'] = $token;
                $this->{$this->model}->insert_user_libraries($insert_data);
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function accept_import_request_get() {
        $id = $this->input->get('id');
        $token = $this->input->get('token');
        $status = $this->input->get('status');
        $share_lib_code = $this->input->get('code');

        $library_data = $this->{$this->model}->get_library_custom(array('user_id' => $id, 'token' => $token, 'is_approved' => '0', 'share_library_code' => $share_lib_code));

        /* AUTHENTICATE THE USER */
        /* Validate the parameters */
        if ($library_data) {
            if ($status == 1) {
                $message = "Your library request for " . $library_data['name'] . ' library has been accepted.';
            } else {
                $message = "Your library request for " . $library_data['name'] . ' library has been rejected.';
            }

            if ($this->{$this->model}->update_user_libraries(array('is_approved' => $status), array('id' => $library_data['id']))) {
                $this->load->model('API_V2/user_model', 'user_model');
                $user_data = $this->user_model->get_user(array('id' => (int) $library_data['user_id']));
                /* EMAIL Library Code TO THE USER */
                $mail_params['ContentType'] = "text/html";
                $mail_params['To'] = $user_data['email'];
                $accepted = $status == 1 ? "Accepted" : "Rejected";
                $mail_params['Subject'] = 'Energy ReMastered Request For Library "' . $library_data['name'] . '"' . $accepted;
                $mail_params['Body'] = $this->load->view('email_format/request_accepted_for_library_email', array('message' => $message, 'user_name' => trim($user_data['first_name'] . " " . $user_data['last_name'])), true);
                $this->{$this->model}->sendMail($mail_params);
                /* SAVE IN NOTIFICATIONS TABLE */
                /* $this->{$this->model}->insert_notification(array('status' => 0, 'message' => $message, 'user_id' => $library_data['user_id'])); */
                echo "You have Updated the library request";
                exit;
            } else {
                echo 'Library can not be updated';
                exit;
            }
        } else {
            echo "Library can not be found";
        }
    }

    function accept_bulk_import_request_get() {

        ini_set('memory_limit', '-1');
        $code = $this->input->get('code');
        $token = $this->input->get('token');
        $status = $this->input->get('status');

        $libraries_data = $this->{$this->model}->get_libraries_custom(array('bulk_share_code' => $code, 'token' => $token, 'is_approved' => '0'));
        /* AUTHENTICATE THE USER */
        /* Validate the parameters */
        if ($libraries_data) {
            $library_names = array();
            $user_id = false;
            foreach ($libraries_data as $library_data) {
                $this->{$this->model}->update_user_libraries(array('is_approved' => $status), array('id' => $library_data['id']));
                $library_names[] = $library_data['name'];
                if (!$user_id) {
                    $user_id = $library_data['user_id'];
                }
            }
            $library_names = implode(',', $library_names);

            if ($status == 1) {
                $message = "Your library request for " . $library_names . ' library has been accepted.';
            } else {
                $message = "Your library request for " . $library_names . ' library has been rejected.';
            }


            $this->load->model('API_V2/user_model');
            $user_data = $this->user_model->get_user(array('id' => (int) $user_id));


            /* EMAIL Library Code TO THE USER */
            $mail_params['ContentType'] = "text/html";
            $mail_params['To'] = $user_data['email'];
            $accepted = $status == 1 ? "Accepted" : "Rejected";
            $mail_params['Subject'] = 'Energy ReMastered Request For Libraries ' . $accepted;
            $mail_params['Body'] = $this->load->view('email_format/request_accepted_for_library_email', array('message' => $message, 'user_name' => trim($user_data['first_name'] . " " . $user_data['last_name'])), true);
            $this->{$this->model}->sendMail($mail_params);
            /* SAVE THE NOTIFICATION */
            /* $this->{$this->model}->insert_notification(array('status' => 0, 'message' => $message, 'user_id' => $library_data['user_id'])); */
            echo "You have Updated the library request";
            exit;
        } else {
            echo "Library can not be found";
        }
    }

    function associate_record_with_library_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }


        /* Validate the parameters */
        $this->form_validation->set_data($data);

        $this->form_validation->set_rules('record_id', 'Record Id', 'trim|required');
        if ($this->form_validation->run() == FALSE || !(isset($data['library_id']) && is_array($data['library_id']) && count($data['library_id']) > 0)) {
            $error = isset($data['library_id']) && is_array($data['library_id']) && count($data['library_id']) > 0 ? validation_errors() : validation_errors() . ' Libraries required ';
            $this->response(array(
                'status' => FALSE, 'message' => 'Validation errors', 'error' => 'Validation errors',), REST_Controller::HTTP_OK);
            die();
        }



        // process the request
        $this->load->model('API_V2/user_record_model');
        $check_user_have_record = $this->user_record_model->get_user_record_detail(array('user_id' => $user_details['user_id'], 'id' => $data['record_id']));
        if (!$check_user_have_record) {
            $this->response(array(
                'status' => FALSE,
                'message' => "You do not have access to this Record"
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $library_response_arr = array();
        $count = 0;
        $already_associated_count = $error_count = 0;
        foreach ($data['library_id'] as $library_id) {

            $check_user_have_library = $this->{$this->model}->get_library_custom(array('user_id' => $user_details['user_id'], 'library_id' => $library_id, 'is_approved' => '1'));
            if (!$check_user_have_library || (count($check_user_have_library) && $check_user_have_library['is_shared'] == '1' && $check_user_have_library['is_imported'] == '0')) {
                $library_response_arr[] = array('library_id' => $library_id, 'message' => 'You do not have access to this library');
                $error_count++;
            } else {
                $this->load->model('API_V2/library_items_model');
                $item_id = $this->library_items_model->count_items($library_id);

                if (!$item_id) {
                    $library_response_arr[] = array('library_id' => $library_id, 'message' => 'Library have no item');
                    $error_count++;
                } else {
                    $check_already_associated = $this->{$this->model}->get_record_library(array('record_id' => $data['record_id'], 'library_id' => $library_id, 'is_group' => '0'));
                    if ($check_already_associated) {
                        $library_response_arr[] = array('library_id' => $library_id, 'message' => 'Library already associated');
                        $already_associated_count++;
                    } else {
                        $associate_id = $this->{$this->model}->associate_record_with_library(array('record_id' => $data['record_id'], 'library_id' => $library_id));
                        if ($associate_id) {
                            $count++;
                        }
                    }
                }
            }
        }
        if ($error_count) {
            $message = 'Unable to associate libraries';
        }
        if ($already_associated_count && $already_associated_count == count($data['library_id']) - $error_count) {
            $message = 'Libraries are already associated';
        }
        if ($count) {
            $message = $count . ' out of ' . count($data['library_id']);
            $message.= count($data['library_id']) > 1 ? ' Libraries associated' : 'Libraries associated';
        }
        $this->response(array(
            'status' => true,
            'message' => $message,
            'data' => $this->api_manager->format_output(array('total_libraries' => count($data['library_id']), 'total_shared' => $count, 'result' => $library_response_arr))
                ), REST_Controller::HTTP_OK);

        die();
    }

    function list_library_for_record_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* Validate the parameters */
        $this->form_validation->set_data($data);

        $this->form_validation->set_rules('record_id', 'Record Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $this->load->model('API_V2/user_record_model');
        $check_user_have_record = $this->user_record_model->get_user_record_detail(array('user_id' => $user_details['user_id'], 'id' => $data['record_id']));
        if (!$check_user_have_record) {
            $this->response(array(
                'status' => FALSE,
                'message' => "you do not have access to this Record"
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $library_data = $this->{$this->model}->get_all_record_library_data($data['record_id']);

        if (!empty($library_data)) {
            $response_data = array();
            foreach ($library_data as $key => $libarary) {
                $response_data[$libarary['id']]['library_id'] = $libarary['id'];
                $response_data[$libarary['id']]['name'] = $libarary['library_name'];
                $response_data[$libarary['id']]['description'] = $libarary['library_description'];
                $response_data[$libarary['id']]['image'] = $libarary['image'];
                if (isset($response_data[$libarary['id']]['items']) && !is_array($response_data[$libarary['id']]['items'])) {
                    $response_data[$libarary['id']]['items'] = array();
                }
                $response_data[$libarary['id']]['items'][] = array('name' => $libarary['item_name'], 'frequency' => json_decode($libarary['frequency'], TRUE));
            }

            $response_data = array_values($response_data);
            $this->response(array(
                'status' => true,
                'message' => "Success",
                'data' => $this->api_manager->format_output($response_data)
                    ), REST_Controller::HTTP_OK);
            die();
        } else {
            $this->response(array(
                'status' => false,
                'message' => "No library exists for this record",
                    ), REST_Controller::HTTP_OK);
            die();
        }
    }

    function get_libraries_count_for_record_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }

        /* Validate the parameters */
        $this->form_validation->set_data($data);

        $this->form_validation->set_rules('record_id', 'Record Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        // check if user have access to this record
        $this->load->model('API_V2/user_record_model');
        $check_user_have_record = $this->user_record_model->get_user_record_detail(array('user_id' => $user_details['user_id'], 'id' => $data['record_id']));
        if (!$check_user_have_record) {
            $this->response(array(
                'status' => FALSE,
                'message' => "you do not have access to this Record"
                    ), REST_Controller::HTTP_OK);
            die();
        }

        $library_count = $this->{$this->model}->get_libraries_count_for_record($data['record_id']);

        $this->response(array(
            'status' => true,
            'message' => "success",
            'data' => $this->api_manager->format_output($library_count)
                ), REST_Controller::HTTP_OK);
        die();
    }

    /* Delete the record Libraries as well as Master branch Ids 
     * parameters : 
     * i) record_id
     * ii) library_id (array of Library ids)
     * iii) master_branch_id (array of master branch ids)
     */

    function delete_record_library_post() {
//        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('record_id', 'Record Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors',), REST_Controller::HTTP_OK);
            die();
        }
        /* Authenticate the record */
        $this->load->model('API_V2/user_record_model');
        $check_user_have_record = $this->user_record_model->get_user_record_detail(array('user_id' => $user_details['user_id'], 'id' => $data['record_id']));
        if (!$check_user_have_record) {
            $this->response(array('status' => FALSE, 'message' => "you do not have access to this Record"), REST_Controller::HTTP_OK);
            die();
        }
        /* DELETE ASSOCIATED LIBRARIES */
        if (isset($data['library_id']) && is_array($data['library_id']) && count($data['library_id'])) {
            $result = $this->{$this->model}->delete_record_library($data['record_id'], $data['library_id']);
        }
        /* DELETE ASSOCIATED MASTER BRANCH */
        if (isset($data['master_branch_id']) && is_array($data['master_branch_id']) && count($data['master_branch_id'])) {
            $this->load->model('API_V2/User_master_branch_model', 'user_master_branch_model');
            $result = $this->user_master_branch_model->delete_record_master_branch($data['record_id'], $data['master_branch_id']);
        }
        if (isset($result) && $result) {
            $this->response(array('status' => true, 'message' => "Success"), REST_Controller::HTTP_OK);
            die();
        } else {
            $this->response(array('status' => FALSE, 'message' => "Error Deleting the records"), REST_Controller::HTTP_OK);
            die();
        }
    }

}
