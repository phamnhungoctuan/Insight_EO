<?php

require(APPPATH . 'libraries/REST_Controller.php');
require_once(APPPATH . 'libraries/Custom_file.php');

class User_clients extends REST_Controller {

    public $model = 'user_model';

    public function __construct() {
        parent::__construct();
        $this->model = 'user_model';
        $this->load->library('api_manager');
        $this->load->library('form_validation');
//        $this->load->library('custom_file');
        $this->load->model('API_V5/user_model', 'user_model');
        $this->load->helper('date');
        $this->data = array();
    }

    /*  Client RECORD ANALYSIS RELATED APIS */

    /**
     * @Name : add_analysis_records()
     * @Purpose : Add the New analysis for the given client record. If item already exists then update the Value else insert it
     * @Call from : App
     * @Receiver params : 1. Record_id 2. data : array of item and values
     * @Return params : True or false
     */
    public function add_analysis_records_post() {
        $this->api_manager->handle_request();
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array('status' => FALSE, 'message' => 'Authorization Failed'), REST_Controller::HTTP_OK);
            die();
        }
        $data = (array) json_decode(file_get_contents("php://input"), true);
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('record_id', 'Record ID', 'trim|required');
        if ($this->form_validation->run() == false || empty($data['data'])) {
            $this->response(array('status' => FALSE, 'message' => 'Validation errors'), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }
        /* Process the request */
        $this->load->model('API_V5/client_analysis_details_model');
        
        $analysis_insert_data = array( array(
            'record_id' => $data['record_id'],
            'data' => $data['data']
        ));
        $status = $this->client_analysis_details_model->save($analysis_insert_data);
        
        if (isset($status) && $status) {
            $this->response(array( 'status' => TRUE, 'message' => "success" ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
        } else {
            $this->response(array( 'status' => FALSE, 'message' => "error inserting the record" ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
        }
    }

    /**
     * @Name : list_analysis()
     * @Purpose : Return List of analysis records for the given client record
     * @Call from : App
     * @Receiver params : record_id
     * @Return params : True with list of analysis or false
     */
    public function list_analysis_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* VALIDATE THE PARAMETERS */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('record_id', 'Record ID', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }

        /* VALIDATE THE RECORD */
        $this->load->model('API_V5/user_record_model');
        $result = $this->user_record_model->get_user_record_detail(array('id' => $data['record_id'], 'user_id' => $user_details['user_id']));
        if (!$result) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Record does not exist'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }

        /* PROCESS THE REQUEST */
        $this->load->model('analysis_records_model');
        $records = $this->analysis_records_model->get_latest_analysis(array('record_id' => $data['record_id']));

        if (!empty($records)) {
            $result = array();
            foreach ($records as $key => $record) {
                $result[$key]['analysis_id'] = $record['analysis_id'];
                //  $result['analysis'] = $record['analysis'];
                $record_date = date('M d, Y', strtotime($record['date_created']));
                $record_time = date('h:i a ', strtotime($record['date_created']));
                $result[$key]['date'] = $record_date;
                $result[$key]['time'] = $record_time;
            }


            $this->response(array(
                'status' => TRUE,
                'message' => 'success',
                'data' => $this->api_manager->format_output($result)
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'NO Records Found',
                    ), REST_Controller::HTTP_OK);
        }
    }

    /**
     * @Name : analysis_detail()
     * @Purpose : Return the detail of single analysis session based on analysis ID
     * @Call from : App
     * @Receiver params : analysis Id
     * @Return params : True or false
     */
    public function analysis_detail_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* VALIDATE THE PARAMETERS */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('analysis_id', 'Analysis ID', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* PROCESS THE REQUEST */
        $this->load->model('analysis_records_model');
        $records = $this->analysis_records_model->get_analysis_detail(array('analysis_id' => $data['analysis_id'], 'user_id' => $user_details['user_id']));
        if ($records) {
            $result = array();
            foreach ($records as $key => $record) {
                $result[$key]['date'] = date('M d, Y', strtotime($record['date_created']));
                $result[$key]['analysis'] = strlen(trim($record['analysis'])) ? json_decode($record['analysis'], TRUE) : array();
                $result[$key]['time'] = date('h:i a', strtotime($record['date_created']));
                $result[$key]['tag'] = $record['tag'];
            }
//              
            $this->response(array(
                'status' => TRUE,
                'message' => 'success',
                'data' => $this->api_manager->format_output($result)
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'NO Records Found',
                    ), REST_Controller::HTTP_OK);
        }
    }

    /* UPLOAD THE FILE IN TEMP_UPLOADS FOLDER AND RETURNS THE FILE NAME */

    /**
     * @Name : upload_file()
     * @Purpose : UPload the file in temp folder and return the name of file saved in temp folder
     * @Call from : App
     * @Receiver params : type of file send, file  (record_id Not used now)
     * @Return params : success or failure
     */
    public function upload_file_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        /* write file parameters */
        $fp = fopen("assets/logfiles/user_record_update.txt", "a");
        fwrite($fp, "\n\n*****************************************upload_file*******    " . date("Y-m-d H:i:s") . "\n");
        foreach ($_REQUEST as $key => $val) {
            fwrite($fp, $key . '=>' . $val . "\n");
        }
        fwrite($fp, json_encode($_FILES));
        fclose($fp);


        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* USER AUTHENTICATED SUCCESSFULLY */
        $data = $this->input->post();
        /* VALIDATE THE PARAMETERS */
        $this->form_validation->set_rules('record_id', 'Record Id', 'trim');
        $this->form_validation->set_rules('type', 'Type', 'required|trim|in_list[image,audio]');
        /* VALIDATE THE FILE */
        $max_size = isset($data['type']) && $data['type'] == "image" ? '1000000' : NULL;
        $allowedExt = isset($data['type']) && $data['type'] == "image" ? array('jpg', 'png', 'jpeg', 'gif') : array('mp3', 'wav', 'pcm', 'raw', 'm4a');
        $min_size = NULL;
        $fileObj = new Custom_file();
        $validationError = $fileObj->validateFile('file', $max_size, $min_size, $allowedExt);
        if ($this->form_validation->run() == FALSE || strlen($validationError)) {
            $error_message = 'Validation errors';
            $error_message['file'] = $validationError;
            $this->response(array(
                'status' => FALSE,
                'message' => $error_message
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $uploaddir = FCPATH . 'assets/uploads/temp_uploads/';
        $fileObj->mkdir_r($uploaddir);
        // remove special characters
        $file = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', strtolower(basename($_FILES['file']['name'])));
        //create a new name
        $randomNumber = time() - 3600 * 24 * 365 * 44;
        $filename = $randomNumber . rand(100, 1000) . rand(1, 100) . $file;
        $newName = $uploaddir . $filename;
        if (!is_uploaded_file($_FILES['file']['tmp_name'])) {
            $message = 'File not uploaded';
        } else if (move_uploaded_file($_FILES['file']['tmp_name'], $newName)) {
            $status = TRUE;
            $message = 'success';
            $response_data = array('filename' => $filename);
        } else {
            $message = 'Error in uploading File';
        }
        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $response_data
                ), REST_Controller::HTTP_OK);
    }

}
