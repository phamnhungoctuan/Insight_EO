<?php

require(APPPATH . 'libraries/REST_Controller.php');
require_once(APPPATH . 'libraries/Custom_file.php');

class User extends CI_Controller {

    public $model = 'user_model';

    public function __construct() {
        parent::__construct();
        $this->model = 'user_model';
        $this->load->library('form_validation');
        $this->load->model('API_V1/' . $this->model);
        $this->data = array();
    }

    /*
     * Register the User in db and infusion soft server 
     */

    public function save_lead() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            if ($this->form_validation->run() == FALSE) {
                $this->data['status'] = FALSE;
                $this->data['message'] = 'Some Values are not correct';
                $this->data['error'] = validation_errors();
            } else {
                /* check user exists */
                $user = $this->{$this->model}->get_user(array('email' => $this->input->post('email')));
                if (count($user)) {
                    $this->data['message'] = 'This email is already registered';
                    $this->data['status'] = FALSE;
                } else {
                    /* FIRST REGISTER THE USER IN INFUSIONSSOFT DB */
                    $result = $this->add_to_infusionList($this->input->post());
                    if ($result) {
                        /* Register the User in DB as well as in Infusionsoft */
                        $name_parts = explode(" ", $this->input->post('name'));
                        $user_data = array(
                            'last_name' => count($name_parts) > 1 ? array_pop($name_parts) : NULL,
                            'first_name' => implode(" ", $name_parts),
                            'email' => $this->input->post('email'),
                            'user_type' => 'web_form',
                        );
                        $return_id = $this->{$this->model}->webform_register($user_data);
                        if ($return_id) {
                            //$this->data['infusion_result'] = $this->add_to_infusionList($this->input->post());
                            /* ADD THE USER RECORD IN USER_INFUSIONSOFT TABLE */
//                            $this->load->model('user_infusionsoft_record_model');
//                            $this->user_infusionsoft_record_model->save(array(
//                                'user_id' => $return_id,
//                                'date_created' => date('Y-m-d Hi:s'),
//                                'updated_on_infusionsoft' => '1'
//                            ));
                            $this->data['status'] = TRUE;
                            $this->data['message'] = 'You are successfully registered with Energy ReMastered';
                        } else {
                            $this->data['status'] = FALSE;
                            $this->data['message'] = 'Something went wrong with Request. Please try again';
                        }
                    } else {
                        $this->data['status'] = FALSE;
                        $this->data['message'] = 'Something went wrong with Request. Please try again';
                    }
                } /* processing ends */
            }
        } else {
            $this->data['status'] = FALSE;
            $this->data['message'] = 'Something went wrong with Request. Please try again';
        }
        echo json_encode($this->data);
        die();
    }

     private function add_to_infusionList($post) {
        $url = 'https://qo332.infusionsoft.com/app/form/process/54a64ff098cd93e4b03bf93bb3eaacc1';
        $req = 'inf_form_xid=54a64ff098cd93e4b03bf93bb3eaacc1&inf_form_name=Submit Web Form'
                . '&inf_field_FirstName='.($post['name']).''
                . '&inf_field_Email='.($post['email'])
                .'&infusionsoft_version=1.57.0.58';
        return $this->make_request($url, $req);
    }

    /* function to make a curl request */

    private function make_request($url, $req) {
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_CAINFO, base_url('assets/certs/cacert.pem'));

        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            //echo curl_error($ch);
        } else {
            $status = curl_getinfo($ch);
            switch ($status['http_code']) {
                case 200:
                    return $response;
                case 404:
                    return FALSE;
                default :
                    return FALSE;
            }
        }
        //close connection
        curl_close($ch);
    }
}
