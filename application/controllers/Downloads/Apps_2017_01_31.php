<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Apps extends MY_Controller {

    public $model = "products_model";

    public function __construct() {
        parent::__construct();

        $this->model = 'apps_model';
        $this->load->model('Products/apps_model');
    }

    /**
     * @Name : index()
     * @Purpose : To show the listing of all Quantum iLife/iNfinity App users to admin.
     * @Call from : When admin click on the Quantum iLife/iNfinity App Tab. 
     * @Functionality : Simply fetch the records from database, and set the data to the view.
     * @Receiver params : No receiver parameters passed.
     * @Return params : No parameters returned
     */
    private function get_device($device = NULL) {
        $valid_devices = array("ios", "android");
        if (empty($device) || !in_array(strtolower($device), $valid_devices)) {
            if (isset($_SERVER['HTTP_USER_AGENT'])) {
                $iPod = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
                $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
                $iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
                $Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");
                $webOS = stripos($_SERVER['HTTP_USER_AGENT'], "webOS");

                // detect os version
                if ($iPod || $iPhone || $iPad) {
                    $device = "ios";
                } else if ($Android) {
                    $device = "android";
                } else
                    $device = "other";
            }
        }
        return $device;
    }

    private function get_latest_version($app_id) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://rink.hockeyapp.net/api/2/apps/" . $app_id . "/app_versions",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "x-hockeyapptoken: de513631f31a48659991e726af572936"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return FALSE;
        } else {
            $response = json_decode($response, TRUE);
            if (isset($response['status']) && $response['status'] == 'success' && isset($response['app_versions']) && count($response['app_versions'])) {
                $version = current($response['app_versions']);
                return $version['id'];
            }
        }
        return FALSE;
    }

    public function genius_insight($device = NULL) {
        $this->layout = "default";
        $this->load->theme($this->layout);
        $data = $app_detail = $this->apps_model->get_app_detail(GENIUS_APP_ID);
        $data['device'] = $this->get_device($device);
        if (strtolower($data['device']) == "ios") {
            $data['link'] = "itms-services://?action=download-manifest&url=" . urlencode("https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_ios_id']);
            $this->load->view('downloads/apps/app_ios', $data);
        } else if (strtolower($data['device']) == "android") {
            /* GET LATEST VERSION FROM RINKHOCKEY OF ANDROID  ID */
            $latest_version = $this->get_latest_version($app_detail['rinkhockey_android_id']);
            if ($latest_version) {
                $data['link'] = "https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_android_id'] . "/app_versions/" . $latest_version . "?format=apk";
            } else {
                $data['link'] = $app_detail["rinkhockey_android_link"];
            }
            $this->load->view('downloads/apps/app_android', $data);
        } else {
            $this->load->view('downloads/apps/app_other', $data);
        }
    }

    public function genius_insight_download_test($platform = NULL) {
        $this->layout = "default";
        $this->load->theme($this->layout);
        $data = $app_detail = $this->apps_model->get_app_detail(GENIUS_APP_ID);
        $data['device'] = $this->get_device();
        if (strtolower($data['device']) == "ios") {
            $link = "itms-services://?action=download-manifest&url=" . urlencode("https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_ios_id']);
        } else if (strtolower($data['device']) == "android") {
            /* GET LATEST VERSION FROM RINKHOCKEY OF ANDROID  ID */
            $latest_version = $this->get_latest_version($app_detail['rinkhockey_android_id']);
            if ($latest_version) {
                $link = "https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_android_id'] . "/app_versions/" . $latest_version . "?format=apk";
            } else {
                $link = $app_detail["rinkhockey_android_link"];
            }
        } else {
            if ($platform == "ios" || $platform == "android") {
                $latest_version = $this->get_latest_version($app_detail['rinkhockey_' . $platform . '_id']);
                if ($latest_version) {
                    $format = $platform == "ios" ? "ipa" :"apk";
                    $link = "https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_'.$platform.'_id'] . "/app_versions/" . $latest_version . "?format=".$format;
                } else {
                    $link = $app_detail['rinkhockey_'.$platform.'_id'];
                }
            }
        }
        header("Location:".$link);
    }
    public function genius_insight_download($platform = NULL)
    {
        $this->layout = "default";
        
        $this->load->theme($this->layout);
        
        $data = $app_detail = $this->apps_model->get_app_detail(GENIUS_APP_ID);
        
        $data['device'] = $this->get_device();
        
        //When Requested for iOS app
        if($platform == "ios")
        {
            if (strtolower($data['device']) == "ios")
            {
                $link = "itms-services://?action=download-manifest&url=" . urlencode("https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_ios_id']);
            }
            else
            {
                $format = $platform == "ios" ? "ipa" :"apk";
                $latest_version = $this->get_latest_version($app_detail['rinkhockey_' . $platform . '_id']);
                $link = "https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_'.$platform.'_id'] . "/app_versions/" . $latest_version . "?format=".$format;
            }
        }
        else
        {
            if (strtolower($data['device']) == "android")
            {
                /* GET LATEST VERSION FROM RINKHOCKEY OF ANDROID  ID */
                $latest_version = $this->get_latest_version($app_detail['rinkhockey_android_id']);
                
                if ($latest_version)
                {
                    $link = "https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_android_id'] . "/app_versions/" . $latest_version . "?format=apk";
                }
                else
                {
                    $link = $app_detail["rinkhockey_android_link"];
                }
            }
            else
            {
                $latest_version = $this->get_latest_version($app_detail['rinkhockey_' . $platform . '_id']);
                $format = $platform == "ios" ? "ipa" :"apk";
                $link = "https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_'.$platform.'_id'] . "/app_versions/" . $latest_version . "?format=".$format;
            }
        }
        header("Location:".$link);
    }

}

/* End of file aura.php */
/* Location: ./application/controllers/aura.php */