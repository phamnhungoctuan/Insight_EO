<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Apps extends MY_Controller {

    public $model = "products_model";

    public function __construct() {
        parent::__construct();

        $this->model = 'apps_model';
        $this->load->model('Products/apps_model');
    }

    /**
     * @Name : index()
     * @Purpose : To show the listing of all Quantum iLife/iNfinity App users to admin.
     * @Call from : When admin click on the Quantum iLife/iNfinity App Tab. 
     * @Functionality : Simply fetch the records from database, and set the data to the view.
     * @Receiver params : No receiver parameters passed.
     * @Return params : No parameters returned
     */
    private function get_device($device = NULL) {
        $valid_devices = array("ios", "android");
        if (empty($device) || !in_array(strtolower($device), $valid_devices)) {
            if (isset($_SERVER['HTTP_USER_AGENT'])) {
                $iPod = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
                $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
                $iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
                $Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");
                $webOS = stripos($_SERVER['HTTP_USER_AGENT'], "webOS");

                // detect os version
                if ($iPod || $iPhone || $iPad) {
                    $device = "ios";
                } else if ($Android) {
                    $device = "android";
                } else
                    $device = "other";
            }
        }
        return $device;
    }

    private function get_latest_version($app_id) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://rink.hockeyapp.net/api/2/apps/" . $app_id . "/app_versions",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "x-hockeyapptoken: de513631f31a48659991e726af572936"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return FALSE;
        } else {
            $response = json_decode($response, TRUE);
            if (isset($response['status']) && $response['status'] == 'success' && isset($response['app_versions']) && count($response['app_versions'])) {
                $version = current($response['app_versions']);
                return $version['id'];
            }
        }
        return FALSE;
    }

    public function get_link_new($app_id, $platform) {
        $link = "http://quantumhealthapps.com/";
        $data = $app_detail = $this->apps_model->get_app_detail($app_id);

//        $data['device'] = $this->get_device();
        //When Requested for iOS app
        if ($platform == "ios") {
            $latest_version = $this->get_latest_version($app_detail['rinkhockey_ios_id']);
            if ($latest_version) {
                $link = "https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_ios_id'] . "/app_versions/" . $latest_version . "?format=ipa";
            } else {
                $link = $app_detail["rinkhockey_ios_link"];
            }
        } else {
            /* GET THE LATEST VERSION OF ANDROID APP ON RINK HOCKEY */
            $latest_version = $this->get_latest_version($app_detail['rinkhockey_android_id']);
            if ($latest_version) {
                $link = "https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_android_id'] . "/app_versions/" . $latest_version . "?format=apk";
            } else {
                $link = $app_detail["rinkhockey_android_link"];
            }
        }
        return $link;
    }

    public function get_link($app_id, $platform) {
        $link = "http://quantumhealthapps.com/";
        $data = $app_detail = $this->apps_model->get_app_detail($app_id);

        $data['device'] = $this->get_device();

        //When Requested for iOS app
        if ($platform == "ios") {
            $latest_version = $this->get_latest_version($app_detail['rinkhockey_ios_id']);
            if ($latest_version) {
                if (strtolower($data['device']) == "ios") {
                    $link = "itms-services://?action=download-manifest&url=" . urlencode("https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_ios_id'] . '/app_versions/' . $latest_version);
                } else {
                    $link = "https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_ios_id'] . "/app_versions/" . $latest_version . "?format=ipa";
                }
            } else {
                $link = $app_detail["rinkhockey_ios_link"];
            }
        } else {
            /* GET LATEST VERSION FROM RINKHOCKEY OF ANDROID  ID */
            $latest_version = $this->get_latest_version($app_detail['rinkhockey_android_id']);
            if ($latest_version) {
                $link = "https://rink.hockeyapp.net/api/2/apps/" . $app_detail['rinkhockey_android_id'] . "/app_versions/" . $latest_version . "?format=apk";
            } else {
                $link = $app_detail["rinkhockey_android_link"];
            }
        }
        $link = empty($link) ? "http://quantumhealthapps.com/" : $link;
        return $link;
    }

    /*
      platform => ios, android
     *      */

    public function genius_insight_download($platform = NULL) {
        $link = $this->get_link(GENIUS_APP_ID, $platform);
        header("Location:" . $link);
    }

    public function quantum_ilife_infinity_download($platform = NULL) {
        $link = $this->get_link(QUANTUM_ILIFE_INFINITY_APP_ID, $platform);
        header("Location:" . $link);
    }

    public function genius_royal_rife_download($platform = NULL) {
        $link = $this->get_link(INSIGHT_ROYAL_RIFE_APP_ID, $platform);
        header("Location:" . $link);
    }

    public function mind_nrg_download($platform = NULL) {
        $link = $this->get_link(MIND_NRG_APP_ID, $platform);
        header("Location:" . $link);
    }

    public function relationship_harmonizer_download($platform = NULL) {
        $link = $this->get_link(RELATIONSHIP_HARMONIZER_APP_ID, $platform);
        header("Location:" . $link);
    }

    public function quanta_capsule_download($platform = NULL) {
        $link = $this->get_link(QUANTA_CAPSULE_APP_ID, $platform);
        header("Location:" . $link);
    }

    public function water_harmonizer_download($platform = NULL) {
        $link = $this->get_link(WATER_HARMONIZER_APP_ID, $platform);
        header("Location:" . $link);
    }

    public function emotional_insight_download($platform = NULL) {
        $link = $this->get_link(EMOTIONAL_INSIGHT_APP_ID, $platform);
        header("Location:" . $link);
    }

    public function holo_photonic_light_download($platform = NULL) {
        $link = $this->get_link(HOLO_PHOTONIC_LIGHT_APP_ID, $platform);
        header("Location:" . $link);
    }
    
    public function insight_water_harmonizer_download($platform = NULL) {
        $link = $this->get_link(INSIGHT_WATER_HARMONIZER_APP_ID, $platform);
        header("Location:" . $link);
    }
    
    public function insight_quanta_cap_download($platform = NULL) {
        $link = $this->get_link(INSIGHT_QUANTA_CAP_APP_ID, $platform);
        header("Location:" . $link);
    }

}

/* End of file aura.php */
/* Location: ./application/controllers/aura.php */