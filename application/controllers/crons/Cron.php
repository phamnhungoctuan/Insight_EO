<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron extends CI_Controller {

    var $data = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('api_manager_V1');
        $this->load->model('API_V6/Payment_model', 'payment_model');
    }

    public function checkReceipt() {
        $paymentdetails = $this->payment_model->getCronData();
//        print_r($paymentdetails);exit;
//        print_r($this->db->last_query());exit;
        if (count($paymentdetails) > 0) {
            foreach ($paymentdetails as $paymentdetail) {
//            print_r($paymentdetail);exit;
                if (!empty($paymentdetail['platform']) && $paymentdetail['platform'] == 'ios') {
                    if ($paymentdetail['app_mod'] == 'test') {
                        $url = "https://sandbox.itunes.apple.com/verifyReceipt";
                    } else {
                        $url = "https://buy.itunes.apple.com/verifyReceipt";
                    }
                    $ch = curl_init($url);
                    $data_string = json_encode(array(
                        'receipt-data' => $paymentdetail['new_receipt'],
                        'password' => IOS_SHARED_KEY,
                        'exclude-old-transactions' => 1
                    ));
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                    );
                    $output = curl_exec($ch);
                    curl_close($ch);
                    $decoded = json_decode($output, TRUE);
                    if (isset($decoded['pending_renewal_info'][0]['expiration_intent']) && $decoded['pending_renewal_info'][0]['expiration_intent'] == '1') {
                        $update['expiry_date'] = $paymentdetail['payment_date'];
                    } else {
                        $update['payment_date'] = date('Y-m-d H:i:s', ($decoded['latest_receipt_info'][0]['purchase_date_ms']) / 1000);
                        $update['expiry_date'] = date('Y-m-d H:i:s', ($decoded['latest_receipt_info'][0]['expires_date_ms']) / 1000);
                    }


                    // FOR REFUND
//                if($decoded['latest_receipt_info'][1]['cancellation_date']){
//                    $update['expiry_date'] = $paymentdetail['payment_date'];
//                    $mail_params['ContentType'] = "text/html";
//                    $mail_params['To'] = 'akshay.intersoft@gmail.com';
//                    $mail_params['Subject'] = "Subscription Expired or for Refund";
//                    $mail_params['Body'] = json_encode($decoded).'<br> User Detail'.  json_encode($paymentdetail);
//                    $this->api_manager_v1->sendMail($mail_params) ;
//                }
                } else {
                    if (!empty($paymentdetail['platform']) && $paymentdetail['platform'] == 'android') {
                        $decoded = verifyAndroidReceipt($paymentdetail['new_receipt']);
                        if (date('Y-m-d', ($decoded['expiryTimeMillis']) / 1000) < date('Y-m-d')) {
                            $update['payment_date'] = date('Y-m-d H:i:s', ($decoded['startTimeMillis']) / 1000);
                            $update['expiry_date'] = date('Y-m-d H:i:s', ($decoded['expiryTimeMillis']) / 1000);
                        }
                    }
                }
                if ($paymentdetail['last_verification_date'] == date("Y-m-d", strtotime("-" . DAY_INTERVAL)) && $paymentdetail['no_of_attempts'] < MAX_ATTEMPTS) {
                    $update['no_of_attempts'] = $paymentdetail['no_of_attempts'] + 1;
                } elseif (date('Y-m-d', strtotime($paymentdetail['expiry_date'])) == date('Y-m-d')) {
                    $update['no_of_attempts'] = 1;
                }
                $update['last_verification_date'] = date('Y-m-d');
                if (!empty($update)) {
                    $this->payment_model->updateSubscription($update, ['id' => $paymentdetail['id']]);
                }
            }
        }
    }

    public function testVerify($receipt = NULL) {
        print_r(verifyAndroidReceipt($receipt));
    }

}
