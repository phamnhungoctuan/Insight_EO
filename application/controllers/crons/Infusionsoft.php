<?php

require(APPPATH . 'libraries/REST_Controller.php');
require_once (APPPATH . 'libraries/Custom_file.php');

class Infusionsoft extends CI_Controller {

    public $model = 'user_model';

    public function __construct() {
        parent::__construct();
        $this->model = 'genius_insight_model';
        $this->load->library('form_validation');
        $this->load->model('Admin/Genius_insight_model', 'genius_insight_model');
        $this->data = array();
    }

    public function test() {
        echo file_get_contents(FCPATH . 'application/logs/log-2016-11-28.php');
        die();
    }

    /* Update the users on infusionsoft DB alongwith their database */
    /* TAGS ID FOR TEST SERVER :
     * IOS -> 296
     * ANDROID-> 298
     * 
     * TAGS FOR LIVE SERVER
     * IOS -> 156
     * ANDROID -> 154
     *  
     */

    Public function update_genius_users_device() {
        /* FETCH THE DEVICES (from the use_registered device table THAT ARE NOT PROCESSED i.e. is_updated_on_infusiondb = "0") 
         * alongwith the users info 
         */
        $data['total_updated'] = $data['total_records'] = 0;
        ini_set('max_execution_time', 0);
        $this->load->model('user_infusionsoft_record_model');
        $infusion_records = $this->user_infusionsoft_record_model->get_records_to_update_infusionsoft();
        if ($infusion_records) {
            $data['total_records'] = count($infusion_records);
            While ($records = array_splice($infusion_records, 0, 50)) {
                $user_ids_array = array_column($records, 'user_id');
                $update_data = array();
                foreach ($records as $row) {
                    $tag_id = array();
                    /* ADD OR UPDATE THE USER ON INFUSIONSOFT */

                    /* COMMENT BELOW SECTION FOR TEST SERVER */
//                if (!empty($row['platform']) && $row['platform'] != $row['infusionsoft_device_status']) {
//                    if ($row['platform'] == 'both') {
//                        $tag_id = array('296', '298');
//                    } else if ($row['platform'] == 'android') {
//                        $tag_id = array('298');
//                    } else if ($row['platform'] == 'ios') {
//                        $tag_id = array('296');
//                    }
//                }
                    /* UNCOMMENT BELOW FOR LIVE SERVER */
                    if (!empty($row['platform']) && $row['platform'] != $row['infusionsoft_device_status']) {
                        if ($row['platform'] == 'both') {
                            $tag_id = array('156', '154');
                        } else if ($row['platform'] == 'android') {
                            $tag_id = array('154');
                        } else if ($row['platform'] == 'ios') {
                            $tag_id = array('156');
                        }
                    }
                    if (count($tag_id) || $row['updated_on_infusionsoft'] == '0') {
                        $this->load->helper('infusionsoft');
                        $infusion_data = array(
                            'first_name' => $row['first_name'],
                            'last_name' => $row['last_name'],
                            'email' => $row['email'],
                        );
                        if (add_to_infusion($infusion_data, $tag_id)) {
                            $update_data[] = array(
                                'updated_on_infusionsoft' => '1',
                                'infusionsoft_device_status' => $row['platform'],
                                'user_id' => $row['user_id'],
                                'date_updated' => date('Y-m-d H:i:s'),
                            );
                        }
                    }
                } /* END OF FOREACH */
                /* UPDATE THE DATA */
                if (count($update_data)) {
                    $data['total_updated'] = $data['total_updated'] + count($update_data);
                    $this->user_infusionsoft_record_model->update_batch($update_data, "user_id");
                }
            }
        }
        $this->load->view("crons/update_genius_users_device", $data);
    }
    

}
