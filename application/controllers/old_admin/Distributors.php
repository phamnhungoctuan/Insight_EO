<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Distributors extends MY_Controller {
    
   public $model="distributors_model";
    
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/aura
     *	- or -  
     * 		http://example.com/index.php/aura/index
     *	- or -
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/aura/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct(){
        parent::__construct();
        $this->model='distributors_model';
        $this->load->library('form_validation');
    }
    
    /**
    * @Name : index()
    * @Purpose : To show the listing of all distributors to admin.
    * @Call from : 
    * @Functionality : Simply fetch the records from database, and set the data to the view.
    * @Receiver params : No receiver parameters passed.
    * @Return params : No parameters returned 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified :
    */
    
    public function index() {
        $this->load->model($this->model);
        $this->load->library('encrypt');
        $data['pageTitle']="Distributors";
        $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results']=$this->{$this->model}->get_distributors();
        $data['total_rows'] = $this->{$this->model}->count_results();
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $start_rec, $data['total_rows'], base_url('distributors/index'));
        $this->load->view('distributors/index', $data);
    }
   
    
    /**
    * @Name : user_add()
    * @Purpose : To show the add user_add form to admin.
    * @Call from : When admin clicks on add new user button.
    * @Functionality : renders the user_add view to the admin.
    * @Receiver params : No parameter passed.
    * @Return params : No parameter returned. 
    * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified :
    */
    public function user_add() {
        $data = array();
        $data['pageTitle'] = 'Add App User';
        $data['subPageTitle'] = '';
        $data['Key']=$this->generate_random_string();
        $this->load->view('distributors/user_add',$data);
    }
    
    /**
    * @Name : save_new_user()
    * @Purpose : To save the user detail in database when add new user form is submitted.
    * @Call from : Add new user form action calls this function.
    * @Functionality : save the new user record in the database
    * @Receiver params : Only post data is accepted to save in distributors table.
    * @Return params : Redirects to the listing page if user saved successfully. 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : 
    */
    function save_new_user() {
        if ($this->input->post()) {
            $this->load->model($this->model);
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_add();
            } 
            else 
            {
                $data = $this->input->post();
                $return = $this->{$this->model}->save($data);
            }
        }
        redirect($this->router->class.'/index');
    }
   
    /**
    * @Name : user_edit()
    * @Purpose : To edit distributor by admin.
    * @Call from : When admin clicks on 'edit' link.
    * @Functionality : Renders the user_edit view to edit user.
    * @Receiver params : User Id of the user to edit is passed.
    * @Return params : No parameters returned 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : 
    */
    function user_edit($userId) {
        $data['pageTitle'] = 'Edit Distributor';
        $data['subPageTitle'] = '';
        $this->load->model($this->model);
        $user_data = $this->{$this->model}->get_distributor_detail(array('id'=>$userId));
        $data['user_data'] = $user_data;
        $this->load->view('distributors/user_edit', $data);
    }
    /**
    * @Name : edit_user()
    * @Purpose : To edit the user details by admin.
    * @Call from : When admin clicks on update link on the edit user form.
    * @Functionality : Updates the user details.
    * @Receiver params :  Only post data is accepted to update in new_payments table.
    * @Return params : No parameters returned.
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : 
    */
    function edit_user() {
        if ($this->input->post()) {
            $this->load->model($this->model);
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_edit($this->input->post('id'));
            } 
            else 
            {
                $data= $this->input->post();
                $where['id']=$data['id'];
                $this->{$this->model}->update($data,$where);
            }
        }
        redirect($this->router->class.'/index');
    }

     /**
    * @Name : delete_user()
    * @Purpose : To delete the user by admin.
    * @Call from : When admin clicks on delete link from distributors listing page and ajax called to delete the distributor using this function.
    * @Functionality : permanently delete user from database  
    * @Receiver params : User_Id is passed with post data.
    * @Return params : No parameters returned 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : 
    */
    function delete_user() {
        $this->load->library('encrypt');
        $user_id = $this->input->post('user_id');
        $user_id = $this->encrypt->decode($user_id);
        $where= array('id'=>$user_id);
        $this->load->model($this->model);
        echo $return = $this->{$this->model}->delete($where);
    }

}

/* End of file distributors.php */
/* Location: ./application/controllers/distributors.php */