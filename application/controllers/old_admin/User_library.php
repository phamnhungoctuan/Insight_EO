<?php

require(APPPATH . 'libraries/REST_Controller.php');
require(APPPATH . 'libraries/Custom_file.php');

class User_library extends REST_Controller {

    public $model = 'user_library_model';

    public function __construct() {
        parent::__construct();
        $this->model = 'user_library_model';
        $this->load->library('api_manager');
        $this->load->library('form_validation');
        $this->load->model($this->model);
        $this->load->helper('date');
        $this->data = array();
    }

    /**
     * @Name : add()
     * @Purpose : add a library 
     * @Call from : when user click on the add library button from the App
     * @Functionality : Add a library in the  user_record_library
     * @Receiver params : array of columns and values
     * @Return params : on successful insertion of library return True, False otherwise
     */
    function add_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* USER AUTHENTICATED SUCCESSFULLY */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('image', 'image', 'trim');
        $this->form_validation->set_rules('record_id', 'Record ID', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');
        $this->form_validation->set_rules('uuid', 'Device ID', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* VALIDATE THE RECORD AND LIBRARY */
        $result = $this->{$this->model}->get_library(array('user_id' => $user_details['user_id'], 'library_name' => $data['name']));
        if ($result) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Library name already exist'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }


//        else if(($result['name'])){
//            $this->response(array(
//                'status' => FALSE,
//                'message' => 'Library Already added with this name'
//                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
//            die();
//        }
        $data['user_id'] = $user_details['user_id'];
        $data['uuid'] = $user_details['uuid'];
        /* PROCESS THE REQUEST */
        $response_data['library_id'] = $this->{$this->model}->add_library($data);
        /* SAVE THE IMAGE */
        if ($response_data['library_id'] && strlen($data['image'])) {
            $fileObj = new Custom_file();
            $image_result = isset($data['image']) && strlen($data['image']) ? $fileObj->move_file($data['image'], $response_data['library_id'], 'assets/uploads/libraries/') : array();
            if (isset($image_result['filename'])) {
                $this->{$this->model}->update_library(array('image' => $image_result['filename']), $response_data['library_id']);
                $response_data['image'] = $image_result;
            }
        }
        $message = $response_data['library_id'] > 0 ? 'Library added successfully' : 'Error inserting the record';
        $status = $response_data['library_id'] > 0 ? TRUE : FALSE;
        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $this->api_manager->format_output($response_data)
                ), REST_Controller::HTTP_OK);
    }

    /**
     * @Name : update()
     * @Purpose : add a library 
     * @Call from : when user click on the add library button from the App
     * @Functionality : updates a library in the  user_record_library with given library_id
     * @Receiver params : array of columns and values, library_id
     * @Return params : on successful updation of library return True, False otherwise
     */
    function update_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* USER AUTHENTICATED SUCCESSFULLY */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('name', 'Name', 'trim');
        $this->form_validation->set_rules('image', 'image', 'trim');
        $this->form_validation->set_rules('record_id', 'Record ID', 'trim');
        $this->form_validation->set_rules('library_id', 'Library ID', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        // Check if library is shared or custom
        $check = $this->{$this->model}->get_library_by_id(array('library_id' => $data['library_id'], 'user_id' => $user_details['user_id']));
        if (!$check) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'You can not update this library'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }


        /* VALIDATE THE RECORD */
        $result = $this->{$this->model}->get_library(array('user_id' => $user_details['user_id'], 'library_name' => $data['name'], 'ul.library_id !=' => $data['library_id']));
//        $result = $this->{$this->model}->get_library(array('library_id' => $data['library_id'], 'user_id' => $user_details['user_id']));

        if ($result) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Library name already exist'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }

        /* PROCESS THE REQUEST */

        // $update_data['record_id'] = $data['record_id'] ? $data['record_id'] : "";
        $update_data['description'] = $data['description'] ? $data['description'] : "";
        $update_data['name'] = $data['name'] ? $data['name'] : "";
        //  $update_data['user_id'] = $user_details['user_id'];
        $update_data['date_updated'] = date('Y-m-d H: :s');

        /* SAVE THE IMAGE */
        if ($data['library_id'] && strlen($data['image'])) {
            $fileObj = new Custom_file();
            $image_result = isset($data['image']) && strlen($data['image']) ? $fileObj->move_file($data['image'], $data['library_id'], 'assets/uploads/libraries/') : array();
            if (isset($image_result['filename'])) {
                $update_data['image'] = $image_result['filename'];
            }
        }

        $result = $this->{$this->model}->update_library($update_data, $data['library_id']);


        $message = $result ? 'Library updated successfully' : 'Error updating the record';
        $status = $result ? TRUE : FALSE;
        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $this->api_manager->format_output($response_data)
                ), REST_Controller::HTTP_OK);
    }

    /**
     * @Name : list()
     * @Purpose : get the list of the libraries of the given logged in user
     * @Call from : from the libraries listing page
     * @Functionality : Fetch the name , image , library_id from the user_record_libraries table
     * @Receiver params : (Headers only)
     * @Return params : return the resultset
     */
    public function list_post() {
        $this->api_manager->handle_request();
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $records = $this->{$this->model}->get_libraries(array('user_id' => $user_details['user_id'], 'ul.is_approved' => '1'));
        if ($records) {
            foreach ($records as $key => $record) {
                if ($record['is_imported'] == '0' && $record['is_shared'] == '1') {
                    unset($records[$key]);
                    continue;
                }
                if ($record['is_imported'] == '1' && $record['is_shared'] == '1' && $record['uuid'] != $user_details['uuid']) {
                    unset($records[$key]);
                    continue;
                }
                if ($record['is_shared'] == '0') {
                    $records[$key]['library_owner'] = $user_details['first_name'] . ' ' . $user_details['last_name'];
                } else {
                    $owner = $this->{$this->model}->get_library_custom(array('library_id' => (int) $record['library_id'], 'is_shared' => '0'));
                    if ($owner) {
                        $owner_id = $owner['user_id'];
                        $response_data = $this->{$this->model}->get_user_name_and_library_name(array('library_id' => $record['library_id'], 'user_id' => $owner_id));
                        $records[$key]['library_owner'] = $response_data['user_name'];
                    } else {
                        $records[$key]['library_owner'] = $user_details['first_name'] . ' ' . $user_details['last_name'];
                    }
                }
                $records[$key]['image'] = strlen($record['image']) ? base_url('assets/uploads/libraries/' . $record['image']) : "";
                unset($records[$key]['date_created']);
                unset($records[$key]['date_updated']);
                unset($records[$key]['record_id']);
                unset($records[$key]['user_id']);
                unset($records[$key]['id']);
                unset($records[$key]['description']);
            }
            $this->response(array(
                'status' => TRUE,
                'message' => 'success',
                'data' => $this->api_manager->format_output(array_values($records))
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'NO Records Found',
                    ), REST_Controller::HTTP_OK);
        }
    }

    /**
     * @Name : detail()
     * @Purpose : return the detail of the given library Id
     * @Call from : the App, when user click on any library on the listing page
     * @Functionality : fetch the detail of the given library 
     * @Receiver params : library_id
     * @Return params : return the Detail of the library , False otherwise
     */
    public function detail_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('library_id', 'library_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $result = $this->{$this->model}->get_library_detail(array('library_id' => $data['library_id'], 'user_id' => $user_details['user_id']));

        if ($result) {
            $result['image'] = strlen($result['image']) ? base_url('assets/uploads/libraries/' . $result['image']) : '';
            $result['item_count'] = $this->{$this->model}->count_items($data['library_id']);
            unset($result ['date_updated']);
            unset($result ['date_created']);
            unset($result ['user_id']);
            unset($result ['record_id']);
            unset($result ['id']);
            unset($result ['is_imported']);
            unset($result ['uuid']);
            unset($result ['share_library_code']);
            unset($result ['id']);
            $this->response(array(
                'status' => TRUE,
                'message' => 'success',
                'data' => $this->api_manager->format_output($result)
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'NO Records Found',
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function check_item_exits_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('library_id', 'Library Id', 'trim|required');
        $this->form_validation->set_rules('name', 'Item Name', 'trim|required');
        $result = $this->{$this->model}->select_record(array('library_id' => $data['library_id'], 'name' => $data['name']), 'library_items');

        if ($result) {
            $this->response(array(
                'status' => true,
                'message' => 'Item Name  Exists'
                    ), REST_Controller::HTTP_OK);
            die();
        } else {
            $this->response(array(
                'status' => false,
                'message' => 'Item Name Do Not Exists'
                    ), REST_Controller::HTTP_OK);
            die();
        }
    }

    public function add_item_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('library_id', 'Library Id', 'trim|required');
        $this->form_validation->set_rules('name', 'Item Name', 'trim|required');
        $this->form_validation->set_rules('frequency', 'Frequency', 'trim');
        $result = $this->{$this->model}->select_record(array('library_id' => $data['library_id'], 'name' => $data['name']), 'library_items');

        if ($result) {
            $requests_keys = array('library_id', 'name', 'frequency');
            foreach ($requests_keys as $key) {
                if (isset($data[$key])) {
                    if ($key == "frequency") {
                        $item_record[$key] = json_encode($data[$key]);
                    }
                }
            }
            $item_record['date_updated'] = date('Y-m-d H:i:s');
            $this->load->model('library_items_model');
            $item_id = $this->library_items_model->update($item_record, array('id' => $result['id']));
            $message = $item_id > 0 ? 'You have Updated the Item ' . $result['name'] . '.' : 'Error Updating the item';
            $status = $item_id > 0 ? TRUE : FALSE;
            $this->response(array(
                'status' => $status,
                'message' => $message,
                    ), REST_Controller::HTTP_OK);
        } else {
            $requests_keys = array('library_id', 'name', 'frequency');
            foreach ($requests_keys as $key) {
                if (isset($data[$key])) {
                    if ($key == "frequency") {
                        $item_record[$key] = json_encode($data[$key]);
                    } else {
                        $item_record[$key] = $data[$key];
                    }
                }
            }
            $item_record['date_created'] = date('Y-m-d H:i:s');
            $this->load->model('library_items_model');
            $item_id = $this->library_items_model->save($item_record);
            $message = $item_id > 0 ? 'You have added a new item.' : 'Error inserting the item';
            $status = $item_id > 0 ? TRUE : FALSE;
            $this->response(array(
                'status' => $status,
                'message' => $message,
                'data' => $this->api_manager->format_output(array('item_id' => $item_id))
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function update_item_post() {
        $status = FALSE;
        $message = '';
        $response_data = array();
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('library_id', 'Library Id', 'trim|required');
        $this->form_validation->set_rules('item_id', 'Item Id', 'trim|required');
        $this->form_validation->set_rules('name', 'Item Name', 'trim|required');
        $this->form_validation->set_rules('frequency', 'Frequency', 'trim');

        /* Authenticate the Library - Whether the library belongs to the current logged in user */
        $library = $this->{$this->model}->get_library_detail(array('library_id' => $data['library_id'], 'user_id' => $user_details['user_id']));
        if (!$library) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Record does not exist'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }
        /* Authenticate the Library - Whether the library belongs to the current logged in user */
        $result = $this->{$this->model}->select_record(array('library_id' => $data['library_id'], 'id !=' => $data['item_id'], 'name' => $data['name']), 'library_items');
        if ($result) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Item name is already taken'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        } else {
            $requests_keys = array('library_id', 'name', 'frequency');
            foreach ($requests_keys as $key) {
                if (isset($data[$key])) {
                    if ($key == "frequency") {
                        $item_record[$key] = json_encode($data[$key]);
                    } else {
                        $item_record[$key] = $data[$key];
                    }
                }
            }
            $item_record['date_updated'] = date('Y-m-d H:i:s');
            $this->load->model('library_items_model');
            $item_id = $this->library_items_model->update($item_record, array('id' => $data['item_id']));
            $message = $item_id > 0 ? 'You have successfully updated an item.' : 'Error updating the item';
            $status = $item_id > 0 ? TRUE : FALSE;
            $this->response(array(
                'status' => $status,
                'message' => $message,
                'data' => $this->api_manager->format_output(array('item_id' => $item_id))
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function list_items_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('library_id', 'library_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* Authenticate the Library - Whether the library belongs to the current logged in user */
        $library = $this->{$this->model}->get_library_detail(array('library_id' => $data['library_id'], 'user_id' => $user_details['user_id']));
        if (!$library) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Library not found'
                    ), REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code 
            die();
        }
        $this->load->model('library_items_model');
        $records = $this->library_items_model->fetch_records(array('library_id' => $data['library_id']));
        if ($records) {
            foreach ($records as $key => $record) {

                $records[$key]['frequency'] = json_decode($records[$key]['frequency']);
                unset($records[$key]['date_created']);
                unset($records[$key]['date_updated']);
                unset($records[$key]['library_id']);
                // unset($records[$key]['frequency']);
            }
            $this->response(array(
                'status' => TRUE,
                'message' => 'success',
                'data' => $this->api_manager->format_output($records)
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Before You Begin. It appears your selected category is empty, would you like to add some individual items now?',
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function item_detail_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);
        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }

        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('item_id', 'item_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* Process the request */
        $this->load->model('library_items_model');
        $result = $this->library_items_model->get_item_detail(array('item_id' => $data['item_id'], 'user_id' => $user_details['user_id']));
        if ($result) {
            $result['frequency'] = json_decode($result['frequency'], true);
            unset($result ['date_created']);
            unset($result ['date_updated']);
            $this->response(array(
                'status' => TRUE,
                'message' => 'success',
                'data' => $this->api_manager->format_output($result)
                    ), REST_Controller::HTTP_OK);
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'NO Records Found',
                    ), REST_Controller::HTTP_OK);
        }
    }

    public function share_library_post() {
        $status = FALSE;
        $message = 'Already shared';
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if (!is_array($data) || count($data) < 1)
            $data = array();
        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        if ($this->form_validation->run() == FALSE || !(isset($data['library_id']) && is_array($data['library_id']) && count($data['library_id']) > 0)) {
            $error = isset($data['library_id']) && is_array($data['library_id']) && count($data['library_id']) > 0 ? validation_errors() : validation_errors() . ' Libraries required ';
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                'error' => explode("\n", $error),
                    ), REST_Controller::HTTP_OK);
            die();
        }
        /* Process the request */
        $this->load->model('user_model');
        $user = $this->user_model->get_user(array("email" => $data['email'], "email !=" => $user_details['email']));
        if (!$user) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'User not found'
                    ), REST_Controller::HTTP_OK);
            die();
        }
        if (is_array($data['library_id']) && count($data['library_id']) > 1) {
            $bulk_share_code = $this->{$this->model}->generate_random_string();
        } else {
            $bulk_share_code = false;
        }
        $library_response_arr = array();
        $count = 0;
        foreach ($data['library_id'] as $library_id) {
            $check_already_shared = $this->{$this->model}->check_library_shared($user['id'], $library_id);
            if ($check_already_shared) {
                $library_response_arr[] = array('library_id' => $library_id, 'message' => 'Already shared');
                if ($check_already_shared['is_imported'] == '0') {
                    $email_data[] = array('code' => $check_already_shared['share_library_code'], 'name' => $check_already_shared['name']);
                }
                continue;
            } else {
                $insert_data = array();
                $insert_data['share_library_code'] = $this->{$this->model}->generate_random_string();
                $insert_data['user_id'] = $user['id'];
                $insert_data['library_id'] = $library_id;
                if ($bulk_share_code) {
                    $insert_data['bulk_share_code'] = $bulk_share_code;
                }
                $insert_result = $this->{$this->model}->share_library($user_details['user_id'], $insert_data);
                if ($insert_result) {
                    $library_response_arr[] = array('library_id' => $library_id, 'message' => 'Imported Successfully');
                    $count++;
                    $email_data[] = array('code' => $insert_data['share_library_code'], 'name' => $insert_result['name']);
                } else {
                    $library_response_arr[] = array('library_id' => $library_id, 'message' => 'Library not found');
                }
            }
        }
        if (isset($email_data) && count($email_data)) {
            /* EMAIL Library Code TO THE USER */
            $mail_params['ContentType'] = "text/html";
            $mail_params['To'] = $user['email'];
            $mail_params['Subject'] = 'Quantum Genius Shared Library Code';
            $mail_params['Body'] = $this->load->view('email_format/shared_library_code_email', array('shared_with' => $user, 'shared_by' => $user_details, 'email_data' => $email_data, 'bulk_share_code' => $bulk_share_code), true);

            if ($this->{$this->model}->sendMail($mail_params)) {
                $status = TRUE;
                $message = count($data['library_id']) > 1 ? 'Libraries' : 'Libraries';
                $message .= strlen($message) ? ' Shared. Please check your email inbox' : '';
            } else {
                $status = TRUE;
                $message = 'Email sending Failed';
            }
        }

        $this->response(array(
            'status' => $status,
            'message' => $message,
            'data' => $this->api_manager->format_output(array('total_libraries' => count($data['library_id']), 'total_shared' => $count, 'result' => $library_response_arr))
                ), REST_Controller::HTTP_OK);
        die();
    }

    public function accept_share_request_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }

        /* Validate the parameters */
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('code', 'Library Code', 'trim|required');
        $this->form_validation->set_rules('is_collective_import', 'Is Collective Import', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }

        // process the request
        if ($data['is_collective_import'] == 1) {
            $libraries = $this->{$this->model}->get_libraries_custom(array('bulk_share_code' => $data['code']));
            if (!$libraries) {
                $this->response(array(
                    'status' => FALSE,
                    'message' => 'No Library found'
                        ), REST_Controller::HTTP_OK);
                die();
            }
            $response = array();
            $request_data = array();
            foreach ($libraries as $library) {
                if ($library['is_imported'] == '1') {
                    $request_data[] = $library;
                } else {
                    $this->{$this->model}->update_user_libraries(array('uuid' => $user_details['uuid'], 'is_imported' => '1'), $library['id']);
                }
            }
            if (!empty($request_data)) {
                self::request_libraries($request_data, $user_details);
                $this->response(array(
                    'status' => true,
                    'message' => 'Some Libraries are already shared in another device.Library share request is sent to owner'
                        ), REST_Controller::HTTP_OK);
                die();
            } else {
                $this->response(array(
                    'status' => true,
                    'message' => 'Libraries imported'
                        ), REST_Controller::HTTP_OK);
                die();
            }
        } else {
            $library = $this->{$this->model}->get_library_custom(array('share_library_code' => $data['code']));
            if ($library) {
                if ($library['is_imported'] == '1') {

                    self::request_library(array('code' => $data['code'], 'user_details' => $user_details));
                } else {
                    if ($this->{$this->model}->update_user_libraries(array('uuid' => $user_details['uuid'], 'is_imported' => '1'), $library['id'])) {
                        $this->response(array(
                            'status' => TRUE,
                            'message' => 'Library Imported'
                                ), REST_Controller::HTTP_OK);
                        die();
                    } else {
                        $this->response(array(
                            'status' => FALSE,
                            'message' => 'library cant be accepted'
                                ), REST_Controller::HTTP_OK);
                        die();
                    }
                }
            } else {
                $this->response(array(
                    'status' => FALSE,
                    'message' => 'Your Library Code is wrong'
                        ), REST_Controller::HTTP_OK);
                die();
            }
        }
    }

    private function request_library($data) {
//        $this->api_manager->handle_request();
//        $data = json_decode(file_get_contents("php://input"), true);
//
//        /* AUTHENTICATE THE USER */
//        if (!($user_details = $this->api_manager->verifyAccessToken())) {
//            $this->response(array(
//                'status' => FALSE,
//                'message' => 'Authorization Failed'
//                    ), REST_Controller::HTTP_OK);
//            die();
//        }
//
//        /* Validate the parameters */
//        $this->form_validation->set_data($data);
//        $this->form_validation->set_rules('code', 'Library Code', 'trim|required');
//        if ($this->form_validation->run() == FALSE) {
//            $this->response(array(
//                'status' => FALSE,
//                'message' => 'Validation errors',
//                    ), REST_Controller::HTTP_OK);
//            die();
//        }

        $check = $this->{$this->model}->get_library_custom(array('share_library_code' => $data['code'], 'is_shared' => '1', 'uuid' => $data['user_details']['uuid']));
        if ($check) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Library already shared with you',
                'data' => $this->api_manager->format_output(array('status' => 0))
                    ), REST_Controller::HTTP_OK);
            die();
        }

        // process the request

        $library = $this->{$this->model}->get_library_custom(array('share_library_code' => $data['code']));
        if ($library) {

            $library_owner = $this->{$this->model}->get_library_custom(array('library_id' => $library['library_id'], 'is_shared' => '0'));
            if (!$library_owner) {
                $this->response(array(
                    'status' => false,
                    'message' => 'Library share request can not be sent to owner'
                        ), REST_Controller::HTTP_OK);
                die();
            } else {
                $this->load->model('user_model');
                $owner_data = $this->user_model->get_user(array('id' => (int) $library_owner['user_id']));
            }
            if ($library['is_imported'] == '1') {
                $insert_data = array();
                $insert_data['share_library_code'] = $library['share_library_code'];
                $insert_data['user_id'] = $data['user_details']['user_id'];
                $insert_data['library_id'] = $library['library_id'];
                $insert_data['is_shared'] = '1';
                $insert_data['bulk_share_code'] = $library['bulk_share_code'];
                $insert_data['is_imported'] = '1';
                $insert_data['uuid'] = $data['user_details']['uuid'];
                $insert_data['is_approved'] = '0';
                $insert_data['token'] = $this->{$this->model}->generate_random_string();
                $library_id = $this->{$this->model}->insert_user_libraries($insert_data);
                if ($library_id) {
                    $names = $this->{$this->model}->get_user_name_and_library_name(array('user_id' => $data['user_details']['user_id'], 'library_id' => $library['library_id']));
                    /* EMAIL Library Code TO THE USER */
                    $mail_params['ContentType'] = "text/html";
                    $mail_params['To'] = $owner_data['email'];
                    $mail_params['Subject'] = 'Quantum Genius Request For Library';
                    $mail_params['Body'] = $this->load->view('email_format/request_for_library_email', array('user_name' => $names['user_name'], 'id' => $library_id, 'library_name' => $names['library_name'], 'token' => $insert_data['token']), true);

                    if ($this->{$this->model}->sendMail($mail_params)) {
                        $this->response(array(
                            'status' => true,
                            'message' => 'Library is already shared in another device.Library share request is sent to owner',
                            'data' => $this->api_manager->format_output(array('status' => 1))
                                ), REST_Controller::HTTP_OK);
                        die();
                    }
                } else {
                    $this->response(array(
                        'status' => FALSE,
                        'message' => 'Library is already shared in another device. Request cant sent',
                        'data' => $this->api_manager->format_output(array('status' => 0))
                            ), REST_Controller::HTTP_OK);
                    die();
                }
            }
        }
    }

    private function request_libraries($data, $user_details) {


        $token = $this->{$this->model}->generate_random_string();
        $email_data = array();
        $bulk_share_code = false;
        foreach ($data as $library) {
            $check = $this->{$this->model}->get_library_custom(array('share_library_code' => $library['share_library_code'], 'is_shared' => '1', 'uuid' => $user_details['uuid']));
            if (!$check) {
                $library_owner = $this->{$this->model}->get_library_custom(array('library_id' => $library['library_id'], 'is_shared' => '0'));
                if ($library_owner) {
                    $this->load->model('user_model');
                    $owner_data = $this->user_model->get_user(array('id' => (int) $library_owner['user_id']));
                }
                if (!$bulk_share_code) {
                    $bulk_share_code = $library['bulk_share_code'];
                }
                $insert_data = array();
                $insert_data['share_library_code'] = $library['share_library_code'];
                $insert_data['user_id'] = $user_details['user_id'];
                $insert_data['library_id'] = $library['library_id'];
                $insert_data['is_shared'] = '1';
                $insert_data['bulk_share_code'] = $library['bulk_share_code'];
                $insert_data['is_imported'] = '1';
                $insert_data['uuid'] = $user_details['uuid'];
                $insert_data['is_approved'] = '0';
                $insert_data['token'] = $token;
                $library_id = $this->{$this->model}->insert_user_libraries($insert_data);
                if ($library_id) {
                    $names = $this->{$this->model}->get_user_name_and_library_name(array('user_id' => $user_details['user_id'], 'library_id' => $library['library_id']));
                }
                $email_data[] = array('names' => $names, 'id' => $library_id);
            }
        }
        if (!empty($email_data)) {
            $mail_params['ContentType'] = "text/html";
            $mail_params['To'] = $owner_data['email'];
            $mail_params['Subject'] = 'Quantum Genius Request For Library';
            $mail_params['Body'] = $this->load->view('email_format/bulk_request_for_library_email', array('token' => $token, 'data' => $email_data, 'bulk_share_code' => $bulk_share_code), true);
            if ($this->{$this->model}->sendMail($mail_params)) {
                return true;
            } else {
                return false;
            }
        }
    }

    function list_import_requests_post() {
        $this->api_manager->handle_request();

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }

        $libraries = $this->{$this->model}->get_libraries(array('user_id' => $user_details['user_id'], 'is_shared' => '0'));


        if ($libraries) {

            $library_ids = array();
            foreach ($libraries as $library_id) {
                $library_ids[] = $library_id['library_id'];
            }
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Currently you have no Library'
                    ), REST_Controller::HTTP_OK);
            die();
        }

        $pending_requests = $this->{$this->model}->get_import_request($library_ids);


        if ($pending_requests) {
            foreach ($pending_requests as $pending_request) {
                $response_data[] = $this->{$this->model}->get_user_name_and_library_name(array('library_id' => $pending_request['library_id'], 'user_id' => $pending_request['user_id'], 'id' => $pending_request['id']));
            }

            $this->response(array(
                'status' => TRUE,
                'message' => 'Sucess',
                'data' => $this->api_manager->format_output($response_data)
                    ), REST_Controller::HTTP_OK);
            die();
        } else {
            $this->response(array(
                'status' => TRUE,
                'message' => 'No pending requests'
                    ), REST_Controller::HTTP_OK);
            die();
        }
    }

    function accept_import_request_get() {
        $id = $this->input->get('id');
        $token = $this->input->get('token');
        $status = $this->input->get('status');

        $library_data = $this->{$this->model}->get_library_custom(array('id' => $id, 'token' => $token, 'is_approved' => '0'));

        /* AUTHENTICATE THE USER */
        /* Validate the parameters */
        if ($library_data) {
            $names = $this->{$this->model}->get_user_name_and_library_name(array('library_id' => $library_data['library_id'], 'user_id' => $library_data['user_id']));
            if ($status == 1) {
                $message = "Your library request for " . $names['library_name'] . ' library has been accepted.';
            } else {
                $message = "Your library request for " . $names['library_name'] . ' library has been rejected.';
            }

            if ($this->{$this->model}->update_user_libraries(array('is_approved' => $status), $id)) {
                $this->load->model('user_model');
                $user_data = $this->user_model->get_user(array('id' => (int) $library_data['user_id']));


                /* EMAIL Library Code TO THE USER */
                $mail_params['ContentType'] = "text/html";
                $mail_params['To'] = $user_data['email'];
                $mail_params['Subject'] = 'Quantum Genius Request For Library "' . $names['library_name'] . '" Accepted';
                $mail_params['Body'] = $this->load->view('email_format/request_accepted_for_library_email', array('message' => $message, 'user_name' => $names['user_name']), true);
                if ($this->{$this->model}->sendMail($mail_params)) {
                    
                }

                $notification_id = $this->{$this->model}->insert_notification(array('status' => 0, 'message' => $message, 'user_id' => $library_data['user_id']));
                echo "you have Updated the librery request";
                exit;
            } else {
                echo 'Library can not be updated';
                exit;
            }
        } else {
            echo "Library can not be found";
        }
    }

    function accept_bulk_import_request_get() {

        $code = $this->input->get('code');
        $token = $this->input->get('token');
        $status = $this->input->get('status');

        $libraries_data = $this->{$this->model}->get_libraries_custom(array('bulk_share_code' => $code, 'token' => $token, 'is_approved' => '0'));

        /* AUTHENTICATE THE USER */
        /* Validate the parameters */
        if ($libraries_data) {
            $library_names = array();
            $user_id = false;
            foreach ($libraries_data as $library_data) {
                $this->{$this->model}->update_user_libraries(array('is_approved' => $status), $library_data['id']);
                $names = $this->{$this->model}->get_user_name_and_library_name(array('library_id' => $library_data['library_id'], 'user_id' => $library_data['user_id']));
                $library_names[] = $names['library_name'];
                if (!$user_id) {
                    $user_id = $library_data['user_id'];
                }
            }
            $library_names = implode(',', $library_names);

            if ($status == 1) {
                $message = "Your library request for " . $library_names . ' library has been accepted.';
            } else {
                $message = "Your library request for " . $library_names . ' library has been rejected.';
            }


            $this->load->model('user_model');
            $user_data = $this->user_model->get_user(array('id' => (int) $user_id));


            /* EMAIL Library Code TO THE USER */
            $mail_params['ContentType'] = "text/html";
            $mail_params['To'] = $user_data['email'];
            $mail_params['Subject'] = 'Quantum Genius Request For Libraries Accepted';
            $mail_params['Body'] = $this->load->view('email_format/request_accepted_for_library_email', array('message' => $message, 'user_name' => $names['user_name']), true);
            if ($this->{$this->model}->sendMail($mail_params)) {
                
            }

            $notification_id = $this->{$this->model}->insert_notification(array('status' => 0, 'message' => $message, 'user_id' => $library_data['user_id']));
            echo "you have Updated the librery request";
            exit;
        } else {
            echo "Library can not be found";
        }
    }

    function get_request_status_notification_post() {
        $this->api_manager->handle_request();


        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }

        $notification_data = $this->{$this->model}->get_notifiation(array('user_id' => $user_details['user_id']));
        if ($notification_data) {

            foreach ($notification_data as $key => $notification) {
                $response_data[$key]['message'] = $notification['message'];
                $response_data[$key]['time'] = date('h:i a ', strtotime($notification['created']));
                $response_data[$key]['date'] = date('M d, Y', strtotime($notification['created']));
            }

            $this->response(array(
                'status' => true,
                'message' => 'notification',
                'data' => $this->api_manager->format_output($response_data)
                    ), REST_Controller::HTTP_OK);
            die();
        } else {
            $this->response(array(
                'status' => FALSE,
                'message' => 'No notification'
                    ), REST_Controller::HTTP_OK);
            die();
        }
    }

    function associate_record_with_library_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }


        /* Validate the parameters */
        $this->form_validation->set_data($data);

        $this->form_validation->set_rules('record_id', 'Record Id', 'trim|required');
        if ($this->form_validation->run() == FALSE || !(isset($data['library_id']) && is_array($data['library_id']) && count($data['library_id']) > 0)) {
            $error = isset($data['library_id']) && is_array($data['library_id']) && count($data['library_id']) > 0 ? validation_errors() : validation_errors() . ' Libraries required ';
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                'error' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }



        // process the request
        $this->load->model('user_record_model');
        $check_user_have_record = $this->user_record_model->get_user_record(array('user_id' => $user_details['user_id'], 'id' => $data['record_id']));
        if (!$check_user_have_record) {
            $this->response(array(
                'status' => FALSE,
                'message' => "You do not have access to this Record"
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $library_response_arr = array();
        $count = 0;
        foreach ($data['library_id'] as $library_id) {

            $check_user_have_library = $this->{$this->model}->get_library_custom(array('user_id' => $user_details['user_id'], 'library_id' => $library_id, 'is_approved' => '1'));
            if (!$check_user_have_library || (count($check_user_have_library) && $check_user_have_library['is_shared'] == '1' && $check_user_have_library['is_imported'] == '0')) {
                $library_response_arr[] = array('library_id' => $library_id, 'message' => 'You do not have access to this library');
            } else {
                $this->load->model('library_items_model');
                $item_id = $this->library_items_model->count_items($library_id);

                if (!$item_id) {
                    $library_response_arr[] = array('library_id' => $library_id, 'message' => 'Library have no item');
                } else {
                    $check_already_associated = $this->{$this->model}->get_record_library(array('record_id' => $data['record_id'], 'library_id' => $library_id));
                    if ($check_already_associated) {
                        $library_response_arr[] = array('library_id' => $library_id, 'message' => 'Library already associated');
                    } else {
                        $associate_id = $this->{$this->model}->associate_record_with_library(array('record_id' => $data['record_id'], 'library_id' => $library_id));
                        if ($associate_id) {
                            $count++;
                        }
                    }
                }
            }
        }
            $message =   $count. ' out of '.count($data['library_id']);
            $message.= count($data['library_id']) > 1 ?  ' Libraries associated' : 'Libraries associated';   
            $this->response(array(
            'status' => true,
            'message' => $message,
            'data' => $this->api_manager->format_output(array('total_libraries' => count($data['library_id']), 'total_shared' => $count, 'result' => $library_response_arr))
                ), REST_Controller::HTTP_OK);

die();





       
    }

    function list_library_for_record_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }


        /* Validate the parameters */
        $this->form_validation->set_data($data);

        $this->form_validation->set_rules('record_id', 'Record Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }

        $this->load->model('user_record_model');
        $check_user_have_record = $this->user_record_model->get_user_record(array('user_id' => $user_details['user_id'], 'id' => $data['record_id']));
        if (!$check_user_have_record) {
            $this->response(array(
                'status' => FALSE,
                'message' => "you do not have access to this Record"
                    ), REST_Controller::HTTP_OK);
            die();
        }
        $library_data = $this->{$this->model}->get_all_library_data($data['record_id']);

        if (!empty($library_data)) {
            $response_data = array();
            foreach ($library_data as $key => $libarary) {
                $response_data[$libarary['id']]['name'] = $libarary['library_name'];
                $response_data[$libarary['id']]['description'] = $libarary['library_description'];
                $response_data[$libarary['id']]['image'] = $libarary['image'];
                if (isset($response_data[$libarary['id']]['items']) && !is_array($response_data[$libarary['id']]['items'])) {
                    $response_data[$libarary['id']]['items'] = array();
                }
                $response_data[$libarary['id']]['items'][] = array('name' => $libarary['item_name'], 'frequency' => json_decode($libarary['frequency'], TRUE));
            }

            $response_data = array_values($response_data);
            $this->response(array(
                'status' => true,
                'message' => "Success",
                'data' => $this->api_manager->format_output($response_data)
                    ), REST_Controller::HTTP_OK);
            die();
        } else {
            $this->response(array(
                'status' => false,
                'message' => "No library exists for this record",
                    ), REST_Controller::HTTP_OK);
            die();
        }
    }

    function get_libraries_count_for_record_post() {
        $this->api_manager->handle_request();
        $data = json_decode(file_get_contents("php://input"), true);

        /* AUTHENTICATE THE USER */
        if (!($user_details = $this->api_manager->verifyAccessToken())) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Authorization Failed'
                    ), REST_Controller::HTTP_OK);
            die();
        }

        /* Validate the parameters */
        $this->form_validation->set_data($data);

        $this->form_validation->set_rules('record_id', 'Record Id', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->response(array(
                'status' => FALSE,
                'message' => 'Validation errors',
                    ), REST_Controller::HTTP_OK);
            die();
        }
        // check if user have access to this record
        $this->load->model('user_record_model');
        $check_user_have_record = $this->user_record_model->get_user_record(array('user_id' => $user_details['user_id'], 'id' => $data['record_id']));
        if (!$check_user_have_record) {
            $this->response(array(
                'status' => FALSE,
                'message' => "you do not have access to this Record"
                    ), REST_Controller::HTTP_OK);
            die();
        }

        $library_count = $this->{$this->model}->get_libraries_count_for_record($data['record_id']);

        $this->response(array(
            'status' => true,
            'message' => "success",
            'data' => $this->api_manager->format_output($library_count)
                ), REST_Controller::HTTP_OK);
        die();
    }

}
