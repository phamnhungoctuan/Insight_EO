<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Harmonizer extends MY_Controller {

    public $model = "harmonizer_model";

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/harmonizer
     * 	- or -  
     * 		http://example.com/index.php/harmonizer/index
     * 	- or -
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/harmonizer/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('Admin/harmonizer_model');
        $this->model = 'harmonizer_model';
    }

    /**
     * @Name : index()
     * @Purpose : To show the listing of all Water Harmonizer App users to admin.
     * @Call from : When admin click on the H2O Harmonizer App Tab. 
     * @Functionality : Simply fetch the records from database, and set the data to the view.
     * @Receiver params : No receiver parameters passed.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    public function index($where = NULL) {
        $this->load->helper('date');
        $this->load->library('encrypt');
        $type = $this->input->get('type') ? $this->input->get('type') : "";
        $data['pageTitle'] = "H20 Harmonizer App Users";
        if ($type != "") {
            switch ($type) {
                case 'activated':$where = '`isActivated`="1"';
                    break;
                case 'paid': $where = '`Key`!="" AND `UUID`!=""';
                    break;
                case 'free': $where = '`Key`="" OR `Key` IS NULL';
                    break;
                case 'Android': $where = 'deviceType="A"';
                    break;
                case 'Iphone': $where = 'deviceType="I"';
                    break;
            }
        }
        $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results'] = $this->{$this->model}->get_users($where,ROWS_PER_PAGE,$start_rec);
        $data['total_rows'] = $this->{$this->model}->count_results($where);
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $start_rec, $data['total_rows'], base_url('harmonizer/index'));
        
        /*GET THE DISTRIBUTORS LIST*/
        $this->load->model("distributors_model");
        $data['distributors_data']=$this->distributors_model->get_distributors();
        
        $data['row_counter'] = $start_rec + 1;
        $this->load->view('harmonizer/index', $data);
        
        

    }

    /**
     * @Name : user_add()
     * @Purpose : To show the add user_add form to admin.
     * @Call from : When admin clicks on add new user button.
     * @Functionality : renders the user_add view to the admin.
     * @Receiver params : No parameter passed.
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    public function user_add() {
        $this->load->library('form_validation');
        $data = array();
        $data['pageTitle'] = 'Add App User';
        $data['subPageTitle'] = '';
        $this->load->model($this->model);
        $data['Key'] = $this->{$this->model}->generateKey("Key");
        //--get distributors list--------
        $this->load->model("distributors_model");
        $data['distributors_data'] = $this->distributors_model->get_distributors();
        $this->load->view('harmonizer/user_add', $data);
    }

    /**
     * @Name : save_new_user()
     * @Purpose : To save the user detail in database when add new user form is submitted.
     * @Call from : Add new user form action calls this function.
     * @Functionality : save the new user record in the database
     * @Receiver params : Only post data is accepted to save in harmonizer table.
     * @Return params : Redirects to the listing page if user saved successfully. 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function save_new_user() {
        if ($this->input->post()) {
            $this->load->model($this->model);
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_add();
            } else {
                $data = $this->input->post();
                $data['createdtime'] = date('Y-m-d H:i:s');
                if (isset($data['payment_amount'])) {
                    $data['distributor_commissions'] = $this->set_Commision($data['payment_amount'], 40, 0);
                    $data['affiliate_commissions'] = $this->set_Commision($data['payment_amount'], 20, 0);
                }
                if (isset($data['UUID'])) {
                    $data['deviceType'] = $this->set_deviceType($data['UUID']);
                }
                $return_insertId = $this->{$this->model}->save($data);
                if ($return_insertId) {
                    $this->notify_admin("new_user", $data);
                    $this->notify_user("new_user", $data);
                }
                redirect('harmonizer/index');
            }
        }
    }
    
    /**
     * @Name : set_deviceType()
     * @Purpose : To set the Type of device
     * @Call from : Can be called from any controller file.
     * @Functionality : if length of deviceID is less than 16 characters then return Android else Iphone
     * @Receiver params : DeviceId
     * @Return params : Return the device Type initials (A or I)
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 6 July 2015
     * @Modified :
     */
    //---Function to set the commissions---
    private function set_deviceType($uuid) {
		if(empty($uuid)|| is_null($uuid)) return "";
        $deviceType = (strlen($uuid) <= 16) ? "A" : "I";
        return $deviceType;
    }

    /**
    * @Name : add_activation_key()
    * @Purpose : To show the  add_activation_key form to admin and save the activation key if admin clicks on add button
    * @Call from : When admin clicks on create key button.
    * @Functionality : renders the add_activation_key view to the admin.
    * @Receiver params : User id and random string i.e. key is passed
    * @Return params : No parameter returned. 
    * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified :
    */
    function add_activation_key($userId=NULL) {
        if(is_null($userId) || $userId < 1){
            redirect('user/index');
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Create Key';
        $data['subPageTitle'] = '';
        $data['id'] = $userId;
        $data['action'] = "harmonizer/add_activation_key/".$userId;
        
        if($this->input->post('id')){
            $this->form_validation->set_rules("Key","Activation Key","required");
            if ($this->form_validation->run() == FALSE) {
                $this->add_activation_key($this->input->post('id'));
            } 
            else{
                $user_data= $this->input->post();
                $where['id'] = $data['id'];
                if($this->{$this->model}->update($user_data,$where)){
                    $user_details = $this->{$this->model}->get_user_detail(array('id'=>$user_data['id']));
                    $this->notify_admin("activation_key",$user_details);
                    $this->notify_user("activation_key",$user_details);
                }
                redirect("harmonizer/index");
            }
        }
        $data['Key'] = $this->{$this->model}->generateKey("Key");
        $this->load->view('harmonizer/add_activation_key', $data);
    }


    public function user_detail($id = NULL) {
        if(is_null($id)){
            redirect('harmonizer/index');
        }
        $this->load->library(array('encrypt','form_validation'));
        $where['id'] = $id;
        $data['user_data'] = $this->{$this->model}->get_user_detail($where);
        /*fetch distributors list*/
        $this->load->model("distributors_model");
        $data['distributors_data']=$this->distributors_model->get_distributors();
        
        $this->load->view($this->router->class."/user_detail",$data);
    }
    
    

    /**
     * @Name : edit_user()
     * @Purpose : To edit the user details by admin.
     * @Call from : When admin clicks on update link on the edit user form.
     * @Functionality : Updates the user details.
     * @Receiver params :  Only post data is accepted to update in rife_codes table.
     * @Return params : No parameters returned.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function edit_user() {
        if ($this->input->post()) {
            $this->load->model($this->model);

            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_edit($this->input->post('id'));
            } else {
                $data = $this->input->post();
                if (isset($data['payment_amount'])) {
                    $data['distributor_commissions'] = $this->set_Commision($data['payment_amount'], 40, $data['distributor_commissions']);
                    $data['affiliate_commissions'] = $this->set_Commision($data['payment_amount'], 20, $data['affiliate_commissions']);
                }
                if (isset($data['UUID'])) {
                    $data['deviceType'] = $this->set_deviceType($data['UUID']);
                }
                $where['id'] = $data['id'];
                $this->{$this->model}->update($data, $where);
                $distributer_id = isset($data['distributor_id']) ? $data['distributor_id'] : "0";
                if ($distributer_id != "") {
                    $this->layout = 'blank';
                    $this->load->theme($this->layout);
                    $this->notify_distributor($data['distributor_id'],$where['id']);
                }
                redirect('harmonizer/index');
            }
        }
    }
    
     /**
     * @Name : set_Commision()
     * @Purpose : To set the commission
     * @Call from : Can be called from any controller file.
     * @Functionality : if commission is not specified then return percent share of amount else return commission specified
     * @Receiver params : three parameters: amount, %age share , commission 
     * @Return params : Return the total commission
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 6 July 2015
     * @Modified :
     */
    //---Function to set the commissions---
    private function set_Commision($amount, $percent_share, $commission = 0) {
        if ($amount == 0)
            return 0;
        if ($commission == 0 || empty($commission))
            return ($amount * $percent_share) / 100;
        return $commission;
    }

    /**
     * @Name : delete_user()
     * @Purpose : To delete the user by admin.
     * @Call from : When admin clicks on delete link from users listing page and ajax called to delete the user using this function.
     * @Functionality : permanently delete user from database  
     * @Receiver params : User_Id is passed with post data.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function delete($user_id = NULL) {
        if ($this->input->post()) {
            $user_id = $this->input->post('id');
            if ($this->{$this->model}->delete(array('id' => $user_id))) {
                redirect('quanta_capsule/index');
            }
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Delete User';
        $data['subPageTitle'] = '';
        $this->load->view('harmonizer/delete', array('id' => $user_id));
    }

    function notify_admin($about = '', $user_detail = array()) {
        if ($about == '' || count($user_detail) < 0 || !isset($user_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch ($about) {
            case 'new_user' :
                $subject = 'Water Harmonizer New User Notification';
                $view = 'harmonizer/admin_email_format/new_user_email';
                break;
            case 'activation_key' :
                $subject = 'Water Harmonizer Activation Key Notification';
                $view = 'harmonizer/admin_email_format/acivation_key_email';
                break;
        }
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = admin_email;
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view, array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function notify_user($about = '', $user_detail = array()) {
        if ($about == '' || count($user_detail) < 0 || !isset($user_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch ($about) {
            case 'new_user' :
                $subject = 'Your Water Harmonizer App Download Link';
                $view = 'harmonizer/user_email_format/new_user_email';
                break;
            case 'activation_key' :
                $subject = 'Water Harmonizer App Activation Key';
                $view = 'harmonizer/user_email_format/acivation_key_email';
                break;
        }
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $user_detail['email'];
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view, array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @Name : set_distributor()
     * @Purpose : To update the distributor for the record.
     * @Call from : When admin selects the distributor from the drop down list on  users listing page and ajax called to set distributor using this function.
     * @Functionality : to update the distributor for the record.
     * @Receiver params : two post parameters :  distributor id and encrypted user Id 
     * @Return params : Return 1 incase of successfull updation
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 6 July 2015
     * @Modified :
     */
    public function set_distributor() {
        if ($this->input->post()) {
            $this->load->library('encrypt');
            $this->load->model($this->model);
            $data['distributor_id'] = $this->input->post('distributor_id');
            /* CHECK WHETHER DISTRIBUTOR EXISTS OR NOT */
            $this->load->model('distributors_model');
            $distributor = $this->distributors_model->get_distributor_detail(array('id' => $data['distributor_id']));
            if ($distributor) {
                $where['id'] = $this->encrypt->decode($this->input->post('user_id'));
                echo $this->{$this->model}->update($data, $where);
            } else
                echo '0';
        }
    }

    /**
     * @Name : send_distributor_email()
     * @Purpose : To send the email to distributor regarding the details of the user assigned to her.
     * @Call from : When admin clicks on sendEmail button on users listing page and ajax called to send Email to  distributor using this function.
     * @Functionality : to update the distributor for the record.
     * @Receiver params : two post parameters :  distributor id and encrypted user Id 
     * @Return params : Return 1 incase of success
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 6 July 2015
     * @Modified :
     */
    public function send_distributor_email() {
        if ($this->input->post()) {
            $this->load->library('encrypt');
            $this->load->model($this->model);
            $distributor_id = $this->input->post('distributor_id');
            $userid = $this->encrypt->decode($this->input->post('user_id'));
            echo $this->notify_distributor($distributor_id, $userid);
        }
    }
    
    private function notify_distributor($distributor_id, $user_id){
        $this->load->model('distributors_model');
        $distributor = $this->distributors_model->get_distributor_detail(array('id'=>$distributor_id));
        if($distributor){
            $user = $this->{$this->model}->get_user_detail(array('id'=>$user_id));
            if($user){
                /* SEND EMAIL TO DISTRIBUTOR */
                $params = array(
                    'ContentType' => 'text/html',
                    'To' => $distributor['email'],
                    'Subject' => 'A Lead Is Generated',
                    'Body' => $this->load->view('harmonizer/admin_email_format/distributor_email',array('user_detail' => $user, 'distributor'=>$distributor), TRUE)
                );
                return  $this->{$this->model}->sendMail($params);
            }
        }
        return 0;
    }

}

/* End of file harmonizer.php */
/* Location: ./application/controllers/harmonizer.php */