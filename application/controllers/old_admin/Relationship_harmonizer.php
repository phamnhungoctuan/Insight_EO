<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Relationship_Harmonizer extends MY_Controller {

    public $model = "relationship_harmonizer_model";

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/relationship_harmonizer
     * 	- or -  
     * 		http://example.com/index.php/relationship_harmonizer/index
     * 	- or -

     * So any other public methods not prefixed with an underscore will
     * map to /index.php/relationship_harmonizer/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->model = 'relationship_harmonizer_model';
        $this->load->library('form_validation');
        $this->load->model('Admin/relationship_harmonizer_model');
    }

    /**
     * @Name : index()
     * @Purpose : To show the listing of all relationship_harmonizer App users to admin.
     * @Call from : When admin click on the relationship harmonizer App Tab. 
     * @Functionality : Simply fetch the records from database, and set the data to the view.
     * @Receiver params : No receiver parameters passed.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    public function index($where = NULL) {
        $this->load->library('encrypt');
        $data['pageTitle'] = "Relationship Harmonizer App Users";

        $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results'] = $this->{$this->model}->get_users($where, ROWS_PER_PAGE, $start_rec);
        $data['total_rows'] = $this->{$this->model}->count_results($where);
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $start_rec, $data['total_rows'], base_url('relationship_harmonizer/index'));
        $data['row_counter'] = $start_rec + 1;

        $this->load->view('relationship_harmonizer/index', $data);
    }

    /**
     * @Name : user_add()
     * @Purpose : To show the add user_add form to admin.
     * @Call from : When admin clicks on add new user button.
     * @Functionality : renders the user_add view to the admin.
     * @Receiver params : No parameter passed.
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    public function user_add() {
        $data = array();
        $data['pageTitle'] = 'Add App User';
        $data['subPageTitle'] = '';
        $data['Key'] = $this->{$this->model}->generateKey('payment_key', 'cr_payments');
        $this->load->view('relationship_harmonizer/user_add', $data);
    }

    /**
     * @Name : save_new_user()
     * @Purpose : To save the user detail in database when add new user form is submitted.
     * @Call from : Add new user form action calls this function.
     * @Functionality : save the new user record in the database
     * @Receiver params : Only post data is accepted to save in cr_users, cr_payments table.
     * @Return params : Redirects to the listing page if user saved successfully. 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    public function save_new_user() {
        if ($this->input->post()) {
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_add();
            } else {
                $data = $this->input->post();
                $return_insertId = $this->{$this->model}->save_data($data);
                if ($return_insertId) {
                    $this->notify_admin("new_user", $data);
                    $this->notify_user("new_user", $data);
                }
                redirect('relationship_harmonizer/index');
            }
        }
    }

    /**
     * @Name : add_key()
     * @Purpose : To show the  add_key form to admin.
     * @Call from : When admin clicks on create key button.
     * @Functionality : renders the add_key view to the admin.
     * @Receiver params : User id and random string i.e. key is passed
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    public function add_key($userId = NULL) {
        $data['pageTitle'] = 'Create Key';
        $data['subPageTitle'] = '';
        $data['action'] = "relationship_harmonizer/save_key";
//        $data['payment_key'] = $this->generate_random_string();
        $data['payment_key'] = $this->{$this->model}->generateKey('payment_key', 'cr_payments');
        $where['userid'] = $userId;
        $user_data = $this->{$this->model}->get_user_detail($where);
        $data['user_data'] = $user_data;
        $this->load->view('relationship_harmonizer/add_key', $data);
    }

    /**
     * @Name : save_key()
     * @Purpose : To update the key generated for the record when add_key form is submitted.
     * @Call from : Add_key form action calls this function.
     * @Functionality : update the key for the record
     * @Receiver params : Only post data is accepted to update in cr_payments table (key and userid).
     * @Return params : Redirects to the listing page if user saved successfully. 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    public function save_key() {
        if ($this->input->post()) {
            $this->load->model($this->model);
            if ($this->{$this->model}->validates(array($this->{$this->model}->validate[2])) == FALSE) {
                $this->add_key($this->input->post('payment_userid'));
            } else {
                $data = $this->input->post();
                $user_record = $this->{$this->model}->save_key($data);
                if ($user_record) {
                    $this->notify_admin("activation_key", $user_record);
                    $this->notify_user("activation_key", $user_record);
                }
                redirect('relationship_harmonizer/index');
            }
        }
    }

    /**
     * @Name : user_edit()
     * @Purpose : To edit rife App user by admin.
     * @Call from : When admin clicks on 'edit' link.
     * @Functionality : Renders the user_edit view to edit user.
     * @Receiver params : User Id of the user to edit is passed.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    public function user_edit($userId) {
        $data['pageTitle'] = 'Edit User';
        $data['subPageTitle'] = '';
        $user_data = $this->{$this->model}->get_user_detail(array('userid' => $userId));
        $data['user_data'] = $user_data;
        $this->load->view('relationship_harmonizer/user_edit', $data);
    }

    /**
     * @Name : edit_user()
     * @Purpose : To edit the user details by admin.
     * @Call from : When admin clicks on update link on the edit user form.
     * @Functionality : Updates the user details.
     * @Receiver params :  Only post data is accepted to update in cr_users,  cr_payments table.
     * @Return params : No parameters returned.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    public function edit_user() {
        if ($this->input->post()) {
            $validate_data = $this->{$this->model}->validate;
            unset($validate_data[2]);
            if ($this->{$this->model}->validates($validate_data) == FALSE) {
                $this->user_edit($this->input->post('userid'));
            } else {
                $data = $this->input->post();
                $where['userid'] = $data['userid'];
                $this->{$this->model}->update_data($data, $where);
                redirect('relationship_harmonizer/index');
            }
        }
    }

    /**
     * @Name : delete_user()
     * @Purpose : To delete the user by admin.
     * @Call from : When admin clicks on delete link from users listing page and ajax called to delete the user using this function.
     * @Functionality : permanently delete user from database  
     * @Receiver params : User_Id is passed with post data.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
//    public function delete_user() {
//        $this->load->library('encrypt');
//        $user_id = $this->input->post('user_id');
//        $user_id = $this->encrypt->decode($user_id);
//        if ($this->{$this->model}->delete(array('userid' => $user_id))) {
//            redirect('relationship_harmonizer/index');
//        }
//    }

    public function delete($user_id = NULL) {
        if ($this->input->post()) {
            $user_id = $this->input->post('id');
            if ($this->{$this->model}->delete(array('userid' => $user_id))) {
                redirect('relationship_harmonizer/index');
            }
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Delete User';
        $data['subPageTitle'] = '';
        $this->load->view('relationship_harmonizer/delete', array('id' => $user_id));
    }

    function notify_admin($about = '', $user_detail = array()) {
        if ($about == '' || count($user_detail) < 0 || !isset($user_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch ($about) {
            case 'new_user' :
                $subject = 'Relationship Harmonizer New User Notification';
                $view = 'relationship_harmonizer/admin_email_format/new_user_email';
                break;
            case 'activation_key' :
                $subject = 'Relationship Harmonizer Activation Key Notification';
                $view = 'relationship_harmonizer/admin_email_format/acivation_key_email';
                break;
        }
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = admin_email;
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view, array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function notify_user($about = '', $user_detail = array()) {
        if ($about == '' || count($user_detail) < 0 || !isset($user_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch ($about) {
            case 'new_user' :
                $subject = 'Your Relationship Harmonizer Download Link';
                $view = 'relationship_harmonizer/user_email_format/new_user_email';
                break;
            case 'activation_key' :
                $subject = 'Relationship Harmonizer Activation Key';
                $view = 'relationship_harmonizer/user_email_format/acivation_key_email';
                break;
        }
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $user_detail['email'];
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view, array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

/* End of file relationship_harmonizer.php */
/* Location: ./application/controllers/relationship_harmonizer.php */