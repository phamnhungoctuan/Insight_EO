<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rife extends MY_Controller {
    
   public $model="rife_model";
    
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/Rife
     *	- or -  
     * 		http://example.com/index.php/Rife/index
     *	- or -
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct(){
        parent::__construct();
        $this->model='rife_model';
        $this->load->model('Admin/rife_model');
    }
    
    /**
    * @Name : index()
    * @Purpose : To show the listing of all Rife App users to admin.
    * @Call from : When admin click on the Quantum Life Rife App Tab. 
    * @Functionality : Simply fetch the records from database, and set the data to the view.
    * @Receiver params : No receiver parameters passed.
    * @Return params : No parameters returned 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified :
    */
    
    
    public function index() {
        $this->load->library('encrypt');
        $data['pageTitle']="Quanta Life Rife App Users";
        $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results'] = $this->{$this->model}->get_users(NULL,ROWS_PER_PAGE,$start_rec);
        $data['total_rows'] = $this->{$this->model}->count_results();
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $start_rec, $data['total_rows'], base_url('rife/index'));
        $data['row_counter'] = $start_rec + 1;
        $this->load->view('rife/index', $data);
    }
   
    
    /**
    * @Name : user_add()
    * @Purpose : To show the add user_add form to admin.
    * @Call from : When admin clicks on add new user button.
    * @Functionality : renders the user_add view to the admin.
    * @Receiver params : No parameter passed.
    * @Return params : No parameter returned. 
    * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified :
    */
    public function user_add() {
        $this->load->library('form_validation');
        $data = array();
        $data['pageTitle'] = 'Add App User';
        $data['subPageTitle'] = '';
        $data['Key']=$this->{$this->model}->generateKey("Key");
        $this->load->view('rife/user_add',$data);
    }
    
    /**
    * @Name : save_new_user()
    * @Purpose : To save the user detail in database when add new user form is submitted.
    * @Call from : Add new user form action calls this function.
    * @Functionality : save the new user record in the database
    * @Receiver params : Only post data is accepted to save in rife_codes table.
    * @Return params : Redirects to the listing page if user saved successfully. 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : 
    */
    function save_new_user() {
        if ($this->input->post()) {
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_add();
            } else 
            {
                $data = $this->input->post();
                $data['CreatedDate'] = date('Y-m-d H:i:s');
                $return_insertId = $this->{$this->model}->save($data);
                if ($return_insertId) {
                    $this->notify_admin("new_user",$data);
                    $this->notify_user("new_user",$data);
                }
                redirect('rife/index');
            }
        }
    }
    /**
    * @Name : add_activation_key()
    * @Purpose : To show the  add_activation_key form to admin and save the activation key if admin clicks on add button
    * @Call from : When admin clicks on create key button.
    * @Functionality : renders the add_activation_key view to the admin.
    * @Receiver params : User id and random string i.e. key is passed
    * @Return params : No parameter returned.
    */
    
    function add_activation_key($userId=NULL) {
        if(is_null($userId) || $userId < 1){
            redirect('user/index');
        }
        $this->load->library('form_validation');
        $data['id'] = $userId;
        if($this->input->post('id')){
            $this->form_validation->set_rules("Key","Activation Key","required");
            if ($this->form_validation->run() == FALSE) {
                $this->add_activation_key($this->input->post('id'));
            } 
            else{
                $user_data= $this->input->post();
                $where['Id'] = $data['id'];
                if($this->{$this->model}->update($user_data,$where)){
                    $user_details = $this->{$this->model}->get_user_detail($where);
                    $this->notify_admin("activation_key",$user_details);
                    $this->notify_user("activation_key",$user_details);
                }
                redirect("rife/index");
            }
        }
        $data['pageTitle'] = 'Create Key';
        $data['subPageTitle'] = '';
        
        $data['action'] = "rife/add_activation_key/".$userId;
        $data['Key'] = $this->{$this->model}->generateKey("Key");
        $this->load->view('rife/add_activation_key', $data);
    }
    /**
    * @Name : user_edit()
    * @Purpose : To edit rife App user by admin.
    * @Call from : When admin clicks on 'edit' link.
    * @Functionality : Renders the user_edit view to edit user.
    * @Receiver params : User Id of the user to edit is passed.
    * @Return params : No parameters returned 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : 
    */
    function user_edit($userId) {
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Edit User';
        $data['subPageTitle'] = '';
        $user_data = $this->{$this->model}->get_user_detail(array('Id'=>$userId));
        if($user_data){
            $user_data['ActivatedDate']= $this->format_date($user_data['ActivatedDate'],"Y-m-d");
            $user_data['ExpiredDate']= $this->format_date($user_data['ExpiredDate'],"Y-m-d");
            $user_data['CreatedDate']= $this->format_date($user_data['CreatedDate'],"Y-m-d");
            $user_data['payment_date']= $this->format_date($user_data['payment_date'],"Y-m-d");
        }
        $data['user_data'] = $user_data;
        $this->load->view('rife/user_edit', $data);
    }
    /**
    * @Name : edit_user()
    * @Purpose : To edit the user details by admin.
    * @Call from : When admin clicks on update link on the edit user form.
    * @Functionality : Updates the user details.
    * @Receiver params :  Only post data is accepted to update in rife_codes table.
    * @Return params : No parameters returned.
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : 
    */
    function edit_user() {
        if ($this->input->post()) {
            $this->load->model($this->model);

            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_edit($this->input->post('Id'));
            } else 
            {
                $data= $this->input->post();
                $where['Id']=$data['Id'];
                $this->{$this->model}->update($data,$where);
                redirect('rife/index');
            }
        }
    }

    /**
     * @Name : delete_user()
     * @Purpose : To delete the user by admin.
     * @Call from : When admin clicks on delete link from users listing page and ajax called to delete the user using this function.
     * @Functionality : permanently delete user from database  
     * @Receiver params : User_Id is passed with post data.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function delete($user_id = NULL) {
        if($this->input->post()){
            $user_id = $this->input->post('id');
            if ($this->{$this->model}->delete(array('Id'=>$user_id))) {
                redirect('rife/index');
            }
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Delete User';
        $data['subPageTitle'] = '';
        $this->load->view('rife/delete', array('id'=>$user_id));
        
    }
    
    /** USED ONLLY FOR CODES(30 days activation : quantum ilife/iNfinity) and rife codes table (Quantum Life rife app)
    * @Name : generateCodes()
    * @Purpose : To generate 1000 activation codes.
    * @Call from : when admin clicks on generate 1K codes button an ajax called to generate 1000 codes using this function.
    * @Functionality : geberate 1000 activation codes and save it in database
    * @Receiver params : Only post data is accepted to save in codes table.
    * @Return params : Redirects to the listing page if user saved successfully. 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 17 July 2015
    * @Modified : 
    */
    function generateCodes() {
        if($this->input->get('genCode')&& $this->input->get('genCode')== '1'){
            $numOfCodes = $this->input->get('numOfCodes');
            $numOfCodes = 3;
            if($numOfCodes > 0)
            {   
                $batch_array=array();
                $maxNumber = $this->{$this->model}->count_results();
		$today = new DateTime('NOW');
		
		$data['ExpiredDate'] = 'NULL'; //add_month($today, 1)->format(DateTime::W3C);
		$data['ActivatedDate'] = 'NULL';
		$data['CreatedDate'] = $today->format(DateTime::W3C);
		$data['UUID'] = '';
		for($i = 1; $i <= $numOfCodes; $i++ )
		{
                    $prefix = str_pad(dechex(($maxNumber + $i) / 10000), 2, '0');
                    $generatedCode = $prefix.substr(hash('ripemd128',  uniqid('', true)), 0, 8);
                    $data['Key'] = $generatedCode;
                    array_push($batch_array,$data);
		}
                $this->db_q = $this->load->database('quantum', TRUE);
            $this->db_q->insert_batch($this->{$this->model}->table,$batch_array);
                redirect($this->router->class.'/index');
            }   
        }
    }
    
    function notify_admin($about = '', $user_detail = array()){
        if($about == '' || count($user_detail)<0 || !isset($user_detail['Email'])){
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch($about){
            case 'new_user' : 
                $subject = 'Quantum Life Rife New User Notification';
                $view = 'rife/admin_email_format/new_user_email';
                break;
            case 'activation_key' : 
                $subject = 'Quantum Life Rife Activation Key Notification';
                $view = 'rife/admin_email_format/acivation_key_email';
                break;
        }
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = admin_email;
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view,array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        }else{
            return FALSE;
        }
    }
    function notify_user($about = '', $user_detail = array()){
        if($about == '' || count($user_detail)<0 || !isset($user_detail['Email'])){
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch($about){
            case 'new_user' : 
                $subject = 'Your Royal Rife Machine Download Link';
                $view = 'rife/user_email_format/new_user_email';
                break;
            case 'activation_key' : 
                $subject = 'Your Royal Rife Machine Activation Key';
                $view = 'rife/user_email_format/acivation_key_email';
                break;
        }
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $user_detail['Email'];
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view,array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */