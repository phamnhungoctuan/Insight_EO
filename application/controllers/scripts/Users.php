<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public $model = 'user_model';

    /**
     * @Name : import_csv()
     * @Purpose : To validate the username and password.
     * @Call from : Authentication.php controller file.
     * @Functionality : Convert the value hash using sha1 algorithm functions.
     * @Receiver params : $value_to_hash value to be converted.
     * @Return params : return converted hash value.
     */
    public function __construct() {
        parent::__construct();
        $this->data['pageTitle'] = "Import Users";
        $this->data['subPageTitle'] = '';
    }

    public function confirm_otp() {
        $this->load->library('api_manager');
        $this->load->model('user_model');
        $this->api_manager->handle_request();
        $view_data['status'] = FALSE;
        $data = $this->input->get();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules('tk', 'User ID', 'trim|required');
        $this->form_validation->set_rules('code', 'Access Token', 'trim|required');

        if ($this->form_validation->run() == false) {
            $status = FALSE;
            $view_data['message'] = "Missing parameters to confirm the account";
        } else {
            $user_id = base64_decode($data['tk']);
            $userDetails = $this->{$this->model}->get_user(array('id' => $user_id));
            if (!empty($userDetails)) {
                $view_data['user'] = $userDetails;
                /* IF ALREADY CONFIRMED */
                if ($userDetails['status'] == 0) {
                    $diff_time = (strtotime(date('Y-m-d H:m:i')) - strtotime($userDetails['otp_date_created'])) / (60 * 60);
                    if ($diff_time >= 24) {
                        $view_data['message'] = 'Your Account Confirmation link is expired. Please login the App to get the new account confirmation link';
                    } else if ($data['code'] == md5($userDetails['otp'] . $userDetails['id'] . 'Qg')) {
                        $user['otp'] = '';
                        $user['status'] = '1';
                        $user['otp_date_confirmed'] = date('Y-m-d H:m:i');
                        $user['freetrial_start_date'] = date('Y-m-d');
                        $this->{$this->model}->update_user($user, $userDetails['id']);
                        $view_data['status'] = TRUE;
                        $view_data['message'] = 'Your account is activated successfully.';
                        /* CALL INFUSIONSOFT API */
                        $this->load->helper('infusionsoft');
                        trigger_emailconfirmation_api($userDetails, 104);
                    } else {
                        $view_data['message'] = 'OTP does not match';
                    }
                } else {
                    $view_data['message'] = "Your Account is already confirmed.";
                }
            } else {
                $view_data['message'] = "No match found for this account.";
            }
        }
        $this->load->view('genius_insight/otp_confirmed', $view_data);
    }

}
