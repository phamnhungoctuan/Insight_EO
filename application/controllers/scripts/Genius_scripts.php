<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Genius_scripts extends CI_Controller {

    public $model = 'user_model';

    public function __construct() {
        parent::__construct();
        $this->data['pageTitle'] = "Import Users";
        $this->data['subPageTitle'] = '';
    }

    public function get_users_masterbranch_data_char() {
        $processed_array = array();
      /*  $sql = 'SELECT u.id AS user_id, email, umb.master_branch_id, 
                    umb.name AS master_branch, 
                    l.id AS library_id, l.name AS library_name, l.description AS library_description,l.lib_inserted_from, l.image AS library_image, 
                    li.id AS item_id,li.name AS item_name, li.frequency
                    FROM `users` u LEFT JOIN user_master_branch umb ON  u.id = umb.user_id
                    LEFT JOIN master_branch_libraries mbl ON umb.master_branch_id = mbl.master_branch_id
                    LEFT JOIN libraries l ON mbl.library_id = l.id
                    LEFT JOIN library_items li ON l.id = li.library_id
                    WHERE email In ("dental101@mailinator.com", "cet12@mailinator.com", "cechar@mailinator.com")'; */
        $sql = 'SELECT u.id AS user_id, email, umb.master_branch_id, 
                  umb.name AS master_branch, 
                  l.id AS library_id, l.name AS library_name, l.description AS library_description,l.lib_inserted_from, l.image AS library_image, 
                  li.id AS item_id,li.name AS item_name, li.frequency
                  FROM `users` u LEFT JOIN user_master_branch umb ON  u.id = umb.user_id
                    LEFT JOIN master_branch_libraries mbl ON umb.master_branch_id = mbl.master_branch_id
                    LEFT JOIN libraries l ON mbl.library_id = l.id
                   LEFT JOIN library_items li ON l.id = li.library_id
                   WHERE email In ("1emotions@mailinator.com", "Immanence@mailinator.com", "Nutritionfoods@mailinator.com", "v@mailinator.com",
                   "personalcare@mailinator.com","maintenance@mailinator.com","cet12@mailinator.com","injuries@mailinator.com","dental101@mailinator.com","immune101@mailinator.com", "cechar@mailinator.com")';

        $query = $this->db->query($sql);
        if ($query->num_rows()) {
            $result = $query->result_array();
            foreach ($result as $key => $row) {
                $processed_array[$row['user_id']]['user_id'] = $row['user_id'];
                $processed_array[$row['user_id']]['email'] = $row['email'];
                if (!isset($processed_array[$row['user_id']]['master_branch'])) {
                    $processed_array[$row['user_id']]['master_branch'] = array();
                }
                if (!empty($row['master_branch_id'])) {
                    $processed_array[$row['user_id']]['master_branch'][$row['master_branch_id']]['master_branch_id'] = $row['master_branch_id'];
                    $processed_array[$row['user_id']]['master_branch'][$row['master_branch_id']]['name'] = $row['master_branch'];
                    if (!isset($processed_array[$row['user_id']]['master_branch'][$row['master_branch_id']]['libraries'])) {
                        $processed_array[$row['user_id']]['master_branch'][$row['master_branch_id']]['libraries'] = array();
                    }
                    if (!empty($row['library_id'])) {
                        $processed_array[$row['user_id']]['master_branch'][$row['master_branch_id']]['libraries'][$row['library_id']]['library_id'] = $row['library_id'];
                        $processed_array[$row['user_id']]['master_branch'][$row['master_branch_id']]['libraries'][$row['library_id']]['name'] = $row['library_name'];
                        $processed_array[$row['user_id']]['master_branch'][$row['master_branch_id']]['libraries'][$row['library_id']]['description'] = $row['library_description'];
                        $processed_array[$row['user_id']]['master_branch'][$row['master_branch_id']]['libraries'][$row['library_id']]['lib_inserted_from'] = $row['lib_inserted_from'];
                        
                        $image = strlen($row['image']) && file_exists(FCPATH . 'assets/uploads/libraries/' . $row['image']) ? base_url('assets/uploads/libraries/' . $row['image']) : '';
                        $processed_array[$row['user_id']]['master_branch'][$row['master_branch_id']]['libraries'][$row['library_id']]['image'] = $image;
                        if (!isset($processed_array[$row['user_id']]['master_branch'][$row['master_branch_id']]['libraries'][$row['library_id']]['items'])) {
                            $processed_array[$row['user_id']]['master_branch'][$row['master_branch_id']]['libraries'][$row['library_id']]['items'] = array();
                        }
                        if (!empty($row['item_id'])) {
                            $processed_array[$row['user_id']]['master_branch'][$row['master_branch_id']]['libraries'][$row['library_id']]['items'][] = array(
                                'item_id' => $row['item_id'],
                                'name' => $row['item_name'],
                                'frequency' => strlen($row['frequency'])? json_decode($row['frequency'], true): array(),
                            );
                        }
                    }
                }
            }
            array_walk_recursive($processed_array, 'self::filter_array');
            array_walk_recursive($processed_array, 'self::filter_output');
            foreach ($processed_array as $key => $row ){
                $master_branches = $processed_array[$key]['master_branch'];
                if(count($master_branches)){
                    $processed_array[$key]['master_branch'] = array_values($master_branches);
                    
                    if(count( $processed_array[$key]['master_branch'])){
                        foreach ( $processed_array[$key]['master_branch'] as $mbkey=> $mb){
                            $processed_array[$key]['master_branch'][$mbkey]['libraries'] = array_values($mb['libraries']);
                        }
                    }
                    
                    
                    if(count($libraries)){
                        $processed_array[$key]['master_branch']['libraries'] = array_values($processed_array[$key]['master_branch']['libraries']);
                    }
                }
            }
        }
        $processed_array = array_values($processed_array);
        echo json_encode($processed_array);
    }
    
    public static function filter_output(&$param, $key) {
        if (is_null($param)) {
            $param = '';
        }
    }

    public static function filter_array(&$param, $key) {
        if (is_object($param)) {
            $param = (array) $param;
        }
    }

    /* ADD THE USERS AND THEIR DEVICES IN THE USER_INFUSIONSOFT_RECORDS 
     * FETCH THE DATA FROM THE USER_REGISTERED_DEVICE TABLE  
     */
    public function insert_users_and_devices() {
        $processed_array = array();
//        $query = $this->db->query('SELECT u.id as user_id, u.email, ud.device_type  FROM `users` AS u LEFT join user_registered_devices AS ud ON u.id = ud.user_id');
        $query = $this->db->query('SELECT u.id as user_id, u.email, ud.device_type  FROM `users` AS u LEFT join user_registered_devices AS ud ON u.id = ud.user_id LEFT JOIN user_infusionsoft_record uir ON u.id = uir.user_id WHERE uir.user_id IS NULL');
        if ($query->num_rows()) {
            echo "<pre>";
            print_r("Total Records : " . $query->num_rows());
            echo "</pre>";
            $records = $query->result_array();
            foreach ($records as $row) {
//                if($row['device_type'] == 'ios'){
//                    $processed_array[$row['user_id']]['ios'] = isset($processed_array[$row['user_id']]['ios']) ? ++$processed_array[$row['user_id']]['ios'] : 1;
//                }else if($row['device_type'] == 'android'){
//                    $processed_array[$row['user_id']]['android'] = isset($processed_array[$row['user_id']]['android']) ? ++$processed_array[$row['user_id']]['android'] : 1;
//                }
                if (!empty($processed_array[$row['user_id']]['platform']) && $processed_array[$row['user_id']]['platform'] != "both" && $processed_array[$row['user_id']]['platform'] != $row['device_type']) {
                    $processed_array[$row['user_id']]['platform'] = "both";
                } else {
                    $processed_array[$row['user_id']]['platform'] = $row['device_type'];
                }
                $processed_array[$row['user_id']]['user_id'] = $row['user_id'];
                $processed_array[$row['user_id']]['date_created'] = date('Y-m-d H:i:s');
            }
        }
        $processed_array = array_values($processed_array);
        echo "<pre>";
        print_r("Total Processed : " . count($processed_array));
        echo "</pre>";
        echo "<pre>";
        print_r($processed_array);
        echo "</pre>";
        /*UNCOMMENT THE FOLLOWING CODE TO ADD*/
        /* INSERT THE DATA IN THE DB */
        while ($rows = array_splice($processed_array, 0, 50)) {
            $this->db->insert_batch("user_infusionsoft_record", $rows);
        }
    }
    
    
    /*CREATE DUMMY LIBRARIES
     * USER ID SET STATICALLY IN MODEL */
    public function create_dummy_libraries() {
        $this->load->model('dummy_model');
        $this->data['pageTitle'] = "Dummy Libraries";
        if ($this->{$this->model}->create_dummy_libraries(5)) {
            echo "libraries created";
        }
    }


    public function import_csv() {
        $data['action'] = 'users/import_csv';

        if (!isset($_FILES['csvfile'])) {
            $this->load->view('user/import_csv', $data);
            return false;
        }
        require_once (APPPATH . 'libraries/Custom_file.php');
        $fileObj = new Custom_file();
        $data['errors'] = $fileObj->validateFile('csvfile', NULL, NULL, array('csv'));

        if (!empty($data['errors'])) {
            $this->load->view('user/import_csv', $data);
        } else {
            $filename = $_FILES['csvfile']['name'];
            $targetFileName = pathinfo(strtolower($filename), PATHINFO_FILENAME) . date('Y-m-d') . '.' . pathinfo($_FILES['csvfile']['name'], PATHINFO_EXTENSION);

            if (!@dir(FCPATH . 'uploads/imported_csv')) {
                $fileObj->mkdir_r(FCPATH . 'uploads/imported_csv');
            }

            if (move_uploaded_file($_FILES["csvfile"]["tmp_name"], FCPATH . 'uploads/imported_csv/' . $targetFileName)) {
                /* Get the records and insert the users */
                ini_set('auto_detect_line_endings', true);
                if (($handle = fopen(FCPATH . 'uploads/imported_csv/' . $targetFileName, "r")) !== FALSE) {

                    $records_data = array();
                    $header = null;

                    /* Iterate the data in the file */
                    while ($row = fgetcsv($handle, 0, ',', '"', '"')) {
                        if ($header === null) {
                            $header = $row;
                            continue;
                        }
                        $records_data[] = array_combine($header, $row);
                    }
//                    echo '<pre>';
//                    print_r($records_data);
//                    echo '</pre>';
                    $result = $this->{$this->model}->move_imported_records($records_data);
                    exit;
                    $this->session->set_flashdata('success', 'Records Imported Successfully');
                    redirect('Users/import_csv');
                }
            }
        }
    }

    /* DELETE TEST ACCOUNTS */
    public function delete_accounts() {
        $this->db->select("id,email");
        $this->db->where(" (email LIKE '%mailinator%' OR email LIKE '%yopmail%' OR EMAIL LIKE '%test%' )");
//        $this->db->where(" (email LIKE '%mailinator%' OR email LIKE '%yopmail%' OR EMAIL LIKE '%test%' ) AND id NOT IN (647,804, 816,915,917, 924, 993, 995, 999, 1055, 1160, 1170, 1265)");
        $query = $this->db->get('users');
        echo "<pre>Total :";
        print_r($query->num_rows());
        echo "</pre>";
        echo "<pre>";
        print_r($result = $query->result_array());
        echo "</pre>";
        die();
        /* if($query->num_rows()){
          $result = $query->result_array();
          $this->load->model('Admin/Genius_insight_model','genius_insight_model');
          foreach ($result as $user){
          echo "<pre>";
          print_r($user);
          echo "</pre>";
          $this->genius_insight_model->delete_user($user['id']);
          }
          } */
    }

    public function get_deleted_users() {
        $complete_array = array();
        $users = $this->db->get_where('users', array('is_deleted' => '1'));
        echo "<pre>";
        print_r($this->db->last_query());
        echo "</pre>";
        echo "<pre>";
        print_r($users->num_rows());
        echo "</pre>";
        if ($users->num_rows()) {
            $deleted_users = $users->result_array();
            foreach ($deleted_users as $key => $user) {
                $complete_array[$key]['user'] = $user;
                /* GET CLIENT RECORDS AND DATA */
                $this->load->model('Admin/user_model');
                $user_records = $this->user_model->get_user_records($user['id']);
                if ($user_records) {
                    $this->load->model('user_record_model');
                    foreach ($user_records as $val) {
                        $query = $this->db->get_where('record_library', array('record_id' => $val['id']));
                        $record_lib = $query->result_array();
                        $query = $this->db->get_where('analysis_records', array('record_id' => $val['id']));
                        $analysis = $query->result_array();
                        $complete_array[$key]['user_records'][] = array('client' => $val, 'record_library' => $record_lib, 'analysis_record' => $analysis);
                    }
                }



                /* neuro_remedy_items */
                $neuro_remedy_item = $this->db->get_where('neuro_remedy_items', array('user_id' => $user['id']));
                $complete_array[$key]['neuro_remedy_item'] = $neuro_remedy_item->result_array();
                /* DELETE activation_keys */
                $keys = $this->db->get_where('activation_keys', array('user_id' => $user['id']));
                $complete_array[$key]['activation_key'] = $keys->result_array();
                /* DELETE auth_access_tokens */
                $token = $this->db->get_where('auth_access_tokens', array('user_id' => $user['id']));
                $complete_array[$key]['auth_token'] = $token->result_array();
                /* DELETE user_registered_devices */

                $devices = $this->db->get_where('user_registered_devices', array('user_id' => $user['id']));
                $complete_array[$key]['devices'] = $devices->result_array();

                /* DELETE LIBRARIES */
                $this->load->model('user_library_model');
                $libraries = $this->user_library_model->get_libraries(array('user_id' => $user['id'], 'ul.is_shared' => '0'));
                $complete_array[$key]['created_libraries'] = $libraries;
                if ($libraries && count($libraries)) {
                    $libraries = array_column($libraries, 'library_id');
                    $this->db->where('user_id', $user['id']); /* DELETE ALL  SHARED LIBRRAY RECORDS */
                    $this->db->where('is_shared', '1');
                    $shared = $this->db->get_where('user_libraries');
                    $complete_array[$key]['shared_libraries'] = $shared->result_array();

                    $this->db->where_in('library_id', $libraries); /* DELETE ALL THE RECORDS OF THE LIBARIES OWNED BY THIS USER AND SHARED WITH OTHER USERS */
                    $this->db->where('user_id !=', $user['id']); /* DELETE ALL THE RECORDS OF THE LIBARIES OWNED BY THIS USER AND SHARED WITH OTHER USERS */
                    $shared_other = $this->db->get_where('user_libraries');
                    $complete_array[$key]['shared_with_others'] = $shared_other->result_array();
                }
            }
        }

        echo "<pre>";
        print_r($complete_array);
        echo "</pre>";
    }

    
    /* DELETE USERS WHOSE is_deleted = 1 */
    public function delete_users() {
        $this->load->model('Admin/user_model');
        $users = $this->db->get_where('users', array('is_deleted' => '1'));
        if ($users->num_rows()) {
            $deleted_users = $users->result_array();
            foreach ($deleted_users as $user) {
                $this->user_model->delete_user($user['id']);
            }
        }
    }
    
    

    public function delete_libraries() {
        $query = $this->db->query('SELECT ul.user_id, ul.library_id, l.* FROM `user_libraries` ul JOIN libraries l ON ul.library_id = l.id WHERE ul.user_id = 315 AND l.lib_inserted_from = "app" ORDER BY `l`.`date_created` ');
        if ($query->num_rows()) {
            $libraries_record = $query->result_array();
            echo "total rows : " . $query->num_rows();
            $library_ids = array_column($libraries_record, 'library_id');
            $this->db->query('UPDATE libraries SET lib_inserted_from = "dropbox" WHERE id IN(' . implode(',', $library_ids) . ')');
//            $this->db->query('DELETE FROM library_items WHERE library_id IN(' . implode(',', $library_ids) . ')');
//            $this->db->query('DELETE FROM user_libraries WHERE library_id IN(' . implode(',', $library_ids) . ')');
//            $this->db->query('DELETE FROM record_library WHERE library_id IN(' . implode(',', $library_ids) . ')');
//            $this->db->query('DELETE FROM libraries WHERE id IN(' . implode(',', $library_ids) . ')');
            echo "<pre>";
            print_r($library_ids);
            echo "</pre>";
        }
    }

    public function delete_duplicate_library() {
        $query = $this->db->query('SELECT l.id as library_id, ul.user_id, l.name FROM `libraries` l join user_libraries ul on ul.library_id=l.id WHERE (l.description is NULL OR l.description ="") AND DATE(l.date_created) > DATE("2016-10-20") order by ul.user_id');
        if ($query->num_rows()) {
            echo $query->num_rows() . '<br/>';
            $records = $query->result_array();
            foreach ($records as $record) {
                $data[$record['user_id']][trim($record['name'])][] = $record['library_id'];
            }
            foreach ($data as $user_id => $library_list) {
                foreach ($library_list as $library_name => $library) {
                    if (count($library) > 1) {
                        echo $user_id . ' = ' . $library_name . ' = ' . count($library) . '<br/>';
                    }
                }
            }
//            echo count($data);
//            echo '<pre>';
//            print_r($data);
            exit;
            foreach ($data as $user_id => $library_details) {
                foreach ($library_details as $library_ids) {
                    //if (count($library_ids) > 1) {
//                        $temp = array();
//                        foreach ($library_ids as $library_id) {
//                            print_r($library_id);exit;
//                            $temp[$library_id] = 0;
//                            $query1 = $this->db->query('select library_id from user_libraries where library_id=' . $library_id . ' and is_shared = "1"');
//                            if ($query1->num_rows()) {
//                                $temp[$library_id]+=1;
//                            }
//                            $query2 = $this->db->query('select library_id from record_library where library_id=' . $library_id);
//                            if ($query2->num_rows()) {
//                                $temp[$library_id]+=1;
//                            }
//                        }
                    arsort($library_ids);
                    $lib_id = array_shift($library_ids);
//                        echo '<pre>';
//                        print_r($library_ids);
                    $this->db->update('libraries', array('lib_inserted_from' => 'dropbox'), array('id' => $lib_id));
                    echo $this->db->last_query() . '<br/>';
                    if (!empty($library_ids)) {
                        $this->db->query('DELETE FROM library_items WHERE library_id IN(' . implode(',', $library_ids) . ')');
                        $this->db->query('DELETE FROM user_libraries WHERE library_id IN(' . implode(',', $library_ids) . ')');
                        $this->db->query('DELETE FROM libraries WHERE id IN(' . implode(',', $library_ids) . ')');
                        $this->db->query('DELETE FROM record_library WHERE library_id IN(' . implode(',', $library_ids) . ')');
                    }
                    //}
                }
            }
        }
    }

    /* NOT USED
     * process the device ids of the users and save them in the users_infusionsoft_record 
     * Get the devices from auth access token also and users table
     *      */
    public function insert_user_device_types_for_infusion() {
        $response_ary = array();
        $sql = 'SELECT u.id, u.email, u.uuid, d.uuid as registered_uuid , d.device_type, t.uuid as login_uuid from users u left join user_registered_devices d on u.id = d.user_id left JOIN auth_access_tokens t ON u.id = t.user_id;';
        $query = $this->db->query($sql);
        echo "Total records " . $query->num_rows() . "<br/>";
        if ($query->num_rows()) {
            $records = $query->result_array();
            echo "Total distinct user ids " . count(array_unique(array_column($records, 'id')));
//            echo "<pre>";
//            print_r($records);
//            echo "</pre>";
            $columns = array('registered_uuid', 'login_uuid', 'uuid');
            foreach ($records as $row) {
                if (empty($response_ary[$row['id']]['ios']) || empty($response_ary[$row['id']]['android']) || !$response_ary[$row['id']]['ios'] || !$response_ary[$row['id']]['android']) {
                    foreach ($columns as $col) {
                        if (!empty($row[$col])) {
                            if (empty($response_ary[$row['id']]['ios']) && $this->check_device_platform($row[$col]) == 'ios') {
                                $response_ary[$row['id']]['ios'] = 1;
                            }
                            if (empty($response_ary[$row['id']]['android']) && $this->check_device_platform($row[$col]) == 'android') {
                                $response_ary[$row['id']]['android'] = 1;
                            }
                        }
                    }
                }
            }
        }
        echo "Total Users Processed : " . count($response_ary);
//        echo "<pre>";
//        print_r($response_ary);
//        echo "</pre>";


        /* PROCESS THE DATA */

        foreach ($response_ary as $key => $row) {
            $insert_data[] = array(
                'user_id' => $key,
                'platform' => isset($row['ios']) && $row['ios'] ? (isset($row['android']) && $row['android'] ? 'both' : 'ios') : ((isset($row['android']) && $row['android']) ? 'android' : NULL ),
                'date_created' => date('Y-m-d H:i:s')
            );
        }
        echo "***************************************";
        echo "<pre>";
        print_r($insert_data);
        echo "</pre>";
        die();
        while ($rows = array_splice($insert_data, 0, 50)) {
            $this->db->insert_batch('record_library', $rows);
        }
    }

    private function check_device_platform($uuid) {
        if (!empty($uuid)) {
            if (strlen($uuid) <= 16)
                return 'android';
            else
                return 'ios';
        }
        return "";
    }

    public function addOldUsersSubscription() {
        $sql = $this->db->query("SELECT id, activation_key_created_date FROM users WHERE id NOT IN (SELECT user_id from payment) AND NOT(activation_key IS NULL AND activation_key = '') AND is_deleted ='0' ");
        $records = $sql->result_array();
        foreach ($records as $row) {
            $insert_data[] = array(
                'user_id' => $row['id'],
                'payment_type' => 'lifetime',
                'payment_date' => isset($row['activation_key_created_date']) && $row['activation_key_created_date'] ? $row['activation_key_created_date'] : date('Y-m-d H:i:s'),
                'payment_mode' => 'oldUser'
            );
        }
        $result= $this->db->insert_batch('payment',$insert_data);
        print_r($result);
    }

}
