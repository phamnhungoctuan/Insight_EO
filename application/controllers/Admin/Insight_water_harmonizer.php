<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Insight_water_harmonizer extends MY_Controller {

    public $model = "harmonizer_model";

    public function __construct() {
        parent::__construct();
        $this->model = 'insight_water_harmonizer_model';
        $this->load->model('Admin/insight_water_harmonizer_model');
    }

    /**
     * @Name : index()
     * @Purpose : To show the listing of all Water Harmonizer App users to admin.
     * @Call from : When admin click on the H2O Harmonizer App Tab. 
     * @Functionality : Simply fetch the records from database, and set the data to the view.
     * @Receiver params : No receiver parameters passed.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 09 Feb 2017
     * @Modified :
     */
    public function index() {
        $this->load->helper('date');
        $this->load->library('encrypt');
        $data['pageTitle'] = "Insight H20 Harmonizer App Users";
        $data['type'] = $this->input->get('type') ? $this->input->get('type') : "";

        $keyword = $this->input->get('q') ? $this->input->get('q') : "";
        $data['q'] = $keyword = addslashes(trim($keyword));

        $where = $this->get_where_condition($data['q'], $data['type']);


        $data['page'] = $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results'] = $this->{$this->model}->get_users($where, ROWS_PER_PAGE, $start_rec);
        $data['total_rows'] = $this->{$this->model}->count_results($where);
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $start_rec, $data['total_rows'], base_url('Admin/insight_water_harmonizer/index'));

        $data['row_counter'] = $start_rec + 1;
        $this->load->view('insight_water_harmonizer/index', $data);
    }

    private function get_where_condition($complete_keyword = "", $type = "") {

        $where = "1";
        if (strlen($complete_keyword)) {
            $cols = array("email", "first_name", "last_name", "uuid", "activation_key");
            $keywordCondition = "0 ";
            $keywords = explode(" ", $complete_keyword);
            foreach ($cols as $col) {
                $keywordCondition .= " OR `$col` LIKE '%" . $complete_keyword . "%'";
                if (count($keywords) > 1) {
                    foreach ($keywords as $word) {
                        $keywordCondition .= " OR `$col` LIKE '%" . $word . "%'";
                    }
                }
            }
            $where .= " AND ($keywordCondition)";
        }


        switch ($type) {
            case 'activated': $where .=' AND `isActivated`="1"';
                break;
            case 'paid': $where .=' AND NOT (`activation_key`="" OR `activation_key` IS NULL)';
                break;
            case 'free': $where .=' AND (`activation_key`="" OR `activation_key` IS NULL)';
                break;
            case 'Android': $where .=' AND deviceType = "a"';
                break;
            case 'Iphone': $where .=' AND deviceType = "i"';
                break;
        }
        $where = ($where == '1') ? NULL : $where;
        return $where;
    }

    public function free_trial_devices() {
        $this->load->helper('date');
        $this->load->library('encrypt');
        $data['pageTitle'] = "Insight H20 Harmonizer Free Trial App Users";
        $data['type'] = $this->input->get('type') ? $this->input->get('type') : "";

        $keyword = $this->input->get('q') ? $this->input->get('q') : "";
        $data['q'] = $keyword = addslashes(trim($keyword));

        $where = $this->get_free_trial_where_condition($data['q'], $data['type']);


        $data['page'] = $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results'] = $this->{$this->model}->get_free_trial_devices($where, ROWS_PER_PAGE, $start_rec);
        $data['total_rows'] = $this->{$this->model}->count_free_trial_devices($where);
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $start_rec, $data['total_rows'], base_url('Admin/insight_water_harmonizer/free_trial_devices'));

        $data['row_counter'] = $start_rec + 1;
        $this->load->view('insight_water_harmonizer/free_trial_devices', $data);
    }

    private function get_free_trial_where_condition($complete_keyword = "", $type = "") {

        $where = "1";
        if (strlen($complete_keyword)) {
            $keywordCondition = "0 ";
            $keywords = explode(" ", $complete_keyword);
            $keywordCondition .= " OR `device_uuid` LIKE '%" . $complete_keyword . "%'";
            if (count($keywords) > 1) {
                foreach ($keywords as $word) {
                    $keywordCondition .= " OR `device_uuid` LIKE '%" . $word . "%'";
                }
            }
            $where .= " AND ($keywordCondition)";
        }
        switch ($type) {
            case 'activated': $where .=' AND `device_status`="activated"';
                break;
            case 'free': $where .=' AND `device_status`="free_trial"';
                break;
        }
        $where = ($where == '1') ? NULL : $where;
        return $where;
    }

    /**
     * @Name : user_add()
     * @Purpose : To show the add user_add form to admin.
     * @Call from : When admin clicks on add new user button.
     * @Functionality : renders the user_add view to the admin.
     * @Receiver params : No parameter passed.
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    public function user_add() {
        $this->load->library('form_validation');
        $data = array();
        $data['pageTitle'] = 'Add App User';
        $data['subPageTitle'] = '';
        $data['activation_key'] = $this->{$this->model}->generateKey("activation_key");
        $this->load->view('insight_water_harmonizer/user_add', $data);
    }

    /**
     * @Name : save_new_user()
     * @Purpose : To save the user detail in database when add new user form is submitted.
     * @Call from : Add new user form action calls this function.
     * @Functionality : save the new user record in the database
     * @Receiver params : Only post data is accepted to save in harmonizer table.
     * @Return params : Redirects to the listing page if user saved successfully. 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function save_new_user() {
        if ($this->input->post()) {
            $this->load->model($this->model);
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_add();
            } else {
                $data = $this->input->post();
                $data['date_created'] = date('Y-m-d H:i:s');
                if (isset($data['uuid'])) {
                    $data['deviceType'] = $this->set_deviceType($data['uuid']);
                }
                $return_insertId = $this->{$this->model}->save($data);
                if ($return_insertId) {
                    $this->notify_admin("new_user", $data);
                    $this->notify_user("new_user", $data);
                }
                redirect('Admin/insight_water_harmonizer/index');
            }
        }
    }

    /**
     * @Name : set_deviceType()
     * @Purpose : To set the Type of device
     * @Call from : Can be called from any controller file.
     * @Functionality : if length of deviceID is less than 16 characters then return Android else Iphone
     * @Receiver params : DeviceId
     * @Return params : Return the device Type initials (A or I)
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 6 July 2015
     * @Modified :
     */
    //---Function to set the commissions---
    private function set_deviceType($uuid) {
        if (empty($uuid) || is_null($uuid))
            return "";
        $deviceType = (strlen($uuid) <= 16) ? "a" : "i";
        return $deviceType;
    }

    /**
     * @Name : add_activation_key()
     * @Purpose : To show the  add_activation_key form to admin and save the activation key if admin clicks on add button
     * @Call from : When admin clicks on create key button.
     * @Functionality : renders the add_activation_key view to the admin.
     * @Receiver params : User id and random string i.e. key is passed
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    function add_activation_key($userId = NULL) {
        if (is_null($userId) || $userId < 1) {
            redirect('Admin/insight_water_harmonizer/index');
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Create Key';
        $data['subPageTitle'] = '';
        $data['id'] = $userId;
        $data['action'] = "Admin/insight_water_harmonizer/add_activation_key/" . $userId;

        if ($this->input->post('id')) {
            $this->form_validation->set_rules("activation_key", "Activation Key", "required");
            if ($this->form_validation->run() == FALSE) {
                $this->add_activation_key($this->input->post('id'));
            } else {
                $user_data = $this->input->post();
                $where['id'] = $data['id'];
                if ($this->{$this->model}->update($user_data, $where)) {
                    $user_details = $this->{$this->model}->get_user_detail(array('id' => $user_data['id']));
                    $this->notify_admin("activation_key", $user_details);
                    $this->notify_user("activation_key", $user_details);
                }
                redirect("Admin/insight_water_harmonizer/index");
            }
        }
        $data['activation_key'] = $this->{$this->model}->generateKey("activation_key");
        $this->load->view('insight_water_harmonizer/add_activation_key', $data);
    }

    public function user_detail($id = NULL) {
        if (is_null($id)) {
            redirect('Admin/insight_water_harmonizer/index');
        }
        $this->load->library(array('encrypt', 'form_validation'));
        $where['id'] = $id;
        $data['user_data'] = $this->{$this->model}->get_user_detail($where);
        $this->load->view("insight_water_harmonizer/user_detail", $data);
    }

    /**
     * @Name : edit_user()
     * @Purpose : To edit the user details by admin.
     * @Call from : When admin clicks on update link on the edit user form.
     * @Functionality : Updates the user details.
     * @Receiver params :  Only post data is accepted to update in rife_codes table.
     * @Return params : No parameters returned.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function edit_user() {
        if ($this->input->post()) {
            $this->load->model($this->model);

            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_detail($this->input->post('id'));
            } else {
                $data = $this->input->post();

                if (isset($data['uuid'])) {
                    $data['deviceType'] = $this->set_deviceType($data['uuid']);
                }
                $where['id'] = $data['id'];
                $this->{$this->model}->update($data, $where);
                redirect('Admin/insight_water_harmonizer/index');
            }
        }
    }

    /**
     * @Name : delete_user()
     * @Purpose : To delete the user by admin.
     * @Call from : When admin clicks on delete link from users listing page and ajax called to delete the user using this function.
     * @Functionality : permanently delete user from database  
     * @Receiver params : User_Id is passed with post data.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function delete_user() {
        $this->load->library('encrypt');
        $user_id = $this->input->post('user_id');
        $user_id = $this->encrypt->decode($user_id);
        $where = array('id' => $user_id);
        echo $return = $this->{$this->model}->delete_user($where);
    }
    
    /**
     * EMAIL App Activation key to the user
     * Use : controller/user(to send key) and aura_genie.js (to resend key)
     */
    public function send_activation_key($user_details = array()) {
        if (empty($user_details)) {
            $id = $this->input->post('id');
            $user_details = $this->{$this->model}->get_user_detail(array('id' => $id));
        }
        $response['status'] = FALSE;
        if (count($user_details)) {
            if ($this->notify_user("activation_key", $user_details)) {
                $response['status'] = TRUE;
            }
        }
        if ($this->input->is_ajax_request()) {
            echo json_encode($response);
        }
        return $response;
    }

    /**
     * @Name : block_device()
     * @Purpose : To block the device by admin.
     * @Call from : When admin clicks on block button from users listing page and ajax called to block the device using this function.
     * @Functionality :block user device  
     * @Receiver params : Id is passed with post data.
     * @Return params : No parameters returned 
     */
    function block_device() {
        $this->load->library('encrypt');
        $user_id = $this->input->post('id');
        $where = array('id' => $user_id);
        $set = array('uuid_is_blocked' => '1');
        if ($this->{$this->model}->update($set, $where)) {
            $user_details = $this->{$this->model}->get_user_detail(array('id' => $user_id));
            $this->notify_admin("block_uuid", $user_details);
            $this->notify_user("block_uuid", $user_details);
            echo 1;
        }
    }

    /**
     * @Name : unblock_device()
     * @Purpose : To unblock the the blocked device by admin.
     * @Call from : When admin clicks on unblock button from users listing page and ajax called to unblock the device using this function.
     * @Functionality :unblock user device  
     * @Receiver params : Id is passed with post data.
     * @Return params : No parameters returned 
     */
    function unblock_device() {
        $this->load->library('encrypt');
        $user_id = $this->input->post('id');
        $where = array('id' => $user_id);
        $set = array('uuid_is_blocked' => '0');
        if ($this->{$this->model}->update($set, $where)) {
            $user_details = $this->{$this->model}->get_user_detail(array('id' => $user_id));
            $this->notify_admin("unblock_uuid", $user_details);
            $this->notify_user("unblock_uuid", $user_details);
            echo 1;
        }
    }

    function notify_admin($about = '', $user_detail = array()) {
        if ($about == '' || count($user_detail) < 0 || !isset($user_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch ($about) {
            case 'new_user' :
                $subject = 'Insight Water Harmonizer New User Notification';
                $view = 'insight_water_harmonizer/admin_email_format/new_user_email';
                break;
            case 'activation_key' :
                $subject = 'Insight Water Harmonizer Activation Key Notification';
                $view = 'insight_water_harmonizer/admin_email_format/acivation_key_email';
                break;
            case 'block_uuid' :
                $subject = 'Device blocked for Insight Water Harmonizer App';
                $view = 'insight_water_harmonizer/admin_email_format/block_uuid';
                break;
            case 'unblock_uuid' :
                $subject = 'Device unblocked for Insight Water Harmonizer App';
                $view = 'insight_water_harmonizer/admin_email_format/unblock_uuid';
                break;
        }
        $user_detail['name'] = trim($user_detail['first_name'] . " " . $user_detail['last_name']);
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = admin_email;
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view, array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function notify_user($about = '', $user_detail = array()) {
        if ($about == '' || count($user_detail) < 0 || !isset($user_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch ($about) {
            case 'new_user' :
                $subject = 'Your Insight Water Harmonizer App Download Link';
                $view = 'insight_water_harmonizer/user_email_format/new_user_email';
                break;
            case 'activation_key' :
                $subject = 'Insight Water Harmonizer App Activation Key';
                $view = 'insight_water_harmonizer/user_email_format/new_user_email';
                break;
//            case 'activation_key' :
//                $subject = 'Insight Water Harmonizer App Activation Key';
//                $view = 'insight_water_harmonizer/user_email_format/acivation_key_email';
//                break;
            case 'block_uuid' :
                $subject = 'Your device has been blocked for Insight Water Harmonizer App';
                $view = 'insight_water_harmonizer/user_email_format/block_uuid';
                break;
            case 'unblock_uuid' :
                $subject = 'Your device is now unblocked for Insight Water Harmonizer App';
                $view = 'insight_water_harmonizer/user_email_format/unblock_uuid';
                break;
        }
        /* SEND EMAIL TO ADMIN */

        $user_detail['name'] = trim($user_detail['first_name'] . " " . $user_detail['last_name']);
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $user_detail['email'];
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view, array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

/* End of file harmonizer.php */
/* Location: ./application/controllers/harmonizer.php */