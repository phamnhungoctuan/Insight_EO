<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quantum_ilife extends MY_Controller {

    public $model = "aura_model";

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/aura
     * 	- or -  
     * 		http://example.com/index.php/aura/index
     * 	- or -
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/aura/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->model = 'quantum_ilife_model';
        $this->load->model('Admin/quantum_ilife_model');
    }

    /**
     * @Name : index()
     * @Purpose : To show the listing of all Quantum iLife/iNfinity App users to admin.
     * @Call from : When admin click on the Quantum iLife/iNfinity App Tab. 
     * @Functionality : Simply fetch the records from database, and set the data to the view.
     * @Receiver params : No receiver parameters passed.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    public function index() {
        $where = '1';
        $this->load->library('encrypt');
        $this->load->helper('date');
        $get = $this->uri->uri_to_assoc();
        $type = isset($get['type']) ? $get['type'] : "";
        $type = $this->input->get('type') ? $this->input->get('type') : $type;
        $keyword = $this->input->get('q') ? $this->input->get('q') : "";
        $data['pageTitle'] = "Quantum iLife/iNfinity Users";
        if ($keyword) {
            $data['q'] = $keyword;
            $where .= " AND (`email` LIKE '%" . $keyword . "%' or `txnid` LIKE '%" . $keyword . "%' or `last_name` LIKE '%" . $keyword . "' or concat_ws(' ',txnid,last_name) LIKE '%" . $keyword . "%')";
        }
        if ($type != "") {
            $data['type'] = $type;
            switch ($type) {
                case 'activated':
                    $where .= ' AND `isActivated`="1"';
                    break;
                case 'paid':
                    $where .= ' AND NOT(`Key`="" OR `Key` IS NULL) AND `UUID`!=""';
                    break;
                case 'free':
                    $where .= ' AND (`Key`="" OR `Key` IS NULL) ';
                    break;
                case 'Android':
                    $where .= ' AND deviceType="A"';
                    break;
                case 'Iphone':
                    $where .= ' AND deviceType="I"';
                    break;
            }
        }
        $where = $where == '1' ? NULL : $where;
        $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results'] = $this->{$this->model}->get_users($where, ROWS_PER_PAGE, $start_rec);
        $data['total_rows'] = $this->{$this->model}->count_results($where);
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $start_rec, $data['total_rows'], base_url('Admin/quantum_ilife/index'));

        /* GET THE DISTRIBUTORS LIST */
        $this->load->model("distributors_model");
        $data['distributors_data'] = $this->distributors_model->get_distributors();

        $data['row_counter'] = $start_rec + 1;
        $this->load->view('quantum_ilife/index', $data);
    }

    public function set_distributor(){
        if ($this->input->post()) {
            $this->load->library('encrypt');
            $data['distributor_id']= $this->input->post('distributor_id');
            $where['id']=$this->encrypt->decode($this->input->post('user_id'));
            echo $this->{$this->model}->update_user($data,$where);
        }
    }
    /**
     * @Name : user_detail()
     * @Purpose : To render a view for admin to display the details of the selected User
     * @Call from : Can be called from any controller file.
     * @Functionality : to fetch the record from the database and render a veiw
     * @Receiver params : id of the record 
     * @Return params : return details of the selected user  
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 11 August 2015
     * @Modified :
     */
    public function user_detail($id) {
        $this->load->library(array('encrypt','form_validation'));
        $this->load->helper('date');
        $where['id'] = $id;
        $return = $this->{$this->model}->get_user_detail($where);
        $data['user_data']=$return;
        /*fetch distributors list*/
        $this->load->model("distributors_model");
        $data['distributors_data']=$this->distributors_model->fetch_records();
        $this->load->view("quantum_ilife/user_detail",$data);
    }
    /**
     * @Name : user_add()
     * @Purpose : To show the add user_add form to admin.
     * @Call from : When admin clicks on add new user button.
     * @Functionality : renders the user_add view to the admin.
     * @Receiver params : No parameter passed.
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    public function user_add() {
        $this->load->library('form_validation');
        $data = array();
        $data['pageTitle'] = '';
        $data['subPageTitle'] = '';
        //---generate key----
        $data['Key'] = $this->{$this->model}->generateKey("Key");
        //--get distributors list--------
        $this->load->model("distributors_model");
        $data['distributors_data'] = $this->distributors_model->fetch_records();
        $this->load->view('quantum_ilife/user_add', $data);
    }

    /**
     * @Name : save_new_user()
     * @Purpose : To save the user detail in database when add new user form is submitted.
     * @Call from : Add new user form action calls this function.
     * @Functionality : save the new user record in the database
     * @Receiver params : Only post data is accepted to save in new_payments table.
     * @Return params : Redirects to the listing page if user saved successfully. 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function save_new_user() {
        if ($this->input->post()) {
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_add();
            } else {
                $data = $this->input->post();
                $data['createdtime'] = date('Y-m-d H:i:s');
                $data['key_created_from'] = "dev_server";
                if (isset($data['payment_amount'])) {
                    $data['distributor_commissions'] = $this->set_Commision($data['payment_amount'], 40, 0);
                    $data['affiliate_commissions'] = $this->set_Commision($data['payment_amount'], 20, 0);
                }
                if (isset($data['UUID'])) {
                    $data['deviceType'] = $this->set_deviceType($data['UUID']);
                }
                $return_insertId = $this->{$this->model}->add_user($data);
                if ($return_insertId) {
                    $this->notify_admin("new_user",$this->input->post());
                    $this->notify_user("new_user",$this->input->post());
                } else {
                    mail("hardeep.intersoft@gmail.com", 'Quantum iLife/iNfinity App Notification', "Failure to send the email notification becuase of error in insertion");
                }
                redirect('Admin/quantum_ilife/index');
            }
        }
    }
     function check_email() {
        $response = 0;
        if ($this->input->post()) {
            $where['`email`'] = $this->input->post("email");
            if ($this->input->post('id')) {
                $where['id !='] = $this->input->post('id');
            }
            $this->load->model($this->model);
            $result = $this->{$this->model}->count_results($where);
            if ($result) {
                $response = "1";
            }
        }
        echo $response;
    }

    /**
     * @Name : user_edit()
     * @Purpose : To edit rife App user by admin.
     * @Call from : When admin clicks on 'edit' link.
     * @Functionality : Renders the user_edit view to edit user.
     * @Receiver params : User Id of the user to edit is passed.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */

    /**
     * @Name : edit_user()
     * @Purpose : To edit the user details by admin.
     * @Call from : When admin clicks on update link on the edit user form.
     * @Functionality : Updates the user details.
     * @Receiver params :  Only post data is accepted to update in new_payments table.
     * @Return params : No parameters returned.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function edit_user() {
        if ($this->input->post()) {
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_edit($this->input->post('id'));
            } else {
                $data = $this->input->post();
                if (isset($data['payment_amount'])) {
                    $data['distributor_commissions'] = $this->set_Commision($data['payment_amount'], 40, $data['distributor_commissions']);
                    $data['affiliate_commissions'] = $this->set_Commision($data['payment_amount'], 20, $data['affiliate_commissions']);
                }
                if (isset($data['UUID'])) {
                    $data['deviceType'] = $this->set_deviceType($data['UUID']);
                }
//                $data['Key'] = $this->{$this->model}->hash_this($data['Key']);
                $where['id'] = $data['id'];
                $distributor_id = isset($data['distributor_id']) ? $data['distributor_id'] : "0";
                $this->{$this->model}->update_user($data, $where);
                if ($distributor_id) {
                    $this->notify_distributor($distributor_id, $data['id']);
                }
                redirect('Admin/quantum_ilife/index');
            }
        }
    }

    /**
     * @Name : delete_user()
     * @Purpose : To delete the user by admin.
     * @Call from : When admin clicks on delete link from users listing page and ajax called to delete the user using this function.
     * @Functionality : permanently delete user from database  
     * @Receiver params : User_Id is passed with post data.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function delete_user() {
        $this->load->library('encrypt');
        $user_id = $this->input->post('user_id');
        $user_id = $this->encrypt->decode($user_id);
        $where = array('id' => $user_id);
        echo $return = $this->{$this->model}->delete_user($where);
    }

     /**
     * @Name : add_activation_key()
     * @Purpose : To show the  add_activation_key form to admin and save the activation key if admin clicks on add button
     * @Call from : When admin clicks on create key button.
     * @Functionality : renders the add_activation_key view to the admin.
     * @Receiver params : User id and random string i.e. key is passed
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    function add_activation_key($userId = NULL) {
        if (is_null($userId) || $userId < 1) {
            redirect('Admin/quantum_ilife/index');
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Create Key';
        $data['subPageTitle'] = '';
        $data['id'] = $userId;
        $data['action'] = "Admin/quantum_ilife/add_activation_key/" . $userId;
        $data['activation_key'] = $this->{$this->model}->generateKey("Key");
        if ($this->input->post('id')) {
            $this->form_validation->set_rules("Key", "Activation Key", "required");
            if ($this->form_validation->run() == FALSE) {
                $this->add_activation_key($this->input->post('id'));
            } else {
                $user_data = $this->input->post();
                $user_data['key_created_from'] = "dev_server";
                $where['id'] = $data['id'];
                if ($this->{$this->model}->update_user($user_data, $where)) {
                    $this->load->model($this->model);
                    $user_details = $this->{$this->model}->get_user_detail(array('id' => $user_data['id']));
                    $this->notify_admin("activation_key", $user_details);
                    $this->notify_user("activation_key",$user_details);
                } else {
                    mail("hardeep.intersoft@gmail.com", 'Quantum Error Notification', "Failure to send the email notification becuae of error in insertion MODEL " . $this->router->class);
                }
                redirect("Admin/quantum_ilife/index");
            }
        }
        $this->load->view('quantum_ilife/add_activation_key', $data);
    }
    
    /**
     * EMAIL App Activation key to the user
     * Use : controller/user(to send key) and aura_genie.js (to resend key)
     */
    public function send_activation_key($user_details = array()) {
        if (empty($user_details)) {
            $id = $this->input->post('id');
            $user_details = $this->{$this->model}->get_user_detail(array('id' => $id));
        }
        $response['status'] = FALSE;
        if (count($user_details)) {
            if ($this->notify_user("activation_key", $user_details)) {
                $response['status'] = TRUE;
            }
        }
        if ($this->input->is_ajax_request()) {
            echo json_encode($response);
        }
        return $response;
    }

    /**
     * @Name : add_3d_activation_key()
     * @Purpose : To show the  Create 3D-Animated Videos Activation Key form to admin.
     * @Call from : When admin clicks on create new key in 3d-Video Activation Key column s.
     * @Functionality : renders the "creating Key" view to the admin.
     * @Receiver params : userId of the user
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 31 August, 2015
     * @Modified :
     */
    function add_3d_activation_key($userId = NULL) {
        if (is_null($userId) || $userId < 1) {
            redirect('Admin/quantum_ilife/index');
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Create 3D-Animated Videos Activation Key';
        $data['subPageTitle'] = '';
        $data['id'] = $userId;
        $data['action'] = "Admin/quantum_ilife/add_3d_activation_key/" . $userId;
        $data['Key'] = $this->{$this->model}->generateKey("video_activation_key", 'users');
        if ($this->input->post('id')) {
            $this->form_validation->set_rules("video_activation_key", "Key", "required");
            if ($this->form_validation->run() == FALSE) {
                $this->add_3d_activation_key($this->input->post('id'));
            } else {
                $user_data = array(
                    'video_activation_status' => 'created',
                    'videoActivation_createdDate' => date('Y-m-d H:i:s'),
                    'video_activation_key' => $this->input->post('video_activation_key'),
                    'video_activation_created_from' => "dev_server"
                );
                $where['id'] = $this->input->post('id');
                if ($this->{$this->model}->update_user($user_data, $where)) {
                    $this->load->model($this->model);
                    $user_details = $this->{$this->model}->get_user_detail(array('id' => $this->input->post('id')));

                    $this->notify_admin('3d_activation_key', $user_details);
                    $this->notify_user('3d_activation_key',$user_details);
                } else {
                    mail("hardeep.intersoft@gmail.com", 'Quantum Error Notification', "Failure to send the email notification becuae of error in insertion MODEL " . $this->router->class);
                }
                redirect("Admin/quantum_ilife/index");
            }
        }
        $this->load->view('quantum_ilife/add_3d_activation_key', $data);
    }

    public function send_distributor_email() {
        if ($this->input->post()) {
            $this->load->library('encrypt');
            $this->load->model($this->model);
            $distributor_id = $this->input->post('distributor_id');
            $userid = $this->encrypt->decode($this->input->post('user_id'));
            echo $this->notify_distributor($distributor_id, $userid);
        }
    }

    private function notify_distributor($distributor_id, $user_id) {
        $this->load->model('distributors_model');
        $distributor = $this->distributors_model->get_distributor_detail(array('id' => $distributor_id));
        if ($distributor) {
            $user = $this->{$this->model}->get_user_detail(array('id' => $user_id), 'users');
            if ($user) {
                /* SEND EMAIL TO DISTRIBUTOR */
                $params = array(
                    'ContentType' => 'text/html',
                    'To' => $distributor['email'],
                    'Subject' => 'A Lead Is Generated',
                    'Body' => $this->load->view('quantum_ilife/admin_email_format/distributor_email', array('user_detail' => $user, 'distributor' => $distributor), TRUE)
                );
                return $this->{$this->model}->sendMail($params);
            }
        }
        return 0;
    }
    
    private function notify_admin($about = '', $user_detail = array()) {
        if ($about == '' || count($user_detail) < 0 || !isset($user_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch ($about) {
            case 'new_user' :
                $subject = 'Quantum iLife/iNfinity New User Notification';
                $view = 'quantum_ilife/admin_email_format/new_user_email';
                break;
            case 'activation_key' :
                $subject = 'Quantum iLife/iNfinity Activation Key Notification';
                $view = 'quantum_ilife/admin_email_format/acivation_key_email';
                break;
            case '3d_activation_key' :
                $subject = 'Quantum iLife/iNfinity 3D Video Activation Key Notification';
                $view = 'quantum_ilife/admin_email_format/3d_acivation_key_email';
                break;
        }
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = admin_email;
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view, array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    private function notify_user($about = '', $user_detail = array()) {
        if ($about == '' || count($user_detail) < 0 || !isset($user_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch ($about) {
            case 'new_user' :
                $subject = 'Quantum iLife/iNfinity : Download & Activation Codes';
                $view = 'quantum_ilife/user_email_format/new_user_email';
                break;
            case 'activation_key' :
                $subject = 'Your Quantum iLife/iNfinity Activation Code';
                $view = 'quantum_ilife/user_email_format/new_user_email';
                break;
//            case 'activation_key' :
//                $subject = 'Your Quantum iLife/iNfinity Activation Code';
//                $view = 'quantum_ilife/user_email_format/acivation_key_email';
//                break;
            case '3d_activation_key' :
                $subject = 'Your Quantum iLife/iNfinity 3D Video Activation Code';
                $view = 'quantum_ilife/user_email_format/3d_acivation_key_email';
                break;
        }
        /* SEND EMAIL TO USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $user_detail['email'];
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view, array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    //---Function to set the commissions---
    private function set_Commision($amount, $percent_share, $commission = 0) {
        if ($amount == 0)
            return 0;
        if ($commission == 0 || empty($commission))
            return ($amount * $percent_share) / 100;
        return $commission;
    }
    function set_deviceType($uuid) {
		if(empty($uuid)|| is_null($uuid)) return "";
        $deviceType = (strlen($uuid) <= 16) ? "A" : "I";
        return $deviceType;
    }

}

/* End of file aura.php */
/* Location: ./application/controllers/aura.php */
