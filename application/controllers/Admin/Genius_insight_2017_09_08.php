<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Genius_insight extends MY_Controller {

    public $model = "user_model";

    public function __construct() {
        parent::__construct();

        $this->model = 'genius_insight_model';
        $this->load->model('Admin/genius_insight_model');
        $this->load->model('payment_model');
    }

    /**
     * @Name : index()
     * @Purpose : To show the listing of all Quantum iLife/iNfinity App users to admin.
     * @Call from : When admin click on the Quantum iLife/iNfinity App Tab. 
     * @Functionality : Simply fetch the records from database, and set the data to the view.
     * @Receiver params : No receiver parameters passed.
     * @Return params : No parameters returned
     */
    public function index() {
        $where = '1';
        $this->load->library('encrypt');
        $this->load->helper('date');
        $get = $this->uri->uri_to_assoc();
        $type = isset($get['type']) ? $get['type'] : "";
        $type = $this->input->get('type') ? $this->input->get('type') : $type;
        $keyword = $this->input->get('q') ? $this->input->get('q') : "";
        $data['pageTitle'] = "Energy ReMastered Users";
        if ($keyword) {
            $data['q'] = $keyword;
            $where .= " AND (`email` LIKE '%" . $keyword . "%' or `first_name` LIKE '%" . $keyword . "%' or `last_name` LIKE '%" . $keyword . "' or concat_ws(' ',first_name,last_name) LIKE '%" . $keyword . "%' or payment.payment_type = '" . $keyword . "')";
        }
        if ($type != "") {
            $data['type'] = $type;
            switch ($type) {
                case 'subscribed':
                    $where .= ' AND users.id= payment.user_id';
                    break;
                case 'free':
                    $where .= ' AND payment.payment_type IS NULL';
                    break;
                case 'app':
                    $where .= ' AND user_type="app"';
                    break;
                case 'facebook':
                    $where .= ' AND user_type="facebook"';
                    break;
            }
        }
        $where = $where == '1' ? NULL : $where;
        $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results'] = $this->{$this->model}->get_users($where, ROWS_PER_PAGE, $start_rec);
        $data['total_rows'] = $this->{$this->model}->count_results($where);
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $start_rec, $data['total_rows'], base_url('Admin/genius_insight/index'));

        /* GET THE DISTRIBUTORS LIST */
        $this->load->model("distributors_model");
        $data['distributors_data'] = $this->distributors_model->get_distributors();

        $data['row_counter'] = $start_rec + 1;
        $this->load->view('genius_insight/index', $data);
    }

    /**
     * @Name : user_add()
     * @Purpose : To show the add user_add form to admin.
     * @Call from : When admin clicks on add new user button.
     * @Functionality : renders the user_add view to the admin.
     * @Receiver params : No parameter passed.
     * @Return params : No parameter returned.
     */
    public function add() {
        $this->load->library('form_validation');
        $data = array();
        $data['pageTitle'] = '';
        $data['subPageTitle'] = '';
        //---generate key----
        $data['activation_key'] = $this->{$this->model}->generateKey("activation_key");

        //--get distributors list--------
        $this->load->model("distributors_model");
        $data['distributors_data'] = $this->distributors_model->get_distributors();
        $this->load->view('genius_insight/add', $data);
    }

    /**
     * @Name : save_new_user()
     * @Purpose : To save the user detail in database when add new user form is submitted.
     * @Call from : Add new user form action calls this function.
     * @Functionality : save the new user record in the database
     * @Receiver params : Only post data is accepted to save in new_payments table.
     * @Return params : Redirects to the listing page if user saved successfully. 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function save_new_user() {
        if ($this->input->post()) {

            if ($this->{$this->model}->validates() == FALSE) {
                $this->add();
            } else {
                $data = $this->input->post();
                $payment_type = $data['subscription'];
                unset($data['subscription']);
                unset($data['confirm_password']);
                $data['password'] = $this->{$this->model}->hash_this($data['password']);
                $data['date_created'] = date('Y-m-d H:i:s');
                $data['status'] = "1";
                $data['user_type'] = "admin_panel";
                $data['distributor_id'] = isset($data['distributor_id']) ? $data['distributor_id'] : 0;
                $return_insertId = $this->{$this->model}->save($data);
                if ($return_insertId) {
                    /* SAVE THE USER RECORD IN USER_INFUSIONSOFT_RECORD TABLE */
                    $this->load->model('user_infusionsoft_record_model');
                    $this->user_infusionsoft_record_model->save(array('user_id' => $return_insertId, 'date_created' => date('Y-m-d H:i:s')));
                    $this->notify_admin("new_user", $this->input->post());
                    $this->notify_user($this->input->post());
                    if ($data['distributor_id']) {
                        $this->layout = 'blank';
                        $this->load->theme($this->layout);
                        $this->notify_distributor($data['distributor_id'], $return_insertId);
                    }
                    /* SAVE THE USER SUBSCRIPTION PAYMENT DETAILS IN PAYMENT TABLE */
                    $payment_info = array();
                    $payment_info['user_id'] = $return_insertId;
                    $payment_info['payment_type'] = $payment_type;
                    $payment_info['payment_date'] = date('Y-m-d H:i:s');
                    if ($payment_type == 'monthly') {
                        $payment_info['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 month'));
                    } elseif ($payment_type == 'annually') {
                        $payment_info['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 year'));
                    }
                    $this->payment_model->savePayment($payment_info);
                } else {
                    mail("hardeep.intersoft@gmail.com", 'Error: Energy ReMastered App Notification', "Failure to send the email notification because of error in insertion");
                }
                redirect('Admin/genius_insight/index');
            }
        }
    }

    /**
     * @Name : check_email()
     * @Purpose : To check whether the email is registered with the application
     * @Call from : When admin clicks on Add new user button, an ajax call to this function to check whether the email already exist or not.
     * @Functionality : renders the add_key view to the admin.
     * @Receiver params : email  entered by the admin
     * @Return params : return 1 for success i.e. email exist or 0 for failure i.e email doesn't exist. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 23 July 2015
     * @Modified :
     */
    function check_email() {
        $response = 0;
        if ($this->input->post()) {
            $where['`email`'] = $this->input->post("email");
            if ($this->input->post('id')) {
                $where['id !='] = $this->input->post('id');
            }
            $this->load->model($this->model);
            $result = $this->{$this->model}->get_user_detail($where);
            if ($result) {
                $response = "1";
            }
        }
        echo $response;
    }

    /**
     * @Name : user_edit()
     * @Purpose : To edit rife App user by admin.
     * @Call from : When admin clicks on 'edit' link.
     * @Functionality : Renders the user_edit view to edit user.
     * @Receiver params : User Id of the user to edit is passed.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function edit($userId) {
        $this->load->library('form_validation');
        $data['pageTitle'] = '';
        $data['subPageTitle'] = '';
        /* fetch user data to edit */
        $where = array();
        $where['id'] = $userId;
        $this->load->model($this->model);
        $user_data = $this->{$this->model}->get_user_detail($where);

        $user_data['date_created'] = $this->format_date($user_data['date_created'], "Y-m-d");
        $user_data['otp_date_created'] = $this->format_date($user_data['otp_date_created'], "Y-m-d");
        $data['user_data'] = $user_data;
        //--get distributors list--------
        $this->load->model("distributors_model");
        $data['distributors_data'] = $this->distributors_model->get_distributors();
        $this->load->view('genius_insight/edit', $data);
    }

    /**
     * @Name : edit_user()
     * @Purpose : To edit the user details by admin.
     * @Call from : When admin clicks on update link on the edit user form.
     * @Functionality : Updates the user details.
     * @Receiver params :  Only post data is accepted to update in new_payments table.
     * @Return params : No parameters returned.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function edit_user() {
        if ($this->input->post()) {

            $data = $this->input->post();
            $where['id'] = $data['id'];
            unset($data['id']);
            unset($data['subcscription_check']);
            $data['date_updated'] = date("Y-m-d H:i:s");
            if (isset($data['subscription'])) {
                $paymentDetail = $this->payment_model->getPaymentDetail(['user_id' => $where['id']]);
                $payment_info = array();
                if (count($paymentDetail) > 0) {
                    $payment_info['payment_type'] = $data['subscription'];
                    $payment_info['payment_date'] = date('Y-m-d H:i:s');
                    $expiry_date = date('Y-m-d', strtotime($paymentDetail['expiry_date']));
                    if ($expiry_date >= date('Y-m-d')) {
                        if ($data['subscription'] == 'monthly') {
                            $payment_info['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 month', strtotime($paymentDetail['expiry_date'])));
                        } elseif ($data['subscription'] == 'annually') {
                            $payment_info['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($paymentDetail['expiry_date'])));
                        } else {
                            $payment_info['expiry_date'] = NULL;
                        }
                    } else {
                        if ($data['subscription'] == 'monthly') {
                            $payment_info['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 month'));
                        } elseif ($data['subscription'] == 'annually') {
                            $payment_info['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 year'));
                        } else {
                            $payment_info['expiry_date'] = NULL;
                        }
                    }
                    $this->payment_model->updateSubscription($payment_info, ['user_id' => $where['id']]);
                } else {
                    $payment_info['user_id'] = $where['id'];
                    $payment_info['payment_type'] = $data['subscription'];
                    $payment_info['payment_date'] = date('Y-m-d H:i:s');
                    if ($data['subscription'] == 'monthly') {
                        $payment_info['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 month'));
                    } elseif ($data['subscription'] == 'annually') {
                        $payment_info['expiry_date'] = date('Y-m-d H:i:s', strtotime('+1 year'));
                    }
                    $this->payment_model->savePayment($payment_info);
                }
                unset($data['subscription']);
            }
            $this->{$this->model}->update_user($data, $where);
            if (isset($data['distributor_id'])) {
                $this->layout = 'blank';
                $this->load->theme($this->layout);
                $this->notify_distributor($data['distributor_id'], $where['id']);
            }
            redirect('Admin/genius_insight/index');
        }
    }

    /**
     * @Name : delete_user()
     * @Purpose : To delete the user by admin.
     * @Call from : When admin clicks on delete link from users listing page and ajax called to delete the user using this function.
     * @Functionality : permanently delete user from database  
     * @Receiver params : User_Id is passed with post data.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function delete($user_id = NULL) {
        if ($this->input->post()) {
            $user_id = $this->input->post('id');
            if ($this->{$this->model}->delete_user($user_id)) {
                redirect('Admin/genius_insight/index');
            }
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Delete User';
        $data['subPageTitle'] = '';
        $this->load->view('genius_insight/delete', array('id' => $user_id));
    }

    /**
     * @Name : change_password()
     * @Purpose : Render the view to change the password of the selected user and process the parameter
     * @Call from : When admin clicks on change password link from users listing page and ajax called to chnage the password of the user using this function.
     * @Receiver params : User_id via Get to render the view  , and via POST -> id, new_password and confirm password
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 22 Dec 2016
     * @Modified : 
     */
    function change_password($user_id = NULL) {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $user_id = $this->input->post("id");
            $this->form_validation->set_rules("id", "User Id", "trim|required|numeric");
            $this->form_validation->set_rules("password", "Password", "trim|required|min_length[8]");
            $this->form_validation->set_rules("confirm_password", "Confirm Password", "trim|required|matches[password]");
            if ($this->form_validation->run() == FALSE) {
                $response_array['status'] = FALSE;
                $response_array['message'] = $this->form_validation->error_array();
            } else {
                $user_detail = $this->{$this->model}->get_user_detail(array('id' => $user_id));
                if ($user_detail) {
                    $this->{$this->model}->update_user(array('password' => $this->input->post('password')), array('id' => $this->input->post('id')));
                    $response_array['status'] = TRUE;
                    $response_array['message'] = "Password updated successfully";
                    /* SEND EMAIL TO USER */
                    $this->layout = 'blank';
                    $this->load->theme($this->layout);
                    /* SEND EMAIL TO ADMIN */
                    $mail_params['ContentType'] = "text/html";
                    $mail_params['To'] = $user_detail['email'];
                    $mail_params['Subject'] = "Energy ReMastered : New Password";
                    $mail_params['Body'] = $this->load->view('email_format/forgot_password_email', array('user' => $user_detail, 'password' => $this->input->post('password')), true);
                    if (!$this->{$this->model}->sendMail($mail_params)) {
                        $response_array['status'] = FALSE;
                        $response_array['server_error'] = "Email not sent to user.";
                    }
                }else{
                    $response_array['status'] = FALSE;
                    $response_array['server_error'] = "User not found";
                }
            }
            echo json_encode($response_array);
            die();
        }

        if (is_null($user_id)) {
            redirect('/');
        } else {
            $data['pageTitle'] = 'Change Password';
            $data['subPageTitle'] = '';
            $this->load->view('genius_insight/change_password', array('id' => $user_id));
        }
    }

    function list_devices($user_id = NULL) {
        if (!strlen($user_id)) {
            redirect('Admin/genius_insight/index');
        }
        $this->load->library('form_validation');
        $this->load->library('encrypt');
        $data['pageTitle'] = 'Registered Devices';
        $data['user_id'] = $user_id;
        $this->load->model('user_registered_device_model');
        $data['devices'] = $this->user_registered_device_model->get_devices(array('user_id' => $user_id));
        $this->load->view('genius_insight/list_devices', $data);
    }

    function add_device() {
        $this->load->library('form_validation');
        if ($this->input->post()) {
            $this->load->model('user_registered_device_model');
            if ($this->user_registered_device_model->validates()) {
                $save_data = $this->input->post();
                /* CHECK UUID IS ALREADY REGISTERED */
                $device_record = $this->user_registered_device_model->get_device_detail(array('user_id' => $save_data['user_id'], 'uuid' => $save_data['uuid']));
                if ($device_record) {
                    $result_array['status'] = 'failed';
                    $result_array['message'] = 'Device is already registered.';
                } else {
                    $save_data['registered_on'] = date('Y-m-d H:i:s');
                    if (empty($save_data['device_type'])) {
                        $save_data['device_type'] = strlen($save_data['uuid']) <= 16 ? 'android' : 'ios';
                    }
                    $return_id = $this->user_registered_device_model->add_device($save_data);
                    if ($return_id) {
                        /* SEND EMAIL TO USER */
                        $user = $this->{$this->model}->get_user_detail(array('id' => $save_data['user_id']));
                        $user = array_merge($user, $save_data);
                        $this->notify_admin('add_device', $user);
                        if ($this->notify_user_registered_device($user)) {
                            $result_array['status'] = 'success';
                            $result_array['message'] = 'Device Added Successfully';
                        } else {
                            $result_array['status'] = 'failed';
                            $result_array['message'] = 'Error sending email to user';
                        }
                    } else {
                        $result_array['status'] = 'failed';
                        $result_array['message'] = 'Error while inserting in the Database.';
                    }
                }
            } else {
                $result_array['status'] = 'failed';
                $result_array['message'] = 'Validation errors';
                $result_array['error'] = $this->form_validation->error_array();
            }
        } else {
            $result_array['status'] = 'failed';
            $result_array['message'] = 'No Data Recieved';
        }
        echo json_encode($result_array);
    }

    public function delete_registered_device() {
        if ($this->input->post('device_id')) {
            $this->load->model("user_registered_device_model");
            $return_val = $this->user_registered_device_model->delete_device($this->input->post('device_id'));
            echo $return_val ? '1' : '0';
            die();
        }
        echo '0';
    }

    /**
     * @Name : add_activation_key()
     * @Purpose : To show the  add_activation_key form to admin and save the activation key if admin clicks on add button
     * @Call from : When admin clicks on create key button.
     * @Functionality : renders the add_activation_key view to the admin.
     * @Receiver params : User id and random string i.e. key is passed
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    function add_activation_key($userId = NULL) {
        if (is_null($userId) || $userId < 1) {
            redirect('Admin/genius_insight/index');
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Create Key';
        $data['subPageTitle'] = '';
        $data['id'] = $userId;
        $data['action'] = "Admin/genius_insight/add_activation_key/" . $userId;
        $data['activation_key'] = $this->{$this->model}->generateKey("activation_key");
        if ($this->input->post('id')) {
            $this->form_validation->set_rules("activation_key", "Activation Key", "required");
            if ($this->form_validation->run() == FALSE) {
                $this->add_activation_key($this->input->post('id'));
            } else {
                $user_data = $this->input->post();
                $where['id'] = $data['id'];
                if ($this->{$this->model}->update($user_data, $where)) {
                    $this->load->model($this->model);
                    $user_details = $this->{$this->model}->get_user_detail(array('id' => $user_data['id']));
                    $this->notify_admin("activation_key", $user_details);
                    $this->send_activation_key($user_details);
                }
                redirect("Admin/genius_insight/index");
            }
        }
        $this->load->view('genius_insight/add_activation_key', $data);
    }

    /**
     * @Name : add_3d_activation_key()
     * @Purpose : To show the  Create 3D-Animated Videos Activation Key form to admin.
     * @Call from : When admin clicks on create new key in 3d-Video Activation Key column s.
     * @Functionality : renders the "creating Key" view to the admin.
     * @Receiver params : userId of the user
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 31 August, 2015
     * @Modified :
     */
    function add_3d_activation_key($userId = NULL) {
        if (is_null($userId) || $userId < 1) {
            redirect('Admin/genius_insight/index');
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Create 3D-Animated Videos Activation Key';
        $data['subPageTitle'] = '';
        $data['id'] = $userId;
        $data['action'] = "Admin/genius_insight/add_3d_activation_key/" . $userId;
        $data['Key'] = $this->{$this->model}->generateKey("3d_activation_key");
        if ($this->input->post('id')) {
            $this->form_validation->set_rules("3d_activation_key", "Key", "required");
            if ($this->form_validation->run() == FALSE) {
                $this->add_3d_activation_key($this->input->post('id'));
            } else {
                $user_data = array(
                    '3d_activation_status' => 'created',
                    '3d_activation_createdDate' => date('Y-m-d H:i:s'),
                    '3d_activation_key' => $this->input->post('3d_activation_key'),
                );
                $where['id'] = $this->input->post('id');
                if ($this->{$this->model}->update($user_data, $where)) {
                    $this->load->model($this->model);
                    $user_details = $this->{$this->model}->get_user_detail(array('id' => $this->input->post('id')));

                    $this->notify_admin('3d_activation_key', $user_details);
                    $this->send_3d_activation_key($user_details);
                } else {
                    mail("hardeep.intersoft@gmail.com", 'Quantum Error Notification', "Failure to send the email notification becuae of error in insertion MODEL " . $this->router->class);
                }
                redirect("Admin/genius_insight/index");
            }
        }
        $this->load->view('genius_insight/add_3d_activation_key', $data);
    }

    /**
     * EMAIL App Activation key to the user
     * Use : controller/user(to send key) and aura_genie.js (to resend key)
     */
    public function send_activation_key($user_details = array()) {
        if (empty($user_details)) {
            $id = $this->input->post('id');
            $user_details = $this->{$this->model}->get_user_detail(array('id' => $id));
        }
        $response['status'] = FALSE;
        if (count($user_details)) {
            $this->layout = 'blank';
            $this->load->theme($this->layout);
            /* EMAIL OTP TO THE USER */
            $mail_params['ContentType'] = "text/html";
            $mail_params['To'] = $user_details['email'];
            $mail_params['Subject'] = 'Your Energy ReMastered Activation Code';
            $mail_params['Body'] = $this->load->view('email_format/acivation_key_email', $user_details, true);

            if ($this->{$this->model}->sendMail($mail_params)) {
                $response['status'] = TRUE;
            }
        }
        if ($this->input->is_ajax_request()) {
            echo json_encode($response);
        }
        return $response;
    }

    /* EMAIL 3D Video Activation key to the user */

    function send_3d_activation_key($user_details = array()) {
        if (empty($user_details)) {
            $id = $this->input->post('id');
            $user_details = $this->{$this->model}->get_user_detail(array('id' => $id));
        }
        $response['status'] = FALSE;
        if ($user_details) {
            $this->layout = 'blank';
            $this->load->theme($this->layout);
            /* EMAIL OTP TO THE USER */
            $mail_params['ContentType'] = "text/html";
            $mail_params['To'] = $user_details['email'];
            $mail_params['Subject'] = 'Your Energy ReMastered 3D Graphics Activation Code';
            $mail_params['Body'] = $this->load->view('email_format/3d_acivation_key_email', array('first_name' => $user_details['first_name'], 'video_activation_key' => $user_details['3d_activation_key']), true);
            if ($this->{$this->model}->sendMail($mail_params)) {
                $response['status'] = TRUE;
            }
        }
        if ($this->input->is_ajax_request()) {
            echo json_encode($response);
        }
        return $response;
    }

    /* Send Email TO user created by admin */

    function notify_user($user_detail = array()) {
        if (count($user_detail) < 0 || !isset($user_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        /* SEND EMAIL TO ADMIN */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $user_detail['email'];
        $mail_params['Subject'] = "Energy ReMastered : Download & Activation Codes";
        $mail_params['Body'] = $this->load->view('email_format/new_user_email', array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /* EMAIL the details of the new device registered in his/her account by the admin */

    private function notify_user_registered_device($user_detail = array()) {
        if (count($user_detail) < 0 || !isset($user_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        /* SEND EMAIL TO ADMIN */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $user_detail['email'];
        $mail_params['BCC'] = 'hardeep.intersoft@gmail.com';
        $mail_params['Subject'] = "Energy ReMastered New Device Registration Notification";
        $mail_params['Body'] = $this->load->view('email_format/add_device', array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /* @Name : set_distributor()
     * @Purpose : To update the distributor for the record.
     * @Call from : When admin selects the distributor from the drop down list on  users listing page and ajax called to set distributor using this function.
     * @Functionality : to update the distributor for the record.
     * @Receiver params : two post parameters :  distributor id and encrypted user Id 
     * @Return params : Return 1 incase of successfull updation
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 6 July 2015
     * @Modified :
     */

    public function set_distributor() {
        if ($this->input->post()) {
            $this->load->library('encrypt');
            $this->load->model($this->model);
            $data['distributor_id'] = $this->input->post('distributor_id');
            $where['id'] = $this->encrypt->decode($this->input->post('user_id'));
            echo $this->{$this->model}->update($data, $where);
        }
    }

    /**
     * @Name : send_distributor_email()
     * @Purpose : To send the email to distributor regarding the details of the user assigned to her.
     * @Call from : When admin clicks on sendEmail button on users listing page and ajax called to send Email to  distributor using this function.
     * @Functionality : to update the distributor for the record.
     * @Receiver params : two post parameters :  distributor id and encrypted user Id 
     * @Return params : Return 1 incase of success
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 25 July 2015
     * @Modified :
     */
    public function send_distributor_email() {
        if ($this->input->post()) {
            $this->load->library('encrypt');
            $this->load->model($this->model);
            $distributor_id = $this->input->post('distributor_id');
            $userid = $this->encrypt->decode($this->input->post('user_id'));
            echo $this->notify_distributor($distributor_id, $userid);
        }
    }

    private function notify_distributor($distributor_id, $user_id) {
        $this->load->model('distributors_model');
        $distributor = $this->distributors_model->get_distributor_detail(array('id' => $distributor_id));
        if ($distributor) {
            $user = $this->{$this->model}->get_user_detail(array('id' => $user_id), 'users');
            if ($user) {
                /* SEND EMAIL TO DISTRIBUTOR */
                $params = array(
                    'ContentType' => 'text/html',
                    'To' => $distributor['email'],
                    'Subject' => 'A Lead Is Generated',
                    'Body' => $this->load->view('email_format/admin/distributor_email', array('user_detail' => $user, 'distributor' => $distributor), TRUE)
                );
                return $this->{$this->model}->sendMail($params);
            }
        }
        return 0;
    }

    function notify_admin($about = '', $user_detail = array()) {
        if ($about == '' || count($user_detail) < 0 || !isset($user_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch ($about) {
            case 'new_user' :
                $subject = 'Energy ReMastered New User Notification';
                $view = 'email_format/admin/new_user_email';
                break;
            case 'activation_key' :
                $subject = 'Energy ReMastered Activation Key Notification';
                $view = 'email_format/admin/acivation_key_email';
                break;
            case '3d_activation_key' :
                $subject = 'Energy ReMastered 3D Video Activation Key Notification';
                $view = 'email_format/admin/3d_acivation_key_email';
                break;
            case 'add_device' :
                $subject = 'Energy ReMastered Device Registration Notification';
                $view = 'email_format/admin/add_device';
                break;
        }
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = admin_email;
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view, array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

/* End of file aura.php */
/* Location: ./application/controllers/aura.php */