<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_user extends MY_Controller {

    public $model = "admin_user_model";

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/admin
     * 	- or -  
     * 		http://example.com/index.php/admin/index
     * 	- or -
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/admin/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->model = 'admin_user_model';
        $this->load->library('form_validation');
        $this->load->model('Admin/admin_user_model');
    }

    /**
     * @Name : index()
     * @Purpose : To show the listing of all Admin users to admin.
     * @Call from : When admin Login 
     * @Functionality : Simply fetch the records from database, and set the data to the view.
     * @Receiver params : No receiver parameters passed.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 03 July 2015
     * @Modified :
     */
    public function index() {
        $this->load->library('encrypt');
        $data['pageTitle'] = "Administrators";
        $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results'] = $this->{$this->model}->get_users();
        $data['total_rows'] = $this->{$this->model}->count_results();
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $start_rec, $data['total_rows'], base_url('Admin/admin_users/index'));
        $this->load->view('admin_user/index', $data);
    }

    /**
     * @Name : admin_add()
     * @Purpose : To show the add admin_add form to admin.
     * @Call from : When admin clicks on add new user button.
     * @Functionality : renders the adminUser_add view to the admin.
     * @Receiver params : No parameter passed.
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 03 July 2015
     * @Modified :
     */
    public function add() {
        $data = array();
        $data['pageTitle'] = 'Add Administrator';
        $data['subPageTitle'] = '';
        $this->load->view('admin_user/add');
    }

    /**
     * @Name : save_new_user()
     * @Purpose : To save the user detail in database when add new user form is submitted.
     * @Call from : Add new user form action calls this function.
     * @Functionality : renders the admin_add view to the admin.
     * @Receiver params : Only post data is accepted to save in users table.
     * @Return params : Redirects to the listing page if user saved successfully. 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 03 July 2015
     * @Modified : 
     */
    function save_new_user() {
        if ($this->input->post()) {
            if ($this->{$this->model}->validates() == FALSE) {
                $this->admin_add();
            } else {
                $data = $this->input->post();
                unset($data['confirm_password']);
                $data['Password'] = $this->{$this->model}->hash_this($data['Password']);
                $data['CreateDate'] = date('Y-m-d H:i:s');
                $this->{$this->model}->save($data);
                redirect('Admin/admin_user/index');
            }
        }
    }

    /**
     * @Name : admin_edit()
     * @Purpose : To edit admin user by admin.
     * @Call from : When admin clicks on 'edit' link.
     * @Functionality : Renders the admin_edit view to edit project.
     * @Receiver params : User Id of the user to edit is passed.
     * @Return params : No parameters returned 
     * @Created : sonia <hardeep.intersoft@gmail.com> on 03 July 2015
     * @Modified : 
     */
    function edit($userId) {
        $this->load->library('encrypt');
        $data['pageTitle'] = 'Edit Admininstrator';
        $data['subPageTitle'] = '';
        $user_data = $this->{$this->model}->get_admin_detail(array('UserId' => $userId));
        $data['user_data'] = $user_data;
        $this->load->view('admin_user/edit', $data);
    }

    /**
     * @Name : edit_user()
     * @Purpose : To edit the user details by admin.
     * @Call from : When admin clicks on update link on the edit user form.
     * @Functionality : Updates the user details .
     * @Receiver params : Only post data is accepted to update in users table.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 03 July 2015
     * @Modified : 
     */
    function edit_user() {
        if ($this->input->post()) {
            if ($this->{$this->model}->validates() == FALSE) {
                $this->admin_edit($this->input->post('UserId'));
            } else {
                $data = $this->input->post();
                unset($data['confirm_password']);
                if (!empty($data['Password'])) {
                    $data['Password'] = $this->{$this->model}->hash_this($data['Password']);
                } else {
                    unset($data['Password']);
                }
                $data['UpdateDate'] = date('Y-m-d H:i:s');
                $where['UserId'] = $data['UserId'];
                unset($data['UserId']);
                $this->{$this->model}->update($data, $where);
                redirect('Admin/admin_user/index');
            }
        }
    }

    /**
     * @Name : delete_user()
     * @Purpose : To delete the user by admin.
     * @Call from : When admin clicks on delete link from admin users listing page and ajax called to delete the project using this function.
     * @Functionality : permanently delete admin user from database  
     * @Receiver params : User_Id is passed with post data.
     * @Return params : No parameters returned 
     * @Created : sonia <sonia.intersoft@gmail.com> on 11 June 2015
     * @Modified : sonia <sonia.intersoft@gmail.com> on 11 June 2015
     */
    function delete($user_id = NULL) {
        if ($this->input->post()) {
            $user_id = $this->input->post('id');
            if ($this->{$this->model}->delete_user($user_id)) {
                redirect('Admin/admin_user/index');
            }
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Delete User';
        $data['subPageTitle'] = '';
        $this->load->view('admin_user/delete', array('id' => $user_id));
    }

    /**
     * @Name : change_password()
     * @Purpose : To load chnage password view for the front end user.
     * @Call from : When user clicks on change password link
     * @Functionality : Set the required parameters and load the change password view.
     * @Receiver params : No receiver parameters passed.
     * @Return params : No parameters returned. 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 03 July 2015
     * @Modified : Hardeep Kaur <hardeep.intersoft@gmail.com> on 03 July 2015
     */
    function change_password() {
        $data = array();
        $data['pageTitle'] = 'Change Password';
        $data['subPageTitle'] = '';
        $data['results'] = array();
        $this->load->view('admin_user/change_password', $data);
    }

    /**
     * @Name : reset_password()
     * @Purpose : To change the old password with new password.
     * @Call from : When user submits the change password form.
     * @Functionality : .
     * @Receiver params : No receiver parameters passed.
     * @Return params : No parameters returned. 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 03 July 2015
     * @Modified : Hardeep Kaur <hardeep.intersoft@gmail.com> on 03 July 2015
     */
    function reset_password() {
        $data = array();
        $data['pageTitle'] = 'Change Password';
        $data['subPageTitle'] = '';
        $data['results'] = array();
        $this->load->library("user_agent");
        if (($this->session->userdata('Password') == $this->{$this->model}->hash_this($this->input->post('old_password'))) && ($this->input->post('new_password') == $this->input->post('confirm_password'))) {
            $where['UserId'] = $this->session->userdata("UserId");
            $update_data['Password'] = $this->{$this->model}->hash_this($this->input->post('new_password'));
            $result = $this->{$this->model}->update($update_data, $where);
            if ($result) {
                $this->session->set_flashdata('message', 'Password has been changed successfully.');
                $this->session->set_flashdata('type', 'success');
                redirect('authentication/logout');
            } else {
                $this->session->set_flashdata('message', 'Password coundn\'t be changed. Please, try again.');
                $this->session->set_flashdata('type', 'danger');
                redirect($this->agent->referrer());
            }
        } else {
            $this->session->set_flashdata('message', 'Password coundn\'t be changed. Please, try again.');
            $this->session->set_flashdata('type', 'danger');
            redirect($this->agent->referrer());
        }
    }

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */