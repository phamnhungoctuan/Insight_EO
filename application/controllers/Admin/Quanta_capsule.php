<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quanta_Capsule extends MY_Controller {

    public $model = "quanta_capsule_model";

    public function __construct() {
        parent::__construct();
        $this->model = 'quanta_capsule_model';
        $this->load->model('Admin/quanta_capsule_model');
    }

    /**
     * @Name : index()
     * @Purpose : To show the listing of all quanta_capsule App users to admin.
     * @Call from : When admin click on the quanta_capsule App Tab. 
     * @Functionality : Simply fetch the records from database, and set the data to the view.
     * @Receiver params : No receiver parameters passed.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    public function index() {
        $this->load->library('encrypt');
        $data['pageTitle'] = "Quanta Capsule App Users";
        $keywordCondition = NULL;
        $keyword = $this->input->get('q') ? $this->input->get('q') : "";
        $keyword = addslashes(trim($keyword));
        $data['q'] = $keyword;
        if (strlen($keyword)) {
            $keywordCondition = "`email` LIKE '%" . $keyword . "%' OR `name` LIKE '%" . $keyword . "%'";
            $keyword = explode(" ", $keyword);
            if (count($keyword) > 1) {
                foreach ($keyword as $word) {
                    $keywordCondition .= " OR `email` LIKE '%" . $word . "%' OR `name` LIKE '%" . $word . "%'";
                }
            }
        }
        $data['page'] = $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results'] = $this->{$this->model}->get_users($keywordCondition, ROWS_PER_PAGE, $start_rec);
        $data['total_rows'] = $this->{$this->model}->count_results($keywordCondition);
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $data['total_rows'], base_url('Admin/quanta_capsule/index'));
        $data['row_counter'] = $start_rec + 1;
        $this->load->view('quanta_capsule/index', $data);
    }

    /**
     * @Name : user_add()
     * @Purpose : To show the add user_add form to admin.
     * @Call from : When admin clicks on add new user button.
     * @Functionality : renders the user_add view to the admin.
     * @Receiver params : No parameter passed.
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    public function user_add() {
        $this->load->library('form_validation');
        $data = array();
        $data['pageTitle'] = 'Add App User';
        $data['subPageTitle'] = '';
        $data['Key'] = $this->{$this->model}->generateKey("Key");
        $this->load->view('quanta_capsule/user_add', $data);
    }

    /**
     * @Name : save_new_user()
     * @Purpose : To save the user detail in database when add new user form is submitted.
     * @Call from : Add new user form action calls this function.
     * @Functionality : save the new user record in the database
     * @Receiver params : Only post data is accepted to save in quantacapsule table.
     * @Return params : Redirects to the listing page if user saved successfully. 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function save_new_user() {
        if ($this->input->post()) {
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_add();
            } else {
                $data = $this->input->post();
                $data['createdtime'] = date('Y-m-d H:i:s');
                $data['user_created_from'] = "dev_server";
                $data['key_created_from'] = "dev_server";
                $return_insertId = $this->{$this->model}->save($data);
                if ($return_insertId) {
                    $this->notify_admin("new_user", $data);
                    $this->notify_user("new_user", $data);
                }
                redirect('Admin/quanta_capsule/index');
            }
        }
    }

    /**
     * @Name : add_activation_key()
     * @Purpose : To show the  add_activation_key form to admin and save the activation key if admin clicks on add button
     * @Call from : When admin clicks on create key button.
     * @Functionality : renders the add_activation_key view to the admin.
     * @Receiver params : User id and random string i.e. key is passed
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    function add_activation_key($userId = NULL) {
        if (is_null($userId) || $userId < 1) {
            redirect('Admin/quanta_capsule/index');
        }
        $this->load->library('form_validation');
        $data['id'] = $userId;
        if ($this->input->post('id')) {
            $this->form_validation->set_rules("Key", "Activation Key", "required");
            if ($this->form_validation->run() == FALSE) {
                $this->add_activation_key($this->input->post('id'));
            } else {
                $user_data = $this->input->post();
                $user_data['key_created_from'] = "dev_server";
                $where['id'] = $data['id'];
                if ($this->{$this->model}->update($user_data, $where)) {
                    $user_details = $this->{$this->model}->get_user_detail($where);
                    $this->notify_admin("activation_key", $user_details);
                    $this->notify_user("activation_key", $user_details);
                }
                redirect("Admin/quanta_capsule/index");
            }
        }
        $data['pageTitle'] = 'Create Key';
        $data['subPageTitle'] = '';

        $data['action'] = "Admin/quanta_capsule/add_activation_key/" . $userId;
        $data['Key'] = $this->{$this->model}->generateKey("Key");
        $this->load->view('quanta_capsule/add_activation_key', $data);
    }
    
    /**
     * EMAIL App Activation key to the user
     * Use : controller/user(to send key) and aura_genie.js (to resend key)
     */
    public function send_activation_key($user_details = array()) {
        if (empty($user_details)) {
            $id = $this->input->post('id');
            $user_details = $this->{$this->model}->get_user_detail(array('id' => $id));
        }
        $response['status'] = FALSE;
        if (count($user_details)) {
            if ($this->notify_user("activation_key", $user_details)) {
                $response['status'] = TRUE;
            }
        }
        if ($this->input->is_ajax_request()) {
            echo json_encode($response);
        }
        return $response;
    }

    /**
     * @Name : user_edit()
     * @Purpose : To edit rife App user by admin.
     * @Call from : When admin clicks on 'edit' link.
     * @Functionality : Renders the user_edit view to edit user.
     * @Receiver params : User Id of the user to edit is passed.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function user_edit($userId) {
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Edit User';
        $data['subPageTitle'] = '';
        $user_data = $this->{$this->model}->get_user_detail(array('id' => $userId));
        if ($user_data) {
            $user_data['ActivatedDate'] = $this->format_date($user_data['ActivatedDate'], "Y-m-d");
            $user_data['createdtime'] = $this->format_date($user_data['createdtime'], "Y-m-d");
            $user_data['payment_date'] = $this->format_date($user_data['payment_date'], "Y-m-d");
        }
        $data['user_data'] = $user_data;
        $this->load->view('quanta_capsule/user_edit', $data);
    }

    /**
     * @Name : edit_user()
     * @Purpose : To edit the user details by admin.
     * @Call from : When admin clicks on update link on the edit user form.
     * @Functionality : Updates the user details.
     * @Receiver params :  Only post data is accepted to update in quantacapsule table.
     * @Return params : No parameters returned.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function edit_user() {
        if ($this->input->post()) {
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_edit($this->input->post('id'));
            } else {
                $data = $this->input->post();
                $where['id'] = $data['id'];
                $this->{$this->model}->update($data, $where);
                redirect('Admin/quanta_capsule/index');
            }
        }
    }

    /**
     * @Name : delete_user()
     * @Purpose : To delete the user by admin.
     * @Call from : When admin clicks on delete link from users listing page and ajax called to delete the user using this function.
     * @Functionality : permanently delete user from database  
     * @Receiver params : User_Id is passed with post data.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function delete($user_id = NULL) {
        if ($this->input->post()) {
            $user_id = $this->input->post('id');
            if ($this->{$this->model}->delete(array('id' => $user_id))) {
                redirect('Admin/quanta_capsule/index');
            }
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Delete User';
        $data['subPageTitle'] = '';
        $this->load->view('quanta_capsule/delete', array('id' => $user_id));
    }

    function notify_admin($about = '', $user_detail = array()) {
        if ($about == '' || count($user_detail) < 0 || !isset($user_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch ($about) {
            case 'new_user' :
                $subject = 'Quanta Capsule New User Notification';
                $view = 'quanta_capsule/admin_email_format/new_user_email';
                break;
            case 'activation_key' :
                $subject = 'Quanta Capsule Activation Key Notification';
                $view = 'quanta_capsule/admin_email_format/acivation_key_email';
                break;
        }
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = admin_email;
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view, array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function notify_user($about = '', $user_detail = array()) {
        if ($about == '' || count($user_detail) < 0 || !isset($user_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch ($about) {
            case 'new_user' :
                $subject = 'Your Quanta Capsule Download Link';
                $view = 'quanta_capsule/user_email_format/new_user_email';
                break;
            case 'activation_key' :
                $subject = 'Quanta Capsule Activation Key';
                $view = 'quanta_capsule/user_email_format/new_user_email';
                break;
//            case 'activation_key' :
//                $subject = 'Quanta Capsule Activation Key';
//                $view = 'quanta_capsule/user_email_format/acivation_key_email';
//                break;
        }
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $user_detail['email'];
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view, array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

/* End of file quanta_capsule.php */
/* Location: ./application/controllers/quanta_capsule.php */