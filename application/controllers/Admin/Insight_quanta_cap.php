<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Insight_quanta_cap extends MY_Controller {

    public $model = "insight_quanta_cap_model";

    public function __construct() {
        parent::__construct();
        $this->model = 'insight_quanta_cap_model';
        $this->load->model('Admin/insight_quanta_cap_model');
    }

    /**
     * @Name : index()
     * @Purpose : To show the listing of all insight_quanta_cap App users to admin.
     * @Call from : When admin click on the insight_quanta_cap App Tab. 
     * @Functionality : Simply fetch the records from database, and set the data to the view.
     * @Receiver params : No receiver parameters passed.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    public function index() {
        $this->load->library('encrypt');
        $data['pageTitle'] = "Insight Quanta Cap App Users";
        $data['type'] = $this->input->get('type') ? $this->input->get('type') : "";
        $keyword = $this->input->get('q') ? $this->input->get('q') : "";
        $data['q'] = $keyword = addslashes(trim($keyword));

        $where = $this->get_where_condition($data['q'], $data['type']);
        $data['page'] = $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results'] = $this->{$this->model}->get_users($where, ROWS_PER_PAGE, $start_rec);
        $data['total_rows'] = $this->{$this->model}->count_results($where);
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $start_rec, $data['total_rows'], base_url('Admin/insight_quanta_cap/index'));
        $data['row_counter'] = $start_rec + 1;
        $this->load->view('insight_quanta_cap/index', $data);
    }

    private function get_where_condition($complete_keyword = "", $type = "") {

        $where = "1";
        if (strlen($complete_keyword)) {
            $cols = array("email", "uuid");
            $keywordCondition = "0 ";
            $keywords = explode(" ", $complete_keyword);
            foreach ($cols as $col) {
                $keywordCondition .= " OR `$col` LIKE '%" . $complete_keyword . "%'";
                if (count($keywords) > 1) {
                    foreach ($keywords as $word) {
                        $keywordCondition .= " OR `$col` LIKE '%" . $word . "%'";
                    }
                }
            }
            $where .= " AND ($keywordCondition)";
        }

        switch ($type) {
            case 'activated': $where .=' AND `status`="activated"';
                break;
            case 'free': $where .=' AND `status`="free_trial"';
                break;
        }
        $where = ($where == '1') ? NULL : $where;
        return $where;
    }

    public function product_keys() {
        $this->load->helper('date');
        $this->load->library('encrypt');
        $data['pageTitle'] = "Insight Quanta Cap App Keys";
        $data['type'] = $this->input->get('type') ? $this->input->get('type') : "";

        $keyword = $this->input->get('q') ? $this->input->get('q') : "";
        $data['q'] = $keyword = addslashes(trim($keyword));

        $where = $this->get_keys_details_where_condition($data['q'], $data['type']);


        $data['page'] = $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results'] = $this->{$this->model}->get_product_keys($where, ROWS_PER_PAGE, $start_rec);
        $data['total_rows'] = $this->{$this->model}->count_product_keys($where);
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $start_rec, $data['total_rows'], base_url('Admin/insight_quanta_cap/product_keys'));

        $data['row_counter'] = $start_rec + 1;
        $this->load->view('insight_quanta_cap/key_listing', $data);
    }

    private function get_keys_details_where_condition($complete_keyword = "", $type = "") {

        $where = "1";
        if (strlen($complete_keyword)) {
            $cols = array("email", "first_name", "last_name", "activation_key");
            $keywordCondition = "0 ";
            $keywords = explode(" ", $complete_keyword);
            foreach ($cols as $col) {
                $keywordCondition .= " OR `$col` LIKE '%" . $complete_keyword . "%'";
                if (count($keywords) > 1) {
                    foreach ($keywords as $word) {
                        $keywordCondition .= " OR `$col` LIKE '%" . $word . "%'";
                    }
                }
            }
            $where .= " AND ($keywordCondition)";
        }
        $where = ($where == '1') ? NULL : $where;
        return $where;
    }

    /**
     * @Name : user_add()
     * @Purpose : To show the add user_add form to admin.
     * @Call from : When admin clicks on add new user button.
     * @Functionality : renders the user_add view to the admin.
     * @Receiver params : No parameter passed.
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    // NOT IN USE
    public function user_add() {
        $this->load->library('form_validation');
        $data = array();
        $data['pageTitle'] = 'Add App User';
        $data['subPageTitle'] = '';
        $this->load->view('insight_quanta_cap/user_add', $data);
    }

    /**
     * @Name : key_add()
     * @Purpose : To show the add key_add form to admin.
     * @Call from : When admin clicks on add new key button.
     * @Functionality : renders the key_add view to the admin.
     * @Receiver params : No parameter passed.
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     */
    public function key_add() {
        $this->load->library('form_validation');
        $data = array();
        $data['pageTitle'] = 'Add App Key';
        $data['subPageTitle'] = '';
        $data['activation_key'] = $this->{$this->model}->generateKey("activation_key");
        $this->load->view('insight_quanta_cap/key_add', $data);
    }

    /**
     * @Name : save_new_user()
     * @Purpose : To save the user detail in database when add new user form is submitted.
     * @Call from : Add new user form action calls this function.
     * @Functionality : save the new user record in the database
     * @Receiver params : Only post data is accepted to save in quantacapsule table.
     * @Return params : Redirects to the listing page if user saved successfully. 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    //NOT IN USE
    function save_new_user() {
        if ($this->input->post()) {
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_add();
            } else {
                $data = $this->input->post();
                $data['date_created'] = date('Y-m-d H:i:s');
                $return_insertId = $this->{$this->model}->save($data);
                if ($return_insertId) {
//                    $this->notify_admin("new_user", $data);
//                    $this->notify_user("new_user", $data);
                }
                redirect('Admin/insight_quanta_cap/index');
            }
        }
    }

    /**
     * @Name : save_new_key()
     * @Purpose : To save the key detail in database when add new key form is submitted.
     * @Call from : Add new key form action calls this function.
     * @Functionality : save the new key record in the database
     * @Receiver params : Only post data is accepted to save in insight quanta cap table.
     * @Return params : Redirects to the listing page if key saved successfully. 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function save_new_key() {
        if ($this->input->post()) {
            if ($this->{$this->model}->validates() == FALSE) {
                $this->key_add();
            } else {
                $data = $this->input->post();
                $data['date_created'] = date('Y-m-d H:i:s');
                $return_insertId = $this->{$this->model}->save_key($data);
                if ($return_insertId) {
                    $this->notify_admin("new_key", $data);
                    $this->notify_user("new_key", $data);
                }
                redirect('Admin/insight_quanta_cap/product_keys');
            }
        }
    }

    /**
     * @Name : add_activation_key()
     * @Purpose : To show the  add_activation_key form to admin and save the activation key if admin clicks on add button
     * @Call from : When admin clicks on create key button.
     * @Functionality : renders the add_activation_key view to the admin.
     * @Receiver params : User id and random string i.e. key is passed
     * @Return params : No parameter returned. 
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    //NOT IN USE

    function add_activation_key($userId = NULL) {
        if (is_null($userId) || $userId < 1) {
            redirect('Admin/insight_quanta_cap/index');
        }
        $this->load->library('form_validation');
        $data['user_id'] = $userId;
        if ($this->input->post('user_id')) {
            $this->form_validation->set_rules("activation_key", "Activation Key", "required");
            if ($this->form_validation->run() == FALSE) {
                $this->add_activation_key($this->input->post('user_id'));
            } else {
                $user_data = $this->input->post();
                $user_data['date_key_created'] = date("Y-m-d H:i:s");
                $where['user_id'] = $user_data['user_id'];
                if ($this->{$this->model}->update($user_data, $where)) {
                    $user_details = $this->{$this->model}->get_user_detail($where);
                    $this->notify_admin("activation_key", $user_details);
                    $this->notify_user("activation_key", $user_details);
                }
                redirect("Admin/insight_quanta_cap/index");
            }
        }
        $data['pageTitle'] = 'Create Key';
        $data['subPageTitle'] = '';

        $data['action'] = "Admin/insight_quanta_cap/add_activation_key/" . $userId;
        $data['activation_key'] = $this->{$this->model}->generateKey("activation_key");
        $this->load->view('insight_quanta_cap/add_activation_key', $data);
    }

    /**
     * @Name : user_edit()
     * @Purpose : To edit rife App user by admin.
     * @Call from : When admin clicks on 'edit' link.
     * @Functionality : Renders the user_edit view to edit user.
     * @Receiver params : User Id of the user to edit is passed.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function user_edit($userId) {
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Edit User';
        $data['subPageTitle'] = '';
        $user_data = $this->{$this->model}->get_user_detail(array('user_id' => $userId));
        if ($user_data) {
            $user_data['date_activated'] = format_date($user_data['date_activated'], "Y-m-d");
            $user_data['date_created'] = format_date($user_data['date_created'], "Y-m-d H:i:s");
        }
        $data['user_data'] = $user_data;
        $this->load->view('insight_quanta_cap/user_edit', $data);
    }

    /**
     * @Name : edit_user()
     * @Purpose : To edit the user details by admin.
     * @Call from : When admin clicks on update link on the edit user form.
     * @Functionality : Updates the user details.
     * @Receiver params :  Only post data is accepted to update in quantacapsule table.
     * @Return params : No parameters returned.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function edit_user() {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $data = $this->input->post();
            $user_id = $data['user_id'];
            unset($data['user_id']);
            $this->form_validation->set_rules("first_name", "First Name", "trim");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
            $this->form_validation->set_rules("uuid", "uuid", "trim");
            if ($this->form_validation->run() == FALSE) {
                $response_array['status'] = FALSE;
                $response_array['message'] = $this->form_validation->error_array();
            } else {
                $where['email'] = $data['email'];
                if ($user_id) {
                    $where['user_id !='] = $user_id;
                }
                $this->load->model($this->model);
                $result = $this->{$this->model}->count_results($where);
                if ($result > 0) {
                    $response_array['status'] = FALSE;
                    $response_array['message'] = "Email id already exits. Please enter other email id";
                } else {
                    $this->{$this->model}->update($data, array('user_id' => $user_id));
                    $response_array['status'] = TRUE;
                    $response_array['message'] = "Data updated successfully";
                }
            }
            echo json_encode($response_array);
            die();
        }
    }

    /**
     * @Name : key_edit()
     * @Purpose : To edit  App user by admin.
     * @Call from : When admin clicks on 'edit' link.
     * @Functionality : Renders the key_edit view to edit key details.
     * @Receiver params : Key Id of the key to edit is passed.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function key_edit($keyId) {
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Edit User';
        $data['subPageTitle'] = '';
        $key_data = $this->{$this->model}->get_key_detail(array('key_id' => $keyId));
        if ($key_data) {
            $key_data['date_activated'] = format_date($key_data['date_activated'], "Y-m-d");
            $key_data['date_created'] = format_date($key_data['date_created'], "Y-m-d H:i:s");
        }
        $data['key_data'] = $key_data;
        $this->load->view('insight_quanta_cap/key_edit', $data);
    }

    /**
     * @Name : edit_key()
     * @Purpose : To edit the key regarding details by admin.
     * @Call from : When admin clicks on update link on the edit key form.
     * @Functionality : Updates the user details.
     * @Receiver params :  Only post data is accepted to update in insight_quanta_cap table.
     * @Return params : No parameters returned.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function edit_key() {
        if ($this->input->post()) {
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_edit($this->input->post('key_id'));
            } else {
                $data = $this->input->post();
                $where['key_id'] = $data['key_id'];
                $this->{$this->model}->update_key($data, $where);
                redirect('Admin/insight_quanta_cap/product_keys');
            }
        }
    }
    
     /**
     * EMAIL App Activation key to the user
     * Use : controller/user(to send key) and aura_genie.js (to resend key)
     */
    public function send_activation_key($key_data = array()) {
        if (empty($key_data)) {
            $keyId = $this->input->post('id');
            $key_data = $this->{$this->model}->get_key_detail(array('key_id' => $keyId));
        }
        $response['status'] = FALSE;
        if (count($key_data)) {
            if ($this->notify_user("new_key", $key_data)) {
                $response['status'] = TRUE;
            }
        }
        if ($this->input->is_ajax_request()) {
            echo json_encode($response);
        }
        return $response;
    }
    
    /**
     * @Name : block_device()
     * @Purpose : To block the device by admin.
     * @Call from : When admin clicks on block button from users listing page and ajax called to block the device using this function.
     * @Functionality :block user device  
     * @Receiver params : Id is passed with post data.
     * @Return params : No parameters returned 
     */
    function block_device() {
        $this->load->library('encrypt');
        $user_id = $this->input->post('id');
        $where = array('user_id' => $user_id);
        $set = array('uuid_is_blocked' => '1');
        if ($this->{$this->model}->update($set, $where)) {
            $user_details = $this->{$this->model}->get_user_detail($where);
            $this->notify_admin("block_uuid", $user_details);
            $this->notify_user("block_uuid", $user_details);
            echo 1;
        }
    }

    /**
     * @Name : unblock_device()
     * @Purpose : To unblock the the blocked device by admin.
     * @Call from : When admin clicks on unblock button from users listing page and ajax called to unblock the device using this function.
     * @Functionality :unblock user device  
     * @Receiver params : Id is passed with post data.
     * @Return params : No parameters returned 
     */
    function unblock_device() {
        $this->load->library('encrypt');
        $user_id = $this->input->post('id');
        $where = array('user_id' => $user_id);
        $set = array('uuid_is_blocked' => '0');
        if ($this->{$this->model}->update($set, $where)) {
            $user_details = $this->{$this->model}->get_user_detail($where);
            $this->notify_admin("unblock_uuid", $user_details);
            $this->notify_user("unblock_uuid", $user_details);
            echo 1;
        }
    }

    /**
     * @Name : delete()
     * @Purpose : To delete the key by admin.
     * @Call from : When admin clicks on delete link from key listing page and ajax called to delete the user using this function.
     * @Functionality : permanently delete key from database and remove that key from user table.
     * @Receiver params : key_Id is passed with post data.
     * @Return params : No parameters returned 
     */
    function delete($key_id = NULL) {
        if ($this->input->post()) {
            $key_id = $this->input->post('key_id');
            $key_detail = $this->{$this->model}->get_key_detail(array('key_id' => $key_id));
            if ($this->{$this->model}->update(array('activation_key' => '', 'status' => 'free_trial'), array('activation_key' => $key_detail['activation_key']))) {
                if ($this->{$this->model}->delete(array('key_id' => $key_id))) {
                    redirect('Admin/insight_quanta_cap/product_keys');
                }
            }
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Delete Key';
        $data['subPageTitle'] = '';
        $data['key_id'] = $key_id;
        $this->load->view('insight_quanta_cap/delete', $data);
    }

    function notify_admin($about = '', $key_detail = array()) {
        if ($about == '' || count($key_detail) < 0 || !isset($key_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch ($about) {
            case 'new_user' :
                $subject = 'Insight Quanta Cap New User Notification';
                $view = 'insight_quanta_cap/admin_email_format/new_user_email';
                break;
            case 'new_key' :
                $subject = 'Insight Quanta Cap New User Notification';
                $view = 'insight_quanta_cap/admin_email_format/new_key_email';
                break;
            case 'activation_key' :
                $subject = 'Insight Quanta Cap Activation Key Notification';
                $view = 'insight_quanta_cap/admin_email_format/acivation_key_email';
                break;
            case 'block_uuid' :
                $subject = 'Device blocked for Insight Quanta Cap';
                $view = 'insight_quanta_cap/admin_email_format/block_uuid';
                break;
            case 'unblock_uuid' :
                $subject = 'Device unblocked for Insight Quanta Cap';
                $view = 'insight_quanta_cap/admin_email_format/unblock_uuid';
                break;
        }
        $key_detail['name'] = trim($key_detail['first_name'] . " " . (isset($key_detail['last_name']) ? $key_detail['last_name'] : "") );
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = admin_email;
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view, array('key_detail' => $key_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function notify_user($about = '', $key_detail = array()) {
        if ($about == '' || count($key_detail) < 0 || !isset($key_detail['email'])) {
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch ($about) {
            case 'new_user' :
                $subject = 'Your Insight Quanta Cap Download Link';
                $view = 'insight_quanta_cap/user_email_format/new_user_email';
                break;
            case 'new_key' :
                $subject = 'Your Insight Quanta Cap Download Link';
                $view = 'insight_quanta_cap/user_email_format/new_key_email';
                break;
            case 'activation_key' :
                $subject = 'Insight Quanta Cap Activation Key';
                $view = 'insight_quanta_cap/user_email_format/new_key_email';
                break;
//            case 'activation_key' :
//                $subject = 'Insight Quanta Cap Activation Key';
//                $view = 'insight_quanta_cap/user_email_format/acivation_key_email';
//                break;
            case 'block_uuid' :
                $subject = 'Your device has been blocked for Insight Quanta Cap App';
                $view = 'insight_quanta_cap/user_email_format/block_uuid';
                break;
            case 'unblock_uuid' :
                $subject = 'Your device is now unblocked for Insight Quanta Cap App';
                $view = 'insight_quanta_cap/user_email_format/unblock_uuid';
                break;
        }
        $key_detail['name'] = trim($key_detail['first_name'] . " " .(isset($key_detail['last_name']) ? $key_detail['last_name']:"") );
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $key_detail['email'];
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view, array('key_detail' => $key_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function check_email() {
        $response = 0;
        if ($this->input->post()) {
            $where['`email`'] = $this->input->post("email");
            if ($this->input->post('id')) {
                $where['user_id !='] = $this->input->post('id');
            }
            $this->load->model($this->model);
            $result = $this->{$this->model}->count_results($where);
            if ($result) {
                $response = "1";
            }
        }
        echo $response;
    }

}

/* End of file insight_quanta_cap.php */
/* Location: ./application/controllers/insight_quanta_cap.php */