<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pacific_health_foods extends MY_Controller {
    
   public $model="pacific_health_foods_model";
    
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/admin
     *	- or -  
     * 		http://example.com/index.php/admin/index
     *	- or -
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/admin/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct(){
        parent::__construct();
        $this->model='pacific_health_foods_model';
        $this->load->library('form_validation');
    }
    
    /**
    * @Name : index()
    * @Purpose : To show the listing of all Hours Of Rest App users to admin.
    * @Call from : When admin click on the  Hours Of Rest App Tab. 
    * @Functionality : Simply fetch the records from database, and set the data to the view.
    * @Receiver params : No receiver parameters passed.
    * @Return params : No parameters returned 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified :
    */
    
    public function index($where=NULL) {
        $this->load->view('pacific_health_foods/index');
    }
    
}/* End of file admin.php */
/* Location: ./application/controllers/admin.php */