<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Energy_mastered extends MY_Controller {

    public $model = "user_model_old";

    public function __construct() {
        parent::__construct();

        $this->model = 'energy_mastered_model';
        $this->load->model('Admin/energy_mastered_model');
        $this->load->model('payment_model');
    }

    /**
     * @Name : index()
     * @Purpose : To show the listing of all Quantum iLife/iNfinity App users to admin.
     * @Call from : When admin click on the Quantum iLife/iNfinity App Tab.
     * @Functionality : Simply fetch the records from database, and set the data to the view.
     * @Receiver params : No receiver parameters passed.
     * @Return params : No parameters returned
     */
    public function index() {

        $where = '1';
        $this->load->library('encrypt');
        $this->load->helper('date');
        $get = $this->uri->uri_to_assoc();
        $type = isset($get['type']) ? $get['type'] : "";
        $type = $this->input->get('type') ? $this->input->get('type') : $type;
        $keyword = $this->input->get('q') ? $this->input->get('q') : "";
        $data['pageTitle'] = "Energy Mastered";
        if($keyword){
            $data['q'] = $keyword;
            $where .= " AND (`email` LIKE '%" . $keyword . "%' or `first_name` LIKE '%" . $keyword . "%' or `last_name` LIKE '%" . $keyword . "' or concat_ws(' ',first_name,last_name) LIKE '%" . $keyword . "%')";
        }
        if ($type != "") {
            $data['type'] = $type;
            switch ($type) {
                case 'activated':
                    $where .= ' AND `status`="2"';
                    break;
                case 'paid':
                    $where .= ' AND `activation_key`!=""';
                    break;
                case 'free':
                    $where .= ' AND (`activation_key`="" OR `activation_key` IS NULL) ';
                    break;
                case 'app':
                    $where .= ' AND user_type="app"';
                    break;
                case 'facebook':
                    $where .= ' AND user_type="facebook"';
                    break;
            }
        }
        $where = $where == '1' ? NULL : $where;
        $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results'] = $this->{$this->model}->get_users($where, ROWS_PER_PAGE, $start_rec);
        $data['total_rows'] = $this->{$this->model}->count_results($where);
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $start_rec, $data['total_rows'], base_url('Admin/energy_mastered/index'));

        /* GET THE DISTRIBUTORS LIST */
//        $this->load->model("distributors_model");
//        $data['distributors_data'] = $this->distributors_model->get_distributors();

        $data['row_counter'] = $start_rec + 1;
        $this->load->view('energy_mastered_old/index', $data);

    }

    /**
     * @Name : user_add()
     * @Purpose : To show the add user_add form to admin.
     * @Call from : When admin clicks on add new user button.
     * @Functionality : renders the user_add view to the admin.
     * @Receiver params : No parameter passed.
     * @Return params : No parameter returned.
     */
    public function add() {
        $this->load->library('form_validation');
        $data = array();
        $data['pageTitle'] = '';
        $data['subPageTitle'] = '';
        //---generate key----
        $data['activation_key'] = $this->{$this->model}->generateKey("activation_key");
        $this->load->view('energy_mastered_old/add', $data);
    }

    /**
     * @Name : save_new_user()
     * @Purpose : To save the user detail in database when add new user form is submitted.
     * @Call from : Add new user form action calls this function.
     * @Functionality : save the new user record in the database
     * @Receiver params : Only post data is accepted to save in new_payments table.
     * @Return params : Redirects to the listing page if user saved successfully.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    function save_new_user() {
        if ($this->input->post()) {
            if ($this->{$this->model}->validates() == FALSE) {
                $this->add();
            } else {
                $data = $this->input->post();
                unset($data['confirm_password']);
                $data['password'] = $this->{$this->model}->hash_this($data['password']);
                $data['date_created'] = date('Y-m-d H:i:s');
                $data['status'] = "1";
                $data['user_type'] = "admin_panel";
                $return_insertId = $this->{$this->model}->save($data);
                if ($return_insertId) {
                    $this->notify_admin("new_user",$this->input->post());
                    $this->notify_user($this->input->post());
                } else {
                    mail("hardeep.intersoft@gmail.com", 'Error: Quantum Genius App Notification', "Failure to send the email notification becuae of error in insertion");
                }
                redirect('Admin/energy_mastered/index');
            }
        }
    }

    /**
     * @Name : check_email()
     * @Purpose : To check whether the email is registered with the application
     * @Call from : When admin clicks on Add new user button, an ajax call to this function to check whether the email already exist or not.
     * @Functionality : renders the add_key view to the admin.
     * @Receiver params : email  entered by the admin
     * @Return params : return 1 for success i.e. email exist or 0 for failure i.e email doesn't exist.
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 23 July 2015
     * @Modified :
     */
    function check_email() {
        $response = 0;
        if ($this->input->post()) {
            $where['`email`'] = $this->input->post("email");
            if ($this->input->post('id')) {
                $where['id !='] = $this->input->post('id');
            }
            $this->load->model($this->model);
            $result = $this->{$this->model}->get_user_detail($where);
            if ($result) {
                $response = "1";
            }
        }
        echo $response;
    }

    /**
     * @Name : user_edit()
     * @Purpose : To edit rife App user by admin.
     * @Call from : When admin clicks on 'edit' link.
     * @Functionality : Renders the user_edit view to edit user.
     * @Receiver params : User Id of the user to edit is passed.
     * @Return params : No parameters returned
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    function edit($userId) {
        $this->load->library('form_validation');
        $data['pageTitle'] = '';
        $data['subPageTitle'] = '';
        /* fetch user data to edit */
        $where = array();
        $where['id'] = $userId;
        $this->load->model($this->model);
        $user_data = $this->{$this->model}->get_user_detail($where);

        $user_data['date_created'] = $this->format_date($user_data['date_created'], "Y-m-d");
        $user_data['otp_date_created'] = $this->format_date($user_data['otp_date_created'], "Y-m-d");
        $data['user_data'] = $user_data;
        $this->load->view('energy_mastered_old/edit', $data);

    }

    /**
     * @Name : edit_user()
     * @Purpose : To edit the user details by admin.
     * @Call from : When admin clicks on update link on the edit user form.
     * @Functionality : Updates the user details.
     * @Receiver params :  Only post data is accepted to update in new_payments table.
     * @Return params : No parameters returned.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    function edit_user() {
        if ($this->input->post()) {

            $data = $this->input->post();
            $where['id'] = $data['id'];
            unset($data['id']);
            $data['date_updated'] = date("Y-m-d H:i:s");
            $this->{$this->model}->update_user($data, $where);
            redirect('Admin/energy_mastered/index');
        }
    }

    /**
     * @Name : delete_user()
     * @Purpose : To delete the user by admin.
     * @Call from : When admin clicks on delete link from users listing page and ajax called to delete the user using this function.
     * @Functionality : permanently delete user from database
     * @Receiver params : User_Id is passed with post data.
     * @Return params : No parameters returned
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    function delete($user_id = NULL) {
        if ($this->input->post()) {
            $user_id = $this->input->post('id');

            if ($this->{$this->model}->delete_user(array('id'=>$user_id))) {
                redirect('Admin/energy_mastered/index');
            }
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Delete User';
        $data['subPageTitle'] = '';
        $this->load->view('energy_mastered_old/delete', array('id' => $user_id));
    }


    /**
     * @Name : add_activation_key()
     * @Purpose : To show the  add_activation_key form to admin and save the activation key if admin clicks on add button
     * @Call from : When admin clicks on create key button.
     * @Functionality : renders the add_activation_key view to the admin.
     * @Receiver params : User id and random string i.e. key is passed
     * @Return params : No parameter returned.
     * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified :
     */
    function add_activation_key($userId=NULL) {
        if (is_null($userId) || $userId < 1) {
            redirect('Admin/genius_insight/index');
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Create Key';
        $data['subPageTitle'] = '';
        $data['id'] = $userId;
        $data['action'] = "Admin/energy_mastered/add_activation_key/" . $userId;
        $data['activation_key'] = $this->{$this->model}->generateKey("activation_key");
        if ($this->input->post('id')) {
            $this->form_validation->set_rules("activation_key", "Activation Key", "required");
            if ($this->form_validation->run() == FALSE) {
                $this->add_activation_key($this->input->post('id'));
            } else {
                $user_data = $this->input->post();
                $where['id'] = $data['id'];
                if ($this->{$this->model}->update($user_data, $where)) {
                    $this->load->model($this->model);
                    $user_details = $this->{$this->model}->get_user_detail(array('id' => $user_data['id']));
                    $this->notify_admin("activation_key", $user_details);
                    $this->send_activation_key($user_details);
                }
                redirect("Admin/energy_mastered/index");
            }
        }
        $this->load->view('energy_mastered_old/add_activation_key', $data);
    }

    function send_activation_key($user_details = array()){
        $response = FALSE;
        if(count($user_details)){
            $this->layout = 'blank';
            $this->load->theme($this->layout);
            /* EMAIL OTP TO THE USER */
            $mail_params['ContentType'] = "text/html";
            $mail_params['To'] = $user_details['email'];
            $mail_params['Subject'] = 'Energy Mastered Activation Key';
            $mail_params['Body'] = $this->load->view('email_format/acivation_key_email', $user_details, true);

            if ($this->{$this->model}->sendMail($mail_params)) {
                $response = TRUE;
            }
        }
        return $response;
    }



    function notify_admin($about = '', $user_detail = array()){
        if($about == '' || count($user_detail)<0 || !isset($user_detail['email'])){
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch($about){
            case 'new_user' :
                $subject = 'Energy Mastered New User Notification';
                $view = 'email_format/admin/new_user_email';
                break;
            case 'activation_key' :
                $subject = 'Energy Mastered Activation Key Notification';
                $view = 'email_format/admin/acivation_key_email';
                break;
        }
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = "Support@quantumhealthapps.com";
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view,array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        }else{
            return FALSE;
        }
    }
    function notify_user($user_detail = array()){
        if(count($user_detail)<0 || !isset($user_detail['email'])){
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        /* SEND EMAIL TO ADMIN */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $user_detail['email'];
        $mail_params['Subject'] = "Energy Mastered Notification";
        $mail_params['Body'] = $this->load->view('email_format/new_user_email',array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function genrate_activation_key() {
        $id = $this->input->post('id');
        $key = $this->{$this->model}->generate_random_string(7);
        $response = array();
        if ($this->{$this->model}->update_user(array('activation_key' => $key), array('id'=>$id))) {
            $response['status'] = true;
            $response['message'] = 'Activation key generated';
            $response['key'] = $key;
        } else {
            $response['status'] = false;
            $response['message'] = 'Activation key can not be generated';
        }

        echo json_encode($response);
    }

}

/* End of file aura.php */
/* Location: ./application/controllers/aura.php */