<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Royal_rife extends MY_Controller {
    
    public $model="rife_model";

    public function __construct(){
        parent::__construct();
        $this->model='rife_model';
        $this->load->model('Admin/rife_model');
        $this->app_id = INSIGHT_ROYAL_RIFE_APP_ID;
    }
    
    /**
    * @Name : index()
    * @Purpose : To show the listing of all Rife App users to admin.
    * @Call from : When admin click on the Quantum Life Rife App Tab. 
    * @Functionality : Simply fetch the records from database, and set the data to the view.
    * @Receiver params : No receiver parameters passed.
    * @Return params : No parameters returned 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified :
    */
    
    
    public function index() {
        $this->load->library('encrypt');
        $data['pageTitle']="Royal Rife Machine App Users";
        $keyword = $this->input->get('q') ? $this->input->get('q') : "";
        $data['q'] = $keyword = addslashes(trim($keyword));
        $keywordCondition = NULL;
        if (strlen($keyword)) {
            $keywordCondition = "`Email` LIKE '%" . $keyword . "%' OR `Name` LIKE '%" . $keyword . "%'";
            $keyword = explode(" ", $keyword);
            if (count($keyword) > 1) {
                foreach ($keyword as $word) {
                    $keywordCondition .= " OR `Email` LIKE '%" . $word . "%' OR `Name` LIKE '%" . $word . "%'";
                }
            }
        }
        $data['page'] = $start_rec = $this->input->get('page') ? $this->input->get('page') : 0;
        $data['results'] = $this->{$this->model}->get_users($keywordCondition,ROWS_PER_PAGE,$start_rec);
        $data['total_rows'] = $this->{$this->model}->count_results($keywordCondition);
        $data['links'] = $this->get_pagination_links(ROWS_PER_PAGE, $start_rec, $data['total_rows'], base_url('Admin/royal_rife/index'));
        $data['row_counter'] = $start_rec + 1;
        $this->load->view('royal_rife/index', $data);
    }
   
    
    /**
    * @Name : user_add()
    * @Purpose : To show the add user_add form to admin.
    * @Call from : When admin clicks on add new user button.
    * @Functionality : renders the user_add view to the admin.
    * @Receiver params : No parameter passed.
    * @Return params : No parameter returned. 
    * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified :
    */
    public function user_add() {
        $this->load->library('form_validation');
        $data = array();
        $data['pageTitle'] = 'Add App User';
        $data['subPageTitle'] = '';
        $data['Key']=$this->{$this->model}->generateKey("Key");
        $this->load->view('royal_rife/user_add',$data);
    }
    
    /**
    * @Name : save_new_user()
    * @Purpose : To save the user detail in database when add new user form is submitted.
    * @Call from : Add new user form action calls this function.
    * @Functionality : save the new user record in the database
    * @Receiver params : Only post data is accepted to save in rife_codes table.
    * @Return params : Redirects to the listing page if user saved successfully. 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : 
    */
    function save_new_user() {
        if ($this->input->post()) {
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_add();
            } else 
            {
                $data = $this->input->post();
                $data['CreatedDate'] = date('Y-m-d H:i:s');
                $data['user_created_from'] = "dev_server";
                $data['key_created_from'] = "dev_server";
                $return_insertId = $this->{$this->model}->save($data);
                if ($return_insertId) {
                    $this->notify_admin("new_user",$data);
                    $this->notify_user("new_user",$data);
                }
                redirect('Admin/royal_rife/index');
            }
        }
    }
    /**
    * @Name : add_activation_key()
    * @Purpose : To show the  add_activation_key form to admin and save the activation key if admin clicks on add button
    * @Call from : When admin clicks on create key button.
    * @Functionality : renders the add_activation_key view to the admin.
    * @Receiver params : User id and random string i.e. key is passed
    * @Return params : No parameter returned.
    */
    
    function add_activation_key($userId=NULL) {
        if(is_null($userId) || $userId < 1){
            redirect('Admin/royal_rife/index');
        }
        $this->load->library('form_validation');
        $data['id'] = $userId;
        if($this->input->post('id')){
            $this->form_validation->set_rules("Key","Activation Key","required");
            if ($this->form_validation->run() == FALSE) {
                $this->add_activation_key($this->input->post('id'));
            } 
            else{
                $user_data= $this->input->post();
                $user_data['key_created_from'] = "dev_server";
                $where['Id'] = $data['id'];
                if($this->{$this->model}->update($user_data,$where)){
                    $user_details = $this->{$this->model}->get_user_detail($where);
                    $this->notify_admin("activation_key",$user_details);
                    $this->notify_user("activation_key",$user_details);
                }
                redirect("Admin/royal_rife/index");
            }
        }
        $data['pageTitle'] = 'Create Key';
        $data['subPageTitle'] = '';
        
        $data['action'] = "Admin/royal_rife/add_activation_key/".$userId;
        $data['Key'] = $this->{$this->model}->generateKey("Key");
        $this->load->view('royal_rife/add_activation_key', $data);
    }
    
    /**
     * EMAIL App Activation key to the user
     * Use : controller/user(to send key) and aura_genie.js (to resend key)
     */
    public function send_activation_key($user_details = array()) {
        if (empty($user_details)) {
            $id = $this->input->post('id');
            $user_details = $this->{$this->model}->get_user_detail(array('id' => $id));
        }
        $response['status'] = FALSE;
        if (count($user_details)) {
            if ($this->notify_user("activation_key", $user_details)) {
                $response['status'] = TRUE;
            }
        }
        if ($this->input->is_ajax_request()) {
            echo json_encode($response);
        }
        return $response;
    }
    /**
    * @Name : user_edit()
    * @Purpose : To edit rife App user by admin.
    * @Call from : When admin clicks on 'edit' link.
    * @Functionality : Renders the user_edit view to edit user.
    * @Receiver params : User Id of the user to edit is passed.
    * @Return params : No parameters returned 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : 
    */
    function user_edit($userId) {
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Edit User';
        $data['subPageTitle'] = '';
        $user_data = $this->{$this->model}->get_user_detail(array('Id'=>$userId));
        if($user_data){
            $user_data['ActivatedDate']= $this->format_date($user_data['ActivatedDate'],"Y-m-d");
            $user_data['ExpiredDate']= $this->format_date($user_data['ExpiredDate'],"Y-m-d");
            $user_data['CreatedDate']= $this->format_date($user_data['CreatedDate'],"Y-m-d");
            $user_data['payment_date']= $this->format_date($user_data['payment_date'],"Y-m-d");
        }
        $data['user_data'] = $user_data;
        
        if ($this->input->post()) {
            if ($this->{$this->model}->validates()) {
                $update_data= $this->input->post();
                $where['Id'] = $update_data['Id'];
                $result = $this->{$this->model}->update($update_data,$where);
                if($result && $update_data['has_upgrade_app'] && $user_data['has_upgrade_app'] == "0"){
                    /* SEND EMAIL TO USER FOR UPGRADE */
                    $this->notify_user('upgrade_app',$update_data);
                }
                redirect('Admin/royal_rife/index');
            }
        }
        
        $this->load->view('royal_rife/user_edit', $data);
    }
    /**
    * @Name : edit_user()
    * @Purpose : To edit the user details by admin.
    * @Call from : When admin clicks on update link on the edit user form.
    * @Functionality : Updates the user details.
    * @Receiver params :  Only post data is accepted to update in rife_codes table.
    * @Return params : No parameters returned.
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : 
    */
    function edit_user() {
        if ($this->input->post()) {
            $this->load->model($this->model);
            if ($this->{$this->model}->validates() == FALSE) {
                $this->user_edit($this->input->post('Id'));
            } else 
            {
                $data= $this->input->post();
                $where['Id']=$data['Id'];
                $this->{$this->model}->update($data,$where);
                redirect('Admin/royal_rife/index');
            }
        }
    }

    /**
     * @Name : delete_user()
     * @Purpose : To delete the user by admin.
     * @Call from : When admin clicks on delete link from users listing page and ajax called to delete the user using this function.
     * @Functionality : permanently delete user from database  
     * @Receiver params : User_Id is passed with post data.
     * @Return params : No parameters returned 
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
     * @Modified : 
     */
    function delete($user_id = NULL) {
        if($this->input->post()){
            $user_id = $this->input->post('id');
            if ($this->{$this->model}->delete(array('Id'=>$user_id))) {
                redirect('Admin/royal_rife/index');
            }
        }
        $this->load->library('form_validation');
        $data['pageTitle'] = 'Delete User';
        $data['subPageTitle'] = '';
        $this->load->view('royal_rife/delete', array('id'=>$user_id));
        
    }
    
    /** USED ONLLY FOR CODES(30 days activation : quantum ilife/iNfinity) and rife codes table (rife app)
    * @Name : generateCodes()
    * @Purpose : To generate 1000 activation codes.
    * @Call from : when admin clicks on generate 1K codes button an ajax called to generate 1000 codes using this function.
    * @Functionality : geberate 1000 activation codes and save it in database
    * @Receiver params : Only post data is accepted to save in codes table.
    * @Return params : Redirects to the listing page if user saved successfully. 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 17 July 2015
    * @Modified : 
    */
    function generateCodes() {
        if($this->input->get('genCode')&& $this->input->get('genCode')== '1'){
            $numOfCodes = $this->input->get('numOfCodes');
            $numOfCodes = 3;
            if($numOfCodes > 0)
            {   
                $batch_array=array();
                $maxNumber = $this->{$this->model}->count_results();
		$today = new DateTime('NOW');
		
		$data['ExpiredDate'] = 'NULL'; //add_month($today, 1)->format(DateTime::W3C);
		$data['ActivatedDate'] = 'NULL';
		$data['CreatedDate'] = $today->format(DateTime::W3C);
		$data['UUID'] = '';
		for($i = 1; $i <= $numOfCodes; $i++ )
		{
                    $prefix = str_pad(dechex(($maxNumber + $i) / 10000), 2, '0');
                    $generatedCode = $prefix.substr(hash('ripemd128',  uniqid('', true)), 0, 8);
                    $data['Key'] = $generatedCode;
                    array_push($batch_array,$data);
		}
                $this->db_q = $this->load->database('quantum', TRUE);
            $this->db_q->insert_batch($this->{$this->model}->table,$batch_array);
                redirect('Admin/royal_rife/index');
            }   
        }
    }
    
    function change_upgrade_status(){
        if ($this->input->post()) {
            $this->form_valdation->set_rules('Id','Id', 'trim|required|numeric');
            $this->form_valdation->set_rules('has_upgrade_app','Has Upgrade App', 'trim|required|numeric');
            if ($this->form_valdation->run() == FALSE) {
                $data['status'] = FALSE;
                $data['message'] = "Validation Errors";
            } else 
            {
                $update_data= $this->input->post('has_upgrade_app');
                $where['Id'] = $this->input->post('Id');
                $result = $this->{$this->model}->update($update_data,$where);
                if($result && $update_data['has_upgrade_app']){
                    /* SEND EMAIL TO USER */
                    $user_details = $this->{$this->model}->get_user_detail($where);
                    if($this->notify_user('upgrade_app', $user_details)){
                        $data['status'] = TRUE;
                        $data['message'] = "User updated successfully and email has been sent to user";
                    }else{
                        $data['status'] = FALSE;
                        $data['message'] = "User updated successfully but failed to send email to user";
                    }
                }
            }
        }
        echo json_encode($data);
    }
            
    function notify_admin($about = '', $user_detail = array()){
        if($about == '' || count($user_detail)<0 || !isset($user_detail['Email'])){
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch($about){
            case 'new_user' : 
                $subject = 'Insight Royal Rife Machine New User Notification';
                $view = 'royal_rife/admin_email_format/new_user_email';
                break;
            case 'activation_key' : 
                $subject = 'Insight Royal Rife Machine Activation Key Notification';
                $view = 'royal_rife/admin_email_format/acivation_key_email';
                break;
        }
        /* SEND EMAIL TO ADMIN */
        /* EMAIL OTP TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = admin_email;
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view,array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function notify_user($about = '', $user_detail = array()){
        if($about == '' || count($user_detail)<0 || !isset($user_detail['Email'])){
            return FALSE;
        }
        $this->layout = 'blank';
        $this->load->theme($this->layout);
        switch($about){
            case 'new_user' : 
                $subject = 'Your Insight Royal Rife App Download & Activation';
                $view = 'royal_rife/user_email_format/new_user_email';
                break;
            case 'activation_key' : 
                $subject = 'Your Insight Royal Rife App Activation Key';
                $view = 'royal_rife/user_email_format/new_user_email';
                break;
//            case 'activation_key' : 
//                $subject = 'Your Insight Royal Rife App Activation Key';
//                $view = 'royal_rife/user_email_format/acivation_key_email';
//                break;
            case 'upgrade_app' : 
                $subject = 'Your Energy Re-Mastered App Download & Activation';
                $view = 'royal_rife/user_email_format/acivation_key_email';
                break;
        }
        /* EMAIL EMAIL TO THE USER */
        $mail_params['ContentType'] = "text/html";
        $mail_params['To'] = $user_detail['Email'];
        $mail_params['Subject'] = $subject;
        $mail_params['Body'] = $this->load->view($view,array('user_detail' => $user_detail), true);
        if ($this->{$this->model}->sendMail($mail_params)) {
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    
    

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */