<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family:arial,sans-serif; font-size: 34px; color: #57697e;">
                                Download & Activation Codes: Holistic Prosomatics App
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family:arial,sans-serif;">
                            <p style="line-height:15px;">Hi<?php echo isset($user_detail['first_name']) ? " " . trim($user_detail['first_name']) : ' User' ?>,</p>
                            <p style="line-height:15px;">Thank you for joining Quantum Health Apps and investing in the Holistic Prosomatics App.</p>
                            <p style="line-height:15px;">Below is your activation code required to activate your app, once you have fully downloaded and installed Holistic Prosomatics App.</p>

                            <p style="line-height:15px">
                                <img src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTBpsip4SYpSkb9QZchxwZlGMzDuqsjYhv-gwFjbBtm0b1yphLYja7046y7" alt="iOS" width="60" height="60"/>
                                <br/>
                                <font>
                                <b>iOS Download Link:</b>
                                <a href="https://rink.hockeyapp.net/apps/f90e952d9610436dad298a18d2629472"> https://rink.hockeyapp.net/apps/f90e952d9610436dad298a18d2629472</a>
                                </font>
                            </p>
                            <p style="line-height:15px">
                                <img src="http://i.stack.imgur.com/YKTGq.png" alt="Untruested popup" height="200"/>
                            </p>
                            <p style="line-height:15px">
                                <b>"Untrusted Developer Pop Up Box"</b> <br/>
                                Once this appears, select your iOS �Settings� icon, then �General� then scroll down to the bottom and select �Device Management� and Trust the Canada Inc profile.
                            </p>
                            <p style="line-height:15px">
                                <img src="http://www.parkeasier.com/wp-content/uploads/2015/05/REL-Android.png" alt="Android" width="60" height="60"/>
                                <br/>
                                <font>
                                <b>Android Download Link:</b>
                                <a href="https://rink.hockeyapp.net/apps/e6e748aac7ea4ac180f12ef553871af1">https://rink.hockeyapp.net/apps/e6e748aac7ea4ac180f12ef553871af1</a>
                                </font>
                            </p>
                            <p style="line-height:15px"> 
                                If prompted go to your �Settings� then �Apps� and allow installation from 3rd party apps
                            </p>
                            <p style="line-height:15px"> 
                                <font><b>Installation Instructions</b></font><br/>
                                You will have a 15 day trial before you will be prompted to enter your activation code below.<br/>
                                Once the software is downloaded, sign up using your own email and an easy to remember 8 digit password. If you are ever logged out of your app, always gain access using this email and 8 digit password.
                            </p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%;border: 1px solid #999;text-align: left;border-collapse: collapse;margin: 0 0 1em 0;" cellspacing="0" cellpadding="0" >
                            <tr style="background: #eee;">
                                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;" colspan="2">
                                    Login Credentials :
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                    Email Login Username
                                </th>
                                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                    <?php echo $user_detail['email']; ?>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                    Email Login Password 
                                </th>
                                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                    <?php echo $user_detail['password']; ?>
                                </th>
                            </tr>
                            <?php if (strlen($user_detail['activation_key'])) { ?>
                                <tr>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                        One Time Activation Key Code
                                    </th>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                        <?php echo $user_detail['activation_key'] ?>
                                    </th>
                                </tr>
                            <?php } ?>
                        </table>
                        <br/>
                        <p style="line-height:15px"> 
                            More Software Applications and Products : <a href="http://quantumhealthapps.com/">http://quantumhealthapps.com</a>
                        </p>
                        <p style="line-height:15px"> 
                            <b>FOR ALL INSTALLATION OR REINSTALLATION LINKS PLEASE SAVE THIS WEBPAGE</b> : <a href="http://quantumhealthapps.com/software-downloads/">http://quantumhealthapps.com/software-downloads/</a>
                        </p>
                        <p style="line-height:15px"> 
                            Genius Hardware Instructions : <a href="http://quantumhealthapps.com/genius-hardware-instructions/">http://quantumhealthapps.com/genius-hardware-instructions/</a>
                        </p>
                        <p style="line-height:20px">
                            <font>Thank you,<br/>
                            Energy Re-Mastered Apps Support</font>
                        </p>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
