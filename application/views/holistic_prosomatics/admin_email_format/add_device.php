<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                Holistic Prosomatics Device Registration Notification
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Hi Admin,</p>
                            <p>Following are the details of the new registered device.</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%;border: 1px solid #999;text-align: left;border-collapse: collapse;margin: 0 0 1em 0;" cellspacing="0" cellpadding="0" >
                                <tr style="background: #eee;">
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;" colspan="2">
                                        New Device Description  :
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                        Email
                                    </th>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                       <?php echo $user_detail['email']; ?>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                        Name
                                    </th>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                        <?php echo isset($user_detail['first_name']) ? trim($user_detail['first_name']):'';?>
                                    </th>
                                </tr>
                                <?php if(strlen($user_detail['device_description'])){?>
                                <tr>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                        Device
                                    </th>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                        <?php echo $user_detail['device_description'] ?>
                                    </th>
                                </tr>
                                <?php }?>
                                <tr>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                        Device ID 
                                    </th>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                        <?php echo $user_detail['uuid']; ?>
                                    </th>
                                </tr>
                            </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Thanks,</p>
                            <p>Genius Insight Team</p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
