<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<style>
    .select2{
        width: 250px;
    }
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
<!--    <h4 class="modal-title"></h4>-->
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>Search Form
                    </div>
                    <div class="tools">
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form action="<?php echo base_url('aura/get_search_results');?>" method="post" class="form-horizontal form-bordered">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Seacrh By Text</label>
                                <div class="col-md-4">
                                    <input type="text" role="Search Text" id="searchText" field-pair="select2_sample_modal_5" name="searchText" value="<?php set_value('searchTxt') ?>" size="16" class="form-control form-control-inline input-medium check ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Search By Date</label>
                                <div class="col-md-4">
                                    <input type="text" role="Seacrh Date" id="dateSeacrhText" field-pair="select2_sample_modal_15" name="dateSearchText" value="<?php set_value('dateSearchTxt') ?>" size="16" data-date-format="yyyy-mm-dd" class="form-control form-control-inline input-medium date-picker check">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Select Filters</label>
                                <div class="col-md-4">
                                    <input type="hidden"  name="textFilters" field-pair="searchText" id="select2_sample_modal_5" size="16"  class="form-control select2 select2_sample_modal_5 " value="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Date Filters</label>
                                <div class="col-md-4">
                                    <input type="hidden" name="dateFilters" field-pair="dateSeacrhText" id="select2_sample_modal_15" size="16"  class="form-control select2 select2_sample_modal_15  " value="Payment Date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">More Filters</label>
                                <div class="col-md-4">
                                    <input type="hidden" name="commonFilters" id="select2_sample_modal_16" size="16"  class="form-control select2 select2_sample_modal_16 moreFilters" value="All Paid Users">
                                </div>
                            </div>

                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
</div>
<div class="modal-footer">
    <?php
    echo form_input(array('class' => 'btn blue', 'value' => 'Search', 'onclick' => 'check_search_validations()'));
    echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
    ?>
</div>
<script type="text/javascript">
    /*$('#dialog-form-search').show();*/
</script>
<script>

    jQuery(document).ready(function() {
        ComponentsPickers.init();
        ComponentsDropdowns.init();
    });
</script>
