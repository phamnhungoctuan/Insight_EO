<div class="my-modal">
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    echo validation_errors('<span class="error">', '</span>');
    ?>
    <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add New User</h4>
    </div>
    <?php echo form_open('Admin/holistic_prosomatics/save_new_user', array('method' => 'post', "id" => "UserForm", "class" => "validate")); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <p class="form-group">
                    <?php echo form_label('First Name', 'first_name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'first_name', 'id' => 'first_name', 'value' => set_value('first_name'), 'class' => 'col-md-12 form-control')); ?>		
                </p>
                <p>
                    <?php echo form_label('Last Name', 'last_name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'last_name', 'id' => 'last_name', 'value' => set_value('last_name'), 'class' => 'col-md-12 form-control')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Email', 'email'); ?>
                    <span class="mandatory">*</span>
                    <input type="text" name="email" id="email" autocomplete="off" value="<?php echo set_value('email') ?>"  class="col-md-12 form-control required email" onblur="check_email(this)" />
                </p>
                <p class="form-group">
                    <?php echo form_label('Password', 'password'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_password(array('type' => 'password', 'name' => 'password', 'id' => 'Password', 'value' => '', 'class' => 'col-md-12 form-control required')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Confirm Password', 'confirm_password'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_password(array('type' => 'password', 'name' => 'confirm_password', 'id' => 'confirm_password', 'value' => '', 'class' => 'col-md-12 form-control required confirmPassword')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Phone Number', 'phone_no'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'phone_no', 'id' => 'phone_no', 'value' => set_value('phone_no'), 'class' => 'col-md-12 form-control')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Activation Key Code', 'activation_key'); ?>
                    <span class="mandatory">*</span>
                    <?php $val = isset($activation_key) ? $activation_key : set_value('activation_key') ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'activation_key', 'readonly' => 'readonly', 'id' => 'activation_key', 'value' => $val, 'class' => 'col-md-12 form-control required')); ?>
                </p>
                <p>
                    <?php echo form_label('Distributors', 'distributor'); ?>
                    <select id="distributor" name="distributor_id" class="col-md-12 form-control"> 
                        <option value="0" disabled="disabled" selected="selected">Select Distributor</option>
                        <?php foreach ($distributors_data as $distributor) : ?>
                            <option value="<?php echo $distributor['id']; ?>"><?php echo $distributor['distributor'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php echo form_input(array('class' => 'btn  blue', 'value' => 'Add', 'type' => 'submit'));
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        ?>
    </div>
<?php echo form_close(); ?>
    <script>
        FormValidation.init();
        function check_email(obj){
            var email = $.trim($(obj).val());
            if(email==="") return false;
            $.ajax({
                url :site_url+"Admin/holistic_prosomatics/check_email",
                data: {email : email},
                type:"POST",
                async:false,
                success: function(html){
                    var response= $.trim(html);
                    if(response==1){
                        alert("Email You Entered Already Exist. Please use another email");
                        $(obj).val("");
                    }
                },
                failure:function(xhr,status,code){
                    alert("Error Occured");
                }
            });
            return false;
        }
    </script>
</div>