<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//echo validation_errors('<span class="error">', '</span>');
?>
<style type="text/css">
    .hide_tag{ display:none;}
    .show_tag{display: block;}
    .quant-device-popup table{
        padding: 2px;
        margin: 1% 1% 5% 1%; 
        width: 100%;
    }
    .quant-device-popup table th, .quant-device-popup table td{
        padding: 4px;
    }
    /*.modal{min-width:800px;}*/	
    .modal-scrollable #ajax-modal .modal-body .col-md-12 form{height:auto;overflow:hidden}
    #ajax-modal{
        left:50%;
    }
</style>
<?php $encrypt_user_id = $this->encrypt->encode($user_id); ?>
<div class="my-modal" id="div1^^<?php echo $encrypt_user_id; ?>">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">User Registered Devices</h4>
    </div>

    <div id="div^^<?php echo $encrypt_user_id; ?>">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12 quant-device-popup" >
                    <?php
                    if ($devices) {
                        $html = '<div class="list-holder"><table border="1">'
                                . '<tr>'
                                . '<th>Device</th>'
                                . '<th>UUID</th>'
                                . '<th>Platform</th>'
                                . '<th>Action</th>'
                                . '<tr>';

                        foreach ($devices as $device) {
                            $html.= '<tr>'
                                    . '<td>' . $device['device_description'] . '</td>'
                                    . '<td>' . $device['uuid'] . '</td>'
                                    . '<td>' . $device['device_type'] . '</td>'
                                    . '<td><a class="delete_device" data-device="' . $device['device_id'] . '">Delete</a></td>'
                                    . '</tr>';
                        }
                        $html .= '</table></div>';
                        echo $html;
                    }
                    ?>
                    <a class="edit btn blue" onclick="edit_devices(this)"><?php echo $devices ? 'Add more device' : 'Add Device'; ?></a>
                    <form id="UserForm" class="validate" style="display:none">
                        <p class="form-group" style="width:31.5%">
                            <label>Description</label>
                            <input type="text" name="device_description" id="device_description" value="" class="col-md-12 form-control"/>
                        </p>
                        <p class="form-group"  style="width:31.5%">
                            <label>UUID</label>
                            <input type="text" name="uuid" id="uuid" value="" class="col-md-12 form-control required"/>
                        </p>
                        <p class="form-group" style="width:31.5%">
                            <label>Device Type</label>
                            <select id="device_type" name="device_type" class="col-md-12 form-control">
                                <option value="">Select Platform</option>
                                <option value="ios">Ios</option>
                                <option value="android">Android</option>
                            </select>
                        </p>
                        <p class="form-group">
                            <input type="button" class="btn default add_device" value="Save"/>
                            <input type="button" class="btn default cancel_adding" value="Cancel"/>
                            <input type="hidden" name="user_id" value="<?php echo $user_id ?>"/>
                        </p>
                        <div class="alert alert-danger display-hide alert_message_div">
                            <button data-close="alert" class="close"></button>
                            <span></span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="loader" style="display:none;"><img src="<?php echo base_url('assets/img/loader.gif') ?>" alt="Processing..."/></div>
        </div>


    </div>
    <div class="modal-footer">
        <?php
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        ?>

    </div>

    <script>
        FormValidation.init();
    </script>
    <script type="text/javascript">

        $('.modal-body').on('click', '.delete_device', function() {
            var id = $(this).data('device');
            if (!id)
                return false;
            var confirm_result = confirm('Do you really want to delete this device?');
            if (confirm_result) {
                $.ajax({
                    url: site_url + 'Admin/holistic_prosomatics/delete_registered_device',
                    method: 'POST',
                    data: {device_id: id},
                    beforeSend: function() {
                        $('.loader').show();
                    },
                    success: function(response) {
                        console.log(response);
                        if ($.trim(response) == '1') {
                            var url = site_url + 'Admin/holistic_prosomatics/list_devices/' + $('#UserForm').find('input[name="user_id"]').val();
                            $('#ajax-modal').load(url, function(e) {
                                $('#ajax_modal').modal('show');
                            });
                        } else {
                            alert("Unable to delete");
                        }
                    },
                    complete: function() {
                        $('.loader').hide();
                    }
                });
            }
        });

        function edit_devices(obj) {
            $('#UserForm')[0].reset();
            $('#UserForm').show();
            $('.alert_message_div span').html('');
            $('.alert_message_div').hide();
        }

        $('input[name="uuid"]').blur(function() {
            var length = ($.trim($(this).val())).length;
            if (length) {
                if (length <= 16) {
                    $('[name="device_type"]').val("android");
                } else {
                    $('[name="device_type"]').val("ios");
                }
            }
        });
        $('.add_device').click(function() {
            var this_form = $('#UserForm');
            var flag = this_form.valid();
            if (flag) {
                $.ajax({
                    url: site_url + 'Admin/holistic_prosomatics/add_device',
                    method: 'POST',
                    dataType: "json",
                    data: {
                        user_id: this_form.find('input[name="user_id"]').val(),
                        uuid: this_form.find('input[name="uuid"]').val(),
                        device_description: this_form.find('input[name="device_description"]').val(),
                        device_type: this_form.find('select[name="device_type"]').val()
                    },
                    beforeSend: function() {
                        $('.loader').show();
                        $('.add_device').attr('disabled', 'disabled');
                    },
                    success: function(response) {
                        console.log(response);
                        if (response.status === 'success') {/*reload the modal*/
                            $('#UserForm').hide();
                            $('#UserForm')[0].reset();
                            var url = site_url + 'Admin/holistic_prosomatics/list_devices/' + this_form.find('input[name="user_id"]').val();
                            $('#ajax-modal').load(url, function(e) {
                                $('#ajax_modal').modal('show');
                            });
                        } else {
                            $('.alert_message_div span').html(response.message);
                            $('.alert_message_div').show();
                        }
                    },
                    complete: function() {
                        $('.loader').hide();
                        $('.add_device').removeAttr('disabled');
                    },
                    error: function(xhr, status, error) {
                        $('.alert_message_div span').html("Network Error");
                        $('.alert_message_div').show();
                    }
                });
            }
        });
        $('.cancel_adding').click(function() {
            $('#UserForm').hide();
        });
    </script>
</div>