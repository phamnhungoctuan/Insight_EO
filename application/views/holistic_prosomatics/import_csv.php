<html>
    <body>
        <div class="dashbard-content">
            <div class="main-content clearfix">
                <form id="usualValidate" class="form" method="post" action="<?php echo base_url($action); ?>" enctype="multipart/form-data">
                    <fieldset>
                        <div class="widget">
                            <div class="title"><img src="<?php echo $this->config->config['base_url']; ?>/assets/images/icons/dark/alert.png" alt="" class="titleIcon" />
                            </div>

                            <div class="success" style="margin: 10px;color:green;"><?php echo $this->session->flashdata('success') ?></div>
                            <div class="formRow">
                                <label>Upload CSV</label>
                                <div class="formRight">
                                    <input type="file" name="csvfile" class="required"/>
                                    <div class="error"><?php echo isset($errors) ? $errors : ''; ?></div>
                                </div>            
                            </div>

                            <div class="formSubmit formRow">
                                <label style="font-size:0">data</label>
                                <div class="formRight"><input type="submit" value="Submit" class="blueB newcase_submit"/></div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </body>
</html>


