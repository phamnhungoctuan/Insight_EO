
<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue-hoki">
        <div class="portlet-title">
            <div class="caption">
                <?php $total_rows = isset($total_rows) ? $total_rows : ($results && !empty($results) ? count($results) : "0") ?>
                <i class="fa fa-globe"></i><span> <?php echo "Total Records : " . $total_rows; ?></span>
                <?php if (isset($report_title)) { ?>
                    <i class="fa fa-globe"></i><span><?php echo $report_title ?></span>
                <?php }
                ?>
            </div>
            <div class="actions">
                <div class="btn-group">
                    <a class="btn default ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/holistic_prosomatics/add'; ?>"><i class="fa fa-pencil"></i><span>Create New User</span> </a>
                </div>
            </div>
        </div> 

        <div class="portlet-body set-table" >
            <?php if ($this->router->class != 'reports') { ?>
                <div class="cat-nav" >
                    <div id="search1" style="display:inline-block;">
                        <form id="search" method="get">
                            <input type="text" name="q" placeholder="Search..." value="<?php echo isset($q) ? $q : "" ?>"/>
                            <input type="hidden" name="type"  value="<?php echo isset($type) ? $type : "all" ?>"/>
                            <input type="hidden" name="page"  value="0"/>
                            <input id='searchBtn' type="submit" value="Search"/>
                        </form>
                    </div>
                    <div id="viewAll1" class="c-list <?php echo isset($type) ? ($type == 'all' ? "active_submenu" : "") : "active_submenu"; ?>">
                        <a href="javascript:void(0)" class="js-tabs user_type btn-blue" data-url="all"  >View All Users</a>
                        <i class="fa fa-angle-downcds c-list-ico" style="display:none"></i>
                    </div>
                    <div id="activated" class="c-list <?php echo isset($type) && $type == 'activated' ? "active_submenu" : ""; ?>">
                        <a href="javascript:void(0)" class="js-tabs user_type btn-blue"  data-url="activated" >View Activated Users</a>
                    </div>
                    <div id="paid" class="c-list <?php echo isset($type) && $type == 'paid' ? "active_submenu" : ""; ?>">
                        <a  href="javascript:void(0)" class="js-tabs user_type btn-blue" data-url="paid"  >View Paid Users</a>
                    </div>
                    <div id="free" class="c-list <?php echo isset($type) && $type == 'free' ? "active_submenu" : ""; ?>">
                        <a href="javascript:void(0)" class="js-tabs user_type btn-blue"  data-url="free">View Free Users</a>
                    </div>
                    <div id="app" class="c-list <?php echo isset($type) && $type == 'app' ? "active_submenu" : ""; ?>">
                        <a href="javascript:void(0)" class="js-tabs user_type btn-blue" data-url="app">App Users</a>
                    </div>
                    <div id="facebook" class="c-list <?php echo isset($type) && $type == 'facebook' ? "active_submenu" : ""; ?>">
                        <a href="javascript:void(0)" class="js-tabs user_type btn-blue" data-url="facebook" >Facebook Users</a>
                    </div>
                </div>
            <?php } ?>
            <?php if ($results && !empty($results)) { ?>
                <div class="set-table-div secnd-tab" style="width:100%; height:65%; overflow:auto;">
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                        <thead>
                            <tr>
                                <th class="id" name="id" align="center" style="text-align:center"><a style="text-decoration:none; text-align:center" href="javascript:void(0)">#</a></th>
                                <th class="fname"><a style="text-decoration:none" href="javascript:void(0)">First Name</a></th>
                                <th class="fname fix-col-width"><a style="text-decoration:none" href="javascript:void(0)">Last Name</a></th>
                                <th class="td_Key"><a style="text-decoration:none" href="javascript:void(0)">Email</a></th>
                                <th class="td_Key"><a style="text-decoration:none" href="javascript:void(0)">Contact No</a></th>
                                <th class="td_Key" style="width:5%"><a style="text-decoration:none" href="javascript:void(0)">Key</a></th>
                                <th class="td_Key" style="width:9%"><a style="text-decoration:none" href="javascript:void(0)">Videos Activation Key</a></th>
                                <th class="td_Key" style="text-align:center;"><a style="text-decoration:none" href="javascript:void(0)" style="display:block;margin:2px;">Devices</a></th>
                                <th class=""><a style="text-decoration:none" href="javascript:void(0)">Distributors</a></th>
                                <th class="demail"><a style="text-decoration:none" href="javascript:void(0)">Send Email To Distributor</a></th>
                                <th class="data-close"><a style="text-decoration:none" href="javascript:void(0)">Date Created</a></th>
                                <th class="data-close" style="width:8%"><a style="text-decoration:none" href="javascript:void(0)">Action</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($results && !empty($results)) {
                                $counter = isset($row_counter) ? $row_counter : 1;
                                foreach ($results as $row) {
                                    $encrypt_emp_id = $this->encrypt->encode($row['id']);
                                    ?>
                                    <?php $classname = $row['status'] == "2" ? "activated" : ""; ?>
                                    <tr id="tr^^<?php echo $encrypt_emp_id; ?>" class=" <?php echo $classname; ?>">
                                        <td class="td_id id" data="<?php echo $row['id'] ?>" align="center"><?php echo $counter++; ?></td>
                                        <td class="data-close"><?php echo ucfirst($row["first_name"]) ?></td>
                                        <td class="data-close fix-col-width"><?php echo ucfirst($row["last_name"]); ?></td>

                                        <td ><?php echo $row["email"]; ?></td>
                                        <td ><?php echo $row["phone_no"]; ?></td>
                                        <td class="td_Key key" id="<?php echo 'activation_key_' . $row["id"] ?>" >
                                            <?php if (($row["activation_key"] == '' || $row['activation_key'] == NULL)) { ?>
                                                <button id="<?php echo 'button_activation_key_' . $row["id"] ?>"  class="create_key ajax-demo"  data-toggle="modal" data-url="<?php echo base_url() . 'Admin/holistic_prosomatics/add_activation_key/' . $row['id'] ?>" role="button" aria-disabled="false" title="Create new key"><i class="fa fa-key"></i></button> 
                                                <?php
                                            } else {
                                                echo "<span>" . $row['activation_key'] . "</span>";
                                                ?>
                                                <button id="<?php echo 'send_activation_key_' . $row["id"] ?>"  class="sendEmail" role="button"  onclick="send_activation_key('<?php echo $row["id"]; ?>','Admin/holistic_prosomatics/send_activation_key')" title="Re-Send Activation Key"><i class="fa fa-envelope"></i></button>
                                            <?php } ?>
                                        </td>
                                        <td class="td_Key key">
                                            <?php if (($row["3d_activation_key"] == '' || $row['3d_activation_key'] == NULL)) { ?>
                                                <button class="create_key ajax-demo"  data-toggle="modal" data-url="<?php echo base_url() . 'Admin/holistic_prosomatics/add_3d_activation_key/' . $row['id'] ?>" role="button" aria-disabled="false" title="Create new key"><i class="fa fa-key"></i></button> 
                                                <?php
                                            } else {
                                                echo "<span>" . $row['3d_activation_key'] . "</span>";
                                                ?>
                                                <button id="<?php echo 'send_activation_key_' . $row["id"] ?>"  class="sendEmail" role="button"  onclick="send_3d_activation_key('<?php echo $row["id"]; ?>')" title="Re-Send 3D - Activation Key"><i class="fa fa-envelope"></i></button>
                                            <?php } ?>
                                        </td>
                                        <td class="edit-del" style="text-align:center;"><a class="edit ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/holistic_prosomatics/list_devices/' . $row['id'] ?>" style="display:block;margin:2px"><i class="fa fa-mobile"></i></a></td>
                                        <td class="data-close dis">
                                            <?php
                                            if (!isset($distributors_data)) {
                                                echo $row['distributor'];
                                            } else {
                                                ?>
                                                <select id="<?php echo 'distributor_' . $row['id'] ?>" name="selectDistributor" data-url="Admin/holistic_prosomatics/set_distributor">
                                                    <option value="" disabled="disabled" selected="selected">Select Distributor</option>
                                                    <?php foreach ($distributors_data as $distributor) : ?>
                                                        <?php $select = $row['distributor_id'] == $distributor['id'] ? 'selected="selected"' : '' ?>
                                                        <option value="<?php echo $distributor['id']; ?>" <?php echo $select; ?>><?php echo $distributor['distributor'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>  
                                                <?php
                                            }
                                            ?>
                                        </td>
                                        <?php if (isset($distributors_data)) { ?>
                                            <td class="data-close demail">
                                                <button id="<?php echo $row["id"] ?>"  class="sendEmail" role="button" aria-disabled="false" data-url='Admin/holistic_prosomatics/send_distributor_email' onclick="sendEmail(this)" title="Send E-mail"><i class="fa fa-envelope"></i></button>
                                            </td>
                                        <?php }
                                        ?>
                                        <td class="data-close cdate"><?php echo date('M d, Y', strtotime($row['date_created'])); ?> </td>
                                        <td class="edit-del">
                                            <a class="edit ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/holistic_prosomatics/edit/' . $row['id'] ?>"><i class="fa fa-pencil"></i></a> / &nbsp;
                                            <a class="edit ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/holistic_prosomatics/change_password/' . $row['id'] ?>"><i class="icon-lock"></i></a> / &nbsp;
                                            <a class="edit ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/holistic_prosomatics/delete/' . $row['id'] ?>"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <?php } else echo "<h1>No Results Found<h1>"; ?>
            
            <div class="row" style="cursor: pointer">
                <div class="col-md-12 col-sm-12">
                    <?php echo isset($links) ? $links : ""; ?>
                </div>
            </div> 
        </div>
<!--        <div class="load-spinner" style="z-index: 10050;bottom:0;top:0;left:0;right:0;position:fixed;display:none">
            <div class="loading-spinner in" style="width: 200px; margin-left: -100px; z-index: 10051;"><div class="progress progress-striped active"><div class="progress-bar" style="width: 100%;"></div></div></div>
        </div>-->
    </div></div>
<script type="text/javascript">
    $('.user_type').click(function() {
        $('[name="type"]').val($(this).data('url'));
        $('form#search').submit();
    });
</script>
