<div class="my-modal">
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
//echo validation_errors('<span class="error">', '</span>');
    ?>
    <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add New User</h4>
    </div>
    <?php echo form_open('Admin/rife/save_new_user', array('method' => 'post', "id" => "UserForm", 'class' => 'validate')); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">

                <p class="form-group">
                    <?php echo form_label('Name', 'Name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'Name', 'id' => 'Name', 'value' => set_value('Name'), 'class' => 'col-md-12 form-control')); ?>		
                </p>
                <p class="form-group">
                    <?php echo form_label('Email', 'Email'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_input(array('type' => 'text', 'name' => 'Email', 'id' => 'Email', 'value' => '', 'class' => 'col-md-12 form-control required email')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Phone #', 'phone_no'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'phone_no', 'id' => 'phone_no', 'autocomplete' => 'off', 'value' => set_value('phone_no'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Country', 'Country'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'Country', 'id' => 'Country', 'autocomplete' => 'off', 'value' => set_value('country'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Payment-Date (From Paypal)', 'payment_date'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_date', 'id' => 'payment_date', 'autocomplete' => 'off', 'value' => set_value('payment_date'), 'class' => 'col-md-12 form-control date-picker')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Payment Amount', 'payment_amount'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_amount', 'id' => 'payment_amount', 'autocomplete' => 'off', 'value' => set_value('payment_amount'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Receipt-Id', 'payment_receipt_id'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_receipt_id', 'id' => 'payment_receipt_id', 'autocomplete' => 'off', 'value' => set_value('payment_receipt_id'), 'class' => 'col-md-12 form-control ')); ?>
                </p>

                <p class="form-group">
                    <?php echo form_label('IPN Track-Id (From Paypal)', 'ipn_track_id'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'ipn_track_id', 'id' => 'ipn_track_id', 'autocomplete' => 'off', 'value' => set_value('ipn_track_id'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Number of Licences', 'licence_purchase_count'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'licence_purchase_count', 'id' => 'licence_purchase_count', 'autocomplete' => 'off', 'value' => set_value('licence_purchase_count'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Key', 'Key'); ?>
                    <span class="mandatory">*</span>
                    <?php $val = isset($Key) ? $Key : set_value('Key'); ?>
                    <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'Key', 'id' => 'Key', 'autocomplete' => 'off', 'value' => $val, 'class' => 'col-md-12 form-control required ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('DeviceUDID', 'UUID'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'UUID', 'id' => 'UUID', 'autocomplete' => 'off', 'value' => set_value('UUID'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Date Expired', 'ExpiredDate'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'ExpiredDate', 'id' => 'ExpiredDate', 'autocomplete' => 'off', 'value' => set_value('ExpiredDate'), 'class' => 'col-md-12 form-control date-picker')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Date Activated', 'ActivatedDate'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'ActivatedDate', 'id' => 'ActivatedDate', 'autocomplete' => 'off', 'value' => set_value('ActivatedDate'), 'class' => 'col-md-12 form-control date-picker')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Date Created', 'CreatedDate'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'CreatedDate', 'id' => 'CreatedDate', 'autocomplete' => 'off', 'value' => set_value('CreatedDate'), 'class' => 'col-md-12 form-control date-picker')); ?>
                </p>

            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php echo form_input(array('class' => 'btn blue ', 'value' => 'Add', 'type' => 'submit'));
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        ?>
    </div>
<?php echo form_close(); ?>
    <script>
        jQuery(document).ready(function() {
            FormValidation.init();
        });
    </script>
</div>