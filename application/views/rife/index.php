<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$atts = array(
    'data-toggle' => 'modal',
    'class'=>'btn default ajax-demo'
); ?>
<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue-hoki">
        <div class="portlet-title">
            <div class="caption">
                <?php $total_rows= isset($total_rows)?$total_rows:($results && !empty($results)?count($results):"0")?>
            <i class="fa fa-globe"></i> <span><?php echo "Total Records : ".$total_rows;  ?></span>
            </div>
            <div class="actions">
                <div class="btn-group">
<!--                    <a class="btn btn-default btn-sm" href="javascript:;" data-toggle="dropdown"><i class="fa fa-cogs"></i></a>-->
                    <a class="btn default ajax-demo" data-toggle="modal" data-url="<?php echo base_url().'Admin/rife/user_add'; ?>"><i class="fa fa-pencil"></i>Create Rife Code </a>
                    <a id="generate-codes" href="javascript:void(0)" class="btn default" data-url="Admin/rife"><i class="fa fa-pencil"></i>Generate 1K codes</a>
                    <!--<a class="ajax-demo" data-toggle="modal"  data-url="<?php echo base_url().'Admin/rife/user_add'; ?>"><i class="fa fa-plus-circle"></i></a>-->
                    <?php //echo anchor('project/admin_add', '<i class="fa fa-pencil"></i> Add New Project', $atts);?>
                </div>
            </div>
        </div>
        <div class="portlet-body set-table" >
            <div style="width:100%; height:65%; overflow:auto;" >
                  <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th class="send-email">Email</th>
                        <th>Contact No</th>
                        <th>Country</th>
                        <th>Amount</th>
                        <th>Receipt-Id</th>
                        <th>Payment-Date</th>
                        <th>IPN-Track-Id</th>
                        <th>Licence-Purchased</th>
                        <th>Key</th>
                        <th>UUID (User's Device)</th>
                        <th>Expired Date</th>
                        <th>Activated Date</th>
                        <th>Created Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($results && !empty($results)) {
                        $i = $row_counter;
                        foreach ($results as $row) {

                            $encrypt_user_id = $this->encrypt->encode($row['Id']);
                            ?>
                            <tr id="tr^^<?php echo $encrypt_user_id; ?>">
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $row["Name"]; ?></td>
                                <td class="send-email"><?php echo $row["Email"]; ?></td>
                                <td><?php echo $row["phone_no"]; ?></td>
                                <td><?php echo $row["Country"]; ?></td>
                                <td><?php echo $row["payment_amount"]; ?></td>
                                <td><?php echo $row["payment_receipt_id"]; ?></td>
                                <td><?php echo $row['payment_date']=="0000-00-00 00:00:00" || empty($row['payment_date'])?"":date_format(date_create($row["payment_date"]), 'Y-m-d'); ?></td>
                                <td><?php echo $row["ipn_track_id"]; ?></td>
                                <td><?php echo $row["licence_purchase_count"]; ?></td>
                                <td class="td_Key key"><?php if (($row["Key"] == '' || $row['Key'] == NULL)) { ?> <button id="<?php echo $row["Id"] ?>"  class="create_key ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/rife/add_activation_key/' . $row['Id'] ?>" role="button" aria-disabled="false" title="Create new key"><i class="fa fa-key"></i></button> <?php } else echo $row['Key']; ?></td>
                                <td><?php echo $row["UUID"]; ?></td>
                                <td><?php echo $row['ExpiredDate']=="0000-00-00 00:00:00" || empty($row['ExpiredDate'])?"":date_format(date_create($row["ExpiredDate"]), 'Y-m-d'); ?></td>
                                <td><?php echo $row['ActivatedDate']=="0000-00-00 00:00:00" || empty($row['ActivatedDate']) ? "" : date_format(date_create($row["ActivatedDate"]), 'Y-m-d'); ?></td>
                                <td><?php echo date("m-d-Y", strtotime($row["CreatedDate"])) ?></td>
                                <td class="edit-del">
                                    <a class="edit ajax-demo" data-toggle="modal" data-url="<?php echo base_url().'Admin/rife/user_edit/' . $row['Id']?>"><i class="fa fa-pencil"></i></a>
                                    <a class="edit ajax-demo" data-toggle="modal" data-url="<?php echo base_url().'Admin/rife/delete/' . $row['Id']?>" id="del"  title="Delete" class="delete"> <i class="fa fa-times"></i></a> 
                                </td>
                            </tr>   
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            </div>
            <div class="row">
                <div class="col-md-7 col-sm-12">
<?php  echo $links; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>