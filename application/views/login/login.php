<?php
defined('BASEPATH') OR exit('No direct script access allowed');
echo validation_errors('<span class="error">', '</span>');

?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.4
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Energy Re-Mastered</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="<?php echo $this->config->config['base_url'];?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url'];?>/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url'];?>/assets/css/plugins/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo $this->config->config['base_url'];?>/assets/css/login.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo $this->config->config['base_url'];?>/assets/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url'];?>/assets/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo $this->config->config['base_url'];?>/assets/css/themes/darkblue.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?php echo $this->config->config['base_url'];?>/assets/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login">
    	<!----->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                   <a href="<?php echo base_url().'Admin/admin_user/index'; ?>">
                <img src="<?php echo $this->config->config['base_url'];?>/assets/img/logo.png" alt=""/>
            </a>
                    <div class="menu-toggler sidebar-toggler hide">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a data-target=".navbar-collapse" data-toggle="collapse" class="menu-toggler responsive-toggler" href="javascript:;">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!----->
    
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <?php echo form_open('authentication/login',array('id'=>'form_submit',"class"=>"login-form")); ?>
            <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span>
                        Enter any username and password. </span>
            </div>
                <h3 class="form-title">Sign In</h3>
                
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <?php echo form_input(array('type'=>'text','name'=>'username', 'class'=>'form-control form-control-solid placeholder-no-fix required',  'placeholder'=>'Username', 'id'=>'username','autocomplete'=>'off','value'=>''));?>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <?php echo form_input(array('type'=>'password','name'=>'password', 'class'=>'form-control form-control-solid placeholder-no-fix required',  'placeholder'=>'Password', 'id'=>'password','autocomplete'=>'off','value'=>''));?>
                </div>
                
                <div class="form-actions">
                    <input type="submit" id="submit_btn" value="Login"  class="btn btn-success uppercase" />
<!--                    <label class="rememberme check">
                        <input type="checkbox" name="remember" value="1"/>Remember </label>-->
<!--                        <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>-->
                </div>
            <?php echo form_close(); ?>
            <?php if ($this->session->flashdata('error')) : ?>
                <p><?php echo $this->session->flashdata('error'); ?></p>
            <?php endif; ?>
        </div>
        <div class="copyright">
            <?php echo date('Y');?> &copy; <?php echo "Energy Re-Mastered"?>
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <script src="<?php echo $this->config->config['base_url'];?>/assets/js/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/plugins/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/plugins/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/jquery.validate.min.js'); ?>"></script>
        <!-- END CORE PLUGINS -->
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['base_url']; ?>/assets/js/scripts/layout.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['base_url'];?>/assets/js/scripts/login.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                Login.init();
            });
        </script>
        
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>

