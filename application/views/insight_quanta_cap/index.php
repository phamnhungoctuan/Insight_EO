<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue-hoki">
        <div class="portlet-title">
            <div class="caption">
                <?php if (isset($total_rows)) { ?> 
                    <i class="fa fa-globe"></i><?php echo "Total Records : " . $total_rows; ?>
                <?php } ?>
            </div>
            <!--            <div class="actions">
                            <div class="btn-group">
                                <a class="btn default ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/insight_quanta_cap/user_add'; ?>"><i class="fa fa-pencil"></i>Create New User </a>
                            </div>
                        </div>-->
        </div>
        <div class="portlet-body set-table">
            <?php if ($this->router->class != 'reports') { ?>
                <div class="cat-nav" >
                    <div id="search1" style="display:inline-block;">
                        <form id="search" method="get" class="user-search-form">
                            <input type="text" name="q"  placeholder="Search..." value="<?php echo isset($q) ? ($q) : "" ?>" class="user_search_text"  />
                            <input type="hidden" name="type"  value="<?php echo isset($type) ? $type : "all" ?>"/>
                            <input type="hidden" name="page"  value="0"/>
                            <input id='user_searchBtn' type="submit" value="Search"/>
                        </form>
                    </div>
                    <div id="viewAll1" class="c-list <?php echo!empty($type) ? ($type == 'all' ? "active_submenu" : "") : "active_submenu"; ?>">
                        <a href="javascript:void(0)" data-url="all" class="js-tabs user_type btn-blue" >View All Users</a>
                    </div>
                    <div id="activated" class="c-list <?php echo isset($type) && $type == 'activated' ? "active_submenu" : ""; ?>">
                        <a href="javascript:void(0)" data-url="activated" class="js-tabs user_type btn-blue" >View Activated Users</a>
                    </div>
                    <div id="free" class="c-list <?php echo isset($type) && $type == 'free' ? "active_submenu" : ""; ?>">
                        <a href="javascript:void(0)" class="js-tabs user_type btn-blue"  data-url="free">View Free Users</a>
                    </div>
                    <div id="product" class="c-list <?php echo isset($type) && $type == 'product' ? "active_submenu" : ""; ?>">
                        <a href="<?php echo base_url() . 'Admin/insight_quanta_cap/product_keys'; ?>" data-url="product" class="js-tabs user_type btn-blue">Product-key Listing</a>
                    </div>
                </div>
            <?php } ?>
            <?php $scrollerClass = $this->router->class != 'reports' ? "secnd-tab" : ""; ?>
            <div class="set-table-div <?php echo $scrollerClass; ?>">
                <?php if (!empty($results)) { ?>
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                        <thead>
                            <tr>
                                <th class="id"><a style="text-decoration:none" href="javascript:void(0)">Record#</a></th>
                                <th><a style="text-decoration:none" href="javascript:void(0)">First Name</a></th>
                                <th class="send-email"><a style="text-decoration:none" href="javascript:void(0)">Email</a></th>
                                <th><a style="text-decoration:none" href="javascript:void(0)">Device UUID</a></th>
                                <th><a style="text-decoration:none" href="javascript:void(0)">Account Status</a></th>
                                <th><a style="text-decoration:none" href="javascript:void(0)">Activation Key</a></th>
                                <th><a style="text-decoration:none" href="javascript:void(0)">Date Activated</a></th>
                                <th><a style="text-decoration:none" href="javascript:void(0)">Date Created</a></th>
                                <th class="text-center"><a href="#">Action</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = isset($page) ? $page + 1 : 1;
                            foreach ($results as $row) {

                                $encrypt_user_id = $this->encrypt->encode($row['user_id']);
                                ?>
                                <tr id="tr^^<?php echo $encrypt_user_id; ?>">
                                    <td class="td_id"><?php echo $i++; ?></td>
                                    <td><?php echo $row["first_name"]; ?></td>
                                    <td><?php echo $row["email"]; ?></td>
                                    <td class="td_uuid did">
                                        <?php echo $row["uuid"];
                                        if (!($row['uuid'] == '' || $row['uuid'] == NULL) && $row['uuid_is_blocked'] == 0) {
                                            ?>
                                            <br/><button class="blockDevice" role="button" data-id="<?php echo $row["user_id"]; ?>" data-url="insight_quanta_cap" onclick="block_device(this)">Block</button>
                                        <?php
                                        } else
                                        if (!($row['uuid'] == '' || $row['uuid'] == NULL) && $row['uuid_is_blocked'] == 1) {
                                            ?>
                                            <br/><button class="unblockDevice" role="button" data-id="<?php echo $row["user_id"]; ?>" data-url="insight_quanta_cap" onclick="unblock_device(this)">Unblock</button>

                                    <?php }
                                    ?>
                                    </td>
                                    <td class="td_uuid"><?php echo $row["account_status"]; ?></td>
                                    <td class="td_Key"> <?php echo $row['activation_key']; ?></td>
                                    <td><?php echo format_date($row["date_activated"]); ?></td>
                                    <td><?php echo format_date($row["date_created"]); ?></td>
                                    <td class="edit-del text-center" >
                                        <a class="edit ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/insight_quanta_cap/user_edit/' . $row['user_id'] ?>"><i class="fa fa-pencil"></i> </a> 
                                        <!--<a class="edit ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/insight_quanta_cap/delete/' . $row['user_id'] ?>"> <i class="fa fa-times"></i></a>--> 
                                    </td>
                                </tr>   
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                    <?php
                } else {
                    echo '<h1 style="text-align:center;"> No Results Found <h1>';
                }
                ?>
            </div>
            <div class="row">
                <div class="col-md-7 col-sm-12">
                    <?php echo isset($links) ? $links : ""; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
<script type='text/javascript'>

    $(document).ready(function () {
        $('.user_type').click(function () {
            $('[name="type"]').val($(this).data('url'));
            $('form#search').submit();
        });

    });

</script>
