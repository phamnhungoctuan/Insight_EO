<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                Welcome to The Insight Quanta Cap
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Hi <?php echo isset($user_detail['name']) ? " ". $user_detail['name'] : 'User'; ?>,</p>
                            <?php echo isset($user_detail['activation_key']) ? '<p>Your Activation key is :  '. $user_detail['activation_key'] .'</p>': '' ?>
                            <p>Please enter this into The Insight Quanta Cap to gain instant access. </p>
                            <p style="line-height:15px"> 
                                Quick Insight Capsule Tutorial <a href="https://youtu.be/tQmNpKT7mgo">https://youtu.be/tQmNpKT7mgo</a>
                            </p>
                            <p>A new world awaits you.</p>
                                
                            <p>Thank you, <br/> Energy Re-Mastered Apps Support</p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
