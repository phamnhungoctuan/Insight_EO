<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family:arial,sans-serif; font-size: 34px; color: #57697e;">
                                Download & Activation Codes: Insight Quanta Cap
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family:arial,sans-serif;">
                            <p style="line-height:15px;">Hi<?php echo !empty($key_detail['name']) ? " " . trim($key_detail['name']) : ' User' ?>,</p>
                            <p style="line-height:15px;">Thank you for joining Energy Re-Mastered Apps and investing in the Insight Quanta Cap.</p>
                            <p style="line-height:15px;">Below is your activation code required to activate your app, once you have fully downloaded and installed Insight Quanta Cap.</p>
                            <p style="line-height:15px">
                                App Activation Code : <b><?php echo strlen($key_detail['activation_key'])? $key_detail['activation_key'] : "" ?></b>
                            </p>
                            <p style="line-height:15px">
                                <font>
                                    iOS Download Link:
                                    <a href="https://rink.hockeyapp.net/apps/97b907f232d14ea38c77ebe4fdf5c853"> https://rink.hockeyapp.net/apps/97b907f232d14ea38c77ebe4fdf5c853</a>
                                </font>
                            </p>
                            <p style="line-height:15px">
                                "Untrusted Developer Pop Up Box" <br/>
                                Once this appears, select your iOS "Settings" icon, then "General" then scroll down to the bottom and select "Device Management" and Trust the Canada Inc profile.
                            </p>
                            <p style="line-height:15px">
                                <font>
                                    Android Download Link:
                                    <a href="https://rink.hockeyapp.net/apps/92644cdc9c5e4c629ca9467b86a383aa"> https://rink.hockeyapp.net/apps/92644cdc9c5e4c629ca9467b86a383aa</a>
                                </font>
                            </p>
                            <p style="line-height:15px"> 
                                If prompted go to your "Settings" then "Apps" and allow installation from 3rd party apps.
                            </p>
                            <p style="line-height:15px"> 
                                Quick Insight Capsule Tutorial <a href="https://youtu.be/tQmNpKT7mgo">https://youtu.be/tQmNpKT7mgo</a>
                            </p>
                            <p style="line-height:15px"> 
                                Exporting Healing Frequencies: Insight Quanta Cap for Genius Insight<a href="https://youtu.be/EnlgL0La16M">https://youtu.be/EnlgL0La16M</a>
                            </p>
                            <p style="line-height:15px"> 
                                More Software Applications and Products : <a href="http://quantumhealthapps.com/">http://quantumhealthapps.com</a>
                            </p>
                            <p style="line-height:15px"> 
                                <b>FOR ALL INSTALLATION OR REINSTALLATION LINKS PLEASE SAVE THIS WEBPAGE</b> : <a href="http://quantumhealthapps.com/software-downloads/">http://quantumhealthapps.com/software-downloads/</a>
                            </p>
                            <p style="line-height:15px"> 
                                Genius Hardware Instructions : <a href="http://quantumhealthapps.com/genius-hardware-instructions/">http://quantumhealthapps.com/genius-hardware-instructions/</a>
                            </p>
                            <p style="line-height:15px"> 
                                Thank you, <br/>
                                Energy Re-Mastered Apps Support<br/>
                            </p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
