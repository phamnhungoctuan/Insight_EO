<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                               Device Unblocked : The Insight Quanta Cap
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Hi <?php echo !empty($user_detail['name']) ? " " . $user_detail['name'] : 'User'; ?>,</p>
                      
                            <p>Your device is now unblocked for this App</p>
                                
                            <p>Thank you,<br/>Energy Re-Mastered Apps Support</p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
