<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="my-modal">
    <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add New User</h4>
    </div>
    <?php echo form_open('Admin/insight_quanta_cap/save_new_user', array('method' => 'post', "id" => "UserForm", "class" => "validate")); ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <p class="form-group">
                        <?php echo form_label('Email', 'email'); ?>
                        <span class="mandatory">*</span>
                        <?php echo form_input(array('type' => 'text', 'name' => 'email', 'id' => 'email', 'value' => set_value('email'), 'class' => 'col-md-12 form-control email'/* , "onblur" => "check_email(this)" */)); ?>
                    </p>
                    <p class="form-group">
                        <?php echo form_label('Device Id', 'uuid'); ?>
                        <?php echo form_input(array('type' => 'text', 'name' => 'uuid', 'id' => 'uuid', 'autocomplete' => 'off', 'value' => set_value('UUID'), 'class' => 'col-md-12 form-control ')); ?>
                    </p>

                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php
            echo form_input(array('class' => 'btn blue', 'value' => 'Add', "type" => "submit"));
            echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
            ?>
        </div>
        <?php echo form_close(); ?>
        <script>
            FormValidation.init();
        </script>
    </div>