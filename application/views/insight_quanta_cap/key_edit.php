<div class="my-modal">
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    echo validation_errors('<span class="error">', '</span>');
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit User</h4>
    </div>
    <?php echo form_open('Admin/insight_quanta_cap/edit_key', array('method' => 'post', "id" => "KeyForm", "class" => "validate")); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">

                <p class="form-group">
                    <?php echo form_label('First Name', 'first_name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'first_name', 'id' => 'first_name', 'value' => $key_data['first_name'], 'class' => 'col-md-12 form-control')); ?>		
                </p>
                <p class="form-group">
                    <?php echo form_label('Last Name', 'last_name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'last_name', 'id' => 'last_name', 'value' => $key_data['last_name'], 'class' => 'col-md-12 form-control')); ?>		
                </p>
                <p class="form-group">
                    <?php echo form_label('Email', 'email'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_input(array('type' => 'text', 'name' => 'email', 'id' => 'email', 'value' => $key_data['email'], 'class' => 'col-md-12 form-control email')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Address', 'address'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'address', 'id' => 'address', 'value' => $key_data['address'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('City', 'city'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'city', 'id' => 'city', 'value' => $key_data['city'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('State', 'state'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'state', 'id' => 'state', 'value' => $key_data['state'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Country', 'country'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'country', 'id' => 'country', 'autocomplete' => 'off', 'value' => $key_data['country'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Zip Code', 'zip_code'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'zip_code', 'id' => 'zip_code', 'autocomplete' => 'off', 'value' => $key_data['zip_code'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Activation Key', 'activation_key'); ?>
                    <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'activation_key', 'id' => 'Key', 'autocomplete' => 'off', 'value' => $key_data['activation_key'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Date Activated', 'date_activated'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'date_activated', 'id' => 'date_activated', 'autocomplete' => 'off', 'value' => $key_data['date_activated'], 'class' => 'col-md-12 form-control date-picker')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Date Created', 'date_created'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'date_created', 'id' => 'date_created', 'autocomplete' => 'off', 'value' => $key_data['date_created'], 'class' => 'col-md-12 form-control date-picker')); ?>
                </p>
                <?php
                echo form_input(array('type' => 'hidden', 'value' => $key_data['key_id'], 'name' => 'key_id', 'id' => 'key_id', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control '));
                ?>

            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php
        echo form_input(array('class' => 'btn blue', 'value' => 'Update', "type" => "submit"));
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        ?>
    </div>
    <?php echo form_close(); ?>
    <script>
        jQuery(document).ready(function () {
            FormValidation.init();
        });
    </script>
</div>