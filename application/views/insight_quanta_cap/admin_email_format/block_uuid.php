<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                               The Insight Quanta Cap Device blocked
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Hi Admin,</p>
                            <p>Following is the detail of the user whose device has been blocked for Insight Quanta Cap</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height:normal;" >
                            <table style="width: 100%;border: 1px solid #999;text-align: left;border-collapse: collapse;margin: 0 0 1em 0;" cellspacing="0" cellpadding="0" >
                                <tr style="background: #eee;">
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;" colspan="2">
                                        User Detail
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                        Email
                                    </th>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                        <?php echo $user_detail['email']; ?>
                                    </th>
                                </tr>
                                <?php if(!empty($user_detail['name'])){?>
                                <tr>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                        Name
                                    </th>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                        <?php echo ucfirst($user_detail['name']); ?>
                                    </th>
                                </tr>
                                <?php }?>
                                <tr>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                        Activation Key
                                    </th>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                        <?php echo $user_detail['activation_key'] ?>
                                    </th>
                                </tr>
                            </table>
                        </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
