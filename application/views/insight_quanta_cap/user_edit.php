<div class="my-modal">
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    echo validation_errors('<span class="error">', '</span>');
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit User</h4>
    </div>
    <?php echo form_open('Admin/insight_quanta_cap/edit_user', array('method' => 'post', "id" => "UserForm", "class" => "validate quanta_cap_edit_user_form")); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <p class="form-group">
                    <?php echo form_label('First Name', 'first_name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'first_name', 'id' => 'first_name', 'value' => $user_data['first_name'], 'class' => 'col-md-12 form-control'/* , 'onblur' => 'check_email(this)' */)); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Email', 'email'); ?>
                    <span class="mandatory">*</span>
                    <?php
                    echo form_input(array('type' => 'text', /* 'readonly' => 'readonly', */ 'name' => 'email', 'id' => 'email', 'value' => $user_data['email'], 'class' => 'col-md-12 form-control email'/* , 'onblur' => 'check_email(this)' */));
                    echo form_error('email');
                    ?>

                </p>
                <p class="form-group">
                    <?php echo form_label('Device Id', 'uuid'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'uuid', 'id' => 'uuid', 'autocomplete' => 'off', 'value' => $user_data['uuid'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
<!--                <p class="form-group">
                <?php // echo form_label('Date Created', 'date_created'); ?>
                <?php // echo form_input(array('type' => 'text', 'name' => 'date_created', 'id' => 'date_created', 'autocomplete' => 'off', 'value' => $user_data['date_created'], 'class' => 'col-md-12 form-control date-picker')); ?>
                </p>-->
                <?php
                echo form_input(array('type' => 'hidden', 'value' => $user_data['user_id'], 'name' => 'user_id', 'id' => 'user_id', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control '));
                ?>

            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php
        echo form_input(array('id' => 'quanta_cap_edit_user_btn', 'class' => 'btn blue', 'value' => 'Update', "type" => "button", 'onClick' => 'quanta_cap_edit_user(this)'));
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        ?>
    </div>
    <?php echo form_close(); ?>
    <script>
        FormValidation.init();
        function quanta_cap_edit_user(obj) {
            var form = $(obj).parents('form.quanta_cap_edit_user_form');
            if (form.valid()) {
                $.ajax({
                    url: site_url + 'Admin/insight_quanta_cap/edit_user',
                    method: "post",
                    data: form.serialize(),
                    dataType: "json",
                    beforeSend: function () {
                        $('#quanta_cap_edit_user_btn').attr('disabled', 'disabled');
                        $(form).find('span.help-block-error').remove();
//                        $('div.load-spinner').show();
                    },
                    complete: function () {
//                        $('div.load-spinner').hide();
                        $('#quanta_cap_edit_user_btn').removeAttr('disabled');
                    },
                    success: function (response) {
                        if (response['status'] == true) {
                            $("#ajax-modal").modal('hide');
                            alert(response['message']);
                            location.reload();

                        } else {
                            var message = typeof (response['message']) != 'undefined' ? response['message'] : "";

                            if (typeof (message.email) != 'undefined') {
                                $(form).find('input[name="email"]').parent('p').addClass('has-error');
                                $(form).find('input[name="email"]').parent('p').find('span').remove();
                                $(form).find('input[name="email"]').after('<span id="Email-error" class="help-block help-block-error">' + message.email + '</span>');
                            }
                            else {
                                $(form).find('input[name="email"]').parent('p').addClass('has-error');
                                $(form).find('input[name="email"]').parent('p').find('span').remove();
                                $(form).find('input[name="email"]').after('<span id="Email-error" class="help-block help-block-error">' + response['message'] + '</span>');
                            }
                        }
                    }
                });
            }
        }
    </script>
</div>