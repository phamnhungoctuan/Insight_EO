<div class="my-modal">
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    echo validation_errors('<span class="error">', '</span>');
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Delete User</h4>
    </div>
    <?php echo form_open('Admin/royal_rife/delete', array('method' => 'post', "id" => "UserForm","style"=>'height:150px;' ,"class" => "validate")); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <p >
                    Are You Sure You Want to delete this record ?
                </p>
                <?php
                echo form_input(array('type' => 'hidden', 'value' => $id, 'name' => 'id', 'id' => 'id', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control '));
                ?>

            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php
        echo form_input(array('class' => 'btn blue', 'value' => 'Delete', 'type' => 'submit'));
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Cancel'));
        ?>
    </div>
    <?php echo form_close(); ?>
    <script>
        FormValidation.init();
    </script>
</div>