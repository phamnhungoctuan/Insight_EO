<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Helvetica,Arial,sans-serif;" size="5" color="#57697e" style="font-size: 34px;">
                                <h1 style="color: rgb(56, 56, 56);font-family: Helvetica,Arial,sans-serif;font-size: 22px; line-height: 1.6; margin: 0 0 16px; padding: 0; text-align: center;">Download &amp; Activation Codes:&nbsp;</h1>
                                <h1 style="color: rgb(56, 56, 56);font-family: Helvetica,Arial,sans-serif;font-size: 22px; line-height: 1.6; margin: 0 0 16px; padding: 0; text-align: center;">Energy Re-Mastered Royal Rife Application</h1>
                            </font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="color: rgb(52, 52, 52);font-family: Helvetica,Arial,sans-serif;font-size: 16px; line-height: 1.6;">
                            <p style="margin: 0 0 13px; padding: 0;">Hi<?php echo !empty($user_detail['first_name']) ? " " . trim($user_detail['first_name']) : ' User' ?>,</p>
                            <p style="margin: 13px 0; padding: 0;">Thank you for joining Energy Re-Mastered  Apps and investing in the Royal Rife Machine App</p>
                            <p style="margin: 13px 0; padding: 0;">Below is your activation code required to activate your app, once you have fully downloaded and installed Your Royal Rife Machine App.</p>
                            <?php if(count($activation_keys)) { ?>
                            <div>
                                <table style="width: 100%;border: 1px solid #999;text-align: left;border-collapse: collapse;margin: 0 0 1em 0;" cellspacing="0" cellpadding="0" >
                                    <tr style="background: #eee;">
                                        <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                            Activation Keys
                                        </th>
                                    </tr>
                                    <?php foreach($activation_keys as $key){ ?>
                                    <tr>
                                        <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                             <?php echo $key; ?>
                                        </th>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                            <?php } /*END OF IF*/ ?>
                            <p style="margin: 13px 0; padding: 0;">
                                <font>
                                iOS Download Link : <a style=" color: rgb(43, 170, 223);line-height: 1.6;margin: 0;padding: 0;" href="https://rink.hockeyapp.net/apps/37d5bbeec758754d9c4f00cb057ccc20"> https://rink.hockeyapp.net/apps/37d5bbeec758754d9c4f00cb057ccc20</a>
                                </font>
                            </p>
                            <p style="margin: 13px 0; padding: 0;">
                            <ul style="list-style:disc;font-size:12.8px">
                                <li>
                                    "Untrusted Developer Pop Up Box" Once this appears, select your iOS "Settings" icon, then "General" then scroll down to the bottom and select "Device Management" and Trust the Canada Inc profile.
                                </li>
                            </ul>
                            </p>
                            <p style="margin: 13px 0; padding: 0;">
                                <font>
                                    Android Download Link : <a style=" color: rgb(43, 170, 223);line-height: 1.6;margin: 0;padding: 0;" href="https://rink.hockeyapp.net/apps/37202d7696d663c559b813f3f41dd30a">https://rink.hockeyapp.net/apps/37202d7696d663c559b813f3f41dd30a</a>
                                </font>
                            </p>
                            <p style="margin: 13px 0; padding: 0;"> 
                                <ul style="list-style: disc;font-size:12.8px">
                                    <li>
                                        If prompted go to your "Settings" then "Apps" and allow installation from 3rd party apps
                                    </li>
                                </ul>
                            </p>
                            <p style="margin: 13px 0; padding: 0;"> 
                                More Software Applications and Products : <a href="http://quantumhealthapps.com/">http://quantumhealthapps.com/</a>
                            </p>
                            <p style="margin: 13px 0; padding: 0;"> 
                                Genius Hardware Instructions : <a href="http://quantumhealthapps.com/genius-hardware-instructions/">http://quantumhealthapps.com/genius-hardware-instructions/</a>
                            </p>
                            <p style="margin: 13px 0; padding: 0;"> 
                                FOR ALL INSTALLATION OR REINSTALLATION LINKS PLEASE SAVE THIS WEBPAGE : <a href="http://quantumhealthapps.com/software-downloads/">http://quantumhealthapps.com/software-downloads/</a>
                            </p>
                            <p style="margin: 13px 0; padding: 0;"> 
                                Thank you,
                            </p>
                            <p style="margin: 13px 0; padding: 0;">
                                Energy Re-Mastered  Apps Support
                            </p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
