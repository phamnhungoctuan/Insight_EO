<div class="my-modal">
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    echo validation_errors('<span class="error">', '</span>');
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit User</h4>
    </div>
    <?php echo form_open('Admin/relationship_harmonizer/save_new_user', array('method' => 'post', "id" => "UserForm", "class" => "validate")); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">

                <p class="form-group">
                    <?php echo form_label('Name', 'name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'name', 'id' => 'name', 'value' => "", 'class' => 'col-md-12 form-control ')); ?>		
                </p>
                <p class="form-group">
                    <?php echo form_label('Email', 'email'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_input(array('type' => 'text', 'name' => 'email', 'id' => 'email', 'value' => "", 'class' => 'col-md-12 form-control email')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Phone #', 'phone'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'phone', 'id' => 'phone', 'autocomplete' => 'off', 'value' => "", 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('UUID', 'uuid'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'uuid', 'id' => 'uuid', 'autocomplete' => 'off', 'value' => "", 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Key', 'payment_key'); ?>
                    <?php $val = isset($Key) ? $Key : set_value('Key') ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_key', 'readonly' => 'readonly', 'id' => 'payment_key', 'value' => $val, 'class' => 'col-md-12 form-control required')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Payment-Status', 'payment_status'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_status', 'id' => 'payment_status', 'autocomplete' => 'off', 'value' => "", 'class' => 'col-md-12 form-control')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Gross Income', 'payment_amount'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_amount', 'id' => 'payment_amount', 'autocomplete' => 'off', 'value' =>"", 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Transaction ID', 'txnid'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'txnid', 'id' => 'txnid', 'autocomplete' => 'off', 'value' => "", 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Item ID', 'itemid'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'itemid', 'id' => 'itemid', 'autocomplete' => 'off', 'value' => "", 'class' => 'col-md-12 form-control ')); ?>
                </p>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php
        echo form_input(array('class' => 'btn blue', 'value' => 'Add', "type" => "submit"));
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        ?>
    </div>
    <?php echo form_close(); ?>
    <script>
        jQuery(document).ready(function() {
            FormValidation.init();
        });
    </script>
</div>