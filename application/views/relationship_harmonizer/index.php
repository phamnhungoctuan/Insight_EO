<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue-hoki">
        <div class="portlet-title">
            <div class="caption">
                <?php if (isset($total_rows)) { ?> 
                    <i class="fa fa-globe"></i><?php echo "Total Records : " . $total_rows; ?>
                <?php } ?>
            </div>
            <div class="actions">
                <div class="btn-group">
                    <a class="btn default ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/relationship_harmonizer/user_add'; ?>"><i class="fa fa-pencil"></i><span>Create New Key</span> </a>
                </div>
            </div>
        </div>
        <div class="portlet-body set-table">
            <div class="cat-nav" >
                <div id="search1" style="display:inline-block;">
                    <form id="search" method="get" class="user-search-form">
                        <input type="text" name="q"  placeholder="Search..." value="<?php echo isset($q) ? ($q) : "" ?>" class="user_search_text"  />
                        <input type="hidden" name="page"  value="0"/>
                        <input id='user_searchBtn' type="button" value="Search"/>
                    </form>
                </div>
            </div>
            <div class="set-table-div">
                <?php if (!empty($results)) { ?>
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th><a style="text-decoration:none" href="javascript:void(0)">Id</a></th>
                            <th><a style="text-decoration:none" href="javascript:void(0)">Name</a></th>
                            <th class="send-email"><a style="text-decoration:none" href="javascript:void(0)">Email</a></th>
                            <th><a style="text-decoration:none" href="javascript:void(0)">Phone </a></th>
                            <th><a style="text-decoration:none" href="javascript:void(0)">UUID</a></th>
                            <th><a style="text-decoration:none" href="javascript:void(0)">Txnid</a></th>
                            <th><a style="text-decoration:none" href="javascript:void(0)">Amount</a></th>
                            <th><a style="text-decoration:none" href="javascript:void(0)">Status</a></th>
                            <th><a style="text-decoration:none" href="javascript:void(0)">itemid</a></th>
                            <th><a style="text-decoration:none" href="javascript:void(0)">Key</a></th>
                            <th>Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $i =  isset($page) ? $page + 1 : 1;
                            foreach ($results as $row) {

                                $encrypt_user_id = $this->encrypt->encode($row['userid']);
                                ?>
                                <tr id="tr^^<?php echo $encrypt_user_id; ?>">
                                    <td class="td_userid"><?php echo $i++; ?></td>
                                    <td class=""><?php echo $row["name"]; ?></td>
                                    <td class="send-email"><?php echo $row["email"]; ?></td>
                                    <td class=""><?php echo $row["phone"]; ?></td>
                                    <td class="td_uuid"><?php echo $row["uuid"]; ?></td>
                                    <td class=""><?php echo $row["txnid"]; ?></td>
                                    <td class=""><?php echo $row["payment_amount"]; ?></td>
                                    <td class=""><?php echo $row["payment_status"]; ?></td>
                                    <td class=""><?php echo $row["itemid"]; ?></td>
                                    <td class="td_Key key"><?php if (($row["payment_key"] == '' || $row['payment_key'] == NULL)) { ?> 
                                        <button id="<?php echo $row["userid"] ?>"  class="create_key ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/relationship_harmonizer/add_key/' . $row['userid'] ?>" role="button" aria-disabled="false"><i class="fa fa-key"></i></button>
                                         <?php } else {
                                                echo "<span>" . $row['payment_key'] . "</span>"; ?> 
                                        <br/><button id="<?php echo 'send_activation_key_' . $row["userid"] ?>"  class="sendEmail" role="button"  onclick="send_activation_key('<?php echo $row["userid"]; ?>','Admin/relationship_harmonizer/send_activation_key')" title="Re-Send Activation Key"><i class="fa fa-envelope"></i></button>
                                        <?php    } ?></td>
                                    <td class="edit-del">
                                        <a class="edit ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/relationship_harmonizer/user_edit/' . $row['userid'] ?>"><i class="fa fa-pencil"></i></a>
                                        <a class="edit ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/relationship_harmonizer/delete/' . $row['userid'] ?>" title="Delete"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>   
                                <?php
                            }
                        
                        ?>
                    </tbody>
                </table>
                <?php } else {echo '<h1 style="text-align:center;"> No Results Found <h1>'; }  ?>
            </div>
            <div class="row">
                <div class="col-md-7 col-sm-12">
<?php  echo isset($links) ? $links :"";   ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>