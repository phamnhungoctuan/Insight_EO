<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//echo validation_errors('<span class="error">', '</span>');
?>
<div class="modal-header" >
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Add New Key</h4>
</div>
<?php echo form_open($action, array('method' => 'post', "id" => "UserForm","class"=>"validate")); ?>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            
            <p class="form-group">
                <?php $id= isset($user_data['userid'])?$user_data['userid']:set_value("payment_userid"); ?>
                <?php echo form_label('UserId', 'payment_userid'); ?>
                <?php echo form_input(array('type' => 'text', 'name' => 'payment_userid', 'id' => 'payment_userid', 'value' => $id, 'class' => 'col-md-12 form-control')); ?>		
            </p>
            <p class="form-group">
                <?php echo form_label('UUID', 'payment_uuid'); ?>
                <?php $uuid= isset($user_data['uuid'])?$user_data['uuid']:set_value("payment_uuid");?>
                <?php echo form_input(array('type' => 'text', "readonly"=>"readonly",'name' => 'payment_uuid', 'id' => 'payment_uuid', 'value' => $uuid, 'class' => 'col-md-12 form-control')); ?>		
            </p>
            <p class="form-group">
                <?php echo form_label('Transaction Id', 'txnid'); ?>
                <?php echo form_input(array('type' => 'text', 'name' => 'txnid', 'id' => 'txnid', 'value' => set_value('txnid'), 'class' => 'col-md-12 form-control ')); ?>
            </p>
            <p class="form-group">
                <?php echo form_label('Gross Income', 'payment_amount'); ?>
                <?php echo form_input(array('type' => 'text', 'name' => 'payment_amount', 'id' => 'payment_amount', 'autocomplete' => 'off', 'value' => set_value('payment_amount'), 'class' => 'col-md-12 form-control ')); ?>
            </p>
            <p class="form-group">
                <?php echo form_label('ItemId', 'itemid'); ?>
                <?php echo form_input(array('type' => 'text', 'name' => 'itemid', 'id' => 'itemid', 'value' => set_value('itemid'), 'class' => 'col-md-12 form-control ')); ?>
            </p>
            <p class="form-group">
                <?php echo form_label('Payment Status', 'payment_status'); ?>
                <?php echo form_input(array('type' => 'text', 'name' => 'payment_status', 'id' => 'payment_status', 'value' => set_value('payment_status'), 'class' => 'col-md-12 form-control ')); ?>
            </p>
           
            <p class="form-group">
                <?php echo form_label('Key', 'payment_key'); ?>
                <span class="mandatory">*</span>
				<?php $val=isset($payment_key)?$payment_key:set_value('Key')?>
                <?php echo form_input(array('type' => 'text', 'name' => 'payment_key','readonly'=>'readonly', 'id' => 'payment_key', 'autocomplete' => 'off', 'value' =>$val , 'class' => 'col-md-12 form-control required ')); ?>
            </p>
            
        </div>
    </div>
</div>
<div class="modal-footer">
    <?php echo form_input(array('class'=>'btn blue',  'value' => 'Add', "type"=>"submit"));
    echo form_input(array('class'=>'btn default', 'data-dismiss'=>'modal', 'type' => 'button', 'value' => 'Close')); ?>
</div>
<?php echo form_close(); ?>
<script>
jQuery(document).ready(function() {   
   FormValidation.init();
});
</script>