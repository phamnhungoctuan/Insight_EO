<div class="my-modal">
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    echo validation_errors('<span class="error">', '</span>');
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit User</h4>
    </div>
    <?php echo form_open('Admin/relationship_harmonizer/edit_user', array('method' => 'post', "id" => "UserForm", "class" => "validate")); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">

                <p class="form-group">
                    <?php echo form_label('Name', 'name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'name', 'id' => 'name', 'value' => $user_data['name'], 'class' => 'col-md-12 form-control ')); ?>		
                </p>
                <p class="form-group">
                    <?php echo form_label('Email', 'email'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_input(array('type' => 'text', 'name' => 'email', 'id' => 'email', 'value' => $user_data['email'], 'class' => 'col-md-12 form-control email')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Phone #', 'phone'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'phone', 'id' => 'phone', 'autocomplete' => 'off', 'value' => $user_data['phone'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Payment-Status', 'payment_status'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_status', 'id' => 'payment_status', 'autocomplete' => 'off', 'value' => $user_data['payment_status'], 'class' => 'col-md-12 form-control')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Gross Income', 'payment_amount'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_amount', 'id' => 'payment_amount', 'autocomplete' => 'off', 'value' => $user_data['payment_amount'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Transaction ID', 'txnid'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'txnid', 'id' => 'txnid', 'autocomplete' => 'off', 'value' => $user_data['txnid'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Item ID', 'itemid'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'itemid', 'id' => 'itemid', 'autocomplete' => 'off', 'value' => $user_data['itemid'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <?php
                echo form_input(array('type' => 'hidden', 'value' => $user_data['userid'], 'name' => 'userid', 'id' => 'id', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control '));
                ?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php
        echo form_input(array('class' => 'btn blue', 'value' => 'Update', "type" => "submit"));
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        ?>
    </div>
    <?php echo form_close(); ?>
    <script>
        jQuery(document).ready(function() {
            FormValidation.init();
        });
    </script>
</div>