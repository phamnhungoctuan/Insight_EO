<div class="my-modal">
    <?php defined('BASEPATH') OR exit('No direct script access allowed');
    echo validation_errors('<span class="error">', '</span>');
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Change Password</h4>
    </div>
<?php echo form_open('Admin/genius_insight/change_password', array('class' => 'validate user_change_password_form', 'method' => 'post', "id" => "UserForm")); ?>
    <div class="modal-body">
        <div class="row clearfix">
            <div class="col-md-12">
                <p class="form-group">
                    <?php
                    echo form_label('New Password', 'password');
                    echo '<span class="mandatory">*</span>';
                    echo form_password(array('name' => 'password', 'id' => 'Password', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control required', 'aria-describedby' => 'Password-error'));
                    echo form_error('new_password');
                    ?>
                </p>
                <p class="form-group">
                    <?php
                    echo form_label('Confirm Password', 'confirm_password');
                    echo '<span class="mandatory">*</span>';
                    echo form_password(array('name' => 'confirm_password', 'id' => 'confirm_password', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control required confirmPassword', 'aria-describedby' => 'confirm_password-error'));
                    echo form_error('confirm_password');
                    ?>
                </p>
            </div>
        </div>
        
    </div>
    <div class="modal-footer">
        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
            <?php echo form_input(array('id' => 'user_change_password_btn', 'class' => 'btn blue', 'type' => 'button', 'value' => 'Update', 'onClick' => 'user_change_password(this)'));
            echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
            ?>
        <div class="server_error alert alert-danger display-hide" style="display: none;margin:10px 0;text-align: left">
            <button class="close" data-close="alert"></button>
            <span></span>
        </div>
    </div>
<?php echo form_close(); ?>
    <script>
        jQuery(document).ready(function() {
            $('input').focus(function() {
    //       $(this).parents('p').find('span').remove();
            });
            FormValidation.init();
        });
    </script>
</div>