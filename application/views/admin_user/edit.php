<?php
defined('BASEPATH') OR exit('No direct script access allowed');
echo validation_errors('<span class="error">', '</span>');
?>
<div class="my_modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit User</h4>
    </div>
    <?php echo form_open('Admin/admin_user/edit_user', array('method' => 'post', "id" => "UserForm", "class" => "validate")); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">

                <p class="form-group">
                    <?php
                    echo form_label('Name', 'name');
                    echo '<span class="mandatory">*</span>';
                    echo form_input(array('type' => 'text', 'name' => 'Fullname', 'id' => 'Fullname', 'autocomplete' => 'off', 'value' => $user_data['Fullname'], 'class' => 'col-md-12 form-control required'));
                    ?>		
                </p>
                <p class="form-group">
                    <?php
                    echo form_label('UserName', 'userName');
                    echo '<span class="mandatory">*</span>';
                    echo form_input(array('type' => 'text', 'name' => 'Username', 'id' => 'username', 'autocomplete' => 'off', 'value' => $user_data['Username'], 'class' => 'col-md-12 form-control required'));
                    ?>
                </p>
                <p class="form-group">
                    <?php
                    echo form_label('Email', 'email');
                    echo '<span class="mandatory">*</span>';
                    echo form_input(array('type' => 'text', 'name' => 'Email', 'id' => 'email', 'autocomplete' => 'off', 'value' => $user_data['Email'], 'class' => 'col-md-12 form-control email'));
                    ?>
                </p>

                <p class="form-group">
                    <?php
                    echo form_label('Password', 'password');
                    echo '<span class="mandatory">*</span>';
                    echo form_input(array('type' => 'password', 'name' => 'Password', 'id' => 'Password', 'autocomplete' => 'off', 'value' => "", 'class' => 'col-md-12 form-control '));
                    ?>
                </p>
                <p class="form-group">
                    <?php
                    echo form_label('Confirm Password', 'confirm_password');
                    echo '<span class="mandatory">*</span>';
                    echo form_input(array('type' => 'password', 'name' => 'confirm_password', 'id' => 'confirm_password', 'autocomplete' => 'off', 'value' => "", 'class' => 'col-md-12 form-control confirmPassword'));
                    ?>
                </p>
                <p class="form-group">
                    <?php
                    echo form_label('Age', 'age');
                    echo form_input(array('type' => 'text', 'name' => 'Age', 'id' => 'age', 'autocomplete' => 'off', 'value' => $user_data['Age'], 'class' => 'col-md-12 form-control'));
                    ?>
                </p>
                <p class="form-group">
                    <?php
                    echo form_label('Position', 'position');
                    echo form_input(array('type' => 'text', 'name' => 'Position', 'id' => 'position', 'autocomplete' => 'off', 'value' => $user_data['Position'], 'class' => 'col-md-12 form-control'));
                    ?>
                </p>
                <?php
                echo form_input(array('type' => 'hidden', 'value' => $user_data['UserId'], 'name' => 'UserId', 'id' => 'UserId', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control '));
                ?>

            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php
        echo form_input(array('class' => 'btn blue', 'value' => 'Update', 'type' => 'submit'));
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        ?>
    </div>
    <?php echo form_close(); ?>
    <script>
        jQuery(document).ready(function() {
            FormValidation.init();
        });

    </script>
</div>