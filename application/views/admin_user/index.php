<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$atts = array(
    'data-toggle' => 'modal',
    'class' => 'btn default ajax-demo'
);
?>
<div class="col-md-12">
<?php if ($this->session->flashdata('message')) : ?>
        <p class="alert alert-<?php echo $this->session->flashdata('type') ?>"><?php echo $this->session->flashdata('message'); ?></p>
<?php endif; ?>
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue-hoki">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-globe"></i><?php echo "Total Records : " . count($results) ?>
            </div>
            <div class="actions">
                <div class="btn-group">
<!--                    <a class="btn btn-default btn-sm" href="javascript:;" data-toggle="dropdown"><i class="fa fa-cogs"></i></a>-->
                    <a class="btn default ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/admin_user/add'; ?>"><i class="fa fa-pencil"></i> Create New User </a>
                    <!--<a class="ajax-demo" data-toggle="modal"  data-url="<?php /* echo base_url().'admin/admin_add'; */ ?>"><i class="fa fa-plus-circle"></i></a>-->
<?php /* echo anchor('project/admin_add', '<i class="fa fa-pencil"></i> Add New Project', $atts); */ ?>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Username</th>
                        <th>Age</th>
                        <th>Position</th>
                        <th>Date Created</th>
                        <th>Date Updated</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($results && !empty($results)) {
                        $i = 1;
                        foreach ($results as $row) {
                            $encrypt_user_id = $this->encrypt->encode($row['UserId']);
                            ?>
                            <tr id="tr^^<?php echo $encrypt_user_id; ?>">
                                <td><?php echo $i++; ?></td>
                                <td><?php echo $row['Fullname']; ?></td>
                                <td><?php echo $row['Email']; ?></td>
                                <td><?php echo $row['Username']; ?></td>
                                <td><?php echo $row['Age']; ?></td>
                                <td><?php echo $row['Position']; ?></td>
                                <td><?php echo $row['CreateDate']; ?></td>
                                <td><?php echo $row['UpdateDate']; ?></td>
                                <td class="edit-del">
                                    <a class="edit ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/admin_user/edit/' . $row['UserId'] ?>"><i class="fa fa-pencil"></i></a> / &nbsp;
                                    <a class="delete ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/admin_user/delete/' . $row['UserId'] ?>"  title="Delete" > <i class="fa fa-times"></i></a> 
                                </td>
                            </tr>   
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>



            <div class="row">
                <div class="col-md-7 col-sm-12">
<?php echo $links; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
