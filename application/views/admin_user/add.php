<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//echo validation_errors('<span class="error">', '</span>');
?>
<div class="my-modal">

    <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add New User</h4>
    </div>
    <?php echo form_open('Admin/admin_user/save_new_user', array('method' => 'post', "id" => "UserForm", "class" => "validate")); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <p class="form-group">
                    <?php echo form_label('Name', 'name'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_input(array('type' => 'text', 'name' => 'Fullname', 'id' => 'full_name', 'value' => set_value('full_name'), 'class' => 'col-md-12 form-control required')); ?>		
                </p>
                <p class="form-group">
                    <?php echo form_label('UserName', 'userName'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_input(array('type' => 'text', 'name' => 'Username', 'id' => 'username', 'value' => '', 'class' => 'col-md-12 form-control required')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Email', 'email'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_input(array('type' => 'text', 'name' => 'Email', 'id' => 'email', 'autocomplete' => 'off', 'value' => set_value('email'), 'class' => 'col-md-12 form-control required email')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Password', 'password'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_password(array('type' => 'password', 'name' => 'Password', 'id' => 'Password', 'value' => '', 'class' => 'col-md-12 form-control required')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Confirm Password', 'confirm_password'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_password(array('type' => 'password', 'name' => 'confirm_password', 'id' => 'confirm_password', 'value' => '', 'class' => 'col-md-12 form-control required confirmPassword')); ?>
                </p>



            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php echo form_input(array('class' => 'btn blue', 'value' => 'Add', 'type' => 'submit'));
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        ?>
    </div>
<?php echo form_close(); ?>


    <script>
        jQuery(document).ready(function() {
            FormValidation.init();
        });

    </script>
</div>