<?php defined('BASEPATH') OR exit('No direct script access allowed');
echo validation_errors('<span class="error">', '</span>'); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Change Password</h4>
</div>
<?php echo form_open('Admin/admin_user/reset_password', array('class'=>'validate', 'method' => 'post', "id" => "userForm")); ?>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">            
            <p class="form-group">
                <?php
                echo form_label('Old Password', 'old_password');
                echo '<span class="mandatory">*</span>';
                echo form_password(array('name' => 'old_password', 'id' => 'old_password', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control required'));
                ?>		
            </p>
            <p class="form-group">
                <?php
                echo form_label('New Password', 'new_password');
                echo '<span class="mandatory">*</span>';
                echo form_password(array('name' => 'new_password', 'id' => 'Password', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control required'));
                ?>
            </p>
            <p class="form-group">
                <?php echo form_label('Confirm Password', 'confirm_password');
                echo '<span class="mandatory">*</span>';
                echo form_password(array('name' => 'confirm_password', 'id' => 'confirm_password', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control required confirmPassword')); ?>
            </p>
        </div>
    </div>
</div>
<div class="modal-footer">
    <?php echo form_input(array('class' => 'btn blue', 'type' => 'submit', 'value' => 'Update'));
    echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close')); ?>
</div>
<?php echo form_close(); ?>
<script>
jQuery(document).ready(function() {   
   FormValidation.init();
});
</script>
