<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                Welcome to Energy Re-Mastered
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Hi <?php echo isset($first_name) ? $first_name : ''; ?>,</p>
                            <p>Please 
                                <a href="<?php echo base_url('scripts/Users/confirm_otp?code='.$otp.'&tk='.  $user_id) ?>">click here</a> or the following link to confirm your account for the Energy Re-Mastered App. </p>
                            <p>
                                <a href="<?php echo base_url('scripts/Users/confirm_otp?code='.$otp.'&tk='.  $user_id) ?>"><?php echo base_url('scripts/Users/confirm_otp?tk='.$otp) ?></a>
                            </p>
                            <p><b>Note:</b><i>The confirmation link will expire after 24 hours.</i></p>
                            <p>A new world awaits you.</p>
                            <p>Thank you, <br/> Energy Re-Mastered Apps Support</p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
