<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                               Welcome to Energy Re-Mastered
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Hi<?php echo isset($user_detail['first_name']) ? " ".trim($user_detail['first_name']):' User'?>,</p>
                            <p>Your Request to access the Energy Re-Mastered  App on the following device has been accepted. </p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%;border: 1px solid #999;text-align: left;border-collapse: collapse;margin: 0 0 1em 0;" cellspacing="0" cellpadding="0" >
                                <tr style="background: #eee;">
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;" colspan="2">
                                        New Device Description  :
                                    </th>
                                </tr>
                                <?php if(strlen($user_detail['device_description'])){?>
                                <tr>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                        Device
                                    </th>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                        <?php echo $user_detail['device_description'] ?>
                                    </th>
                                </tr>
                                <?php }?>
                                <tr>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                        Device ID 
                                    </th>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                        <?php echo $user_detail['uuid']; ?>
                                    </th>
                                </tr>
                            </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Thank you,</p>
                            <p>Energy Re-Mastered  Support</p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
