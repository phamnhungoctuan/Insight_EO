<style>
    .button-email{
        display: inline-block;
        width: 150px;
        background-color: green;
        color: white;
    }
</style>
<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                Welcome to Genius Insight!
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Hi,</p>
                            <?php ?>
                            <p><?php echo isset($user_name)? '<b>'.$user_name.'</b>': "User" ?> has sent you request to share <?php echo isset($library_name)? '<b>'.$library_name.'</b>': ""   ?> library</p>
                            <?php if(isset($user_id) && isset($token)) {?>
                            <p><a class="button-email" href="<?php echo base_url("API_V1/user_library/accept_import_request?id=$user_id&status=1&token=$token&code=".$code); ?>">Accept</a>  <a class="button-email" href="<?php echo base_url("API_V1/user_library/accept_import_request?id=$user_id&status=2&token=$token&code=".$code); ?>" style="margin-left: 10px">Reject</a></p>
                            <?php } ?>
                            
                            <p>Thank you,<br/>Insight Health Apps Support</p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
