<style>
    .library_code tr th,.library_code tr td{
        width: 25%;
        text-align: left; 
        vertical-align: top; 
        border: 1px solid #000;
        border-collapse: collapse;
        padding: 0.3em;
    }
    .library_code {
        width: 100%;
        border: 1px solid #999;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
}
</style>
<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;font-size: 24px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="95%" border="0" cellspacing="0" cellpadding="0" style="max-width: 680px; min-width: 300px;font-size: 17px;">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                Welcome to Genius Insight!
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align:top;padding:0.3em 0">Hi ,</td>
                </tr>
                <?php 
                
                    $msg =  trim($user_details['first_name']." ".$user_details['last_name']).' has Sent request for following libraries.';
                
                if(strlen(strip_tags($msg))){?>
                    <tr>
                        <td style="vertical-align:top;padding:0.3em 0"><?php echo $msg; ?></td>
                    </tr>
                    <?php if(count($data)){ ?>
                    
                    <tr>
                        <td style="line-height:normal;" >
                            <table style="width: 100%;border: 1px solid #999;text-align: left;border-collapse: collapse;margin: 0 0 1em 0;" cellspacing="0" cellpadding="0" >
                                <tr style="background: #eee;">
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                        Library
                                    </th>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                        Accept/Reject
                                    </th>
                                </tr>
                                <?php foreach ($data as $library) {
                                    echo '<tr>'
                                    . '<td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:2%;">'
                                    .$library['name'].'</td>'
                                    . '<td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 2%;"><a class="button-email" href="'.base_url("API_V1/user_library/accept_import_request?id=".$library['user_id']."&status=1&token=$token&code=".$library['share_library_code']).'">Accept</a>  <a class="button-email" href="'.base_url("API_V1/user_library/accept_import_request?id=".$library['user_id']."&status=2&token=$token&code=".$library['share_library_code']).'" style="margin-left: 10px">Reject</a>'.'</td></tr>';
                                }?>
                                <?php if($bulk_share_code)  {?>
                                <tr>
                                     <td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:2%;">Collective Accept/Reject</td>
                                     <td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:2%;">
                                         <a class="button-email" href="<?php  echo base_url("API_V1/user_library/accept_bulk_import_request?code=".$bulk_share_code."&status=1&token=$token") ?>">Accept</a>  <a class="button-email" href="<?php  echo base_url("API_V1/user_library/accept_bulk_import_request?code=".$bulk_share_code."&status=2&token=$token") ?>" style="margin-left: 10px">Reject</a>
                                     </td>
                                </tr>
                                <?php } ?>
                                
                            </table>
                        </td>
                    </tr>
                    <?php }?>
                <?php } ?>
                    <tr>
                        <td style="vertical-align:top;padding:0.3em 0">
                            Thank you,<br/>Insight Health Apps Support
                        </td>
                    </tr>
            </table>		
        </td>
    </tr>
</table>
