<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Energy Re-Mastered </title>
    </head>
    <body style="margin:0;padding:0;font-family:  Arial,Helvetica Neue,Helvetica,sans-serif;">
        <style type="text/css">
            body { margin:0;padding:0; font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#878787; }
            table,tr, td, tbody{ border-collapse: collapse; border-spacing: 0px; text-indent: 0; }
            * { padding:0px; margin:0px; border:0px; border-spacing:0px; border-collapse: collapse; }
            a img,img{ font:0%; font-size:0%; line-height:0%; margin:0; padding:0 }
            table, td, tr{ margin:0; padding:0;  }
            table, td, tr img{ margin:0; padding:0 ; font-size:12px ; }
            td{ font: 12px arial; }
        </style>
        <!--main table-->
        <table border="0" cellpadding="0" cellspacing="0" style="width: 620px; margin: 0 auto; vertical-align: middle; background-color: #f9f9f9;">
            <tr>
                <td width="10">&nbsp;</td>
                <!--Main TD-->
                <td width="600">
                    <!--Inner Table-->
                    <table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#fff" style=" vertical-align: middle;">
                        <tbody>
                            <!--newsletter header-->
                            <tr>
                                <td align="center" valign="middle" bgcolor="#555" style="padding: 10px 0;" >
                                    <img src="<?php echo base_url('assets/img/email/logo.png') ?>" alt="Logo"/>
                                </td>
                            </tr>
                            <!--NEWSLETTER HEADER-->
                            <tr>
                                <td align="center" valign="middle" style="padding: 10px 0; font-size: 20px; color: #141414;" >
                                    Energy Re-Mastered App : Download &amp; Activation Codes
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">
                                    <table>
                                        <tr>
                                            <td align="center" style="padding: 10px;">
                                                <table>
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <img src="<?php echo base_url('assets/img/email/ios-logo.png') ?>" width="75" height="75" alt="IOS"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top" style="padding: 10px 0;">
                                                            <span style="font-size: 12px; color: #222; font-weight: bold;">iOS Download Link: <br/><a href="https://rink.hockeyapp.net/apps/f90e952d9610436dad298a18d2629472" style="font-weight: normal; color: #1155cc;">https://rink.hockeyapp.net/apps/f90e952d9610436dad298a18d2629472</a></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <img src="<?php echo base_url('assets/img/email/ios.png') ?>" width="200" height="161" alt="IOS"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="font-size: 12px; color: #222 ;padding: 10px 0 5px; "><b>&quot;Untrusted Developer Pop Up Box&quot;</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top" style="padding: 0 5px;">
                                                            <span style="font-size: 12px; color: #222">Once this appears, select your iOS &quot;Settings&quot; icon, then &quot;General&quot; then scroll down to the bottom and select &quot;Device Management&quot; and Trust the Canada Inc profile.</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="center" valign="top" style="padding: 10px;">
                                                <table>
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <img src="<?php echo base_url('assets/img/email/andriod-logo.png') ?>" width="75" height="75" alt="andriod"/> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top" style="font-size: 12px; color: #222; font-weight: bold; padding: 10px 0;"> 
                                                            <span>Android Download Link:<br/><a href="https://rink.hockeyapp.net/apps/e6e748aac7ea4ac180f12ef553871af1" style="font-weight: normal; color: #1155cc;">https://rink.hockeyapp.net/apps/e6e748aac7ea4ac180f12ef553871af1</a></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <img src="<?php echo base_url('assets/img/email/andriod.png') ?>" height="161" alt="IOS"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="font-size: 12px; color: #222 ;padding: 10px 0 5px; "><b>&quot;Untrusted Developer Pop Up Box&quot;</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" valign="top" style="font-size: 12px; color: #222; padding: 0 5px">
                                                            <span>If prompted go to your &quot;Settings&quot; then &quot;Apps&quot; and allow installation from 3rd party apps</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="middle" style="font-size: 12px; color: #222; font-weight: bold; padding:10px">
                                    Installation Instructions
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="middle" style="font-size: 12px; color: #444;padding:0 10px;">
                                    You will have a 15 day trial before you will be prompted to enter your activation code below. Once the software is downloaded, sign up using your own email and an easy to remember 8 digit password. If you are ever logged out of your app, always gain access using this email and 8 digit password.
                                </td>
                            </tr>
                            <tr><td height="10px"></td></tr>
                            <tr>
                                <td style="padding: 0 5px 10px; ">
                                    <table cellpadding="0" cellspacing="0" border="1"  bordercolor="#222">
                                        <thead>
                                            <tr>
                                                <th align="left" colspan="2" style="border: 1px solid #222; width: 580px; background-color: #eeeeee;padding: 5px; color:#222;">
                                                    Login Credentials :
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="border: 1px solid #222;background-color: #fff; padding: 5px; color:#222;" >Email Login Username</td>
                                                <td style="border: 1px solid #222;background-color: #fff; padding: 5px; color:#222;"><?php echo $user_detail['email']; ?></td>
                                            </tr>
                                            <tr>
                                                <td style="border: 1px solid #222;background-color: #fff; padding: 5px; color:#222;">Email Login Password</td>
                                                <td style="border: 1px solid #222;background-color: #fff; padding: 5px; color:#222;"><?php echo $user_detail['password']; ?></td>
                                            </tr>
                                            <?php if (strlen($user_detail['activation_key'])) { ?>
                                                <tr>
                                                    <td style="border: 1px solid #222;background-color: #fff; padding: 5px; color:#222;">One Time Activation Key Code</td>
                                                    <td style="border: 1px solid #222;background-color: #fff; padding: 5px; color:#222;"><?php echo $user_detail['activation_key'] ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="color:#222;"> Thank you,</td>
                            </tr>
                            <tr>
                                <td style="color:#222;">Energy Re-Mastered  Apps Support</td>
                            </tr>
                        </tbody>
                    </table>
                    <!--//Inner Table-->
                </td>
                <!--// Main TD-->
                <td width="10">&nbsp;</td>
            </tr>
        </table>
        <!--//main table-->
    </body>
</html>