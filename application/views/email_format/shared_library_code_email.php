<style>
    .library_code tr th,.library_code tr td{
        width: 25%;
        text-align: left; 
        vertical-align: top; 
        border: 1px solid #000;
        border-collapse: collapse;
        padding: 0.3em;
    }
    .library_code {
        width: 100%;
        border: 1px solid #999;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
}
</style>
<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;font-size: 24px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="95%" border="0" cellspacing="0" cellpadding="0" style="max-width: 680px; min-width: 300px;font-size: 17px;">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                Welcome to  Energy Re-Mastered!
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align:top;padding:0.3em 0">Hi <?php echo isset($shared_with['first_name']) ? $shared_with['first_name'] : ''; ?>,</td>
                </tr>
                <?php 
                $msg = isset($shared_by['first_name']) && strlen($shared_by['first_name'])?'<b>'.$shared_by['first_name'].'</b>':'<b>User </b>';
                $msg .= isset($shared_by['email']) && strlen($shared_by['email'])?' ('.$shared_by['email'].' )':'';
                if(strlen($msg)){
                    $msg .= count($email_data)>1 ? ' has shared some libraries with you. ':' has shared a library with you.';
                }
                if(strlen(strip_tags($msg))){?>
                    <tr>
                        <td style="vertical-align:top;padding:0.3em 0"><?php echo $msg; ?></td>
                    </tr>
                    <?php if(count($email_data)){ ?>
                    <tr>
                        <td style="vertical-align:top;padding:0.3em 0">Below are the details you will need to import your library.</td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;padding:0.3em 0">Go to the libraries panel and click on the import feature and enter the details below.</td>
                    </tr>
                    <tr>
                        <td style="line-height:normal;" >
                            <table style="width: 100%;border: 1px solid #999;text-align: left;border-collapse: collapse;margin: 0 0 1em 0;" cellspacing="0" cellpadding="0" >
                                <tr style="background: #eee;">
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                        Library
                                    </th>
                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                        Code
                                    </th>
                                </tr>
                                <?php foreach ($email_data as $data) {
                                    echo '<tr>'
                                    . '<td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:2%;">'
                                    .$data['name'].'</td>'
                                    . '<td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 2%;">'
                                    .$data['code'].'</td></tr>';
                                }?>
                                <?php if($bulk_share_code) { ?>
                                <tr> 
                                    <td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:2%;">Bulk Share Code</td>
                                    <td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 2%;"><?php echo $bulk_share_code ?></td>
                                </tr>
                                <?php } ?>
                            </table>
                        </td>
                    </tr>
                    <?php }?>
                <?php } ?>
                    <tr>
                        <td style="vertical-align:top;padding:0.3em 0">
                            Thank you,<br/>Energy Re-Mastered Apps Support
                        </td>
                    </tr>
            </table>		
        </td>
    </tr>
</table>
