<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 800px; min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                Welcome to Energy Re-Mastered !
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;width:99%">
                            <p>Hi <?php echo isset($first_name) ? $first_name : ''; ?>,</p>
                            <?php if(isset($url)){?>
                            <p>Details of the Audio Clip uploaded for <b><?php echo $client_record['first_name']?  ucfirst($client_record['first_name']):'Client'?> </b></p>
                            <table style="text-align: left;width:100%">
                                <tr>
                                    <th>Audio Url</th>
                                    <td><?php echo $url ?></td>
                                </tr>
                                <tr>
                                    <th>Uploaded On</th>
                                    <td><?php echo date('Y-m-d H:i') .' GMT +0'?></td>
                                </tr>
                            </table>
                            <?php }?>
                            <p>Thank you,<br/>Energy Re-Mastered  Apps Support </p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
