<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                               Welcome to The Energy ReMastered
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Hi <?php echo isset($first_name) ? $first_name : ''; ?>,</p>
                            <p>
                                Below is your code required to activate the 3D graphics package. To start the download and enter the activation code, login to Energy ReMastered App and get to the System OverView page (Main panel with Mind, Body, Biofield)
                            </p>
                            <div>
                                Top right, there is a navigation link. Select this drop down and navigate to accessories page. Once there, select the 3D Animation Download option. Ensure you have good Wifi as the download is large.
                            </div>
                            <div>
                                <p style="margin-bottom:0.5"><b>
Please ensure the letters are not to be confused with the numbers. <div>An example capital I versus the number 1 symbol </div></div>
                            <?php echo isset($video_activation_key) ? '<p><b>Your Video Activation key is :  '. $video_activation_key .'</b></p>': '' ?>
                            
                            <p>Please enter this into Energy ReMastered  Software to gain instant access. </p>
                            <p>Thank you,<br/>Energy ReMastered  Apps Support</p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
