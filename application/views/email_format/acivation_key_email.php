<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family:arial,sans-serif; font-size: 34px; color: #57697e;">
                                Download Energy ReMastered App
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family:arial,sans-serif;">
                            <p style="line-height:15px;">Hi<?php echo isset($user_detail['first_name']) ? " " . trim($user_detail['first_name']) : ' User' ?>,</p>
                            <p style="line-height:15px;">Thank you for purchasing Energy ReMastered and joining Dr. Kimberly McGeorge exclusive Membership Group, A Secret To Everything.</p>
                            <p style="line-height:15px;">Currently this app is designed for all Apple iOS Devices. This will not install on an Android Device.</p>
                            <p style="line-height:15px">
                                <img src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTBpsip4SYpSkb9QZchxwZlGMzDuqsjYhv-gwFjbBtm0b1yphLYja7046y7" alt="iOS" width="60" height="60"/>
                                <br/>
                                <font>
                                    <b>iOS Download Link:</b>
                                    <a href="https://itunes.apple.com/us/app/energy-remastered/id1353774238?ls=1&mt=8 "> https://itunes.apple.com/us/app/energy-remastered/id1353774238?ls=1&mt=8 </a>
                                </font>
                            </p>

                            <p style="line-height:15px">
                                <font><b>Installation Instructions</b></font><br/>
                                You will have a 15 day trial before you will be prompted to enter your activation code below.<br/>
                                Once the software is downloaded, sign up using your own email and an easy to remember 8 digit password. If you are ever logged out of your app, always gain access using this email and 8 digit password.
                            </p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%;border: 1px solid #999;text-align: left;border-collapse: collapse;margin: 0 0 1em 0;" cellspacing="0" cellpadding="0" >
                            <tr style="background: #eee;">
                                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;" colspan="2">
                                    Login Credentials :
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                    Email Login Username
                                </th>
                                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                    <?php echo $user_detail['email']; ?>
                                </th>
                            </tr>
                            <tr>
                                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                    Email Login Password
                                </th>
                                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                    <?php echo $user_detail['password']; ?>
                                </th>
                            </tr>
                            <?php if (strlen($user_detail['activation_key'])) { ?>
                            <?php } ?>
                        </table>
                        <br/>
                        <p style="line-height:20px">
                            <font>Thank you,<br/>
                                Energy Re-Mastered Apps Support</font>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
