<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                               Welcome to The Energy Re-Mastered
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Hi <?php echo isset($first_name) ? $first_name : ''; ?>,</p>
                            <p>Thank you for your investment with the Energy Re-Mastered  App.</p>
                            <div>
                                Below is your one time activation code which is required to activate your software purchase. When prompted to enter the activation code, please enter the code as below. Please do not confuses this with your 8 digit password you created. The 8 digit password is used together with your email address to login to the Energy Re-Mastered  App.
                            </div>
                            <div>Please ensure the letters are not to be confused with the numbers. An example capital I versus the number 1 symbol.</div>
                            <?php echo isset($activation_key) ? '<p><b>Your Activation key is :  '. $activation_key .'</b></p>': '' ?>
                            <p>
                                <!--To download the quick start manual, please click here: <a href="https://dl.dropboxusercontent.com/u/36540174/Genius%20Insight%20Installation%20and%20Quick%20Start%20Guide-1.pdf">https://dl.dropboxusercontent.com/u/36540174/Genius%20Insight%20Installation%20and%20Quick%20Start%20Guide-1.pdf</a>-->
                                To download the quick start manual, please <a href="https://www.dropbox.com/s/w6euikwvgkk1fci/Genius%20Insight%20Installation%20and%20Quick%20Start%20Guide.pdf?dl=0&inf_contact_key=b3b7855b44d9e3f9e73a84a6c55b2b4812d76f95080e5d8e7279c4fd9f53b230%5C">click here</a>
                            </p>
                            <div>Below are some instructional video�s we would suggest you watch</div>
                            <div>
                                <div style="font-family:HelveticaNeue;font-size:12px"><a href="https://youtu.be/ZQUeOSFjQgQ" style="font-family:TimesNewRomanPSMT;font-size:14px" target="_blank" >https://youtu.be/ZQUeOSFjQgQ</a><br style="font-family:TimesNewRomanPSMT;font-size:14px"></div>
                                <div style="font-family:HelveticaNeue;font-size:12px"><a href="https://youtu.be/0pVzD486k7g" style="font-family:TimesNewRomanPSMT;font-size:14px" target="_blank" >https://youtu.be/0pVzD486k7g</a><br style="font-family:TimesNewRomanPSMT;font-size:14px"></div>
                                <div style="font-family:HelveticaNeue;font-size:12px"><a href="https://youtu.be/3YRjJy4arLM" style="font-family:TimesNewRomanPSMT;font-size:14px" target="_blank" >https://youtu.be/3YRjJy4arLM</a><br style="font-family:TimesNewRomanPSMT;font-size:14px"></div>
                                <div style="font-family:HelveticaNeue;font-size:12px"><a href="https://youtu.be/GrWlKDptkJI" style="font-family:TimesNewRomanPSMT;font-size:14px" target="_blank" >https://youtu.be/GrWlKDptkJI</a><br style="font-family:TimesNewRomanPSMT;font-size:14px"></div>
                                <div style="font-family:HelveticaNeue;font-size:12px"><a href="https://youtu.be/hqdF7Mza6-g" style="font-family:TimesNewRomanPSMT;font-size:14px" target="_blank" >https://youtu.be/hqdF7Mza6-g</a><br style="font-family:TimesNewRomanPSMT;font-size:14px"></div>
                                <div style="font-family:HelveticaNeue;font-size:12px"><a href="https://youtu.be/RbxNEajJUqc" style="font-family:TimesNewRomanPSMT;font-size:14px" target="_blank" >https://youtu.be/RbxNEajJUqc</a><br style="font-family:TimesNewRomanPSMT;font-size:14px"></div>
                            </div>
                            <!--<p>Please enter this into Genius Insight Software to gain instant access. </p>-->
                            <p>A new world awaits you</p>
                                
                            <p>Thank you,<br/>Energy Re-Mastered  Apps Support</p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
