<style>
    .library_code tr th,.library_code tr td{
        width: 25%;
        text-align: left; 
        vertical-align: top; 
        border: 1px solid #000;
        border-collapse: collapse;
        padding: 0.3em;
    }
    .library_code {
        width: 100%;
        border: 1px solid #999;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
    }
</style>
<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;font-size: 24px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="95%" border="0" cellspacing="0" cellpadding="0" style=" min-width: 300px;font-size: 17px;">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                Welcome to  Genius Insight!
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align:top;padding:0.3em 0">Hi <?php echo isset($shared_with['first_name']) ? $shared_with['first_name'] : ''; ?>,</td>
                </tr>
                <?php
                $msg = isset($shared_by['first_name']) && strlen($shared_by['first_name']) ? '<b>' . ucfirst($shared_by['first_name']) . '</b>' : '<b>User </b>';
                $msg .= isset($shared_by['email']) && strlen($shared_by['email']) ? ' (' . $shared_by['email'] . ' )' : '';
                if (strlen($msg)) {
                    $msg .= isset($email_data['new']) && count($email_data) > 1 ? ' has shared some libraries with you. ' : ' has shared a library with you.';
                }
                if (strlen(strip_tags($msg))) {
                    ?>
                    <tr>
                        <td style="vertical-align:top;padding:0.3em 0"><?php echo $msg; ?></td>
                    </tr>
                    <?php if (isset($email_data['new']) && count($email_data['new']) && isset($email_data['already_shared']) && count($email_data['already_shared'])) { ?>
                        <tr>
                            <td style="vertical-align:top;padding:0.3em 0">Some of the libraries were already shared with you. Please check these libraries at the end of email.</td>
                        </tr>
                    <?php } ?>
                    <?php if (count($email_data)) { ?>
                        <tr>
                            <td style="vertical-align:top;padding:0.3em 0">Below are the details you will need to import your library.</td>
                        </tr>
                        <tr>
                            <td style="vertical-align:top;padding:0.3em 0">
                                <p style="color:#00003e">* Use the <b>Library Share Code</b> to import the individual library.</p> 
                                <p style="color:#00003e">* Use the <b>Master branch Share Code</b> to import all the libraries in that master branch.</p>
                                <?php if (isset($email_data['new']) && count($email_data['new'])) { ?>
                                    <p style="color:#00003e">* Use the <b>Bulk Share Code</b> to import all the libraries at once. </p>
                                <?php } ?>

                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align:top;padding:0.3em 0">Go to the libraries panel and click on the import feature and enter the details below.</td>
                        </tr>
                        <tr>
                            <td style="vertical-align:top;padding:0.3em 0">
                                <?php if (isset($email_data['new']) && count($email_data['new']) > 1) { ?>
                                    <div style="padding:0.5em;background-color: #000;color: #fff;">
                                        <div style="display:inline-block; width:48%;">
                                            Master Bulk Share Code
                                        </div>
                                        <div style="display:inline-block; width:48%;">
                                            <?php echo $master_branch_bulk_share_code ?>
                                        </div>
                                    </div>
                                <?php } /* end of inner if */ ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="line-height:normal;">
                                <?php
                                if (isset($email_data['new']) && count($email_data['new'])) {
                                    foreach ($email_data['new'] as $key => $master_branch) {
                                        ?>
                                        <div style="margin:1em 0">
                                            <div style="padding:0.2em 0.5em;background-color: #c4c4c4;">
                                                <div style="display:inline-block; width:50%;">Master Branch</div>
                                                <div style="display:inline-block; width:48%;"><?php echo $master_branch['master_branch_name'] ?></div>
                                            </div>
                                            <div style="padding:0.2em 0.5em;background-color: #c4c4c4;">
                                                <div style="display:inline-block; width:50%;">Master Branch Share Code</div>
                                                <div style="display:inline-block; width:48%;"><b><?php echo $master_branch['master_branch_share_code'] ?></b></div>
                                            </div>
                                            <div>
                                                <table style="width: 100%;text-align: left;border-collapse: collapse;" cellspacing="0" cellpadding="0" >
                                                    <tr style="background: #eee;">
                                                        <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                                            Library
                                                        </th>
                                                        <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                                            Code
                                                        </th>
                                                    </tr>
                                                    <?php
                                                    foreach ($master_branch['libraries'] as $data) {
                                                        echo '<tr>'
                                                        . '<td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.7%;">'
                                                        . $data['library_name'] . '</td>'
                                                        . '<td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.7%;">'
                                                        . $data['single_share_code'] . '</td></tr>';
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                        </div>
                                    <?php } /* END OF OUTER (MASTERBRANCH)FOREACH */ ?>
                                    <?php
                                } /* end of outer if */
                                ?>

                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                <tr>
                    <td style="vertical-align:top;padding:0.3em 0">
                        Thank you,<br/>Insight Health Apps Support
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
