<style>
    .library_code tr th,.library_code tr td{
        width: 25%;
        text-align: left; 
        vertical-align: top; 
        border: 1px solid #000;
        border-collapse: collapse;
        padding: 0.3em;
    }
    .library_code {
        width: 100%;
        border: 1px solid #999;
        text-align: left;
        border-collapse: collapse;
        margin: 0 0 1em 0;
        caption-side: top;
    }
</style>
<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;font-size: 24px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="95%" border="0" cellspacing="0" cellpadding="0" style=" min-width: 300px;font-size: 17px;">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                Welcome to  Genius Insight!
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align:top;padding:0.3em 0">Hi <?php echo isset($shared_with['first_name']) ? $shared_with['first_name'] : ''; ?>,</td>
                </tr>
                <?php
                $msg = isset($shared_by['first_name']) && strlen($shared_by['first_name']) ? '<b>' . ucfirst($shared_by['first_name']) . '</b>' : '<b>User </b>';
                $msg .= isset($shared_by['email']) && strlen($shared_by['email']) ? ' (' . $shared_by['email'] . ' )' : '';
                if (strlen($msg)) {
                    $msg .= isset($email_data) && isset($already_shared_library_count) && $already_shared_library_count > 1 ? ' had shared some libraries with you before. ' : ' had shared a library with you before.';
                }
                if (strlen(strip_tags($msg))) {
                    ?>
                    <tr>
                        <td style="vertical-align:top;padding:0.3em 0"><?php echo $msg; ?></td>
                    </tr>
                    <?php if (count($email_data)) { ?>
                        <tr>
                            <td style="vertical-align:top;padding:0.3em 0">Below are the details you will need to import your library.</td>
                        </tr>
                        <tr>
                            <td style="vertical-align:top;padding:0.3em 0">
                                <p style="color:#00003e">* Use the <b>Library Share Code</b> to import the individual library.</p> 
                                <?php if (isset($email_data['bulk_share_libraries']) && count($email_data['bulk_share_libraries'])) { ?>
                                    <p style="color:#00003e">* Use the <b>Bulk Share Code</b> to import all the libraries (in that group) at once. </p>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align:top;padding:0.3em 0">Go to the libraries panel and click on the import feature and enter the details below.</td>
                        </tr>
                        <tr>
                            <td style="line-height:normal;">
                                <?php
                                if (isset($email_data['bulk_share_libraries']) && count($email_data['bulk_share_libraries'])) {
                                    foreach ($email_data['bulk_share_libraries'] as $key => $group) {
                                        ?>
                                        <div style="margin:1em 0">
                                            <div style="padding:0.2em 0.5em;background-color: #c4c4c4;">
                                                <div style="display:inline-block; width:50%;">Bulk Share Code</div>
                                                <div style="display:inline-block; width:48%;"><b><?php echo $key ?></b></div>
                                            </div>
                                            <div>
                                                <table style="width: 100%;text-align: left;border-collapse: collapse;" cellspacing="0" cellpadding="0" >
                                                    <tr style="background: #eee;">
                                                        <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                                            Library
                                                        </th>
                                                        <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                                            Code
                                                        </th>
                                                    </tr>
                                                    <?php
                                                    foreach ($group as $library) {
                                                        echo '<tr>'
                                                        . '<td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.7%;">'
                                                        . $library['library_name'] . '</td>'
                                                        . '<td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.7%;">'
                                                        . $library['single_share_code'] . '</td></tr>';
                                                    }
                                                    ?>
                                                </table>
                                            </div>
                                        </div>
                                    <?php } /* END OF OUTER (MASTERBRANCH)FOREACH */ ?>
                                    <?php
                                } /* end of outer if */
                                ?>

                            </td>
                        </tr>
                        <tr>
                            <td style="line-height:normal;">
                                <?php if (isset($email_data['single_share_libraries']) && count($email_data['single_share_libraries'])) { ?>
                                    <div style="margin:1em 0">
                                        <div style="padding:0.2em 0.5em;background-color: #c4c4c4;">
                                            <?php 
                                            if(count($email_data['single_share_libraries']) > 1){
                                                echo "The following libraries were individually shared with you. Use their respective library code to import each of them.";
                                            }else{
                                                echo "The following librarary was individually shared with you. Use this library code to import it.";
                                            }
                                            
                                            ?>
                                        </div>
                                        <div>
                                            <table style="width: 100%;text-align: left;border-collapse: collapse;" cellspacing="0" cellpadding="0" >
                                                <tr style="background: #eee;">
                                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                                                        Library
                                                    </th>
                                                    <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                                                        Code
                                                    </th>
                                                </tr>
                                                <?php
                                                foreach ($email_data['single_share_libraries'] as $library) {
                                                    echo '<tr>'
                                                    . '<td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.7%;">'
                                                    . $library['library_name'] . '</td>'
                                                    . '<td style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.7%;">'
                                                    . $library['single_share_code'] . '</td></tr>';
                                                }
                                                ?>
                                            </table>
                                        </div>
                                    </div>
                                    <?php
                                } /* end of outer if */
                                ?>

                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                <tr>
                    <td style="vertical-align:top;padding:0.3em 0">
                        Thank you,<br/>Insight Health Apps Support
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
