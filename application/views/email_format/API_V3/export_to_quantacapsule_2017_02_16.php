<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                Welcome to The Genius Insight
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Hi<?php echo !empty($quanta_user['name']) ? ' ' . $quanta_user['name'] : " User"; ?>,</p>
                            <?php
                                $msg = !empty($genius_user['name']) ? '<b>' . $genius_user['name'] . '</b>' : '<b>User </b>';
                                $msg .=!empty($genius_user['email']) ? ' (' . $genius_user['email'] . ' )' : '';
                                if (strlen($msg)) {
                                    $msg .= ' has shared some items, from Genius Insight App, with you.';
                                }
                            ?>
                            <p><?php echo $msg; ?></p>
                            <div>
                                Below is your code which is required to import the items into Quanta Capsule App.
                            </div>
                            <p><?php echo isset($import_code) ? '<p><b>Your Code is :  ' . $import_code . '</b></p>' : '' ?></p>
                            
                            <p>Thank you,<br/>Insight Health Apps Support</p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
