<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                                Welcome to Insight Quanta Cap
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Hi<?php echo!empty($quanta_user['name']) ? ' ' . $quanta_user['name'] : " User"; ?>,</p>
                            <?php
                            $msg = !empty($genius_user['name']) ? '<b>' . $genius_user['name'] . '</b>' : '<b>User </b>';
                            $msg .=!empty($genius_user['email']) ? ' (' . $genius_user['email'] . ' )' : '';
                            if (strlen($msg)) {
                                $msg .= ' has shared some digital frequencies for your health and wellbeing.';
                            }
                            ?>
                            <p><?php echo $msg; ?></p>
                            <p>To import these frequencies into your Insight Quanta Cap, open the app and select the navigation in the top right</p>
                            <div>
                                Then select the Import Genie Quanta and then enter the code below.
                            </div>
                            <p><?php echo isset($import_code) ? '<p><b>Your Insight Quanta Code is :  ' . $import_code . '</b></p>' : '' ?></p>
                            <p>Once you have entered this code, you will see the frequencies listed.</p>
                            <p>Simply select the specific frequencies and drag them down to the Main Hold Tray.</p>
                            <p>Select the timer for the duration of balancing and select Start.</p>
                            <p>For best results, try to find a safe space where you will not be interrupted and listen to the tones.</p>
                            <p>To purchase The Geo harness for added benefit click here <a target="_blank" href="http://quantumhealthapps.com/genius-hardware-accessories/">http://quantumhealthapps.com/genius-hardware-accessories/</a></p>


                            <p>Thank you,<br/>Energy Re-Mastered Apps Support</p>
                            <p><a target="_blank" href="tel:+18002772853">(800) 277-2853</a></p>
                            <p>Skype: New Paradigm</p>
                            <p>ps: Send an email to <a target="_top" href="mailto:freeapp@biofeedbackapps.com">freeapp@biofeedbackapps.com</a> and we will send you a free trial of another amazing app!</p><br/>
                                    <span><img class="CToWUd a6T" tabindex="0" src="<?php echo base_url('assets/img/email-Banner_HB.jpeg') ?>"/></span>

                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
