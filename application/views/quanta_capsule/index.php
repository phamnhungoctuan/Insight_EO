<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue-hoki">
        <div class="portlet-title">
            <div class="caption">
                <?php if (isset($total_rows)) { ?> 
                    <i class="fa fa-globe"></i><?php echo "Total Records : " . $total_rows; ?>
                <?php } ?>
            </div>
            <div class="actions">
                <div class="btn-group">
                    <a class="btn default ajax-demo" data-toggle="modal" data-url="<?php echo base_url().'Admin/quanta_capsule/user_add'; ?>"><i class="fa fa-pencil"></i>Create New Key </a>
                </div>
            </div>
        </div>
        <div class="portlet-body set-table">
            <div class="cat-nav" >
                <div id="search1" style="display:inline-block;">
                    <form id="search" method="get" class="user-search-form">
                        <input type="text" name="q"  placeholder="Search..." value="<?php echo isset($q) ? ($q) : "" ?>" class="user_search_text"  />
                        <input type="hidden" name="page"  value="0"/>
                        <input id='user_searchBtn' type="button" value="Search"/>
                    </form>
                </div>
            </div>
            <div class="set-table-div">
                <?php if (!empty($results)) { ?>
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                    <tr>
                        <th class="id"><a style="text-decoration:none" href="javascript:void(0)">Record#</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">Name</a></th>
                        <th class="send-email"><a style="text-decoration:none" href="javascript:void(0)">Email</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">Phone#</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">Address</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">City</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">State</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">Country</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">Zip Code</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">Gross Income</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">Receipt-Id</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">Payment-Date</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">IPN Track-Id</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">Payment-Status </a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">Number of Licences</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">ItemId</a></th>
                        <th class="td_Key"><a style="text-decoration:none" href="javascript:void(0)">Key</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">DeviceUDID</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">Date Activated</a></th>
                        <th><a style="text-decoration:none" href="javascript:void(0)">Date Created</a></th>
                        <th><a href="#">Action</a></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                            $i = isset($page) ? $page + 1 : 1;
                        foreach ($results as $row) {

                            $encrypt_user_id = $this->encrypt->encode($row['id']);
                            ?>
                            <tr id="tr^^<?php echo $encrypt_user_id; ?>">
                                <td class="td_id"><?php echo $i++; ?></td>
                                <td><?php echo $row["name"]; ?></td>
                                <td><?php echo $row["email"]; ?></td>
                                <td class="send-email"><?php echo $row["phone_no"]; ?></td>
                                <td><?php echo $row["address"]; ?></td>
                                <td><?php echo $row["city"]; ?></td>
                                <td><?php echo $row["state"]; ?></td>
                                <td><?php echo $row["country"]; ?></td>
                                <td><?php echo $row["zip_code"]; ?></td>
                                <td><?php echo $row["payment_amount"]; ?></td>
                                <td><?php echo $row["payment_receipt_id"]; ?></td>
                                <td><?php echo $row["payment_date"]; ?></td>
                                <td><?php echo $row["ipn_track_id"]; ?></td>
                                <td><?php echo $row["payment_status"]; ?></td>
                                <td><?php echo $row["licence_purchase_count"]; ?></td>
                                <td><?php echo $row["itemid"]; ?></td>
                                <td class="td_Key"><?php if (($row["Key"] == '' || $row['Key'] == NULL)) { ?> 
                                    <button id="<?php echo $row["id"] ?>"  class="create_key ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/quanta_capsule/add_activation_key/' . $row['id'] ?>" role="button" aria-disabled="false"><i class="fa fa-key"></i></button>
                                <?php } else {
                                                echo "<span>" . $row['Key'] . "</span>"; ?> 
                                        <br/><button id="<?php echo 'send_activation_key_' . $row["id"] ?>"  class="sendEmail" role="button"  onclick="send_activation_key('<?php echo $row["id"]; ?>','Admin/quanta_capsule/send_activation_key')" title="Re-Send Activation Key"><i class="fa fa-envelope"></i></button>
                                        <?php    } ?></td>
                                <td class="td_uuid"><?php echo $row["UUID"]; ?></td>
                                <td><?php echo $row["ActivatedDate"]; ?></td>
                                <td><?php echo $row["createdtime"]; ?></td>
                                <td class="edit-del">
                                    <a class="edit ajax-demo" data-toggle="modal" data-url="<?php echo base_url().'Admin/quanta_capsule/user_edit/' . $row['id']?>"><i class="fa fa-pencil"></i> </a> 
                                    <a class="edit ajax-demo" data-toggle="modal" data-url="<?php echo base_url().'Admin/quanta_capsule/delete/' . $row['id']?>"> <i class="fa fa-times"></i></a> 
                                </td>
                            </tr>   
                            <?php
                        }
                    ?>
                </tbody>
            </table>
<?php } else {echo '<h1 style="text-align:center;"> No Results Found <h1>'; }  ?>
            </div>
            <div class="row">
                <div class="col-md-7 col-sm-12">
<?php echo isset($links) ? $links : ""; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
