<div class="my-modal">
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    echo validation_errors('<span class="error">', '</span>');
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit User</h4>
    </div>
    <?php echo form_open('Admin/quanta_capsule/edit_user', array('method' => 'post', "id" => "UserForm", "class" => "validate")); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">

                <p class="form-group">
                    <?php echo form_label('Name', 'name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'name', 'id' => 'name', 'value' => $user_data['name'], 'class' => 'col-md-12 form-control')); ?>		
                </p>
                <p class="form-group">
                    <?php echo form_label('Email', 'email'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_input(array('type' => 'text', 'name' => 'email', 'id' => 'email', 'value' => $user_data['email'], 'class' => 'col-md-12 form-control email')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Phone #', 'phone_no'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'phone_no', 'id' => 'phone_no', 'autocomplete' => 'off', 'value' => $user_data['phone_no'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Address', 'address'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'address', 'id' => 'address', 'value' => $user_data['address'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('City', 'city'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'city', 'id' => 'city', 'value' => $user_data['city'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('State', 'state'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'state', 'id' => 'state', 'value' => $user_data['state'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Country', 'country'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'country', 'id' => 'country', 'autocomplete' => 'off', 'value' => $user_data['country'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Zip Code', 'zip_code'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'zip_code', 'id' => 'zip_code', 'autocomplete' => 'off', 'value' => $user_data['zip_code'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Payment-Date', 'payment_date'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_date', 'id' => 'payment_date', 'autocomplete' => 'off', 'value' => $user_data['payment_date'], 'class' => 'col-md-12 form-control date-picker')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Receipt-Id (From Paypal)', 'payment_receipt_id'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_receipt_id', 'id' => 'payment_receipt_id', 'autocomplete' => 'off', 'value' => $user_data['payment_receipt_id'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('IPN Track-Id (From Paypal)', 'ipn_track_id'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'ipn_track_id', 'id' => 'ipn_track_id', 'autocomplete' => 'off', 'value' => $user_data['ipn_track_id'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Payment Status', 'payment_status'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_status', 'id' => 'payment_status', 'autocomplete' => 'off', 'value' => $user_data['payment_status'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Item Id', 'itemid'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'itemid', 'id' => 'itemid', 'autocomplete' => 'off', 'value' => $user_data['itemid'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Number of Licences', 'licence_purchase_count'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'licence_purchase_count', 'id' => 'licence_purchase_count', 'autocomplete' => 'off', 'value' => $user_data['licence_purchase_count'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Gross Income', 'payment_amount'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_amount', 'id' => 'payment_amount', 'autocomplete' => 'off', 'value' => $user_data['payment_amount'], 'class' => 'col-md-12 form-control ')); ?>
                </p>

                <p class="form-group">
                    <?php echo form_label('Key', 'Key'); ?>
                    <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'Key', 'id' => 'Key', 'autocomplete' => 'off', 'value' => $user_data['Key'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Device Id', 'UUID'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'UUID', 'id' => 'UUID', 'autocomplete' => 'off', 'value' => $user_data['UUID'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Date Activated', 'ActivatedDate'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'ActivatedDate', 'id' => 'ActivatedDate', 'autocomplete' => 'off', 'value' => $user_data['ActivatedDate'], 'class' => 'col-md-12 form-control date-picker')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Date Created', 'createdtime'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'createdtime', 'id' => 'createdtime', 'autocomplete' => 'off', 'value' => $user_data['createdtime'], 'class' => 'col-md-12 form-control date-picker')); ?>
                </p>
                <?php
                echo form_input(array('type' => 'hidden', 'value' => $user_data['id'], 'name' => 'id', 'id' => 'id', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control '));
                ?>

            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php
        echo form_input(array('class' => 'btn blue', 'value' => 'Update', "type" => "submit"));
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        ?>
    </div>
    <?php echo form_close(); ?>
    <script>
        jQuery(document).ready(function() {
            FormValidation.init();
        });
    </script>
</div>