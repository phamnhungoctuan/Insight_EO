<div class="my-modal">
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    echo validation_errors('<span class="error">', '</span>');
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit User</h4>
    </div>
    <?php echo form_open('Admin/energy_mastered/edit_user', array('method' => 'post', "id" => "UserForm", "class" => "validate")); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <p class="form-group">
                    <?php echo form_label('First Name', 'first_name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'first_name', 'id' => 'first_name', 'value' => $user_data['first_name'], 'class' => 'col-md-12 form-control')); ?>
                </p>
                <p>
                    <?php echo form_label('Last Name', 'last_name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'last_name', 'id' => 'last_name', 'value' => $user_data['last_name'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Email', 'email'); ?>
                    <input type="text" name="email" id="email" readonly="readonly" value="<?php echo $user_data['email'] ?>" class="col-md-12 form-control email" onblur="check_email(this)" />
                </p>
                <p class="form-group">
                    <?php echo form_label('Status', 'status'); ?>
                    <select name="status" id="status" class="col-md-12 form-control">
                        <option value="0" <?php if($user_data['status'] == 0)  echo 'selected';?>>
                            0
                        </option>
                        <option value="1" <?php if($user_data['status'] == 1)  echo 'selected';?>>
                            1
                        </option>
                        <option value="2" <?php if($user_data['status'] == 2)  echo 'selected';?>>
                            2
                        </option>
                    </select>
                </p>

                <p>
                    <?php echo form_label('Country', 'country'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'country', 'id' => 'country', 'autocomplete' => 'off', 'value' => $user_data['country'], 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Activation Key Code', 'activation_key'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'activation_key', 'readonly' => 'readonly', 'id' => 'activation_key', 'autocomplete' => 'off', 'value' => $user_data['activation_key'], 'class' => 'col-md-12 form-control')); ?>
                </p>

                <p>
                    <?php echo form_label('Date Created', 'date_created'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'date_created', 'id' => 'date_created', 'autocomplete' => 'off', 'value' => $user_data['date_created'], 'class' => 'col-md-12 form-control date-picker')); ?>
                </p>
                <?php
                echo form_input(array('type' => 'hidden', 'value' => $user_data['id'], 'name' => 'id', 'id' => 'id', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control '));
                ?>

            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php
        echo form_input(array('class' => 'btn blue', 'value' => 'Update', 'type' => 'submit'));
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        ?>
    </div>
    <?php echo form_close(); ?>
    <script>
        FormValidation.init();
        function check_email(obj){
            var email = $.trim($(obj).val());
            if(email==="") return false;
            var id = '';
            if($(obj).parents('form').find('input[name="id"]').length)
                id = $(obj).parents('form').find('input[name="id"]').val();
            else
                id = 0;
            $.ajax({
                url :site_url+"Admin/energy_mastered/check_email",
                data: {email : email, id : id},
                type:"POST",
                async:false,
                success: function(html){
                    var response= $.trim(html);
                    if(response==1){
                        alert("Email You Entered Already Exist. Please use another email");
                        $(obj).val("");
                    }
                },
                failure:function(xhr,status,code){
                    alert("Error Occured");
                }
            });
            return false;
        }
    </script>
</div>