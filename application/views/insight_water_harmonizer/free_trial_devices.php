<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue-hoki">
        <div class="portlet-title">
            <div class="caption">
                <?php if (isset($total_rows)) { ?> 
                    <i class="fa fa-globe"></i><?php echo "Total Records : " . $total_rows; ?>
                <?php } ?>
            </div>
        </div>
        <div class="portlet-body set-table">
            <?php if ($this->router->class != 'reports') { ?>
                <div class="cat-nav" >
                    <div id="search1" style="display:inline-block;">
                        <form id="search" method="get" class="user-search-form">
                            <input type="text" name="q" class="user_search_text"  placeholder="Search..." value="<?php echo isset($q) ? ($q) : "" ?>"/>
                            <input type="hidden" name="type"  value="<?php echo isset($type) ? $type : "all" ?>"/>
                            <input type="hidden" name="page"  value="0"/>
                            <input id='user_searchBtn' type="button" value="Search"/>
                        </form>
                    </div>
                    <div id="viewAll1" class="c-list <?php echo !empty($type) ? ($type == 'all' ? "active_submenu" : "") : "active_submenu"; ?>">
                        <a href="javascript:void(0)" data-url="all" class="js-tabs user_type btn-blue" >View All Devices</a>
                    </div>
                    <div id="paid" class="c-list <?php echo isset($type) && $type == 'free' ? "active_submenu" : ""; ?>">
                        <a  href="javascript:void(0)" data-url="free" class="js-tabs user_type btn-blue" >On Free Trial</a>
                    </div>
                    <div id="activated" class="c-list <?php echo isset($type) && $type == 'activated' ? "active_submenu" : ""; ?>">
                        <a href="javascript:void(0)" data-url="activated" class="js-tabs user_type btn-blue" >Devices Activated Now</a>
                    </div>
                    
<!--                    <div id="android" class="c-list <?php echo isset($type) && $type == 'Android' ? "active_submenu" : ""; ?>">
                        <a href="javascript:void(0)" data-url="Android" class="js-tabs user_type btn-blue" >Android Users</a>
                    </div>
                    <div id="iphone" class="c-list <?php echo isset($type) && $type == 'Iphone' ? "active_submenu" : ""; ?>">
                        <a href="javascript:void(0)" data-url="Iphone" class="js-tabs user_type btn-blue" >Iphone Users</a>
                    </div>-->
                    <div id="free" class="c-list <?php echo isset($type) && $type == 'paid' ? "active_submenu" : ""; ?>">
                        <a href="<?php echo base_url() . 'Admin/insight_water_harmonizer/index'; ?>" data-url="free" class="js-tabs user_type btn-blue"><b>Back to Users Listing</b></a>
                    </div>
                </div>

            <?php } ?>
            <?php $scrollerClass = $this->router->class != 'reports' ? "secnd-tab" : ""; ?>
            <div class="set-table-div <?php echo $scrollerClass; ?>">
                <?php if (!empty($results)) { ?>
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                        <thead>
                            <tr>
                                <th class="id" name="id"><a style="text-decoration:none" href="javascript:void(0)">Record#</a></th>
                                <th class="fname"><a style="text-decoration:none" href="javascript:void(0)">Device UUID</a></th>
                                <th id="data-open" class=""><a style="text-decoration:none" href="javascript:void(0)">Device Status</a></th>
                                <th><a style="text-decoration:none" href="javascript:void(0)">Date Created<br/> (Y-M-D)</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = isset($page) ? $page + 1 : 1;
                            foreach ($results as $row) {

                                $encrypt_user_id = $this->encrypt->encode($row['device_id']);
                                ?>
                                <tr id="tr^^<?php echo $encrypt_user_id; ?>">
                                    <td class="td_id id"><?php echo $i++; ?></td>
                                    <td class="td_uuid did">
                                        <?php echo $row["device_uuid"];?>
                                    </td>
                                    <td class="td_Key" ><?php echo $row['device_status'] ?></td>
                                    <td class="td_Key" ><?php echo format_date($row['created_date']) ?></td>
                                </tr>   
        <?php
    }
    ?>
                        </tbody>
                    </table>
<?php
} else {
    echo '<h1 style="text-align:center;"> No Results Found <h1>';
}
?>
            </div>
            <div class="row" style="cursor: pointer">
                <div class="col-md-7 col-sm-12">
<?php echo!empty($links) ? $links : ""; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
<script type='text/javascript'>

    $(document).ready(function() {
        $('.user_type').click(function() {
            $('[name="type"]').val($(this).data('url'));
            $('form#search').submit();
        });

    });

</script>