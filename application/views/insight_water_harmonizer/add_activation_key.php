<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//echo validation_errors('<span class="error">', '</span>');
?>
<div class="modal-header" >
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Create Key</h4>
</div>
<?php echo form_open($action, array('method' => 'post', "id" => "UserForm","class"=>"validate")); ?>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            
            <p class='form-group'>
                <?php echo form_label('Activation Key Code', 'activation_key'); ?>
                <span class="mandatory">*</span>
                <?php $val= isset($activation_key)?$activation_key : set_value('activation_key') ?>
                <?php echo form_input(array('type' => 'text', 'name' => 'activation_key',"readonly"=>"readonly",  'id' => 'activation_key', 'value' => $val, 'class' => 'col-md-12 form-control required')); ?>		
            </p>
            <?php 
                echo form_input(array('type' => 'hidden', 'value' => $id,'name' => 'id', 'id' => 'id', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control required'));
            ?>
           
        </div>
    </div>
</div>
<div class="modal-footer">
    <?php echo form_input(array('class'=>'btn blue','value' => 'Add', 'type' => 'submit'));
    echo form_input(array('class'=>'btn default', 'data-dismiss'=>'modal', 'type' => 'button', 'value' => 'Close')); ?>
</div>
<?php echo form_close(); ?>
<script>
jQuery(document).ready(function() {   
   FormValidation.init();
});
</script>