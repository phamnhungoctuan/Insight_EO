<div class="my-modal">
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
//echo validation_errors('<span class="error">', '</span>');
    ?>
    <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add New User</h4>
    </div>
    <?php echo form_open('Admin/insight_water_harmonizer/save_new_user', array('method' => 'post', "id" => "UserForm", "class" => "validate")); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">

                <p class="form-group">
                    <?php echo form_label('First Name', 'first_name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'first_name', 'id' => 'first_name', 'value' => set_value('first_name'), 'class' => 'col-md-12 form-control')); ?>		
                </p>
                <p class="form-group">
                    <?php echo form_label('Last Name', 'last_name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'last_name', 'id' => 'last_name', 'value' => set_value('last_name'), 'class' => 'col-md-12 form-control')); ?>		
                </p>
                <p class="form-group">
                    <?php echo form_label('Email', 'email'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_input(array('type' => 'text', 'name' => 'email', 'id' => 'email', 'value' => '', 'class' => 'col-md-12 form-control email', "onblur" => "check_email('Admin/harmonizer',this)")); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Phone #', 'phone_no'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'phone_no', 'id' => 'phone_no', 'autocomplete' => 'off', 'value' => set_value('phone_no'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Address', 'address'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'address', 'id' => 'address', 'value' => set_value('address'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('City', 'city'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'city', 'id' => 'city', 'value' => set_value('city'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('State', 'state'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'state', 'id' => 'state', 'value' => set_value('state'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Country', 'country'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'country', 'id' => 'country', 'autocomplete' => 'off', 'value' => set_value('country'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Zip Code', 'zip_code'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'zip_code', 'id' => 'zip_code', 'autocomplete' => 'off', 'value' => set_value('zip_code'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Activation Key', 'activation_key'); ?>
                    <span class="mandatory">*</span>
                    <?php $val = isset($activation_key) ? $activation_key : set_value('activation_key') ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'activation_key', 'id' => 'Key', 'autocomplete' => 'off', 'value' => $val, 'class' => 'col-md-12 form-control required','readonly'=>'readonly')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Device Id', 'uuid'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'uuid', 'id' => 'uuid', 'autocomplete' => 'off', 'value' => set_value('uuid'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php echo form_input(array('class' => 'btn blue', 'value' => 'Add', 'type' => 'submit'));
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        ?>
    </div>
<?php echo form_close(); ?>
    <script>
        jQuery(document).ready(function() {
            FormValidation.init();
        });
    </script>
</div>