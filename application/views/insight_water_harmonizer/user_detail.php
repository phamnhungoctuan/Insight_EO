<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style type="text/css">
    .hide_tag{ display:none;}
    .show_tag{display: block;}
</style>
<?php $encrypt_user_id = $this->encrypt->encode($user_data['id']); ?>
<div class="my-modal" id="div1^^<?php echo $encrypt_user_id; ?>">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">User Details</h4>
    </div>
    <?php echo form_open('Admin/insight_water_harmonizer/edit_user', array('method' => 'post', "id" => "UserForm", "class" => "validate")); ?>
    <div id="div^^<?php echo $encrypt_user_id; ?>">

        <div class="modal-body">
            <div class="row">

                <div class="col-md-12 quant-descp-popup" >
                    <form>
                        <p class="form-group">
                            <label for="first_name">First Name</label>
                            <label class="show_tag"><?php echo $user_data['first_name'] ?></label>
                            <input type="text" name="first_name" id="first_name" value="<?php echo $user_data['first_name'] ?>" class="hide_tag col-md-12 form-control"/>
                        </p>
                        <p>
                            <label>Last Name</label>
                            <label class="show_tag"><?php echo $user_data['last_name'] ?></label>
                            <input type="text" name="last_name" id="last_name" value="<?php echo $user_data['last_name'] ?>" class="hide_tag col-md-12 form-control"/>
                        </p>
                        <p class="form-group">
                            <label>Email <span class="mandatory hide_tag">*</span></label>
                            <label class="show_tag"><?php echo $user_data['email'] ?></label>

                            <input type="text" name="email" id="email" value="<?php echo $user_data['email'] ?>" class="hide_tag col-md-12 form-control email"/>
                        </p>
                        <p>
                            <label>Phone #</label>
                            <label class="show_tag"><?php echo $user_data['phone_no'] ?></label>
                            <input type="text" name="phone_no" id="phone_no" value="<?php echo $user_data['phone_no'] ?>" class="hide_tag col-md-12 form-control"/>
                        </p>
                        <p>
                            <label>Address</label>
                            <label class="show_tag"><?php echo $user_data['address'] ?></label>
                            <input type="text" name="address" id="address" value="<?php echo $user_data['address'] ?>" class="hide_tag col-md-12 form-control"/>
                        </p>
                        <p>
                            <label>City</label>
                            <label class="show_tag"><?php echo $user_data['city'] ?></label>
                            <input type="text" name="city" id="city" value="<?php echo $user_data['city'] ?>" class="hide_tag col-md-12 form-control"/>
                        </p>
                        <p>
                            <label>State</label>
                            <label class="show_tag"><?php echo $user_data['state'] ?></label>
                            <input type="text" name="state" id="state" value="<?php echo $user_data['state'] ?>" class="hide_tag col-md-12 form-control"/>
                        </p>
                        <p>
                            <label>Country</label>
                            <label class="show_tag"><?php echo $user_data['country'] ?></label>
                            <input type="text" name="country" id="country" value="<?php echo $user_data['country'] ?>" class="hide_tag col-md-12 form-control"/>
                        </p>
                        <p>
                            <label>Zip Code</label>
                            <label class="show_tag"><?php echo $user_data['zip_code'] ?></label>
                            <input type="text" name="zip_code" id="zip_code" value="<?php echo $user_data['zip_code'] ?>" class="hide_tag col-md-12 form-control"/>
                        </p>
                        <p class="form-group">
                            <label>Activation Key</label>
                            <label class="show_tag"><?php echo $user_data['activation_key'] ?></label>
                            <input type="text" name="activation_key" readonly="readonly" id="activation_key" value="<?php echo $user_data['activation_key'] ?>" class="hide_tag col-md-12 form-control required "/>
                        </p>
                        <p>
                            <label>Device Id</label>
                            <label class="show_tag"><?php echo $user_data['uuid'] ?></label>
                            <input type="text" name="uuid" id="uuid" value="<?php echo $user_data['uuid'] ?>" class="hide_tag col-md-12 form-control"/>
                        </p>
                        <p>
                            <label>Date Activated</label>
                            <label class="show_tag"><?php echo $user_data['date_activated'] ?></label>
                            <input type="text" name="date_activated" id="date_activated" value="<?php echo $user_data['date_activated'] ?>" class="date-picker hide_tag col-md-12 form-control"/>
                        </p>
                        <p>
                            <label>Date Created</label>
                            <label class="show_tag"><?php echo $user_data['date_created'] ?></label>
                            <input type="text" name="date_created" id="date_created" value="<?php echo $user_data['date_created'] ?>" class="date-picker hide_tag col-md-12 form-control"/>
                        </p>
                        <?php
                        $deviceType = "";
                        $deviceType = $user_data['deviceType'] == "a" ? "Android" : $deviceType;
                        $deviceType = $user_data['deviceType'] == "i" ? "IPhone" : $deviceType;
                        ?>
                        <p>
                            <label>Device Type</label>
                            <label class="show_tag"><?php echo $deviceType ?></label>
                            <input type="text" name="deviceType" id="deviceType" value="<?php echo $deviceType ?>" readonly="readonly" class="hide_tag col-md-12 form-control"/>
                        </p>
                        <?php
                        echo form_input(array('type' => 'hidden', 'value' => $user_data['id'], 'name' => 'id', 'id' => 'id', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control '));
                        ?>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a class="edit btn blue" data-url="Admin/insight_water_harmonizer/edit_user/" onclick="edit_user(this)">EDIT</a>
            <input type="submit" value="Update" class="btn blue update_btn" style="display: none;"/>
            <a alt="Delete" data_url="Admin/insight_water_harmonizer/delete_user"  title="Delete" class="delete btn blue" onclick="delete_user(this);" href="javascript:void(0);">DELETE</a> 
            <?php
            echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
            ?>

        </div>

    </div>
    <?php echo form_close(); ?>
    <script>
        FormValidation.init();
    </script>

    <script type="text/javascript">
        function edit_user(obj) {
            $('.show_tag').remove();
            $('.hide_tag').show();
            $('.update_btn').css("display", "inline-block");
            $('.modal-title').html("Edit User");
//        $(obj).closest('form').attr('action',site_url+$(obj).attr('data-url'));
            $(obj).remove();
        }
    </script>
</div>