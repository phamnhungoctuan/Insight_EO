<br/>
<div>
    <blockquote type="cite">
        <br>
        <div>Dear  <?php echo isset($distributor['distributor']) && strlen($distributor['distributor']) ? $distributor['distributor'] : 'Distributor' ?>,
            <br><br>
        </div>
    </blockquote>
    Below is the lead details of someone who has downloaded the Water Harmonizer App. As soon as you contact them, ensure they are not currently working with anyone. Secondly please be sure to enter them as a lead under your affiliate program as long as they are not working with anyone currently.
</div>
<div>
    <br>
</div>
<div>
    <table style="width: 100%;border: 1px solid #999;text-align: left;border-collapse: collapse;margin: 0 0 1em 0;" cellspacing="0" cellpadding="0" >
        <tr style="background: #eee;">
            <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;" colspan="2">
                User Detail
            </th>
        </tr>
        <tr>
            <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                Email
            </th>
            <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                <?php echo $user_detail['email']; ?>
            </th>
        </tr>
        <?php if (strlen($user_detail['txnid']) || strlen($user_detail['last_name'])) { ?>
            <tr>
                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                    Name
                </th>
                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                    <?php echo trim($user_detail['txnid'] . '' . $user_detail['last_name']); ?>
                </th>
            </tr>
        <?php } ?>
        <?php if (strlen($user_detail['Key'])) { ?>
            <tr>
                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                    Activation Key
                </th>
                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                    <?php echo $user_detail['Key'] ?>
                </th>
            </tr>
        <?php } ?>
    </table>
</div>
<div>Good luck and happy sales!</div>
<div>
    <div style="word-wrap:break-word">
        <p style="color:rgb(0,0,0);font-family:'Times New Roman';font-size:14px;font-style:normal;font-weight:normal;letter-spacing:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px" class="MsoNormal">
            <span style="font-size:14pt">
                Ryan Williams<u></u><u></u>
            </span>
        </p>
        <p style="color:rgb(0,0,0);font-family:'Times New Roman';font-size:14px;font-style:normal;font-weight:normal;letter-spacing:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px" class="MsoNormal">
            <b><span style="font-size:16pt;color:rgb(0,0,144)">Quantum Health Apps<u></u><u></u></span></b>
        </p>
        <p style="color:rgb(0,0,0);font-family:'Times New Roman';font-size:14px;font-style:normal;font-weight:normal;letter-spacing:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px" class="MsoNormal">
            <i>
                <span style="font-size:14pt;color:green">Tap Into Your Genius
                    <br>Tel: 800-277-2853
                    <br>Email:&nbsp;<a target="_blank" href="mailto:rw@QuantumhealthApps.com">rw@QuantumHealthApps.com</a>
                    <br><a target="_blank" href="http://www.QuantumHealthApps.com">www.QuantumHealthApps.com</a>
                    <br>
                </span>
            </i>
            <span style="font-size:14pt">Download Genius Insight App FREE Trial&nbsp;
                <i><span style="color:green"><u></u><u></u></span></i></span>
        </p>
        <p style="color:rgb(0,0,0);font-family:'Times New Roman';font-size:14px;font-style:normal;font-weight:normal;letter-spacing:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px" class="MsoNormal">
            <span style="font-size:14pt">iOS:&nbsp;
                <a target="_blank" href="https://rink.hockeyapp.net/apps/f90e952d9610436dad298a18d2629472">Click here</a>
            </span>
        </p>
        <p style="color:rgb(0,0,0);font-family:'Times New Roman';font-size:14px;font-style:normal;font-weight:normal;letter-spacing:normal;text-align:start;text-indent:0px;text-transform:none;white-space:normal;word-spacing:0px" class="MsoNormal">
            <span style="font-size:14pt">Android:</span>&nbsp;
            <a target="_blank" style="font-size:14pt" href="https://rink.hockeyapp.net/apps/e6e748aac7ea4ac180f12ef553871af1">Click here</a>
        </p>
        <br>
    </div>
</div>
