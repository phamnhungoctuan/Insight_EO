<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family:arial,sans-serif; font-size: 34px; color: #57697e;">
                                Download & Activation Codes: Emotional Insight App
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family:arial,sans-serif;">
                            <p style="line-height:15px;">Hi<?php echo isset($user_detail['txnid']) ? " " . trim($user_detail['txnid'].' '.$user_detail['last_name']) : ' User' ?>,</p>
                            <p style="line-height:15px;">Thank you for joining Energy Re-Mastered Apps and investing in the Emotional Insight App.</p>
                            <p style="line-height:15px;">Below is your activation code required to activate your app, once you have fully downloaded and installed Emotional Insight App.</p>
                            <p style="line-height:15px">
                                App Activation Code : <br/>
                                <?php echo strlen($user_detail['Key'])? $user_detail['Key'] : "" ?>
                            </p>
                            <p style="line-height:15px">
                                <font>
                                    iOS Download Link:
                                    <a href="https://rink.hockeyapp.net/apps/8b63d764cd5223ac4955f8af479b9934">https://rink.hockeyapp.net/apps/8b63d764cd5223ac4955f8af479b9934</a>
                                </font>
                            </p>
                            <p style="line-height:15px">
                                "Untrusted Developer Pop Up Box" <br/>
                                Once this appears, select your iOS "Settings" icon, then "General" then scroll down to the bottom and select "Device Management" and Trust the Canada Inc profile.
                            </p>
<!--                            <p style="line-height:15px">
                                <font>
                                    Android Download Link:
                                    <a href="https://rink.hockeyapp.net/apps/6874f70531efab7a66ec9c4ea2b9aae4">https://rink.hockeyapp.net/apps/6874f70531efab7a66ec9c4ea2b9aae4</a>
                                </font>
                            </p>
                            <p style="line-height:15px"> 
                                If prompted go to your "Settings" then "Apps" and allow installation from 3rd party apps.
                            </p>-->
                            <p style="line-height:15px"> 
                                More Software Applications and Products : <a href="http://quantumhealthapps.com/">http://quantumhealthapps.com/</a>
                            </p>
                            <p style="line-height:15px"> 
                                <b>FOR ALL INSTALLATION OR REINSTALLATION LINKS PLEASE SAVE THIS WEBPAGE</b> : <a href="http://quantumhealthapps.com/software-downloads/">http://quantumhealthapps.com/software-downloads/</a>
                            </p>
                            <p style="line-height:15px"> 
                                Genius Hardware Instructions : <a href="http://quantumhealthapps.com/genius-hardware-instructions/">http://quantumhealthapps.com/genius-hardware-instructions/</a>
                            </p>
                            <p style="line-height:15px"> 
                                Thank you, <br/>
                                Energy Re-Mastered Apps Support<br/>
                            </p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
