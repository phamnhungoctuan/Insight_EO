<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet box blue-hoki">
        <div class="portlet-title">
            <div class="caption">
                <?php if (isset($total_rows)) { ?> 
                    <i class="fa fa-globe"></i><?php echo "Total Records : " . $total_rows; ?>
                <?php } ?>
            </div>
            <div class="actions">
                <div class="btn-group">
                    <a class="btn default ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/emotional_insight/user_add'; ?>"><i class="fa fa-pencil"></i>Create New Key </a>
                </div>
            </div>
        </div>
        <div class="portlet-body set-table" >
            <div class="cat-nav" >
                <div id="search1" style="display:inline-block;">
                    <form id="search" method="get" class="validate" style="margin-bottom:0">
                        <input type="text" name="q"  placeholder="Search..." value="<?php echo isset($q) ? ($q) : "" ?>"/>
                        <input type="hidden" name="type"  value="<?php echo isset($type) ? $type : "all" ?>"/>
                        <input type="hidden" name="page"  value="0"/>
                        <input id='searchBtn' type="submit" value="Search"/>
                    </form>
                </div>
                <div id="viewAll1" class="c-list <?php echo isset($type) ? ($type == 'all' ? "active_submenu" : "") : "active_submenu"; ?>">
                    <a href="javascript:void(0)" data-url="all" class="js-tabs user_type btn-blue" >View All Users</a>
                </div>
                <div id="activated" class="c-list <?php echo isset($type) && $type == 'activated' ? "active_submenu" : ""; ?>">
                    <a href="javascript:void(0)" data-url="activated" class="js-tabs user_type btn-blue" >View Activated Users</a>
                </div>
                <div id="paid" class="c-list <?php echo isset($type) && $type == 'paid' ? "active_submenu" : ""; ?>">
                    <a  href="javascript:void(0)" data-url="paid" class="js-tabs user_type btn-blue" >View Paid Users</a>
                </div>
                <div id="free" class="c-list <?php echo isset($type) && $type == 'free' ? "active_submenu" : ""; ?>">
                    <a href="javascript:void(0)" data-url="free" class="js-tabs user_type btn-blue" >View Free Users</a>
                </div>
                <div id="android" class="c-list <?php echo isset($type) && $type == 'Android' ? "active_submenu" : ""; ?>">
                    <a href="javascript:void(0)" data-url="Android" class="js-tabs user_type btn-blue" >Android Users</a>
                </div>
                <div id="iphone" class="c-list <?php echo isset($type) && $type == 'Iphone' ? "active_submenu" : ""; ?>">
                    <a href="javascript:void(0)" data-url="Iphone" class="js-tabs user_type btn-blue" >Iphone Users</a>
                </div>
            </div>
            <?php $scrollerClass = $this->router->class != 'reports' ? "secnd-tab" : ""; ?>
            <div class="set-table-div <?php echo $scrollerClass; ?>">
                <?php if ($results && !empty($results)) { ?>
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                        <thead>
                            <tr>
                                <th class="id" name="id" align="center" style="text-align:center"><a style="text-decoration:none; text-align:center" href="javascript:void(0)">#</a></th>
                                <th class="fname"><a style="text-decoration:none" href="javascript:void(0)">Name</a></th>
                                <th id="data-open" class=""><a style="text-decoration:none" href="javascript:void(0)">Email</a></th>
                                <th class="data-close"><a style="text-decoration:none" href="javascript:void(0)">Phone #</a></th>
                                <th class="data-close"><a style="text-decoration:none" href="javascript:void(0)">Country</a></th>
                                <th class="td_Key"><a style="text-decoration:none" href="javascript:void(0)">Key</a></th>
                                <th><a style="text-decoration:none" href="javascript:void(0)">DeviceUDID</a></th>
                                <th class=""><a style="text-decoration:none" href="javascript:void(0)">Distributors</a></th>
                                <th class=""><a style="text-decoration:none" href="javascript:void(0)">Send Email To Distributor</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = isset($page) ? $page + 1 : 1;
                            foreach ($results as $row) {

                                $encrypt_user_id = $this->encrypt->encode($row['id']);
                                ?>
                                <?php $classname = $row['isActivated'] == "1" ? "activated" : ""; ?>
                                <tr id="tr^^<?php echo $encrypt_user_id; ?>" class=" <?php echo $classname; ?>">
                                    <td class="td_id id" data='<?php echo $row['id'] ?>'><?php echo $i++; ?></td>
                                    <td style="cursor: pointer" class="clickable fname ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/emotional_insight/user_detail/' . $row['id']; ?>"><?php echo $row["txnid"] . " " . $row["last_name"]; ?></td>
                                    <td style="cursor: pointer" class="clickable email ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/emotional_insight/user_detail/' . $row['id']; ?>"><?php echo $row["email"]; ?></td>
                                    <td class="data-close phn"><?php echo $row["phone_no"]; ?></td>
                                    <td class="data-close country"><?php echo $row["country"]; ?></td>
                                    <td class="td_Key key"><?php if (($row["Key"] == '' || $row['Key'] == NULL)) { ?> 
                                        <button id="<?php echo $row["id"] ?>"  class="create_key ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/emotional_insight/add_activation_key/' . $row['id'] ?>" role="button" aria-disabled="false"><i class="fa fa-key"></i></button>
                                        <?php } else {
                                                echo "<span>" . $row['Key'] . "</span>"; ?> 
                                        <br/><button id="<?php echo 'send_activation_key_' . $row["id"] ?>"  class="sendEmail" role="button"  onclick="send_activation_key('<?php echo $row["id"]; ?>','Admin/emotional_insight/send_activation_key')" title="Re-Send Activation Key"><i class="fa fa-envelope"></i></button>
                                        <?php    } ?></td>
                                    <td class="td_uuid did"><?php echo $row["UUID"]; ?></td>
                                    <td class="data-close dis">
                                        <?php
                                        if (!isset($distributors_data)) {
                                            echo $row['distributor'];
                                        } else {
                                            ?>
                                            <select id="<?php echo 'distributor_' . $row['id'] ?>" name="selectDistributor" data-url="Admin/emotional_insight/set_distributor">
                                                <option value="" disabled="disabled" selected="selected">Select Distributor</option>
                                                <?php foreach ($distributors_data as $distributor) : ?>
                                                    <?php $select = $row['distributor_id'] == $distributor['id'] ? 'selected="selected"' : '' ?>
                                                    <option value="<?php echo $distributor['id']; ?>" <?php echo $select; ?>><?php echo $distributor['distributor'] ?></option>
                                                <?php endforeach; ?>
                                            </select>  
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <?php if (isset($distributors_data)) { ?>
                                        <td class="data-close demail">
                                            <button id="<?php echo $row["id"] ?>"  class="sendEmail" role="button" aria-disabled="false" data-url='Admin/emotional_insight/send_distributor_email' onclick="sendEmail(this)"><i class="fa fa-envelope"></i></button>
                                        </td>
                                    <?php } ?>
                                </tr>   
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
<?php } else {
    echo '<h1 style="text-align:center;"> No Results Found <h1>';
} ?>
            </div>
            <div class="row">
                <div class="col-md-7 col-sm-12">
                    <?php echo isset($links) ? $links : ""; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
<script type='text/javascript'>

    $(document).ready(function() {
        $('.user_type').click(function() {
            $('[name="type"]').val($(this).data('url'));
            $('form#search').submit();
        });

    });

</script>
