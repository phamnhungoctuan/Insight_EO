
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <?php $total_rows = isset($total_rows) ? $total_rows : ($results && !empty($results) ? count($results) : "0") ?>
                    <i class="fa fa-globe"></i><span> <?php echo "Total Records : " . $total_rows; ?></span>
                    <?php if (isset($report_title)) { ?>
                        <i class="fa fa-globe"></i><span><?php echo $report_title ?></span>
                    <?php }
                    ?>
                </div>
                <div class="actions">
                    <div class="btn-group">
                        <?php if ($this->router->class != "reports") { ?>
<!--                            <a href="<?php echo ADD_SUBSCRIBER; ?> ">
                                <img src="<?php echo base_url() ?>assets/img/quantum_ilife/add-subscriber.png" alt="After Key Generated" title="Add Subscriber" />
                            </a>
                            <a href="<?php echo LIST_CONTACTS; ?>"><img src="<?php echo base_url() ?>assets/img/quantum_ilife/list.png" alt="DB to MailChimp" title="List Contacts" /></a>
                            <a href="<?php echo MOVE_SUBSCRIBER; ?>"><img src="<?php echo base_url() ?>assets/img/quantum_ilife/move.png" alt="Mailchimp to DB" title="Move Subscriber" /></a>-->
                            
                            <a class="btn default ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/quantum_ilife/user_add'; ?>"><i class="fa fa-pencil"></i><span>Create New Key</span> </a>
                        <?php } ?>
                    </div>
                </div>
            </div> 

            <div class="portlet-body set-table" >
                <?php if ($this->router->class != 'reports') { ?>
                    <div class="cat-nav" >
                        <div id="search1" style="display:inline-block;">
                            <form id="search" method="get">
                                <input type="text" name="q" placeholder="Search..." value="<?php echo isset($q) ? $q : "" ?>"/>
                                <input type="hidden" name="type"  value="<?php echo isset($type) ? $type : "all" ?>"/>
                                <input type="hidden" name="page"  value="0"/>
                                <input id='searchBtn' type="submit" value="Search"/>
                            </form>

                        </div>
        <!--                <i class="c-list ico"></i>-->
                        <div id="viewAll1" class="c-list <?php echo isset($type) ? ($type == 'all' ? "active_submenu" : "") : "active_submenu"; ?>">
                            <a href="javascript:void(0)" class="js-tabs user_type btn-blue" data-url="all" class="btn-blue"  >View All Users</a>
                            <i class="fa fa-angle-downcds c-list-ico" style="display:none"></i>
                        </div>
                        <div id="activated" class="c-list <?php echo isset($type) && $type == 'activated' ? "active_submenu" : ""; ?>">
                            <a href="javascript:void(0)" class="js-tabs user_type btn-blue"  data-url="activated" >View Activated Users</a>
                        </div>
                        <div id="paid" class="c-list <?php echo isset($type) && $type == 'paid' ? "active_submenu" : ""; ?>">
                            <a  href="javascript:void(0)" class="js-tabs user_type btn-blue" data-url="paid"  >View Paid Users</a>
                        </div>
                        <div id="free" class="c-list <?php echo isset($type) && $type == 'free' ? "active_submenu" : ""; ?>">
                            <a href="javascript:void(0)" class="js-tabs user_type btn-blue"  data-url="free">View Free Users</a>
                        </div>
                        <div id="Android" class="c-list <?php echo isset($type) && $type == 'Android' ? "active_submenu" : ""; ?>">
                            <a href="javascript:void(0)" class="js-tabs user_type btn-blue" data-url="Android">Android Users</a>
                        </div>
                        <div id="Iphone" class="c-list <?php echo isset($type) && $type == 'Iphone' ? "active_submenu" : ""; ?>">
                            <a href="javascript:void(0)" class="js-tabs user_type btn-blue" data-url="Iphone" >Iphone Users</a>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($results && !empty($results)) { ?>
                    <div class="set-table-div secnd-tab">
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th class="id" name="id" align="center" style="text-align:center"><a style="text-decoration:none; text-align:center" href="javascript:void(0)">#</a></th>
                                    <th class="fname"><a style="text-decoration:none" href="javascript:void(0)">Name</a></th>
                                    <th id="data-open" class=""><a style="text-decoration:none" href="javascript:void(0)">Email</a></th>
                                    <th class="data-close"><a style="text-decoration:none" href="javascript:void(0)">Phone#</a></th>
                                    <th class="data-close"><a style="text-decoration:none" href="javascript:void(0)">Country</a></th>
                                    <th class="td_Key"><a style="text-decoration:none" href="javascript:void(0)">Key</a></th>
                                    <th><a style="text-decoration:none" href="javascript:void(0)">Device UDID</a></th>
                                    <th class="td_Key"><a style="text-decoration:none" href="javascript:void(0)">Videos Activation Key</a></th>
                                    <th class="data-close"><a style="text-decoration:none" href="javascript:void(0)">Date Created</a></th>
                                    <th class=""><a style="text-decoration:none" href="javascript:void(0)">Distributors</a></th>
                                    <th class="demail"><a style="text-decoration:none" href="javascript:void(0)">Send Email To Distributor</a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($results && !empty($results)) {
                                    $counter = 1;
                                    foreach ($results as $row) {
                                        $encrypt_emp_id = $this->encrypt->encode($row['id']);
                                        ?>
                                        <?php $classname = $row['isActivated'] == "1" ? "activated" : ""; ?>
                                        <tr id="tr^^<?php echo $encrypt_emp_id; ?>" class=" <?php echo $classname; ?>">
                                            <td class="td_id id" data="<?php echo $row['id'] ?>" align="center"><?php echo $counter++; ?></td>
                                            <td class="clickable fname ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/quantum_ilife/user_detail/' . $row['id']; ?>"><?php
                                                echo $row["txnid"] . " " . $row["last_name"];
                                                ;
                                                ?></td>
                                            <td class="clickable email ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/quantum_ilife/user_detail/' . $row['id']; ?>"><?php echo $row["email"]; ?></td>
                                            <td class="data-close phn"><?php echo $row["phone_no"]; ?></td>
                                            <td class="data-close country"><?php echo $row["country"]; ?></td>
                                            <td class="td_Key key"><?php if (($row["Key"] == '' || $row['Key'] == NULL)) { ?> 
                                                <button id="<?php echo $row["id"] ?>"  class="create_key ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/quantum_ilife/add_activation_key/' . $row['id'] ?>" role="button" aria-disabled="false" title="Create new key"><i class="fa fa-key"></i></button> 
                                                    <?php } else {
                                                echo "<span>" . $row['Key'] . "</span>"; ?> 
                                        <br/><button id="<?php echo 'send_activation_key_' . $row["id"] ?>"  class="sendEmail" role="button"  onclick="send_activation_key('<?php echo $row["id"]; ?>','Admin/quantum_ilife/send_activation_key')" title="Re-Send Activation Key"><i class="fa fa-envelope"></i></button>
                                        <?php    } ?></td>

                                            <td class="td_uuid did"><?php echo $row["UUID"]; ?></td>
                                            <td class="td_Key key"><?php if (($row["video_activation_key"] == '' || $row['video_activation_key'] == NULL)) { ?> <button id="<?php echo $row["id"] ?>"  class="create_key ajax-demo" data-toggle="modal" data-url="<?php echo base_url() . 'Admin/quantum_ilife/add_3d_activation_key/' . $row['id'] ?>" role="button" aria-disabled="false" title="Create new key"><i class="fa fa-key"></i><?php } else echo $row['video_activation_key']; ?></td>
                                            <td class="data-close cdate"><?php echo $row['createdtime'] == "0000-00-00 00:00:00" || empty($row['createdtime']) ? "" : date_format(date_create($row["createdtime"]), 'F d, Y'); ?></td>
                                            <td class="data-close dis">
                                                <?php
                                                if (!isset($distributors_data)) {
                                                    echo $row['distributor'];
                                                } else {
                                                    ?>
                                                    <select id="<?php echo 'distributor_' . $row['id'] ?>" name="selectDistributor" data-url="Admin/quantum_ilife/set_distributor">
                                                        <option value="" disabled="disabled" selected="selected">Select Distributor</option>
                                                        <?php foreach ($distributors_data as $distributor) : ?>
                                                            <?php $select = $row['distributor_id'] == $distributor['id'] ? 'selected="selected"' : '' ?>
                                                            <option value="<?php echo $distributor['id']; ?>" <?php echo $select; ?>><?php echo $distributor['distributor'] ?></option>
                                                    <?php endforeach; ?>
                                                    </select>  
                                                    <?php
                                                }
                                                ?>
                                            </td>
            <?php if (isset($distributors_data)) { ?>
                                                <td class="data-close demail">
                                                    <button id="<?php echo $row["id"] ?>"  class="sendEmail" role="button" aria-disabled="false" data-url='Admin/quantum_ilife/send_distributor_email' onclick="sendEmail(this)" title="Send E-mail"><i class="fa fa-envelope"></i></button>
                                                </td>
                                            <?php }
                                            ?>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
<?php } else Echo "<h1>No Results Found<h1>"; ?>
                <div class="row" style="cursor: pointer">
                    <div class="col-md-7 col-sm-12">
<?php echo isset($links) ? $links : ""; ?>
                    </div>
                </div>
            </div>
        </div></div>

<script type="text/javascript">
    $('.user_type').click(function() {
        $('[name="type"]').val($(this).data('url'));
        $('form#search').submit();
    });
</script>
