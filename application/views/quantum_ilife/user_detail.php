<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style type="text/css">
    .hide_tag{ display:none;}
    .show_tag{display: block;}
    
</style>
<?php $encrypt_user_id = $this->encrypt->encode($user_data['id']);?>
<div class="my-modal" id="div1^^<?php echo $encrypt_user_id; ?>">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">User Details</h4>
    </div>
    <?php  echo form_open('Admin/quantum_ilife/edit_user', array('method' => 'post', "id" => "UserForm", "class" => "validate")); ?>
    <div id="div^^<?php echo $encrypt_user_id; ?>">
    <div class="modal-body">
        <div class="row">
             
            <div class="col-md-12 quant-descp-popup" >
            <form>
                <p class="form-group">
                    <label for="txnid">First Name</label>
                    <label class="show_tag"><?php echo $user_data['txnid']?></label>
                    <input type="text" name="txnid" id="txnid" value="<?php echo  $user_data['txnid']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Last Name</label>
                    <label class="show_tag"><?php echo $user_data['last_name'] ?></label>
                    <input type="text" name="last_name" id="last_name" value="<?php echo  $user_data['last_name']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p class="form-group">
                    <label>Email <span class="mandatory hide_tag">*</span></label>
                    <label class="show_tag"><?php echo $user_data['email'] ?></label>
                    
                    <input type="text" name="email" id="email" value="<?php echo  $user_data['email']?>" class="hide_tag col-md-12 form-control email"/>
                </p>
                <p>
                    <label>Phone #</label>
                    <label class="show_tag"><?php echo $user_data['phone_no'] ?></label>
                    <input type="text" name="phone_no" id="phone_no" value="<?php echo  $user_data['phone_no']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Address</label>
                    <label class="show_tag"><?php echo $user_data['address'] ?></label>
                    <input type="text" name="address" id="address" value="<?php echo  $user_data['address']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>City</label>
                    <label class="show_tag"><?php echo $user_data['city'] ?></label>
                    <input type="text" name="city" id="city" value="<?php echo  $user_data['city']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>State</label>
                    <label class="show_tag"><?php echo $user_data['state'] ?></label>
                    <input type="text" name="state" id="state" value="<?php echo  $user_data['state']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Country</label>
                    <label class="show_tag"><?php echo $user_data['country'] ?></label>
                    <input type="text" name="country" id="country" value="<?php echo  $user_data['country']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Zip Code</label>
                    <label class="show_tag"><?php echo $user_data['zip_code'] ?></label>
                    <input type="text" name="zip_code" id="zip_code" value="<?php echo  $user_data['zip_code']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Payment-Date</label>
                    <?php $paymentDate= $user_data['payment_date'] == "0000-00-00 00:00:00" || empty($user_data['payment_date']) ? "" : date_format(date_create($user_data["payment_date"]), 'F d, Y'); ?>
                    <label class="show_tag"><?php echo format_date($user_data["payment_date"],'F d, Y' ) ?></label>
                    <input type="text" name="payment_date" id="payment_date" value="<?php echo format_date($user_data["payment_date"]);?>" class="date-picker hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Gross Income</label>
                    <label class="show_tag"><?php echo $user_data['payment_amount'] ?></label>
                    <input type="text" name="payment_amount" id="payment_amount" value="<?php echo  $user_data['payment_amount']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Number of Licences</label>
                    <label class="show_tag"><?php echo $user_data['licence_purchase_count'] ?></label>
                    <input type="text" name="licence_purchase_count" id="licence_purchase_count" value="<?php echo  $user_data['licence_purchase_count']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p class="form-group">
                    <label>Key</label>
                    <label class="show_tag"><?php echo $user_data['Key'] ?></label>
                    <input type="text" name="Key" id="Key" value="<?php echo  $user_data['Key']?>" readonly="readonly" class="hide_tag col-md-12 form-control "/>
                </p>
                <p>
                    <label>Device Id</label>
                    <label class="show_tag"><?php echo $user_data['UUID'] ?></label>
                    <input type="text" name="UUID" id="UUID" value="<?php echo  $user_data['UUID']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p class="form-group">
                    <label>3D Video Activation Key</label>
                    <label class="show_tag"><?php echo $user_data['video_activation_key'] ?></label>
                    <input type="text" name="video_activation_key" id="video_activation_key" readonly="readonly" value="<?php echo  $user_data['video_activation_key']?>" class="hide_tag col-md-12 form-control "/>
                </p>
                <p>
                    <label>Date Activated (3D Video)</label>
                    <label class="show_tag"><?php echo format_date($user_data["videoActivation_createdDate"],'F d, Y' ) ?></label>
                    <input type="text" name="videoActivation_createdDate" id="videoActivation_createdDate" value="<?php echo  format_date($user_data["videoActivation_createdDate"])?>" class="date-picker hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Item Tracking Number - 1</label>
                    <label class="show_tag"><?php echo $user_data['item_tracking_number'] ?></label>
                    <input type="text" name="item_tracking_number" id="item_tracking_number" value="<?php echo  $user_data['item_tracking_number']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Item Tracking Number - 2</label>
                    <label class="show_tag"><?php echo $user_data['item_tracking_number2'] ?></label>
                    <input type="text" name="item_tracking_number2" id="item_tracking_number2" value="<?php echo  $user_data['item_tracking_number2']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Distributors</label>
                    
                    <?php
                        $distributor_name="";
                        foreach ($distributors_data as $distributor){
                        if(in_array($user_data['distributor_id'], $distributor))
                                $distributor_name= $distributor['distributor'];
                    }?>
                    <label class="show_tag"><?php echo $distributor_name ?></label>
                    <select id="distributor" name="distributor_id" class="col-md-12 form-control hide_tag">
                        <?php $defaultSelected = $user_data['distributor_id'] == "0" || is_null($user_data['distributor_id']) || empty($user_data['distributor_id']) ? 'selected="selected"' : ""; ?>
                        <option value="" disabled="disabled" <?php echo $defaultSelected ?> >Select Distributor</option>
                        <?php foreach ($distributors_data as $distributor) : ?>
                            <?php $select = $user_data['distributor_id'] == $distributor['id'] ? 'selected="selected"' : '' ?>
                            <option value="<?php echo $distributor['id']; ?>" <?php echo $select; ?>><?php echo $distributor['distributor'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
                <p>
                    <label>Distributors Commission</label>
                    <label class="show_tag"><?php echo $user_data['distributor_commissions'] ?></label>
                    <input type="text" name="distributor_commissions" id="distributor_commissions" value="<?php echo  $user_data['distributor_commissions']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Affiliates</label>
                    <label class="show_tag"><?php echo $user_data['affiliates'] ?></label>
                    <input type="text" name="affiliates" id="affiliates" value="<?php echo  $user_data['affiliates']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Affiliates Commission</label>
                    <label class="show_tag"><?php echo $user_data['affiliate_commissions'] ?></label>
                    <input type="text" name="affiliate_commissions" id="affiliate_commissions" value="<?php echo  $user_data['affiliate_commissions']?>" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Date Activated</label>
                    <label class="show_tag"><?php echo format_date($user_data["ActivatedDate"],'F d, Y' )?></label>
                    <input type="text" name="ActivatedDate" id="ActivatedDate" value="<?php echo  format_date($user_data["ActivatedDate"] )?>" class="date-picker hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Date Created</label>
                    <label class="show_tag"><?php echo format_date($user_data["createdtime"],'F d, Y' )  ?></label>
                    <input type="text" name="createdtime" id="createdtime" value="<?php echo format_date($user_data["createdtime"] );   ?>" class="date-picker hide_tag col-md-12 form-control"/>
                </p>
                <?php
                    $deviceType = "";
                    $deviceType = $user_data['deviceType'] == "A" ? "Android" : $deviceType;
                    $deviceType = $user_data['deviceType'] == "I" ? "IPhone" : $deviceType;
                ?>
                <p>
                    <label>Device Type</label>
                    <label class="show_tag"><?php echo $deviceType ?></label>
                    <input type="text" name="deviceType" id="deviceType" value="<?php echo  $deviceType?>" readonly="readonly" class="hide_tag col-md-12 form-control"/>
                </p>
                <p>
                    <label>Marketing</label>
                    <label class="show_tag"><?php echo $user_data['inserted_from'] ?></label>
                    <input type="text" name="inserted_from" id="inserted_from" value="<?php echo  $user_data['inserted_from']?>" readonly="readonly" class="hide_tag col-md-12 form-control"/>
                </p>
                <?php
                echo form_input(array('type' => 'hidden', 'value' => $user_data['id'], 'name' => 'id', 'id' => 'id', 'autocomplete' => 'off', 'class' => 'col-md-12 form-control '));
                ?>
			</form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="edit btn blue" data-url="Admin/quantum_ilife/edit_user/" onclick="edit_user(this)">EDIT</a>
        <?php //echo form_input(array('class' => 'hide_tag btn blue', 'value' => 'Update', 'type' => 'submit')); ?>
        <input type="submit" value="Update" class="btn blue update_btn" style="display: none;"/>
        <a alt="Delete" data_url="Admin/quantum_ilife/delete_user"  title="Delete" class="delete btn blue" onclick="delete_user(this);" href="javascript:void(0);">DELETE</a> 
        <?php
        echo  form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        
        ?>
        
    </div>
    
    </div>
    <?php echo form_close(); ?>
    <script>
        FormValidation.init();
    </script>
    <script type='text/javascript'>
        $("#draggable").draggable({
            handle: ".modal-header"
        });
    </script>
    <script type="text/javascript">
    function edit_user(obj){
        $('.show_tag').remove();
        $('.hide_tag').show();
        $('.update_btn').css("display","inline-block");
        $('.modal-title').html("Edit User");
//        $(obj).closest('form').attr('action',site_url+$(obj).attr('data-url'));
        $(obj).remove();
    }
    </script>
</div>