<div class="my-modal">
    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    echo validation_errors('<span class="error">', '</span>');
    ?>
    <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Add New User</h4>
    </div>
    <?php echo form_open('Admin/quantum_ilife/save_new_user', array('method' => 'post', "id" => "UserForm", "class" => "validate")); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <p class="form-group">
                    <?php echo form_label('First Name', 'txnid'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'txnid', 'id' => 'txnid', 'value' => set_value('txnid'), 'class' => 'col-md-12 form-control')); ?>		
                </p>
                <p>
                    <?php echo form_label('Last Name', 'last_name'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'last_name', 'id' => 'last_name', 'value' => set_value('last_name'), 'class' => 'col-md-12 form-control')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Email', 'email'); ?>
                    <span class="mandatory">*</span>
                    <?php echo form_input(array('type' => 'text', 'name' => 'email', 'id' => 'email', 'autocomplete' => 'off', 'value' => set_value('email'), 'class' => 'col-md-12 form-control email', "onblur" => "check_email('Admin/quantum_ilife',this)")); ?>
                </p>
                <p>
                    <?php echo form_label('Phone #', 'phone_no'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'phone_no', 'id' => 'phone_no', 'autocomplete' => 'off', 'value' => set_value('phone_no'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p>
                    <?php echo form_label('Address', 'address'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'address', 'id' => 'address', 'value' => set_value('address'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('City', 'city'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'city', 'id' => 'city', 'value' => set_value('city'), 'class' => 'col-md-12 form-control')); ?>
                </p>
                <p>
                    <?php echo form_label('State', 'state'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'state', 'id' => 'state', 'value' => set_value('state'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p>
                    <?php echo form_label('Country', 'country'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'country', 'id' => 'country', 'value' => set_value('country'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p>
                    <?php echo form_label('Zip Code', 'zip_code'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'zip_code', 'id' => 'zip_code', 'value' => set_value('zip_codes'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p>
                    <?php echo form_label('Payment-Date', 'payment_date'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_date', 'id' => 'payment_date', 'value' => set_value('payment_date'), 'class' => 'col-md-12 form-control date-picker')); ?>
                </p>
                <p>
                    <?php echo form_label('Number of Licences', 'licence_purchase_count'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'licence_purchase_count', 'id' => 'licence_purchase_count', 'value' => set_value('licence_purchase_count'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p>
                    <?php echo form_label('Gross Income', 'payment_amount'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'payment_amount', 'id' => 'payment_amount', 'value' => set_value('payment_amount'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
<!--                <p>
                    <?php // echo form_label('Item Id', 'item_tracking_number'); ?>
                    <?php // echo form_input(array('type' => 'text', 'name' => 'item_tracking_number', 'id' => 'item_tracking_number', 'value' => set_value('item_tracking_number'), 'class' => 'col-md-12 form-control ')); ?>
                </p>-->
                <p>
                    <?php echo form_label('Item Tracking Number - 1', 'item_tracking_number2'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'item_tracking_number', 'id' => 'item_tracking_number', 'value' => set_value('item_tracking_number'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p>
                    <?php echo form_label('Item Tracking Number - 2', 'itemid'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'item_tracking_number2', 'id' => 'item_tracking_number2', 'value' => set_value('item_tracking_number2'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p class="form-group">
                    <?php echo form_label('Key', 'Key'); ?>
                    <span class="mandatory">*</span>
                    <?php $val = isset($Key) ? $Key : set_value('Key') ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'Key', 'readonly' => 'readonly', 'id' => 'Key', 'value' => $val, 'class' => 'col-md-12 form-control required')); ?>
                </p>
                <p>
                    <?php echo form_label('DeviceUDID', 'UUID'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'UUID', 'id' => 'UUID', 'value' => set_value('UUID'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
                <p>
                    <?php echo form_label('Distributors', 'distributor'); ?>
                    <select id="distributor" name="distributor_id" class="col-md-12 form-control"> 
                        <option value="0" disabled="disabled" selected="selected">Select Distributor</option>
                        <?php foreach ($distributors_data as $distributor) : ?>
                            <option value="<?php echo $distributor['id']; ?>"><?php echo $distributor['distributor'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </p>
                <p>
                    <?php echo form_label('Affiliates', 'affiliates'); ?>
                    <?php echo form_input(array('type' => 'text', 'name' => 'affiliates', 'id' => 'affiliates', 'value' => set_value('affiliates'), 'class' => 'col-md-12 form-control ')); ?>
                </p>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php echo form_input(array('class' => 'btn  blue', 'value' => 'Add', 'type' => 'submit'));
        echo form_input(array('class' => 'btn default', 'data-dismiss' => 'modal', 'type' => 'button', 'value' => 'Close'));
        ?>
    </div>
<?php echo form_close(); ?>
    <script>
        FormValidation.init();
    </script>
</div>