<img src="<?php echo base_url('assets/img/email_banner.jpg') ?>" style="width:100%"/>
<br/>
<div>
    <p style="font-size:1.5em;">
        Dear  <?php echo isset($distributor['distributor']) && strlen($distributor['distributor']) ? $distributor['distributor'] : 'Distributor' ?>,
    </p>
    Below is the lead details of someone who has downloaded Energy Re-Mastered App. As soon as you contact them, ensure they are not currently working with anyone. Secondly please be sure to enter them as a lead under your affiliate program as long as they are not working with anyone currently.
</div>
<div>
    <br>
</div>
<div>
    <table style="width: 100%;border: 1px solid #999;text-align: left;border-collapse: collapse;margin: 0 0 1em 0;" cellspacing="0" cellpadding="0" >
        <tr style="background: #eee;">
            <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;" colspan="2">
                User Detail
            </th>
        </tr>
        <tr>
            <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                Email
            </th>
            <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                <?php echo $user_detail['email']; ?>
            </th>
        </tr>
        <?php if (strlen($user_detail['txnid']) || strlen($user_detail['last_name'])) { ?>
            <tr>
                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding: 0.4em;">
                    Name
                </th>
                <th style="width: 25%;text-align: left;vertical-align: top;border: 1px solid #000;border-collapse: collapse;padding:0.4em;">
                    <?php echo trim($user_detail['txnid'] . '' . $user_detail['last_name']); ?>
                </th>
            </tr>
        <?php } ?>
    </table>
</div>
<div>Good luck and happy sales!</div>
<div>Energy Re-Mastered Apps</div>
