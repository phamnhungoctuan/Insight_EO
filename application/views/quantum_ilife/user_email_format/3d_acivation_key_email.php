<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="min-width: 300px;">
    <tr>
        <td align="center" bgcolor="#ffffff">
            <table width="90%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center">
                        <div style="line-height: 44px;">
                            <font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
                            <span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
                               Welcome to The Energy Re-Mastered
                            </span></font>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="line-height: 25px; font-family: Arial, Helvetica, sans-serif; font-size: 15px;">
                            <p>Hi <?php echo isset($user_detail['txnid']) ? $user_detail['txnid'] : 'User'; ?>,</p>
                            <p>Congratulations, Your request for 3D-Animated Video Activation Code has been accepted</p>
                            <?php echo isset($user_detail['video_activation_key']) ? '<p><b>Your Video Activation key is :  '. $user_detail['video_activation_key'].'</b></p>': '' ?>
                            <p>
                                Please login to The Energy Re-Mastered App and go to the settings icon on the Main Davinci Page (near top right hand corner near the Balance Rx Page)
                                <br/><br/>
                                Select the Request for 3D animation and follow the prompts and enter in the respective key to start the download process
                            </p>
                            <p>Please enter this into Energy Re-Mastered Software to gain instant access. </p>
                            <p>Thank you,<br/>Energy Re-Mastered  Support</p>
                        </div>
                    </td>
                </tr>
            </table>		
        </td>
    </tr>
</table>
