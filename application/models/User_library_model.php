<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_library_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'user_libraries';
        $this->validate = array(
            array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'dob',
                'label' => 'Date Of Birth',
                'rules' => 'trim',
            )
        );
    }

    /**
     * @Name : add_library()
     * @Purpose : add the library in the libraries table 
     * @Call from : Users.php API file
     * @Functionality : add the record in libraries table and user_library Table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    function add_library($data) {
        $insert_data['date_created'] = date('Y-m-d H:i:s');
        $insert_data['name'] = $data['name'] ? $data['name'] : "";
        $insert_data['lib_inserted_from'] = isset($data['lib_inserted_from'])? $data['lib_inserted_from'] : "app";
        $insert_data['description'] = $data['description'] ? $data['description'] : "";
        if ($this->db->insert('libraries', $insert_data))
            $library_id = $this->db->insert_id();
        else
            return false;
        /* ADD THE RECORD IN USER_LIBRARY TABLE */
        $user_library_data['library_id'] = $library_id;
        $user_library_data['user_id'] = $data['user_id'];
        $user_library_data['record_id'] = isset($data['record_id']) ? $data['record_id'] : "";
        $user_library_data['uuid'] = isset($data['uuid']) ? $data['uuid'] : "";
        if(isset($data['is_shared']) && $data['is_shared'] == '1'){
            $user_library_data['is_shared'] = "1";
            $user_library_data['is_approved'] = "1";
            $user_library_data['is_imported'] = "1";
        }
        $user_library_data['date_created'] = date('Y-m-d H:i:s');
        $this->save($user_library_data);

        return $library_id;
    }

    public function update_library($data, $library_id) {
        $data['date_updated'] = date('Y-m-d H:i:s');
        if ($this->db->update("libraries", $data, array('id' => $library_id))) {
            return TRUE;
        } else {
            return false;
        }
    }

    /**
     * @Name : get_library()
     * @Purpose : return the single row matching the given criteria
     * @Call from : Users.php API file
     * @Functionality : fetch the record from the users_record table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    function get_library($where = array()) {
        $this->db->select('*');
        $this->db->from('libraries l');
        $this->db->join('user_libraries ul', 'ul.library_id = l.id', 'INNER');
        $this->db->where('l.name = "' . $where['library_name'] . '"');
        unset($where['library_name']);
        $this->db->where('ul.user_id = ' . $where['user_id']);
        unset($where['user_id']);
        $this->db->where('ul.is_shared = "0"');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->row_array();
        } else {
            return FALSE;
        }
    }

    function get_library_custom($where = array()) {
        $this->db->select('*');
        $this->db->from('user_libraries');
        $this->db->where($where);
        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->row_array();
        } else {
            return FALSE;
        }
    }
    function  get_libraries_custom($where = array()){
        $this->db->select('*');
        $this->db->from('user_libraries');
        $this->db->where($where);
        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }

    /**
     * @Name : get_library_detail()
     * @Purpose : Returns the list of libraries saved for the Logged in user along with the detail of record for which it is associated
     * @Call from : can be called from any controller (Detail of a Library  API )
     * @Functionality : fetch the record from the user_record_libraries and user_records table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    function get_library_detail($where = array()) {
        $this->db->select('l.*, ul.*');
        $this->db->from('user_libraries ul');
        //$this->db->join('user_records ur', 'ur.id = ul.record_id', 'INNER');
        $this->db->join('libraries l', 'ul.library_id = l.id', 'INNER');
        if (isset($where['user_id'])) {
            $this->db->where('ul.user_id', $where['user_id']);
        }
        if (isset($where['library_id'])) {
            $this->db->where('ul.library_id', $where['library_id']);
        }
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    /**
     * @Name : get_user()
     * @Purpose : return the array matching the given criteria
     * @Call from : Users.php API file
     * @Functionality : fetch the record from the users_record table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    function get_libraries($where = array()) {
        $this->db->select('*');
        $this->db->from('libraries l');
        $this->db->join('user_libraries ul', 'ul.library_id = l.id');
        if (isset($where['user_id'])) {
            $this->db->where('ul.user_id', $where['user_id']);
            unset($where['user_id']);
        }
        $this->db->where($where);
        $this->db->order_by('l.name', 'ASC');
        $getdata = $this->db->get();
//       echo  $this->db->last_query();
//       exit;
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return false;
        }
    }

    function save_items($data = array()) {
        if ($this->db->insert('library_items', $data))
            return $this->db->insert_id();
        else
            return false;
    }

    function count_items($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db->where('library_id', $where);
        }
        $getData = $this->db->count_all_results('library_items');
        return $getData;
    }

    public function share_library($user_id, $data) {
        $this->db->select('*');
        $this->db->from('user_libraries');
        $this->db->join('libraries', 'libraries.id = user_libraries.library_id');
        $this->db->where('user_id', $user_id);
        $this->db->where('library_id', $data['library_id']);
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            $data['date_created'] = date('Y-m-d H:i:s');
            $data['library_id'] = $getdata->row()->library_id;
            if (!isset($data['is_shared'])) {
                $data['is_shared'] = '1';
            }
            if (!isset($data['is_imported'])) {
                $data['is_imported'] = '0';
            }
            $this->db->insert('user_libraries', $data);
            return array('id' => $this->db->insert_id(), 'name' => $getdata->row()->name);
        } else {
            return false;
        }
    }

    public function check_library_shared($user_id, $library_id) {
        $this->db->select('*');
        $this->db->from('user_libraries');
        $this->db->join('libraries', 'libraries.id = user_libraries.library_id', 'inner');
        $this->db->where('user_id', $user_id);
        $this->db->where('library_id', $library_id);
        $this->db->where('is_shared', '1');
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return true;
        } else {
            return false;
        }
    }

    function get_library_by_id($where = array()) {
        $this->db->select('*');
        $this->db->from('user_libraries');
        $this->db->where('user_id', $where['user_id']);
        $this->db->where('library_id', $where['library_id']);
        $this->db->where('is_shared', '0');
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return true;
        } else {
            return false;
        }
    }

    function update_user_libraries($data = array(), $id) {
        $data['date_updated'] = date('Y-m-d H:i:s');
        if ($this->db->update("user_libraries", $data, array('id' => $id))) {
            return TRUE;
        } else {
            return false;
        }
    }

    function insert_user_libraries($data = array()) {
        $data['date_created'] = date('Y-m-d H:i:s');
        if ($this->db->insert('user_libraries', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function get_import_request($library_ids) {
        $this->db->select('*');
        $this->db->from('user_libraries');
        $this->db->where_in('library_id', $library_ids);
        $this->db->where('is_shared', '1');
        $this->db->where('is_approved', '0');
        $this->db->order_by("id", "desc");
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return false;
        }
    }

    function get_user_name_and_library_name($where = array()) {

        $this->db->select('l.name as library_name,concat(u.first_name ," ", u.last_name) as  user_name');
        if (isset($where['id'])) {
            $this->db->select($where['id'] . ' as id');
        }
        $this->db->from('libraries l');
        $this->db->join('users u', 'u.id =' . $where['user_id']);
        $this->db->where('l.id', $where['library_id']);
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->row_array();
        } else {
            return false;
        }
    }

    function insert_notification($data = array()) {

        if ($this->db->insert('notifications', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function get_notifiation($where = array()) {
        $this->db->select('*');
        $this->db->from('notifications');
        $this->db->where($where);
        $this->db->order_by("id", "desc");
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return false;
        }
    }

    function get_record_library($where = array()) {
        $this->db->select('*');
        $this->db->from('record_library');
        $this->db->where($where);
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return false;
        }
    }

    function associate_record_with_library($data = array()) {
        if ($this->db->insert('record_library', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    function get_all_library_data($user_id = NULL) {
        $this->db->select('l.id, l.name as library_name , l.description as library_description , l.image ,li.name as item_name , li.frequency');
        $this->db->from('record_library rl');
        $this->db->join('libraries l', ' l.id = rl.library_id', 'INNER');
        $this->db->join('library_items li', 'li.library_id = l.id', 'LEFT');
        $this->db->where('rl.record_id', $user_id);
        $this->db->where('rl.is_group', "0");
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return false;
        }
    }

    function get_libraries_count_for_record($record_id = null) {
        $this->db->select('count(library_id) as libraries_count');
        $this->db->from('record_library');
        $this->db->where('record_id', $record_id);
        $this->db->where('is_group', "0");
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->row_array();
        } else {
            return false;
        }
    }
    
    function delete_record_library($record_id, $library_id = array()){
        $this->db->where('record_id',$record_id);
        $this->db->where('is_group', "0");
        if(is_array($library_id) && count($library_id)){
            $this->db->where_in('library_id',$library_id);
        }
        return $this->db->delete('record_library');
    }
    
    function get_record_libraries($record_id){
        $this->db->select('l.id, l.name as library_name , l.description as library_description , l.image');
        $this->db->from('record_library rl');
        $this->db->join('libraries l', ' l.id = rl.library_id', 'INNER');
        $this->db->where('rl.record_id', $record_id);
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return FALSE;
        }
    }

    public function get_dropbox_shared_libraries($user_id){
        $query = $this->db->query('SELECT ul.library_id FROM user_libraries ul JOIN libraries l ON ul.library_id = l.id AND l.lib_inserted_from = "dropbox" AND ul.is_shared="1" AND ul.user_id = '.$user_id.' LEFT JOIN user_libraries ul1 ON ul.library_id = ul1.library_id AND ul.is_shared = "1" AND ul1.is_shared ="0" WHERE ul1.user_id is NULL');
        if($query->num_rows()){
            return  $query->result_array();
        }
        return false;
    }
    
    public function delete_library_files($libraries_id = array()) {
        if (count($libraries_id)) {
            /* GET THE IMAGES AND AUDIO FILES OF THE RECORD IDS */
            $query = $this->db->query('SELECT image FROM libraries WHERE id IN (' . implode(',', $libraries_id) . ') AND NOT(NULLIF(image,"") IS NULL)');
            if ($query->num_rows()) {
                require_once (APPPATH . 'libraries/Custom_file.php');
                $fileObj = new Custom_file();
                $uploaddir = FCPATH . 'assets/uploads/libraries/';
                $result = $query->result_array();
                foreach ($result as $row) {
                    if(!empty($row['image'])){
                        $fileObj->unlink_files($uploaddir . $row['image']);
                    }
                }
            }
        }
        return TRUE;
    }
}
