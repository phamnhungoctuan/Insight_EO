<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Apps_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'genius_apps';
        $this->primary_key = 'app_id';
    }

    public function get_app_detail($app_id) {
        $this->db->where("app_id",$app_id);
        $query = $this->db->get($this->table);
        if($query->num_rows()){
            return $query->row_array();
        }
        return FALSE;
    }
}
?>

