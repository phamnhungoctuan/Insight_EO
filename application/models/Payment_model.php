<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'payment';
        $this->validate = array(
            array(
                'field' => 'subscription',
                'label' => 'Subscriptions',
                'rules' => 'trim|required|in_list[monthly,annually,lifetime]',
            ),
        );
    }

    function getPaymentDetail($where = array()) {
        $getdata = $this->db->get_where($this->table, $where);
        return $getdata->num_rows() ? $getdata->row_array() : array();
    }

    function savePayment($data = array()) {
        $data['payment_mode'] = 'adminPanel';
        if ($this->db->insert($this->table, $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    function updateSubscription($data = array(), $where = array()) {
        $data['payment_mode'] = 'adminPanel';
        return $this->db->update($this->table, $data, $where);
    }

}
