<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Genius_quantacapsule_items_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'genius_quantacapsule_items';
        $this->validate = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'tag',
                'label' => 'Tag',
                'rules' => 'trim|required',
            ),
        );
        /* INITIALIZE TEH DATABASE CONNECTION */
        $this->db_q = $this->load->database('quantum', TRUE);
    }
    
    /**
     * @Name : count_results()
     * @Purpose : To count to number of rows in the database.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Simply count the results using predefined count_all_results() function.
     * @Receiver params : No parameters passed
     * @Return params : Returns the count of rows.
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     */
    function count_results($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db_q->where($where);
        }
        $this->db_q->from($this->table);
        return $this->db_q->count_all_results();
    }
    
    
    public function get_items($where = array()){
        if(count($where)){
            $this->db_q->where($where);
        }
        $query = $this->db_q->get($this->table);
        if($query->num_rows()){
            return $query->result_array();
        }
        return FALSE;
    }
    
    public function get_item_detail($where){
        if(count($where)){
            $this->db_q->where($where);
        }
        $query = $this->db_q->get($this->table);
        if($query->num_rows()){
            return $query->row_array();
        }
        return FALSE;
    }
    public function add_item($data){
        $data['exported_from'] = "genius_insight";
        $data['date_created'] = date('Y-m-d H:i:s');
        $data['import_code'] = $this->generateKey('import_code');
        if($this->db_q->insert($this->table, $data)){
            return $data['import_code'];
        }
        return FALSE;
    }
   

}
