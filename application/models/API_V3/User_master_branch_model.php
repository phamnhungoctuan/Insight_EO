<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_master_branch_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'user_master_branch';
        $this->validate = array(
            array(
                'field' => 'master_branch_name',
                'label' => 'Master Branch Name',
                'rules' => 'required|trim',
            )
        );
    }

    /*CLIENT RECORDS MASTER BRANCH*/
    function delete_record_master_branch($record_id, $master_id = array()) {
        $this->db->where('record_id', $record_id);
        $this->db->where('is_group', "1");
        if (is_array($master_id) && count($master_id)) {
            $this->db->where_in('library_id', $master_id);
        }
        return $this->db->delete('record_library');
    }
    /**
     * @Name : add_master_branch()
     * @Purpose : add a master branch in master_branch table
     * @Call from : master Branch API File
     * @Receiver params : array of columns and values
     * @Return params : return new inser_id if inserted successfully, false otherwise
     */
    function add_master_branch($data) {
        $insert_data['date_created'] = date('Y-m-d H:i:s');
        $insert_data['name'] = $data['name'] ? $data['name'] : "";
        $insert_data['uuid'] = $data['uuid'] ? $data['uuid'] : "";
        $insert_data['user_id'] = $data['user_id'] ? $data['user_id'] : "";
        if ($this->db->insert($this->table, $insert_data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function update_master_branch($data, $master_branch_id) {
        $mb_data['name'] = $data['name'];
        return $this->db->update($this->table, $mb_data, array('master_branch_id' => $master_branch_id));
    }

    public function delete_master_branch($master_branch_id, $only_libraries = FALSE) {
        $flag = $this->db->delete("master_branch_libraries", array('master_branch_id' => $master_branch_id));
        /* If Only libraries are true then no need to delete the master branch */
        if (!$only_libraries && $flag) {
            $this->db->delete("record_library", array("is_group"=>"1","record_id" => $master_branch_id));
            $flag = $this->db->delete($this->table, array("master_branch_id", $master_branch_id));
        }
        return $flag;
    }

    public function delete_master_branch_array($master_branch_ids = array()) {
        if (is_array($master_branch_ids) && count($master_branch_ids)) {
            $this->db->where_in('master_branch_id', $master_branch_ids);
            $this->db->delete('master_branch_libraries');
            
            /*DELETE MASTER BARNCH ASSOCIATED WITH CLIENT RECORDS */
            $this->db->where_in('record_id', $master_branch_ids);
            $this->db->where("is_group","1");
            $this->db->delete("record_library");
            
            /* DELETE the master branch records from user_master_branch table */
            $this->db->where_in('master_branch_id', $master_branch_ids);
            return $this->db->delete($this->table);
        }
    }

    function count_results($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db->where($where);
        }
        return $this->db->count_all_results($this->table);
    }

    /**
     * @Name : get_master_branch_row()
     * @Purpose : return the single row matching the given criteria
     * @Call from : Master_branch.php API file
     * @Functionality : fetch the record from the master_branch table
     * @Receiver params : array of columns and values for a single row
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    function get_master_branch_row($where = NULL) {
        $this->db->select('*');
        $this->db->from($this->table);
        if (!empty($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->row_array();
        }
        return FALSE;
    }

    /**
     * @Name : get_master_branch_array()
     * @Purpose : return the multiple rows matching the given criteria
     * @Call from : Master_branch.php API file
     * @Functionality : fetch the records from the master_branch table
     * @Receiver params : array of columns and values for a multiple row
     * @Return params : if match found then return the array of multiple records ,  false otherwise
     */
    function get_master_branch_array($where = NULL, $limit = NULL, $offset = NULL, $col_to_sort = 'name', $sort_order = 'ASC') {
        $this->db->select('*');
        $this->db->from($this->table);
        if (!empty($where)) {
            $this->db->where($where);
        }
        if (!is_null($limit) && !is_null($offset)) {
            $this->db->limit($limit, $offset);
        }
        if ($col_to_sort) {
            $this->db->order_by($col_to_sort, $sort_order);
        }
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        }
        return FALSE;
    }

    /**
     * @Name : get_master_branch_libraries_id()
     * @Purpose : return the libraries ID belonging to the given master branch from the master_branch_libraries table
     * @Call from : can be Called from any controller or model
     * @Functionality : fetch the records from the master_branch_libraries  table
     * @Receiver params : array of columns and values for a multiple row
     * @Return params : if match found then return the array of multiple records ,  false otherwise
     */
    function get_master_branch_libraries_id($master_branch_id) {
        $this->db->select('l.id');
        $this->db->from('master_branch_libraries mbl');
        $this->db->join('libraries l', ' l.id = mbl.library_id', 'INNER');
        $this->db->where('mbl.master_branch_id', $master_branch_id);
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return array();
        }
    }

    public function add_libraries_to_master_branch($data) {
        
    }

}
