<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Genius_Infinity_export_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'genius_infinity_export';
//        $this->validate = array(
//            array(
//                'field' => 'email',
//                'label' => 'Email',
//                'rules' => 'trim|required|valid_email',
//            ),
//            array(
//                'field' => 'tag',
//                'label' => 'Tag',
//                'rules' => 'trim|required',
//            ),
//        );
        /* INITIALIZE TEH DATABASE CONNECTION */
        $this->db_q = $this->load->database('quantum', TRUE);
    }

    /**
     * @Name : count_results()
     * @Purpose : To count to number of rows in the database.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Simply count the results using predefined count_all_results() function.
     * @Receiver params : No parameters passed
     * @Return params : Returns the count of rows.
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     */
    function count_results($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db_q->where($where);
        }
        $this->db_q->from($this->table);
        return $this->db_q->count_all_results();
    }

    function add_data($data) {
        $data['data_to_be_exported'] = json_encode($data['data_to_be_exported']);
        $data['date_created'] = date('Y-m-d H:i:s');
        $data['password'] = $this->generateKey('password');
        if ($this->db_q->insert($this->table, $data)) {
            return $data['password'];
        }
        return FALSE;
    }

}
