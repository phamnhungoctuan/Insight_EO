<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_Model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'payment';
    }

    function fetch($where = array()) {
        $getdata = $this->db->get_where($this->table, $where);
        return $getdata->num_rows() ? $getdata->row_array() : array();
    }

    function fetchRows($where = array()) {
        $getdata = $this->db->get_where($this->table, $where);
        return $getdata->num_rows() ? $getdata->result_array() : array();
    }

    function savePayment($data = array()) { //, $table = ''
        if (!empty($data)) {
            foreach ($data as $column => $value) {
                $columnNames[] = $column;
                $insertValues[] = "'" . $value . "'";
                $updateValues[] = $column . " = '" . $value . "'";
            }

            return $this->db->query("insert into $this->table(" . implode(', ', $columnNames) . ") values(" . implode(', ', $insertValues) . ") on duplicate key update " . implode(', ', $updateValues));
        } else {
            return false;
        }
    }

    function saveTempDetails($data = array()) {
        if ($this->db->insert('temp_payment_details', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    function getTempDetails($where = array()) {
        $getdata = $this->db->get_where('temp_payment_details', $where);
        return $getdata->num_rows() ? $getdata->row_array() : array();
    }

    function getCronData() {
        $this->db->where("((payment_type = 'monthly' OR payment_type = 'annually' AND last_verification_date = '" . date("Y-m-d", strtotime("-" . DAY_INTERVAL)) . "' AND no_of_attempts < " . MAX_ATTEMPTS . ") OR expiry_date = '" . date('Y-m-d') . "') AND payment_mode = 'app'", NULL, FALSE);
        $getdata = $this->db->get($this->table);
        return $getdata->num_rows() ? $getdata->result_array() : array();
    }

    function updateSubscription($data = array(), $where = array()) {
        return $this->db->update($this->table, $data, $where);
    }

}
