<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_library_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'user_libraries';
        $this->validate = array(
            array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'dob',
                'label' => 'Date Of Birth',
                'rules' => 'trim',
            )
        );
    }

    function get_count($user_id = NULL) {
        $this->db->where('(is_shared="0" OR (is_shared="1" AND is_imported="1" AND is_approved="1"))');
        $this->db->where('user_id', $user_id);
        return $this->db->count_all_results('user_libraries');
    }

    /**
     * @Name : add_library()
     * @Purpose : add the library in the libraries table 
     * @Call from : Users.php API file
     * @Functionality : add the record in libraries table and user_library Table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    function add_library($data) {
        $insert_data['date_created'] = date('Y-m-d H:i:s');
        $insert_data['name'] = $data['name'] ? $data['name'] : "";
        $insert_data['lib_inserted_from'] = isset($data['lib_inserted_from']) ? $data['lib_inserted_from'] : "app";
        $insert_data['description'] = $data['description'] ? $data['description'] : "";
        if ($this->db->insert('libraries', $insert_data))
            $library_id = $this->db->insert_id();
        else
            return false;
        /* ADD THE RECORD IN USER_LIBRARY TABLE */
        $user_library_data['library_id'] = $library_id;
        $user_library_data['user_id'] = $data['user_id'];
        $user_library_data['record_id'] = isset($data['record_id']) ? $data['record_id'] : "";
        $user_library_data['uuid'] = isset($data['uuid']) ? $data['uuid'] : "";
        if (isset($data['is_shared']) && $data['is_shared'] == '1') {
            $user_library_data['is_shared'] = "1";
            $user_library_data['is_approved'] = "1";
            $user_library_data['is_imported'] = "1";
        }
        $user_library_data['date_created'] = date('Y-m-d H:i:s');
        $this->save($user_library_data);

        return $library_id;
    }

    public function update_library($data, $library_id) {
        $data['date_updated'] = date('Y-m-d H:i:s');
        return $this->db->update("libraries", $data, array('id' => $library_id));
    }

    public function delete_library($library_id) {
        /* DELETE ENTRIES FROM record_library table */
        $this->db->delete('record_library', array('library_id' => $library_id,'is_group' =>'0'));
        /* DELETE ENTRIES FROM user_libraries table */
        $this->db->delete('user_libraries', array('library_id' => $library_id));
        /* DELETE MASTER BRANCH LIBRARIES */
        $this->db->delete('master_branch_libraries', array('library_id' => $library_id));
        /* DELETE ITEMS */
         /*DELETE LIBRARY IMAGES*/
        $this->delete_library_files(array($library_id));
        $this->db->delete('library_items', array('library_id' => $library_id));
        /* DELETE ENTRIES FROM libraries table */
        return $this->db->delete('libraries', array('id' => $library_id));
    }
    
    public function delete_library_files($libraries_id = array()) {
        if (count($libraries_id)) {
            /* GET THE IMAGES AND AUDIO FILES OF THE RECORD IDS */
            $query = $this->db->query('SELECT image FROM libraries WHERE id IN (' . implode(',', $libraries_id) . ') AND NOT(NULLIF(image,"") IS NULL)');
            if ($query->num_rows()) {
                require_once (APPPATH . 'libraries/Custom_file.php');
                $fileObj = new Custom_file();
                $uploaddir = FCPATH . 'assets/uploads/libraries/';
                $result = $query->result_array();
                foreach ($result as $row) {
                    if(!empty($row['image'])){
                        $fileObj->unlink_files($uploaddir . $row['image']);
                    }
                }
            }
        }
        return TRUE;
    }

    function get_library_custom($where = array(), $lib_detail_required = TRUE) {
        $this->db->select('user_libraries.*');
        if ($lib_detail_required) {
            $this->db->select('l.name, l.lib_inserted_from');
        }
        $this->db->from('user_libraries');
        if ($lib_detail_required) {
            $this->db->join('libraries l', 'l.id = user_libraries.library_id');
        }
        $this->db->where($where);
        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->row_array();
        } else {
            return FALSE;
        }
    }

    function get_libraries_custom($where = array(), $lib_detail_required = TRUE) {
        $this->db->select('user_libraries.*');
        if ($lib_detail_required) {
            $this->db->select('libraries.name');
        }
        $this->db->from('user_libraries');
        if ($lib_detail_required) {
            $this->db->join('libraries', 'libraries.id = user_libraries.library_id');
        }
        $this->db->where($where);
        $query = $this->db->get();

        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }

    /**   USED IN ONE METHOD user_library/detail_post
     * @Name : get_library_detail()
     * @Purpose : Returns the list of libraries saved for the Logged in user
     * @Call from : can be called from any controller (Detail of a Library  API )
     * @Functionality : fetch the record from the user_record_libraries and user_records table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    function get_library_detail($where = array()) {
        $this->db->select('l.*, ul.*');
        $this->db->from('user_libraries ul');
        $this->db->join('libraries l', 'ul.library_id = l.id', 'INNER');
        if (isset($where['user_id'])) {
            $this->db->where('ul.user_id', $where['user_id']);
        }
        if (isset($where['library_id'])) {
            $this->db->where('ul.library_id', $where['library_id']);
        }
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    /* FETCH THE LIBRARIES (CREATED OR SHARED) WITH THE DETAIL OF OWNER */

    function get_user_libraries($user_id, $where = NULL, $offset = NULL, $per_page = NULL) {
        $this->db->select('ul.library_id,ul.is_shared,ul.is_imported,ul.is_approved,l.name,l.description,l.lib_inserted_from');
        $this->db->select('CONCAT(u.first_name," ",u.last_name) as ownwer_name');
        $this->db->from('user_libraries ul');
        $this->db->join('libraries l', 'ul.library_id=l.id');
        $this->db->join('user_libraries ul1', 'ul.library_id=ul1.library_id  AND ul1.is_shared="0" AND ul.is_approved="1"', 'LEFT');
//        $this->db->join('user_libraries ul1', 'ul.library_id=ul1.library_id AND ul.is_shared="1" AND ul1.is_shared="0" AND ul.is_approved="1"', 'LEFT');
        $this->db->join('users u', 'ul1.user_id=u.id', "LEFT");
        $this->db->where('ul.user_id', $user_id);
        /* If NO CONDITION SPECIFIED THEN GET ALL SHARED AND CREATED LIBRARIES */
        if (!empty($where)) {
            $this->db->where($where);
        }
        if (!is_null($offset) && !is_null($per_page)) {
            $this->db->limit($per_page, $offset);
        }
        $this->db->order_by('l.name', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    /* FETCH THE LIBRARIES AND THEIR ITEMS DATA (CREATED OR SHARED) WITH THE DETAIL OF OWNER + Detail of Items */

    public function fetch_library_and_items($user_id, $where, $offset = NULL, $per_page = NULL) {
        $response_data = $this->get_user_libraries($user_id, $where, $offset, $per_page);
        $this->load->model('API_V1/library_items_model');
        foreach ($response_data as $key => $record) {
            $response_data[$key]['library_items'] = $this->library_items_model->get_items(array('library_id' => $record['library_id']));
            if ($response_data[$key]['library_items']) {
                foreach ($response_data[$key]['library_items'] as $item_key => $item) {
                    $response_data[$key]['library_items'][$item_key]['frequency'] = $item['frequency'] ? json_decode($item['frequency'], TRUE) : array();
                }
            } else {
                $response_data[$key]['library_items'] = array();
            }
        }
        return $response_data;
    }

    public function get_dropbox_shared_libraries($user_id) {
        $query = $this->db->query('SELECT ul.library_id FROM user_libraries ul JOIN libraries l ON ul.library_id = l.id AND l.lib_inserted_from = "dropbox" AND ul.is_shared="1" AND ul.user_id = ' . $user_id . ' LEFT JOIN user_libraries ul1 ON ul.library_id = ul1.library_id AND ul.is_shared = "1" AND ul1.is_shared ="0" WHERE ul1.user_id is NULL');
        if ($query->num_rows()) {
            return $query->result_array();
        }
        return false;
    }

    /*     * ****************************************SHARING RELATED METHODS********************************* */

    public function get_already_shared_libraries($user_id, $library_ids = array()) {
        $this->db->select('user_libraries.*');
        $this->db->from('user_libraries');
        $this->db->where('user_id', $user_id);
        $this->db->where('is_shared', '1');
        if (!empty($library_ids) && count($library_ids)) {
            $this->db->where_in('library_id', $library_ids);
        }
        $this->db->group_by('library_id');
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        }
        return FALSE;
    }

    public function check_sharaeble_libraries($owner_id, $library_ids = array()) {
        $this->db->select('library_id');
        $this->db->from('user_libraries');
        $this->db->where('user_id', $owner_id);
        $this->db->where('is_shared', '0');
        if (count($library_ids)) {
            $this->db->where_in('library_id', $library_ids);
        }
        $query = $this->db->get();
        if($query->num_rows()){
            return array_column($query->result_array(),'library_id');
        }
        return FALSE;
    }
    
    public function save_share_library_data($insert_data){
        while ($rows = array_splice($insert_data, 0, 50)) {
            $this->db->insert_batch('user_libraries', $rows);
        }
        return TRUE;
    }

    public function share_library($user_id, $data) {
        $this->db->select('*');
        $this->db->from('user_libraries');
        $this->db->join('libraries', 'libraries.id = user_libraries.library_id');
        $this->db->where('user_id', $user_id);
        $this->db->where('library_id', $data['library_id']);
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            $data['date_created'] = date('Y-m-d H:i:s');
            $data['library_id'] = $getdata->row()->library_id;
            if (!isset($data['is_shared'])) {
                $data['is_shared'] = '1';
            }
            if (!isset($data['is_imported'])) {
                $data['is_imported'] = '0';
            }
            $this->db->insert('user_libraries', $data);
            return array('id' => $this->db->insert_id(), 'name' => $getdata->row()->name);
        } else {
            return false;
        }
    }

    function update_user_libraries($data = array(), $where = NULL) {
        $data['date_updated'] = date('Y-m-d H:i:s');
        if ($where) {
            $this->db->where($where);
        }
        if ($this->db->update("user_libraries", $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function insert_user_libraries($data = array()) {
        $data['date_created'] = date('Y-m-d H:i:s');
        if ($this->db->insert('user_libraries', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    public function check_lib_share_code($user_id, $code, $is_bulk_import = 0) {
        $code_col = $is_bulk_import ? 'bulk_share_code' : 'share_library_code';
        $this->db->where(array($code_col => $code, 'user_id' => $user_id, 'is_shared' => '1'));
        return $this->db->count_all_results('user_libraries');
    }

    public function get_libraries_for_bulk_share($uuid, $bulk_share_code = 0) {
        $query = $this->db->query('SELECT ul.*, l.name FROM user_libraries ul '
                . ' JOIN libraries l ON l.id = ul.library_id '
                . ' WHERE (ul.uuid = "' . $uuid . '" OR ul.uuid IS NULL OR ul.uuid ="" )AND bulk_share_code = "' . $bulk_share_code . '"');
//                . ' WHERE ul.uuid = "'.$uuid.'" AND bulk_share_code = "'.$bulk_share_code.'" AND NOT(ul.is_imported = "1" AND ul.is_approved = "1")');
        return $query->result_array();
    }

    public function get_libraries_for_bulk_share_request($uuid, $bulk_share_code = 0) {
        $query = $this->db->query('SELECT ul.*, l.name FROM user_libraries ul '
                . ' JOIN libraries l ON l.id = ul.library_id '
                . ' WHERE library_id NOT IN (SELECT library_id From user_libraries WHERE bulk_share_code = "' . $bulk_share_code . '" AND uuid = "' . $uuid . '") AND bulk_share_code = "' . $bulk_share_code . '" GROUP BY library_id');
        return $query->result_array();
    }

    public function get_lib_owner($where = array()) {
        $this->db->select('u.email, CONCAT(u.first_name," ",u.last_name) as ownwer_name');
        $this->db->from('user_libraries ul');
        $this->db->join('user_libraries ul1', 'ul.library_id=ul1.library_id AND ul.is_shared="1" AND ul1.is_shared="0" AND ul.is_approved="1"');
        $this->db->join('users u', 'ul1.user_id = u.id');
        /* If NO CONDITION SPECIFIED THEN GET ALL SHARED AND CREATED LIBRARIES */
        if (isset($where['bulk_share_code'])) {
            $this->db->where('ul.bulk_share_code', $where['bulk_share_code']);
        }
        if (isset($where['share_library_code'])) {
            $this->db->where('ul.share_library_code', $where['share_library_code']);
        }
        $this->db->group_by('u.id');
        $query = $this->db->get();
        return $query->row_array();
    }

    /*     * ****************************************CLIENT RECORDS RELATED METHODS********************************* */

    function get_record_library($where = array()) {
        $this->db->select('*');
        $this->db->from('record_library');
        $this->db->where($where);
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return false;
        }
    }

    function associate_record_with_library($data = array()) {
        if ($this->db->insert('record_library', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    function get_all_record_library_data($user_id = NULL) {
        $this->db->select('l.id, l.name as library_name , l.description as library_description , l.image ,li.name as item_name , li.frequency');
        $this->db->from('record_library rl');
        $this->db->join('libraries l', ' l.id = rl.library_id', 'INNER');
        $this->db->join('library_items li', 'li.library_id = l.id', 'LEFT');
        $this->db->where('rl.record_id', $user_id);
        $this->db->where('rl.is_group', "0");
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return false;
        }
    }

    function get_libraries_count_for_record($record_id = null) {
        $this->db->select('count(library_id) as libraries_count');
        $this->db->from('record_library');
        $this->db->where('record_id', $record_id);
        $this->db->where('is_group', "0");
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->row_array();
        } else {
            return false;
        }
    }

    /* DELETE ONLY RECORD LIBRARY NOT THE ASSOCIATED MASTER BRANCH INFO */

    function delete_record_library($record_id, $library_id = array()) {
        $this->db->where('record_id', $record_id);
        $this->db->where('is_group', "0");
        if (is_array($library_id) && count($library_id)) {
            $this->db->where_in('library_id', $library_id);
        }
        return $this->db->delete('record_library');
    }

    /*     * ************************************************************************************************************************ */






    /*     * ************************************************NOT BEING USED************************************************ */

    function get_import_request($library_ids) {
        $this->db->select('*');
        $this->db->from('user_libraries');
        $this->db->where_in('library_id', $library_ids);
        $this->db->where('is_shared', '1');
        $this->db->where('is_approved', '0');
        $this->db->order_by("id", "desc");
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return false;
        }
    }

    /**
     * @Name : get_user()
     * @Purpose : return the array matching the given criteria
     * @Call from : Users.php API file
     * @Functionality : fetch the record from the users_record table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    function get_libraries($where = array()) {
        $this->db->select('*');
        $this->db->from('libraries l');
        $this->db->join('user_libraries ul', 'ul.library_id = l.id');
        if (isset($where['user_id'])) {
            $this->db->where('ul.user_id', $where['user_id']);
            unset($where['user_id']);
        }
        $this->db->where($where);
        $this->db->order_by('l.name', 'ASC');
        $getdata = $this->db->get();
//       echo  $this->db->last_query();
//       exit;
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return false;
        }
    }

    function get_library_by_id($where = array()) {
        $this->db->select('*');
        $this->db->from('user_libraries');
        $this->db->where('user_id', $where['user_id']);
        $this->db->where('library_id', $where['library_id']);
        $this->db->where('is_shared', '0');
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return true;
        } else {
            return false;
        }
    }

    function insert_notification($data = array()) {

        if ($this->db->insert('notifications', $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    function get_notifiation($where = array()) {
        $this->db->select('*');
        $this->db->from('notifications');
        $this->db->where($where);
        $this->db->order_by("id", "desc");
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return false;
        }
    }

    public function check_library_shared($user_id, $library_id) {
        $this->db->select('*');
        $this->db->from('user_libraries');
        $this->db->join('libraries', 'libraries.id = user_libraries.library_id', 'inner');
        $this->db->where('user_id', $user_id);
        $this->db->where('library_id', $library_id);
        $this->db->where('is_shared', '1');
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return true;
        } else {
            return false;
        }
    }

    function get_user_name_and_library_name($where = array()) {
        $this->db->select('l.name as library_name,concat(u.first_name ," ", u.last_name) as  user_name');
        if (isset($where['id'])) {
            $this->db->select($where['id'] . ' as id');
        }
        $this->db->from('libraries l');
        $this->db->join('users u', 'u.id =' . $where['user_id']);
        $this->db->where('l.id', $where['library_id']);
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->row_array();
        } else {
            return false;
        }
    }

    function user_libraries_and_items($user_id = NULL, $offset = 0, $per_page = 50) {
        $this->db->select('ul.library_id,ul.is_shared,ul.is_imported,ul.is_approved,l.name,l.description,l.lib_inserted_from');
        $this->db->select('CONCAT(u.first_name," ",u.last_name) as ownwer_name');
        $this->db->from('user_libraries ul');
        $this->db->join('libraries l', 'ul.library_id=l.id');
        $this->db->join('user_libraries ul1', 'ul.library_id=ul1.library_id AND ul.is_shared="1" AND ul1.is_shared="0" AND ul.is_approved="1"', 'LEFT');
//        $this->db->join('user_libraries ul1', 'ul.library_id=ul1.library_id AND ul.is_shared="1" AND ul.is_imported="1" AND ul1.is_shared="0" AND ul.is_approved="1"', 'LEFT');
        $this->db->join('users u', 'ul1.user_id=u.id', "LEFT");
        $this->db->where('ul.user_id', $user_id);
        $this->db->where('(ul.is_shared="0" OR (ul.is_shared="1" AND ul.is_imported="1" AND ul.is_approved="1"))');
        $this->db->limit($per_page, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_record_libraries($record_id) {
        $this->db->select('l.id, l.name as library_name , l.description as library_description , l.image');
        $this->db->from('record_library rl');
        $this->db->join('libraries l', ' l.id = rl.library_id', 'INNER');
        $this->db->where('rl.record_id', $record_id);
        $getdata = $this->db->get();
        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return FALSE;
        }
    }

}
