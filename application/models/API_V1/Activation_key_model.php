<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Activation_key_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'activation_keys';
        $this->validate = array(
            array(
                'field' => 'user_id',
                'label' => 'User Id',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'activation_key',
                'label' => 'Activation Key',
                'rules' => 'trim|required',
            ),
        );
    }

    /**
     * @Name : add_key()
     * @Purpose : To validate the username and password.
     * @Call from : Authentication.php controller file.
     * @Functionality : Convert the value hash using sha1 algorithm functions.
     * @Receiver params : $value_to_hash value to be converted.
     * @Return params : return converted hash value.
     */
    

    function add_key($record) {
        $record['date_created'] = date('Y-m-d H:i:s');
        if ($this->db->insert($this->table, $record)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
   

    function get_key($user_id,$key) {
        $where['user_id'] = $user_id;
        $where['activation_key'] = $key;
        $getdata = $this->db->get_where($this->table, $where);

        if ($getdata->num_rows()) {
            return $getdata->row_array();
        } else {
            return FALSE;
        }
    }
   

}
