<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_registered_device_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'user_registered_devices';
        $this->validate = array(
            array(
                'field' => 'uuid',
                'label' => 'Device Id',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'device_description',
                'label' => 'Device Description',
                'rules' => 'trim',
            ),
            array(
                'field' => 'device_type',
                'label' => 'Device Type',
                'rules' => 'trim',
            )
        );
    }
    
    

    /**
     * @Name : get_devices()
     * @Purpose : return the array of devices , false otherwise if no revords found
     * @Call from : Users.php API file
     * @Functionality : fetch the record from the users_registered device table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of records ,  false otherwise
     */
    public function get_devices($where = array(),$limit = null, $start = null, $col_to_sort = 'device_id', $sort_order = "DESC") {
        if (!is_null($limit) && !is_null($start)) {
            $this->db->limit($limit, $start);
        }
        if (!is_null($col_to_sort)) {
            $this->db->order_by($col_to_sort, $sort_order);
        }
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->select($this->table . ".*", FALSE);
        $this->db->from($this->table);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }
    
    public function get_device_detail($where = NULL){
        if(!is_null($where)){
            $this->db->where($where);
        }
        $query = $this->db->get($this->table);
        if($query->num_rows()){
            return $query->row_array();
        }
        return FALSE;
    }
    /**
     * @Name : delete_device()
     * @Purpose : deletes the registered device  of the user
     * @Call from : Users.php API file
     * @Functionality : deletes the device from user_registered_device, auth_access_tokens table
     * @Receiver params : device_id
     * @Return params : return true if deleted successfully ,  false otherwise
     */
    public function delete_device($device_id){
        $record = $this->get_device_detail(array('device_id'=>$device_id));
        if($record){
            /* TRACK THE DELETED ACCESS TOKENS */
            $this->load->library('api_manager');
            $this->api_manager->track_deleted_tokens(array('uuid'=>$record['uuid']),"deleted from admin panel");
            /*DELETE THE DEVICE ID FROM AUTH_ACCESS_TOKEN TABLE AS WELL */
            $this->db->delete('auth_access_tokens', array('uuid'=>$record['uuid']));
            return $this->db->delete($this->table, array('device_id'=>$device_id));
        }
        return FALSE;
    }
    
   

}
