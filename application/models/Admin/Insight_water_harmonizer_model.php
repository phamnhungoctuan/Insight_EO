<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Insight_water_harmonizer_Model extends MY_Model{
    public function __construct() {
        parent::__construct();
        $this->table = 'users';
        $this->primary_key='id';
        $this->validate= array(
            array(
                'field' => 'first_name',
                'label' => 'Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'activation_key',
                'label' => 'Activation Key',
                'rules' => 'trim|required'
            ),
        );
        
        /* INITIALIZE TEH DATABASE CONNECTION */
        $this->db_insight_water_harmonizer = $this->load->database('insight_water_harmonizer', TRUE);
    }
   
    /**
     * @Name : get_users()
     * @Purpose : fetch the details of the user to be used in sending emails to admin or distributor or to user himself
     * @Call from : can be call from any controller
     * @Functionality : to update the distributor for the record.
     * @Receiver params : ONE post parameters :  user Id 
     * @Return params : Return array of the user details incase of success, false otherwise
     */
    public function get_users($where = NULL, $limit = null, $start = null, $col_to_sort = 'id', $sort_order = "DESC") {
        if (!is_null($limit) && !is_null($start)) {
            $this->db_insight_water_harmonizer->limit($limit, $start);
        }
        if (!is_null($col_to_sort)) {
            $this->db_insight_water_harmonizer->order_by($col_to_sort, $sort_order);
        }
        if (!is_null($where)) {
            $this->db_insight_water_harmonizer->where($where);
        }
        $this->db_insight_water_harmonizer->select($this->table . ".*", FALSE);
        $this->db_insight_water_harmonizer->from($this->table);
        $query = $this->db_insight_water_harmonizer->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    /**
     * @Name : count_results()
     * @Purpose : To count to number of rows in the database.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Simply count the results using predefined count_all_results() function.
     * @Receiver params : No parameters passed
     * @Return params : Returns the count of rows.
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     */
    function count_results($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db_insight_water_harmonizer->where($where);
        }
        $this->db_insight_water_harmonizer->from($this->table);
        return $this->db_insight_water_harmonizer->count_all_results();
    }
    
    /**
     * @Name : get_user_detail()
     * @Purpose : To fetch the details of a single record.
     * @Call from : Can be called form any model and controller.
     * @Functionality : return the array of record detail filtered according to the given conditions, False otherwise
     * @Receiver params : array of filters
     * @Return params : Returns the record if a match is found, false otherwise.
     */
    public function get_user_detail($where = NULL){
        if(!is_null($where)){
            $this->db_insight_water_harmonizer->where($where);
        }
        $query = $this->db_insight_water_harmonizer->get($this->table);
        if($query->num_rows()){
            return $query->row_array();
        }
        return FALSE;
    }
    
    
    /**
     * @Name : save()
     * @Purpose : To save the records in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Saves the entry in specified database table.
     * @Receiver params : $data array of values to save in database.
     * @Return params : return last insert id if record is saved else return false.
     */
    function save($data = array()) {
        if ($this->db_insight_water_harmonizer->insert($this->table, $data))
            return $this->db_insight_water_harmonizer->insert_id();
        else
            return false;
    }
     /**
     * @Name : update()
     * @Purpose : To update the record in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : update the records
     * @Receiver params : data to update and array of values to filter the record to be updated
     * @Return params : return true if record is updated sucessfully else return false.
     */

    public function update($data = array(), $where = array()) {
        return $this->db_insight_water_harmonizer->update($this->table, $data, $where);
    }
    
     /**
     * @Name : delete()
     * @Purpose : To delete the record in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : delete the entry from the database table.
     * @Receiver params : array of values to filter the record to be deleted
     * @Return params : return true if record is deleted else return false.
     */
    function delete_user($where = array()) {
        if ($this->db_insight_water_harmonizer->delete($this->table, $where)){
            return true;
        }
        return false;
    }
    
    /**
    * @Name : generateKey()
    * @Purpose : To generate a randon string unique for the specifies column in table.
    * @Call from : Can be called form any model and controller.
    * @Functionality : Generate the random String using helper method and checks the uniqueness of the string in the specified column of specified table
    * @Receiver params : Column in which to check the string uniqueness
    * @Return params : return the unique random string.
    */
    
    public function generateKey($col){
        $randomString = $this->generate_random_string();
        $where[$col] = $randomString;
        while($this->count_results($where)){
            $randomString = generate_random_string();
            $where[$col] = $randomString;
        }
        return $randomString;
    }
    
    
    /* Devices */
    public function get_free_trial_devices($where = NULL, $limit = null, $start = null, $col_to_sort = 'device_id', $sort_order = "DESC") {
        if (!is_null($limit) && !is_null($start)) {
            $this->db_insight_water_harmonizer->limit($limit, $start);
        }
        if (!is_null($col_to_sort)) {
            $this->db_insight_water_harmonizer->order_by($col_to_sort, $sort_order);
        }
        if (!is_null($where)) {
            $this->db_insight_water_harmonizer->where($where);
        }
        $this->db_insight_water_harmonizer->where("registered_for_free_trial","1");
        $this->db_insight_water_harmonizer->select("devices" . ".*", FALSE);
        $this->db_insight_water_harmonizer->from("devices");
        $query = $this->db_insight_water_harmonizer->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }
    
    /* Devices */
    public function count_free_trial_devices($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db_insight_water_harmonizer->where($where);
        }
        $this->db_insight_water_harmonizer->where("registered_for_free_trial","1");
        $this->db_insight_water_harmonizer->from("devices");
        return $this->db_insight_water_harmonizer->count_all_results();
    }
    
      
    /*=========================================== PAYMENT RELATED METHODS ====================================================*/
    /**********************PAYMENT RELATED FUNCTION****************************/
    public function do_payment($data, $quantity = 1){
        $activationkey_array = array();
        /*NO OF LICENCES TO GENERATE*/
            $quantity = $quantity && is_numeric($quantity) ? $quantity : 1;
            /* FIRST CHECK WHETHER THIS EMAIL EXIST IN DATABASE */
            $where = 'email ="'.$data['email'].'" AND (`activation_key` Is NULL OR `activation_key` = "")';
            $records = $this->get_users($where);
            if($records){
                for($optionCounter=1,$nullRowCounter=1; $optionCounter<=$quantity;$optionCounter++,$nullRowCounter++){
                    /* GENERATE LICENCE */
                    $data['activation_key'] = $this->generateKey("activation_key");
                    if($nullRowCounter<=count($records)){
                        $row = $records[$nullRowCounter-1];
                        $user_id = $row['id'];
                        $result = $this->save_transaction($data,$user_id);
                    }else{
                        $result = $this->save_transaction($data, 0);
                    }
                    if($result){
                        $activationkey_array[] = $data['activation_key'];
                    }
                    
                }
            }else{
                for($optionCounter=1; $optionCounter<=$quantity;$optionCounter++){
                    $data['activation_key'] = $this->generateKey("activation_key");
                    $result = $this->save_transaction($data, 0);
                    if($result){
                        $activationkey_array[] = $data['activation_key'];
                    }
                }
            }
            return $activationkey_array;
    }
    /**
    * @Name : save_paypal_transaction()
    * @Purpose : To save the Paypal purchase  VIA Add to cart or installments or buy now button 
    * @Call from : Called from save_transaction method in payment Controller 
    * @Functionality : generate the Key for the given user and  Insert or update the existing row 
    * @Receiver params : $data array of values to be inserted or updated 
    * @Return params : Return true on success, false otherwise 
    */
    public function save_transaction($param, $user_id = 0 ) {
        /****************DATA TO SAVE OR UPDATE***************/
        if($user_id == 0){
            $param['date_created'] = date("Y-m-d H:i:s");
            $param['activationkey_created_date'] = date("Y-m-d H:i:s");
            $insert_id = $this->save($param);
            return $insert_id;
        }
        else{
            $where['id'] = $user_id;
            $this->update($param, $where);
            return $user_id;
        }
    }
  
}