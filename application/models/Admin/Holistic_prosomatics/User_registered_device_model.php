<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_registered_device_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'user_registered_devices';
        $this->validate = array(
            array(
                'field' => 'uuid',
                'label' => 'Device Id',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'device_description',
                'label' => 'Device Description',
                'rules' => 'trim',
            ),
            array(
                'field' => 'device_type',
                'label' => 'Device Type',
                'rules' => 'trim',
            )
        );
        /* INITIALIZE TEH DATABASE CONNECTION */
        $this->db_holi_pros = $this->load->database('holistic_prosomatics', TRUE);
    }

    public function add_device($data = array()) {
        if (count($data)) {
            if (empty($data['device_type'])) {
                $data['device_type'] = strlen($data['uuid']) <= 16 ? 'android' : 'ios';
            }
            $this->db_holi_pros->insert($this->table, $data);
            $device_insert_id = $this->db_holi_pros->insert_id();
            /* If Saved successfully then save the entry in users_infusionsoft_record */
            if ($device_insert_id) {
                /* FETCH THE RECORD FROM USER_INFUSIONSOFT_RECORD TABLE 
                 * IF YES, THEN UPDATE THE RECORD ELSE INSERT
                 */
                $this->load->model("user_infusionsoft_record_model");
                $record = $this->user_infusionsoft_record_model->get_infusionsoft_record_detail(array('user_id' => $data['user_id']));
                if ($record) {
                    /* SET PLATFORM */
                    if ($record['platform'] == 'both' || $record['platform'] == $data['device_type']) {
                        return $device_insert_id; /* DO NOTHING */
                    }
                    if (empty($record['platform'])) {
                        $update_data['platform'] = $data['device_type'];
                    } else if ($record['platform'] != $data['device_type']) {
                        $update_data['platform'] = "both";
                    }
                    $update_data['date_updated'] = date('Y-m-d H:i:s');
                    $this->user_infusionsoft_record_model->update($update_data, array('user_id' => $data['user_id']));
                } else {
                    /* INSERT THE RECORD */
                    $insert_data = array(
                        'user_id' => $data['user_id'],
                        'platform' => $data['device_type'],
                        'date_created' => date('Y-m-d H:i:s')
                    );
                    $this->user_infusionsoft_record_model->save($insert_data);
                }
                return $device_insert_id;
            }
        }
        return FALSE;
    }

    /**
     * @Name : get_devices()
     * @Purpose : return the array of devices , false otherwise if no revords found
     * @Call from : Users.php API file
     * @Functionality : fetch the record from the users_registered device table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of records ,  false otherwise
     */
    public function get_devices($where = array(), $limit = null, $start = null, $col_to_sort = 'device_id', $sort_order = "DESC") {
        if (!is_null($limit) && !is_null($start)) {
            $this->db_holi_pros->limit($limit, $start);
        }
        if (!is_null($col_to_sort)) {
            $this->db_holi_pros->order_by($col_to_sort, $sort_order);
        }
        if (!empty($where)) {
            $this->db_holi_pros->where($where);
        }
        $this->db_holi_pros->select($this->table . ".*", FALSE);
        $this->db_holi_pros->from($this->table);
        $query = $this->db_holi_pros->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_device_detail($where = NULL) {
        if (!is_null($where)) {
            $this->db_holi_pros->where($where);
        }
        $query = $this->db_holi_pros->get($this->table);
        if ($query->num_rows()) {
            return $query->row_array();
        }
        return FALSE;
    }

    /**
     * @Name : delete_device()
     * @Purpose : deletes the registered device  of the user
     * @Call from : Users.php API file
     * @Functionality : deletes the device from user_registered_device, auth_access_tokens table
     * @Receiver params : device_id
     * @Return params : return true if deleted successfully ,  false otherwise
     */
    public function delete_device($device_id) {
        $record = $this->get_device_detail(array('device_id' => $device_id));
        if ($record) {
            /* TRACK THE DELETED ACCESS TOKENS */
            $this->load->library('api_manager');
            $this->api_manager->track_deleted_tokens(array('uuid' => $record['uuid'], 'user_id' => $record['user_id']), "deleted from admin panel");
            /* DELETE THE DEVICE ID FROM AUTH_ACCESS_TOKEN TABLE AS WELL */
            $this->db_holi_pros->delete('auth_access_tokens', array('uuid' => $record['uuid'], 'user_id' => $record['user_id']));
            return $this->db_holi_pros->delete($this->table, array('device_id' => $device_id));
        }
        return FALSE;
    }

}
