<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Distributors_Model extends MY_Model{
    public function __construct() {
        parent::__construct();
        $this->table = 'distributors';
        $this->primary_key='id';
         
        $this->validate= array(
            array(
                'field' => 'distributor',
                'label' => 'Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            )
        ); 
        /* INITIALIZE TEH DATABASE CONNECTION */
        $this->db_holi_pros = $this->load->database('holistic_prosomatics', TRUE);
    }
    
    public function get_distributors($where = array(),$limit = null, $start = null, $col_to_sort = 'distributor', $sort_order = "ASC") {
        if (!is_null($limit) && !is_null($start)) {
            $this->db_holi_pros->limit($limit, $start);
        }
        if (!is_null($col_to_sort)) {
            $this->db_holi_pros->order_by($col_to_sort, $sort_order);
        }
        if (!empty($where)) {
            $this->db_holi_pros->where($where);
        }
        $this->db_holi_pros->select($this->table . ".*", FALSE);
        $this->db_holi_pros->from($this->table);
        $query = $this->db_holi_pros->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }
    
    public function get_distributor_detail($where = NULL){
        if(!is_null($where)){
            $this->db_holi_pros->where($where);
        }
        $query = $this->db_holi_pros->get($this->table);
        if($query->num_rows()){
            return $query->row_array();
        }
        return FALSE;
    }
}