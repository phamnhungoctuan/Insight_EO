<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Energy_mastered_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'users_old';
        $this->primary_key = 'id';
        $this->validate = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|min_length[8]',
            ),
            array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'phone_no',
                'label' => 'Phone Number',
                'rules' => 'trim',
            ),
            array(
                'field' => 'country',
                'label' => 'Country',
                'rules' => 'trim',
            ),
            array(
                'field' => 'uuid',
                'label' => 'UUID',
                'rules' => 'trim',
            ),
        );
    }

    public function get_users($where = array(), $limit = null, $start = null, $col_to_sort = 'id', $sort_order = "DESC") {
        if (!is_null($limit) && !is_null($start)) {
            $this->db->limit($limit, $start);
        }
        if (!is_null($col_to_sort)) {
            $this->db->order_by($col_to_sort, $sort_order);
        }
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->select($this->table . ".*, payment.payment_type, payment.expiry_date", FALSE);
        $this->db->from($this->table);
        $this->db->join('payment', $this->table . ".id = payment.user_id", 'left');
        $this->db->where($this->table . '.is_deleted = "0"');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_user_detail($where = NULL) {
        $payment_type = "";
//         $this->db->select($this->table . ".*, (select payment_type from payment where payment.user_id = users.id) AS payment_type", FALSE);
        if ($where['id']) {
            $where['users.id'] = $where['id'];
            unset($where['id']);
        }
        $this->db->select($this->table . ".*, payment.payment_type, payment.expiry_date", FALSE);
        if (!is_null($where)) {
            $this->db->where($where);
        }
        $this->db->from($this->table);
        $this->db->join('payment', $this->table . ".id = payment.user_id",'left');
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->row_array();
        }
        return FALSE;
    }

    /**
     * @Name : count_results()
     * @Purpose : To count to number of rows in the database.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Simply count the results using predefined count_all_results() function.
     * @Receiver params : No parameters passed
     * @Return params : Returns the count of rows.
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     */
    function count_results($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db->where($where);
        }
        $this->db->from($this->table);
        $this->db->join('payment', $this->table . ".id = payment.user_id", 'left');
        $this->db->where($this->table . '.is_deleted = "0"');
        return $this->db->count_all_results();
    }

    public function update_user($data = array(), $where = array()) {
        if (isset($data['password'])) {
            $data['password'] = $this->hash_this($data['password']);
        }
        return $this->db->update($this->table, $data, $where);
    }

    public function delete_user($user_id) {
        /* DELETE CLIENT RECORDS */
        $user_records = $this->get_user_records($user_id);
        if ($user_records) {
            $this->load->model('user_record_model');
            foreach ($user_records as $val) {
                $this->user_record_model->delete(array('id' => $val['id']));
            }
        }
        /* DELETE MASTER BRANCH */
        $this->db->delete('user_master_branch',array('user_id' => $user_id));
        /* DELETE neuro_remedy_items */
        $this->db->delete('neuro_remedy_items', array('user_id' => $user_id));
        /* DELETE activation_keys */
        $this->db->delete('activation_keys', array('user_id' => $user_id));
        /* TRACK THE DELETED ACCESS TOKENS */
        $this->load->library('api_manager');
        $this->api_manager->track_deleted_tokens(array('user_id' => $user_id), "deleted from genius scripts");
        /* DELETE auth_access_tokens */
        $this->db->delete('auth_access_tokens', array('user_id' => $user_id));
        /* DELETE user_registered_devices */
        $this->db->delete('user_registered_devices', array('user_id' => $user_id));
        /*DELETE SHARED LIBRARIES */
        $this->db->delete('user_libraries', array('user_id' => $user_id,'is_shared'=>'1'));

        /* DELETE DROPBOX AND OWN CREATED LIBRARIES */

        $this->load->model('user_library_model');
        /* DELETE DROPBOX SHARED LIBRARIES */
        $total_libraries_deleted = array();
        $dropbox_shared_libraries = $this->user_library_model->get_dropbox_shared_libraries($user_id);
        if ($dropbox_shared_libraries) {
            $total_libraries_deleted = array_merge($total_libraries_deleted, array_column($dropbox_shared_libraries, 'library_id'));
        }
        /* DELETE LIBRARY AND ITS ITEMS */
        /* DELETE LIBRARIES */

        $libraries = $this->user_library_model->get_libraries(array('user_id' => $user_id, 'ul.is_shared' => '0'));
        if ($libraries && count($libraries)) {
            $total_libraries_deleted = array_merge($total_libraries_deleted, array_column($libraries, 'library_id'));
        }

        if ($total_libraries_deleted && count($total_libraries_deleted)) {

            $this->db->where('user_id', $user_id); /* DELETE ALL THE CREATED AND SHARED LIBRRAY RECORDS */
            $this->db->or_where_in('library_id', $total_libraries_deleted); /* DELETE ALL THE RECORDS OF THE LIBARIES OWNED BY THIS USER AND SHARED WITH OTHER USERS */
            $this->db->delete('user_libraries');

            /* DELETE LIBRARY ITEMS */
            $this->db->where_in('library_id', $total_libraries_deleted);
            $this->db->delete('library_items');
            /* DELETE MASTER BRANCH LIBRARIES */
            $this->db->where_in('library_id', $total_libraries_deleted);
            $this->db->delete('master_branch_libraries');
            /* DELETE FROM record_library */
            $this->db->where_in('library_id', $total_libraries_deleted);
            $this->db->delete('record_library');

            /* DELETE Library images */
            $this->user_library_model->delete_library_files($total_libraries_deleted);
            /* DELETE FROM LIBRARIES */
            $this->db->where_in('id', $total_libraries_deleted);
            $this->db->delete('libraries');
        }

        /* DELETE THE IMAGE */
        $query = $this->db->query('SELECT image FROM users WHERE id =  ' . $user_id);
        if ($query->num_rows()) {
            $result = $query->row_array();
            if (!empty($result['image'])) {
                require_once (APPPATH . 'libraries/Custom_file.php');
                $fileObj = new Custom_file();
                $fileObj->unlink_files(FCPATH . 'assets/uploads/users/' . $result['image']);
            }
        }
        /* FINNALY DELETE THE USER */
        if ($this->db->delete($this->table, array('id' => $user_id)))
            return true;
        else
            return false;
    }

    function get_user_records($user_id) {
        $this->db->where('user_id', $user_id);
        $result = $this->db->get('user_records');
        if ($result->num_rows()) {
            return $result->result_array();
        } else {
            return FALSE;
        }
    }

}
