<?php

class MindNRG_Model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'mind_nrg';
        $this->primary_key = 'id';
        $this->validate = array(
            array(
                'field' => 'txnid',
                'label' => 'Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'Key',
                'label' => 'Key',
                'rules' => 'trim'
            ),
        );
        /* INITIALIZE TEH DATABASE CONNECTION */
        $this->db_q = $this->load->database('quantum', TRUE);
    }

    /**
     * @Name : get_users()
     * @Purpose : fetch the details of the users
     * @Call from : can be call from any controller
     * @Functionality : to get the records 
     * @Receiver params : condition = array or string . no of records i.e. offset , start record, column to sort, sort order
     * @Return params : Return array of the user details incase of success, false otherwise
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 16 Dec 2016
     * @Modified :
     */
    public function get_users($where = array(), $limit = null, $start = null, $col_to_sort = 'id', $sort_order = "DESC") {
        if (!is_null($limit) && !is_null($start)) {
            $this->db_q->limit($limit, $start);
        }
        if (!is_null($col_to_sort)) {
            $this->db_q->order_by($col_to_sort, $sort_order);
        }
        if (!empty($where)) {
            $this->db_q->where($where);
        }
        $this->db_q->select($this->table . ".*", FALSE);
        $this->db_q->from($this->table);
        $query = $this->db_q->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    /**
     * @Name : get_user_detail()
     * @Purpose : fetch the details of the user to be used in sending emails to admin or distributor or to user himself
     * @Call from : can be call from any controller
     * @Functionality : to update the distributor for the record.
     * @Receiver params : ONE post parameters :  user Id 
     * @Return params : Return array of the user details incase of success, false otherwise
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 17 Dec 2016
     * @Modified :
     */
    public function get_user_detail($where = NULL) {
        if (!is_null($where)) {
            $this->db_q->where($where);
        }
        $query = $this->db_q->get($this->table);
        if ($query->num_rows()) {
            return $query->row_array();
        }
        return FALSE;
    }

    /**
     * @Name : count_results()
     * @Purpose : To count to number of rows in the database.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Simply count the results using predefined count_all_results() function.
     * @Receiver params : No parameters passed
     * @Return params : Returns the count of rows.
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     */
    function count_results($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db_q->where($where);
        }
        $this->db_q->from($this->table);
        return $this->db_q->count_all_results();
    }

    /**
     * @Name : save()
     * @Purpose : To save the records in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Saves the entry in specified database table.
     * @Receiver params : $data array of values to save in database.
     * @Return params : return last insert id if record is saved else return false.
     */
    function save($data = array()) {
        if ($this->db_q->insert($this->table, $data))
            return $this->db_q->insert_id();
        else
            return false;
    }

    /**
     * @Name : update()
     * @Purpose : To update the record in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : update the records
     * @Receiver params : data to update and array of values to filter the record to be updated
     * @Return params : return true if record is updated sucessfully else return false.
     */
    public function update($data = array(), $where = array()) {
        return $this->db_q->update($this->table, $data, $where);
    }

    /**
     * @Name : delete()
     * @Purpose : To delete the record in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : delete the entry from the database table.
     * @Receiver params : array of values to filter the record to be deleted
     * @Return params : return true if record is deleted else return false.
     */
    function delete($where = array()) {
        if ($this->db_q->delete($this->table, $where)) {
            return true;
        }
        return false;
    }

    /**
     * @Name : generateKey()
     * @Purpose : To generate a randon string unique for the specifies column in table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Generate the random String using helper method and checks the uniqueness of the string in the specified column of specified table
     * @Receiver params : Column in which to check the string uniqueness
     * @Return params : return the unique random string.
     */
    public function generateKey($col) {
        $randomString = $this->generate_random_string();
        $where[$col] = $randomString;
        while ($this->count_results($where)) {
            $randomString = generate_random_string();
            $where[$col] = $randomString;
        }
        return $randomString;
    }
    
      
    /*=========================================== PAYMENT RELATED METHODS ====================================================*/
    /**********************PAYMENT RELATED FUNCTION****************************/
    public function do_payment($data, $quantity = 1){
        $activationkey_array = array();
        /*NO OF LICENCES TO GENERATE*/
            $quantity = $quantity && is_numeric($quantity) ? $quantity : 1;
            /* FIRST CHECK WHETHER THIS EMAIL EXIST IN DATABASE */
            $where = 'email ="'.$data['email'].'" AND (`Key` Is NULL OR `Key` = "")';
            $records = $this->get_users($where);
            if($records){
                for($optionCounter=1,$nullRowCounter=1; $optionCounter<=$quantity;$optionCounter++,$nullRowCounter++){
                    /* GENERATE LICENCE */
                    $data['Key'] = $this->generateKey("Key");
                    if($nullRowCounter<=count($records)){
                        $row = $records[$nullRowCounter-1];
                        $user_id = $row['id'];
                        $result = $this->save_transaction($data,$user_id);
                    }else{
                        $result = $this->save_transaction($data, 0);
                    }
                    if($result){
                        $activationkey_array[] = $data['Key'];
                    }
                    
                }
            }else{
                for($optionCounter=1; $optionCounter<=$quantity;$optionCounter++){
                    $data['Key'] = $this->generateKey("Key");
                    $result = $this->save_transaction($data, 0);
                    if($result){
                        $activationkey_array[] = $data['Key'];
                    }
                }
            }
            return $activationkey_array;
    }

    /**
    * @Name : save_paypal_transaction()
    * @Purpose : To save the Paypal purchase  VIA Add to cart or installments or buy now button 
    * @Call from : Called from save_transaction method in payment Controller 
    * @Functionality : generate the Key for the given user and  Insert or update the existing row 
    * @Receiver params : $data array of values to be inserted or updated 
    * @Return params : Return true on success, false otherwise 
    */
    public function save_transaction($param, $user_id = 0 ) {
        /****************DATA TO SAVE OR UPDATE***************/
        $param['key_created_from'] = "dev_server";
        $param['payment_date'] = date('Y-m-d H:i:s');
        
        if($user_id == 0){
            $param['createdtime'] = date("Y-m-d H:i:s");
            $param['user_created_from'] = "dev_server";
            $insert_id = $this->save($param);
            return $insert_id;
        }
        else{
            $where['id'] = $user_id;
            $this->update($param, $where);
            return $user_id;
        }
    }
    
  
}