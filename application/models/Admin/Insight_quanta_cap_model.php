<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Insight_quanta_cap_Model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'users';
        $this->primary_key = 'user_id';
        $this->validate = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'activation_key',
                'label' => 'Activation Key',
                'rules' => 'trim'
            ),
        );

        /* INITIALIZE TEH DATABASE CONNECTION */
        $this->db_qc = $this->load->database('insight_quanta_cap', TRUE);
    }

    /**
     * @Name : get_users()
     * @Purpose : fetch the details of the user to be used in sending emails to admin or distributor or to user himself
     * @Call from : can be call from any controller
     * @Functionality : to update the distributor for the record.
     * @Receiver params : ONE post parameters :  user Id 
     * @Return params : Return array of the user details incase of success, false otherwise
     */
    public function get_users($where = NULL, $limit = null, $start = null, $col_to_sort = 'user_id', $sort_order = "DESC") {
        if (!is_null($limit) && !is_null($start)) {
            $this->db_qc->limit($limit, $start);
        }
        if (!is_null($col_to_sort)) {
            $this->db_qc->order_by($col_to_sort, $sort_order);
        }
        if (!is_null($where)) {
            $this->db_qc->where($where);
        }
        $this->db_qc->select($this->table . ".*", FALSE);
        $this->db_qc->from($this->table);
        $query = $this->db_qc->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $key => $row) {
                if ($row['status'] == "activated") {
                    $result[$key]['account_status'] = "Activated";
                } else {
                    $result[$key]['account_status'] = "Free Trial";
                    if (!$this->calculate_trial_period_left($row['date_created'])) {
                        $result[$key]['account_status'] = "Free Trial Expired";
                    }
                }
            }
            return $result;
        }
        return false;
    }

    private function calculate_trial_period_left($date_created) {
        /* Calculate trial Days left */
        $current_date = date('Y-m-d H:i:s');
        $seconds = floor(abs(strtotime($current_date) - strtotime($date_created)));
        return $result = (INSIGHT_QUANTA_CAP_FREE_TRIAL - $seconds) <= 0 ? 0 : (INSIGHT_QUANTA_CAP_FREE_TRIAL - $seconds);
    }

    public function get_product_keys($where = NULL, $limit = null, $start = null, $col_to_sort = 'key_id', $sort_order = "DESC") {
        if (!is_null($limit) && !is_null($start)) {
            $this->db_qc->limit($limit, $start);
        }
        if (!is_null($col_to_sort)) {
            $this->db_qc->order_by($col_to_sort, $sort_order);
        }
        if (!is_null($where)) {
            $this->db_qc->where($where);
        }
        $this->db_qc->select("product_key.*", FALSE);
        $this->db_qc->from('product_key');
        $query = $this->db_qc->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    /**
     * @Name : count_results()
     * @Purpose : To count to number of rows in the database.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Simply count the results using predefined count_all_results() function.
     * @Receiver params : No parameters passed
     * @Return params : Returns the count of rows.
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     */
    function count_results($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db_qc->where($where);
        }
        $this->db_qc->from($this->table);
        return $this->db_qc->count_all_results();
    }

    public function count_product_keys($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db_qc->where($where);
        }
        $this->db_qc->from("product_key");
        return $this->db_qc->count_all_results();
    }

    /**
     * @Name : get_user_detail()
     * @Purpose : To fetch the details of a single record.
     * @Call from : Can be called form any model and controller.
     * @Functionality : return the array of record detail filtered according to the given conditions, False otherwise
     * @Receiver params : array of filters
     * @Return params : Returns the record if a match is found, false otherwise.
     */
    public function get_user_detail($where = NULL) {
        if (!is_null($where)) {
            $this->db_qc->where($where);
        }
        $query = $this->db_qc->get($this->table);
        if ($query->num_rows()) {
            return $query->row_array();
        }
        return FALSE;
    }

    public function get_key_detail($where = NULL) {
        if (!is_null($where)) {
            $this->db_qc->where($where);
        }
        $query = $this->db_qc->get('product_key');
        if ($query->num_rows()) {
            return $query->row_array();
        }
        return FALSE;
    }

    /**
     * @Name : save()
     * @Purpose : To save the records in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Saves the entry in specified database table.
     * @Receiver params : $data array of values to save in database.
     * @Return params : return last insert id if record is saved else return false.
     */
    function save($data = array()) {
        if ($this->db_qc->insert($this->table, $data))
            return $this->db_qc->insert_id();
        else
            return false;
    }

    function save_key($data = array()) {
        if ($this->db_qc->insert('product_key', $data)) {
            return $this->db_qc->insert_id();
        }
        return FALSE;
    }

    /**
     * @Name : update()
     * @Purpose : To update the record in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : update the records
     * @Receiver params : data to update and array of values to filter the record to be updated
     * @Return params : return true if record is updated sucessfully else return false.
     */
    public function update($data = array(), $where = array()) {
        if ($this->db_qc->update($this->table, $data, $where)) {
            return true;
        }
        return false;
    }

    /**
     * @Name : update_key()
     * @Purpose : To update the record of key in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : update the records
     * @Receiver params : data to update and array of values to filter the record to be updated
     * @Return params : return true if record is updated sucessfully else return false.
     */
    public function update_key($data = array(), $where = array()) {
        if ($this->db_qc->update('product_key', $data, $where)) {
            return true;
        }
        return false;
    }

    /**
     * @Name : delete()
     * @Purpose : To delete the record in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : delete the entry from the database table.
     * @Receiver params : array of values to filter the record to be deleted
     * @Return params : return true if record is deleted else return false.
     */
    function delete($where = array()) {
        if ($this->db_qc->delete('product_key', $where)) {
            return true;
        }
        return false;
    }

    /**
     * @Name : generateKey()
     * @Purpose : To generate a randon string unique for the specifies column in table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Generate the random String using helper method and checks the uniqueness of the string in the specified column of specified table
     * @Receiver params : Column in which to check the string uniqueness
     * @Return params : return the unique random string.
     */
    public function generateKey($col) {
        $randomString = $this->generate_random_string();
        $where[$col] = $randomString;
        while ($this->count_results($where)) {
            $randomString = generate_random_string();
            $where[$col] = $randomString;
        }
        return $randomString;
    }

    /* =========================================== PAYMENT RELATED METHODS ==================================================== */
    /*     * ********************PAYMENT RELATED FUNCTION*************************** */

    public function do_payment($data, $quantity = 1) {
        $activationkey_array = array();
        /* NO OF LICENCES TO GENERATE */
        $quantity = $quantity && is_numeric($quantity) ? $quantity : 1;
        /* FIRST CHECK WHETHER THIS EMAIL EXIST IN DATABASE */
        $where = 'email ="' . $data['email'] . '" AND (`activation_key` Is NULL OR `activation_key` = "")';
        $records = $this->get_product_keys($where);
        if ($records) {
            for ($optionCounter = 1, $nullRowCounter = 1; $optionCounter <= $quantity; $optionCounter++, $nullRowCounter++) {
                /* GENERATE LICENCE */
                $data['activation_key'] = $this->generateKey("activation_key");
                if ($nullRowCounter <= count($records)) {
                    $row = $records[$nullRowCounter - 1];
                    $key_id = $row['key_id'];
                    $result = $this->save_transaction($data, $key_id);
                } else {
                    $result = $this->save_transaction($data, 0);
                }
                if ($result) {
                    $activationkey_array[] = $data['activation_key'];
                }
            }
        } else {
            for ($optionCounter = 1; $optionCounter <= $quantity; $optionCounter++) {
                $data['activation_key'] = $this->generateKey("activation_key");
                $result = $this->save_transaction($data, 0);
                if ($result) {
                    $activationkey_array[] = $data['activation_key'];
                }
            }
        }
        return $activationkey_array;
    }

    /**
     * @Name : save_paypal_transaction()
     * @Purpose : To save the Paypal purchase  VIA Add to cart or installments or buy now button 
     * @Call from : Called from save_transaction method in payment Controller 
     * @Functionality : generate the Key for the given user and  Insert or update the existing row 
     * @Receiver params : $data array of values to be inserted or updated 
     * @Return params : Return true on success, false otherwise 
     */
    public function save_transaction($param, $key_id = 0) {
        /*         * **************DATA TO SAVE OR UPDATE************** */
        if ($key_id == 0) {
            $param['date_created'] = date("Y-m-d H:i:s");
            $insert_id = $this->db_qc->insert('product_key', $param);
            return $insert_id;
        } else {
            $where['key_id'] = $key_id;
            $this->update($param, $where);
            return $key_id;
        }
    }

}
