<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Relationship_Harmonizer_Model extends MY_Model{
    public function __construct() {
        parent::__construct();
        $this->table = 'cr_users';
        $this->table2 = 'cr_payments';
        $this->validate= array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'payment_key',
                'label' => 'Key',
                'rules' => 'trim'
            ),
        );
        
        /* INITIALIZE TEH DATABASE CONNECTION */
        $this->db_q = $this->load->database('quantum', TRUE);
    }
    /**
    * @Name : get_users()
    * @Purpose : To fetch the records from cr_users and cr_payments.
    * @Call from : Can be called from relationship harmonizer controller and model
    * @Functionality : Simply gets the records from database tables(cr_user JOINS cr_payments ON Id) with specified limit, conditions and starting point.
    * @Receiver params : $limit, $start, $conditions
    * @Return params : Return array of records from two tables 
    * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : 
    */
    public function get_users($conditions = array(), $limit = null, $start = null, $col_to_sort = "cr_users.userid", $sort_order = "DESC"){
        $this->db_q->select("cr_users.*", FALSE);
        $this->db_q->select("cr_payments.*", FALSE);
        $this->db_q->from("cr_users");
        $this->db_q->join("cr_payments", "cr_payments.payment_userid = cr_users.userid",'left');
        if(!is_null($limit) && !is_null($start)) {
            $this->db_q->limit($limit, $start);
        }
        if(!is_null($col_to_sort)){
            $this->db_q->order_by($col_to_sort,$sort_order);
        }
        if(!empty($conditions)) {
            $this->db_q->where($conditions);
        }
        $query = $this->db_q->get();
 
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    
    /**
     * @Name : count_results()
     * @Purpose : To count to number of rows in the database.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Simply count the results using predefined count_all_results() function.
     * @Receiver params : No parameters passed
     * @Return params : Returns the count of rows.
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     */
    function count_results($where = NULL) {
        $this->db_q->select("cr_users.*", FALSE);
        $this->db_q->select("cr_payments.*", FALSE);
        $this->db_q->from("cr_users");
        $this->db_q->join("cr_payments", "cr_payments.payment_userid = cr_users.userid",'left');
        if (!is_null($where) && !empty($where)) {
            $this->db_q->where($where);
        }
        return $this->db_q->count_all_results();
    }
    
    
    /** OVERRIDES MY_MODEL FUNCTION 
    * @Name : get_user_detail()
    * @Purpose : To fetch single record fro cr_user and cr_payments with the given "type of JOIN" .
    * @Call from : Can be called form Relationship_harmonizer model and controller.
    * @Functionality : fetch single record from database 
    * @Receiver params : User_Id is passed as parameter and type of Join- inner , left ,right
    * @Return params : return  $data incase  Record exist, false otherwise
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : 
    */
    function get_user_detail($where,$join='left') {
        $this->db_q->select("cr_users.*", FALSE);
        $this->db_q->select("cr_payments.*", FALSE);
        $this->db_q->from("cr_users");
        $this->db_q->join("cr_payments", "cr_payments.payment_userid = cr_users.userid",$join);
        if(is_array($where))
            $this->db_q->where($where);
        else 
            $this->db_q->where($where);
        $query = $this->db_q->get();
        if($query->num_rows()>0)
        {
            return $query->row_array();
        }
        else return false;
    }
    /**
    * @Name : update_data()
    * @Purpose : To update the records in the database table(cr_user and cr_payments).
    * @Call from : Can be called form Relationship_harmonizer model and controller.
    * @Functionality : updates the entry in specified database table.
    * @Receiver params : $data array of values to save in database.
    * @Return params : return true if record is saved else return false.
    * @Created : Hardeep <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : Hardeep <hardeep.intersoft@gmail.com>on 04 July 2015
    */
    function update_data($data = array(),$where=array()) {
        //----Update cr_user table---
        $user_data=array(
            'name'=>$data['name'],
            'email'=>$data['email'],
            'phone'=>$data['phone'],
            'UpdateDate'=>date("Y-m-d"),
        );
        $this->db_q->update($this->table, $user_data, $where);
        //---update cr_payments table---
        if(isset($data['payment_amount']) || isset($data['payment_status'])|| isset($data['itemid'])|| isset($data['txnid']))
        $payment_data= array(
            'payment_amount'=>$data['payment_amount'],
            'payment_status'=>$data['payment_status'],
            'paypal_txn_id'=>$data['txnid'],
            'payment_userid'=>$data['userid'],
            'itemid'=>$data['itemid'],
            'txnid'=>$data['txnid'],
        );
        if($this->get_user_detail($where,'inner')){
            $this->db_q->update($this->table2, $payment_data, array('payment_userid'=>$where['userid']));
        }
        else{
            $data['createdtime']=date('Y-m-d H:i:s');
            $this->db_q->insert($this->table2, $payment_data);
        }
        $this->db_q->update($this->table2, $payment_data, array('payment_userid'=>$where['userid']));
    }
    /**
    * @Name : save_data()
    * @Purpose : To add the new user
    * @Call from : Can be called form Relationship_harmonizer model and controller.
    * @Functionality : insert the data in the cr_users and cr_payment table
    * @Receiver params : $data array of values to save in database.
    * @Return params : return userid if record is saved else return false.
    * @Created : Hardeep <hardeep.intersoft@gmail.com> on 24 November 2015
    * @Modified : Hardeep <hardeep.intersoft@gmail.com>on 24 November 2015
    */
    function save_data($data = array()) {
        //----Update cr_user table---
        $user_data=array(
            'name'=>$data['name'],
            'email'=>$data['email'],
            'phone'=>$data['phone'],
            'uuid'=>$data['uuid'],
            'CreateDate'=>date("Y-m-d"),
            'user_created_from' => "dev_server"
        );
        $this->db_q->insert($this->table, $user_data);
        $userid = $this->db_q->insert_id();
        
        /* Add the user payment details in cr_payments table */
        $payment_data= array(
            'payment_amount'=>$data['payment_amount'],
            'payment_status'=>$data['payment_status'],
            'paypal_txn_id'=>$data['txnid'],
            'payment_userid'=>$userid,
            'payment_key'=>$data['payment_key'],
            'payment_uuid'=>$data['uuid'],
            'itemid'=>$data['itemid'],
            'txnid'=>$data['txnid'],
            'createdtime'=>date('Y-m-d'),
            'key_created_from' => "dev_server"
        );
        $this->db_q->insert($this->table2, $payment_data);
        if($userid)return $userid;
        else FALSE;
    }
     /**
    * @Name : delete()
    * @Purpose : To delete the record in the database table.
    * @Call from : Can be called form Relationship_harmonizer model and controller.
    * @Functionality : delete the entry from the database table.
    * @Receiver params : array of values to filter the record to be deleted
    * @Return params : return true if record is deleted else return false.
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 03 July 2015
    * @Modified :
    */
    function delete($where=array()) {
        /*DELETE FROM THE PAYMENT _TABLE*/
        $this->db_q->delete($this->table2,array('payment_userid'=>$where['userid']));
        if($this->db_q->delete($this->table, $where)){
            return true;
        }
        else return false;
    }
    /**
    * @Name : save_key()
    * @Purpose : To update the Key in the database table.
    * @Call from : Can be called form Relationship_harmonizer model and controller.
    * @Functionality : updates the entry in specified database table.
    * @Receiver params : 'Key' to update in database.
    * @Return params : return true if record is saved else return false.
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    * @Modified : Hardeep Kaur <hardeep.intersoft@gmail.com> on 04 July 2015
    */
    function save_key($data = array(),$where=array()) {
        $record = FALSE;
        if(!empty($data['payment_uuid'])){
            $query = $this->db_q->get_where($this->table2,array("payment_uuid"=>$data['payment_uuid']));
            $record = $query->num_rows() ? $query->row_array() : FALSE;
        }
        if(!$record){
            $data['createdtime']=date('Y-m-d H:i:s');
            $data['key_created_from'] = "dev_server";
            $this->db_q->insert($this->table2, $data);
            $user_record = $this->get_user_detail(array("userid"=>$data['payment_userid']),'inner');
            return $user_record;
        }
        return FALSE;
    }
    
    
    /**
    * @Name : generateKey()
    * @Purpose : To generate a randon string unique for the specifies column in table.
    * @Call from : Can be called form any model and controller.
    * @Functionality : Generate the random String using helper method and checks the uniqueness of the string in the specified column of specified table
    * @Receiver params : Column in which to check the string uniqueness
    * @Return params : return the unique random string.
    */
    
    public function generateKey($col,$table=NULL){
        $randomString = $this->generate_random_string();
        $where[$col] = $randomString;
        while($this->count_results($where)){
            $randomString = generate_random_string();
            $where[$col] = $randomString;
        }
        return $randomString;
    }
    
    /**
    * @Name : get_user_details_toEmail()
    * @Purpose : fetch the details of the user to be used in sending emails to admin or distributor or to user himself
    * @Call from : can be call from any controller
    * @Functionality : to update the distributor for the record.
    * @Receiver params : ONE post parameters :  user Id 
    * @Return params : Return array of the user details incase of success, false otherwise
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 6 July 2015
    * @Modified :
    */
    public function get_user_details_toEmail($userid){
        $this->db_q->select("cr_users.name AS name,cr_users.email AS email,cr_users.phone AS phone_no, cr_users.uuid AS UUID", FALSE);
        $this->db_q->select("cr_payments.country AS country, cr_payments.payment_key AS `Key`", FALSE);
        $this->db_q->from("cr_users");
        $this->db_q->join("cr_payments", "cr_payments.payment_userid = cr_users.userid",'left');
        $this->db_q->where(array('cr_users.userid'=>$userid));
        $query = $this->db_q->get();
        $data = $query->row_array();
        if(count($data)>0){
            return $data;
        }
        return false;
    }
  
}