<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Authentications_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'admin_users';
        $this->validate = array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required',
            ),
        );
    }

    /**
    * @Name : check_login()
    * @Purpose : To validate the username and password.
    * @Call from : Authentication.php controller file.
    * @Functionality : Convert the value hash using sha1 algorithm functions.
    * @Receiver params : $value_to_hash value to be converted.
    * @Return params : return converted hash value.
    * @Created : Sonia <sonia.intersoft@gmail.com> on 16 June 2015
    * @Modified : Sonia <sonia.intersoft@gmail.com> on 16 June 2015
    */
    function check_login($where) {
        // password convert in to hashing value
        
        $where['password'] = $this->hash_this($where['password']);
        $getdata = $this->db->get_where($this->table, $where);
        if ($getdata->num_rows() > 0) {
            return $getdata;
        } else {
            return false;
        }
    }
    
}
