<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Quantum_ilife_model extends MY_Model{
    public function __construct() {
        parent::__construct();
        $this->table = 'new_payments';
        $this->primary_key='id';
        $this->validate= array(
            array(
                'field' => 'txnid',
                'label' => 'Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'Key',
                'label' => 'Key',
                'rules' => 'trim'
            ),
        ); 
        /* INITIALIZE TEH DATABASE CONNECTION */
        $this->db_q = $this->load->database('quantum', TRUE);
    }
    public function get_users($where = array(),$limit = null, $start = null, $col_to_sort = 'id', $sort_order = "DESC") {
        if (!is_null($limit) && !is_null($start)) {
            $this->db_q->limit($limit, $start);
        }
        if (!is_null($col_to_sort)) {
            $this->db_q->order_by($col_to_sort, $sort_order);
        }
        if (!empty($where)) {
            $this->db_q->where($where);
        }
        $this->db_q->select($this->table . ".*", FALSE);
        $this->db_q->from($this->table);
        $query = $this->db_q->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }
    
    public function get_user_detail($where = NULL){
        if(!is_null($where)){
            $this->db_q->where($where);
        }
        $query = $this->db_q->get($this->table);
        if($query->num_rows()){
            return $query->row_array();
        }
        return FALSE;
    }
    
    /**
     * @Name : count_results()
     * @Purpose : To count to number of rows in the database.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Simply count the results using predefined count_all_results() function.
     * @Receiver params : No parameters passed
     * @Return params : Returns the count of rows.
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     */
    function count_results($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db_q->where($where);
        }
        $this->db_q->from($this->table);
        return $this->db_q->count_all_results();
    }

    function add_user($data = array()) {
        $data['created_from'] = 'dev_server';
        if ($this->db_q->insert($this->table, $data))
            return $this->db_q->insert_id();
        else
            return false;
    }
    public function update_user($data = array(), $where = array()) {
        return $this->db_q->update($this->table, $data, $where);
    }
    
    public function delete_user($where){
        $this->db_q->where($where);
        return $this->db_q->delete($this->table);
    }
}