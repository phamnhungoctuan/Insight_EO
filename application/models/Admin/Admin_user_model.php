<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Admin_user_Model extends MY_Model {
    /* Properties */
    private $UserId;
    private $Fullname;
    private $Age;
    private $Email;
    private $Position;
    private $Username;
    private $Password;
    private $CreateDate;
    private $UpdateDate;
    // Call the CI_Model constructor
    public function __construct() {
        parent::__construct();
        $this->table = 'admin_users';
        $this->primary_key='UserId';
         
        $this->validate= array(
            array(
                'field' => 'Fullname',
                'label' => 'Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'Username',
                'label' => 'Username',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'Email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'Password',
                'label' => 'Password',
                'rules' => 'trim',
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'Password Confirmation',
                'rules' => 'trim|matches[Password]',
            )
        ); 
    }
    public function get_users($where = array(),$limit = null, $start = null, $col_to_sort = 'UserId', $sort_order = "DESC") {
        if (!is_null($limit) && !is_null($start)) {
            $this->db->limit($limit, $start);
        }
        if (!is_null($col_to_sort)) {
            $this->db->order_by($col_to_sort, $sort_order);
        }
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->select($this->table . ".*", FALSE);
        $this->db->from($this->table);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }
    
    public function get_admin_detail($where = array()){
        if(!is_null($where)){
            $this->db->where($where);
        }
        $query = $this->db->get($this->table);
        if($query->num_rows()){
            return $query->row_array();
        }
        return FALSE;
    }
    public function delete_user($user_id){
        if ($this->db->delete($this->table, array('UserId'=>$user_id)))
            return true;
        else
            return false;
    }

}
