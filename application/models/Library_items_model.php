<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Library_items_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'library_items';
        $this->validate = array(
            array(
                'field' => 'library_id',
                'label' => 'Library Id',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'name',
                'label' => 'Item Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'frequency',
                'label' => 'Frequencies',
                'rules' => 'trim',
            ),
        );
    }

    
    
    /**
     * @Name : get_item_detail()
     * @Purpose : Returns the single item saved for the Logged in user along with the detail of record for which it is associated
     * @Call from : can be called from any controller (Detail of a Library  API )
     * @Functionality : fetch the record from the user_record_libraries and user_records table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    function get_item_detail($where = array()) {
        
        $this->db->select('li.*');
        $this->db->from('libraries l');
        $this->db->join('user_libraries ul', 'ul.library_id = l.id', 'INNER');
        $this->db->join('library_items li', 'li.library_id = l.id', 'INNER');
        if(isset($where['user_id'])){
            $where['ul.user_id'] = $where['user_id'];
            unset($where['user_id']);
        }
        if(isset($where['item_id'])){
            $where['li.id'] = $where['item_id'];
            unset($where['item_id']);
        }
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    /**
     * @Name : get_user()
     * @Purpose : return the single row matching the given criteria
     * @Call from : Users.php API file
     * @Functionality : fetch the record from the users_record table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    function get_libraries($where = array()) {
        $getdata = $this->db->get_where($this->table, $where);

        if ($getdata->num_rows()) {
            return $getdata->result_array();
        } else {
            return false;
        }
    }
    function save_items($data = array()) {
        if ($this->db->insert('library_items', $data))
            return $this->db->insert_id();
        else
            return false;
    }
	
    function count_items($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db->where('library_id',$where);
        }
        $getData = $this->db->count_all_results('library_items');
        return $getData;
    }
    
    function get_items($where = array()){
        $this->db->select('name, frequency');
        if(count($where)){
            $this->db->where($where);
        }
        $query = $this->db->get($this->table);
        if($query->num_rows()){
            return $query->result_array();
        }
        return FALSE;
    }
    

}
