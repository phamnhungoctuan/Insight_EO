<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dummy_Model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        ini_set('max_execution_time', 0);
    }

    public function create_dummy_libraries($user_id = 0){
        if(!$user_id){
            return;
        }
        ini_set('max_execution_time', 3000);
        for($i= 1 ; $i<=2000;$i++){
            $library_id = null;
            $library = array('name'=>'dummy_library'.$i,'description'=>'dummy_library'.$i,'date_created'=> date('Y-m-d H:i:s'));
            $this->db->insert('libraries', $library);
            $library_id = $this->db->insert_id(); 
            $item = array('library_id'=> $library_id ,'name'=>'dummy_item'.$i,'frequency'=>'{"test":"test"}','date_created'=>date('Y-m-d H:i:s'));
            $this->db->insert('library_items', $item);
            $user_lib = array('user_id'=>$user_id,'library_id'=>$library_id,'is_shared'=>'0','uuid'=>'6FE97771BFA64042A16AA039012F31E7','date_created'=>date('Y-m-d H:i:s'));
            $this->db->insert('user_libraries', $user_lib);
         }
         return true;
    }
   

}
