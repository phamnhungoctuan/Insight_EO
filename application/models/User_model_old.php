<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class User_model_old extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'users_old';
        $this->validate = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'phone_no',
                'label' => 'Phone Number',
                'rules' => 'trim',
            ),
            array(
                'field' => 'country',
                'label' => 'Country',
                'rules' => 'trim',
            ),
            array(
                'field' => 'uuid',
                'label' => 'UUID',
                'rules' => 'trim',
            ),
        );
    }

    /**
     * @Name : check_login()
     * @Purpose : To validate the username and password.
     * @Call from : Authentication.php controller file.
     * @Functionality : Convert the value hash using sha1 algorithm functions.
     * @Receiver params : $value_to_hash value to be converted.
     * @Return params : return converted hash value.
     */
    function check_login($email,$password) {
        // password convert in to hashing value
        $where['password'] = $this->hash_this($password);
        $where['email'] = $email;
        $this->db->select('id,first_name,last_name,email,uuid,phone_no,dob,image,`status`,date_created,dropbox_implemented,user_type,otp_date_created,3d_activation_status,otp_date_confirmed,freetrial_start_date,is_deleted');
        $getdata = $this->db->get_where($this->table, $where);

        if ($getdata->num_rows()) {
            return $getdata->row_array();
        } else {
            return false;
        }
    }

    function register($user) {
        // password convert in to hashing value
        if (isset($user['password'])) {
            $user['password'] = $this->hash_this($user['password']);
        }
        $user['date_created'] = date('Y-m-d H:i:s');
        $user['otp_date_created'] = date('Y-m-d H:i:s');
        $user['dropbox_implemented'] = "0";
        $user['status'] = "0";
        if ($this->db->insert($this->table, $user)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }


    function get_user($where = array()) {
        $getdata = $this->db->get_where($this->table, $where);

        if ($getdata->num_rows()) {
            return $getdata->row_array();
        } else {
            return false;
        }
    }

    function update_user($user = array(), $user_id = NULL) {
        if (isset($user['password'])) {
            $user['password'] = $this->hash_this($user['password']);
        }
        return $this->db->update($this->table, $user, array('id' => $user_id));
    }


    function get_user_records($user_id) {
        $this->db->where('user_id',$user_id);
        $result = $this->db->get('user_records');
        if($result->num_rows()){
            return $result->result_array();
        }else{
            return FALSE;
        }
    }

    function add_user_record($user) {
        $user['date_created'] = date('Y-m-d H:i:s');
        if ($this->db->insert("user_records", $user)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    function update_user_record($user, $record_id) {
        $user['date_updated'] = date('Y-m-d H:i:s');
        if ($this->db->update("user_records", $user, array('id'=>$record_id))) {
            return TRUE;
        } else {
            return false;
        }
    }

    function getUserDevices($users_id = '') {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('auth_access_tokens at', $this->table . '.id=at.user_id and at.active="1"');
        $this->db->where($this->table . '.id IN (' . $users_id . ')');
        $this->db->where('at.active', '1');
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }
    public function move_imported_records($imported_records = array()) {
        $success_count = 0;
        $fail_count = 0;
        $detailed_summary = array();
        ini_set('max_execution_time', 20000);
        $base_path = FCPATH . 'uploads/';

        function trim_value(&$value) {
            if (!is_array($value)) {
                $value = trim($value);
            } else {
                array_walk($value, 'trim_value');
            }
        }

        array_walk($imported_records, 'trim_value');

        foreach ($imported_records as $key => $user) {
            if (!isset($user['email']) || $user['email'] == '') {
                continue;
            }
            $insert_data['first_name'] = isset($user['first_name'])?$user['first_name']:(isset($user['txnid'])?$user['txnid']:'');
            $insert_data['last_name'] = isset($user['last_name'])?$user['last_name']:'';
            $insert_data['email'] = $user['email'];
            $insert_data['password'] = $this->hash_this('123456');
            $insert_data['phone_no'] = isset($user['phone_no'])?$user['phone_no']:'';
            $insert_data['country'] = isset($user['country'])?$user['country']:'';
            $insert_data['date_created'] = date('Y-m-d H:i:s');
            $insert_data['otp_date_created'] = date('Y-m-d H:i:s');
            $deviceType= isset($user['deviceType'])?$user['deviceType']:'';
            $device_data['device_type'] = $deviceType =='A'?'android':($deviceType == 'I'?'ios':'');
            $device_data['uuid'] = isset($user['UUID'])?$user['UUID']:'';

//            $result = $this->db->query("CALL add_user('$first_name','$last_name','".$user['email']."','".$data['password']."','$country','$uuid','$deviceType')");
//            $row = $result->row_array();
            $result = $this->get_user(array('email'=>$user['email']));
            if($result){
                /*register the device if not registered before*/
                $device_data['user_id'] = $result['id'];
                $this->add_device($device_data);
                $detailed_summary[] = array('email'=>$user['email'],'message'=>'Email already exists');
                $fail_count++;
            }else{
                $user_id = $this->save($insert_data);
                /* if user added successfully then insert the device info and send the mail to users */
                if($user_id && strlen($device_data['uuid'])){
                    $device_data['user_id'] = $user_id;
                    $this->add_device($device_data);
                    $detailed_summary[] = array('email'=>$user['email'],'message'=>'User registered successfully');
                    $success_count++;
                    /*SEND MAIL*/
//                    $mail_params['ContentType'] = "text/html";
//                    $mail_params['To'] = $data['email'];
//                    $mail_params['Subject'] = 'Genius Insights Registration';
//                    $mail_params['Body'] = $this->load->view('email_format/import_users', $data, true);
//                    $this->{$this->model}->sendMail($mail_params);
                }else{
                    $detailed_summary[] = array('email'=>$user['email'],'message'=>'Failed to register user');
                }
            }
        }
        $detailed_summary['total_users'] = count($imported_records);
        $detailed_summary['success'] = $success_count;
        $detailed_summary['fail'] = $fail_count;
        return $detailed_summary;

    }

    public function add_device($data){
        if(strlen($data['uuid'])){
            /* CHECK WHETHER THIS DEVICE IS ALREADY REGISTERED */
            $this->db->select('*')->from('user_registered_devices')->where(array('user_id'=>$data['user_id'],'uuid'=>$data['uuid']));
            $device = $this->db->get();
            if(!$device->num_rows()){
                $data['registered_on'] = date('Y-m-d H:i:s');
                $this->db->insert('user_registered_devices',$data);
                return TRUE;
            }
        }
        return FALSE;

    }

    function matchOldPassword($user_id = NULL, $oldPass = '') {
        $this->db->where('id', $user_id);
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($this->hash_this($oldPass) == $row->password) {
                return true;
            } else {
                return false;
            }
        }
    }


    function delete_dropbox_data($user_id){
        /* DELETE CLIENT records */
        $this->db->select('id');
        $client_query = $this->db->get_where('user_records', array('user_id'=>$user_id,'inserted_from'=>'dropbox'));
        if($client_query->num_rows()){
            $clients = array_column($client_query->result_array(),'id');
            if($clients){
                /*DELETE from user_record_libraries table*/
                $this->db->where_in('record_id',$clients);
                $this->db->delete('record_library');

                /*DELETE FROM user_records analysis*/
                $this->db->where_in('record_id', $clients);
                $this->db->delete('analysis_records');
                /* DELETE THE RECORD FILES - AUDIO AND IMAGES */
                $this->load->model('User_record_model');
                $this->User_record_model->delete_user_record_files($clients);

                /*DELETE FROM user_records */
                $this->db->delete('user_records', array('user_id'=>$user_id,'inserted_from'=>'dropbox'));
            }
        }
        $this->load->model('user_library_model');
        /* DELETE DROPBOX SHARED LIBRARIES */
        $total_libraries_deleted = array();
        $dropbox_shared_libraries = $this->user_library_model->get_dropbox_shared_libraries($user_id);
        if($dropbox_shared_libraries){
            $total_libraries_deleted = array_merge($total_libraries_deleted, array_column($dropbox_shared_libraries, 'library_id'));
        }
        /* DELETE LIBRARY AND ITS ITEMS */
        /* DELETE LIBRARIES */
        $libraries = $this->user_library_model->get_libraries(array('user_id' => $user_id, 'ul.is_shared' => '0', 'l.lib_inserted_from' => 'dropbox'));
        if ($libraries && count($libraries)) {
            $total_libraries_deleted = array_merge($total_libraries_deleted, array_column($libraries, 'library_id'));
        }
        if(count($total_libraries_deleted)){
            $this->db->where_in('library_id', $total_libraries_deleted); /* DELETE ALL THE RECORDS OF THE LIBARIES OWNED BY THIS USER AND SHARED WITH OTHER USERS */
            $this->db->delete('user_libraries');

            /* DELETE LIBRARY ITEMS */
            $this->db->where_in('library_id', $total_libraries_deleted);
            $this->db->delete('library_items');

            /*DELETE LIBRARIES FROM MASTER_BRANCH_LIBRARIES*/
            $this->db->where_in('library_id', $total_libraries_deleted);
            $this->db->delete('master_branch_libraries');

            /* DELETE RECORD LIBRARY */
            $this->db->where_in('library_id', $total_libraries_deleted);
            $this->db->where("is_group", "0");
            $this->db->delete('record_library');
            /*DELETE Library images*/
            $this->user_library_model->delete_library_files($total_libraries_deleted);

            /* DELETE FROM LIBRARIES */
            $this->db->where_in('id', $total_libraries_deleted);
            $this->db->delete('libraries');
        }
    }

}
