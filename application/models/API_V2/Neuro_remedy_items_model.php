<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Neuro_remedy_items_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'neuro_remedy_items';
        $this->validate = array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'tag',
                'label' => 'Tag',
                'rules' => 'trim|required',
            ),
        );
    }
    
    public function list_all($where = array()){
        if(count($where)){
            $this->db->where($where);
        }
        $query = $this->db->get($this->table);
        if($query->num_rows()){
            return $query->result_array();
        }
        return FALSE;
    }
    
    public function get_item($where){
        if(count($where)){
            $this->db->where($where);
        }
        $query = $this->db->get($this->table);
        if($query->num_rows()){
            return $query->row_array();
        }
        return FALSE;
    }
   

}
