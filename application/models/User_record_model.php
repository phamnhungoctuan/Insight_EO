<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_record_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'user_records';
        $this->validate = array(
            array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'trim',
            ),
            array(
                'field' => 'dob',
                'label' => 'Date Of Birth',
                'rules' => 'trim',
            )
        );
    }
    
    public function get_user_records($where = array(),$limit = null, $start = null, $col_to_sort = 'id', $sort_order = "DESC") {
        if (!is_null($limit) && !is_null($start)) {
            $this->db->limit($limit, $start);
        }
        if (!is_null($col_to_sort)) {
            $this->db->order_by($col_to_sort, $sort_order);
        }
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->select($this->table . ".*", FALSE);
        $this->db->from($this->table);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    /**
     * @Name : get_user()
     * @Purpose : return the single row matching the given criteria
     * @Call from : Users.php API file
     * @Functionality : fetch the record from the users_record table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    function get_user_record_detail($where = array()) {
        $getdata = $this->db->get_where($this->table, $where);
        if ($getdata->num_rows()) {
            return $getdata->row_array();
        } else {
            return false;
        }
    }
    
    function count_user_records($user_id = NULL){
        if($user_id){
            $this->db->where("user_id",$user_id);
        }
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    function add_user_record($user) {
        $user['date_created'] = date('Y-m-d H:i:s');
        if ($this->db->insert("user_records", $user)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    function update_user_record($user, $record_id) {
        $user['date_updated'] = date('Y-m-d H:i:s');
        if(isset($user['image'])){
            $user['record_image'] = $user['image'];
            unset($user['image']);
        }
        if ($this->db->update("user_records", $user, array('id'=>$record_id))) {
            return TRUE;
        } else {
            return false;
        }
    }
    
    function save_record_analysis($record){
        $record['date_created'] = date('Y-m-d H:i:s');
        if ($this->db->insert("analysis_records", $record)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    function delete($where = array()){
        /*DELETE the Library records from record_library table*/
        $this->db->delete('record_library',array('record_id'=>$where['id']));
        /*DELETE the Analysis records from analysis_records table*/
        $this->db->delete('analysis_records',array('record_id'=>$where['id']));
        /* DELETE THE IMAGE AND AUDIO , if any */
        $this->delete_user_record_files(array($where['id']));
        /*DELETE the Library records from record_library table*/
        return $this->db->delete('user_records',$where);
    }
    
    public function delete_user_record_files($user_records_id = array()) {
        if (count($user_records_id)) {
            /* GET THE IMAGES AND AUDIO FILES OF THE RECORD IDS */
            $query = $this->db->query('SELECT audio, record_image FROM user_records WHERE id IN (' . implode(',', $user_records_id) . ') AND NOT(NULLIF(audio,"") IS NULL AND NULLIF(record_image,"") IS NULL)');
            if ($query->num_rows()) {
                require_once(APPPATH . 'libraries/Custom_file.php');
                $fileObj = new Custom_file();
                $uploaddir = FCPATH . 'assets/uploads/user_records/';
                $result = $query->result_array();
                foreach ($result as $row) {
                    if(!empty($row['record_image'])){
                        $fileObj->unlink_files($uploaddir .'profile/'. $row['record_image']);
                    }
                    if(!empty($row['audio'])){
                        $fileObj->unlink_files($uploaddir .'audio/'. $row['audio']);
                    }
                }
            }
        }
        return TRUE;
    }
   

}
