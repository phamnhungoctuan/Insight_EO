<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Analysis_records_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'analysis_records';
        $this->validate = array(
            array(
                'field' => 'record_id',
                'label' => 'Record Id',
                'rules' => 'trim',
            ),
            array(
                'field' => 'analysis',
                'label' => 'Analysis',
                'rules' => 'trim',
            )
        );
    }

    /**
     * @Name : get_analysis_detail()
     * @Purpose : Returns the details of analysis of the record  (saved for the Logged in user) along with the detail of record for which it is associated
     * @Call from : can be called from any controller (Detail of a Library  API )
     * @Functionality : fetch the record from the analysis_records and user_records table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    function get_analysis_detail($where = array()) {
        $this->db->select('a.*');
        $this->db->from('analysis_records a');
        $this->db->join('user_records u', 'a.record_id = u.id', 'INNER');
        if (isset($where['user_id'])) {
            $where['u.user_id'] = $where['user_id'];
            unset($where['user_id']);
        }
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_parent_id($where = array()) {
        $this->db->select('id');
        $this->db->from('analysis_records');
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function get_latest_analysis($where = array()) {
        $this->db->select('*');
        $this->db->from('analysis_records');
        $this->db->where('record_id', $where['record_id']);
        $this->db->where('parent_id', '0');
        $this->db->order_by('id', 'desc');


        $query = $this->db->get();
      
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    public function add_analysis_records_inbatch($insert_data = array()){
        if(count($insert_data)) {
            return $this->db->insert_batch('analysis_records', $insert_data);
        }
        return FALSE;
    }

}
