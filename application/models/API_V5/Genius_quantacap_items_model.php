<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Genius_quantacap_items_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'genius_quantacap_items';
        $this->validate = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'tag',
                'label' => 'Tag',
                'rules' => 'trim|required',
            ),
        );
         /* INITIALIZE TEH DATABASE CONNECTION */
        $this->db_qc = $this->load->database('insight_quanta_cap', TRUE);
    }
    
    public function get_items($where = array()){
        if(count($where)){
            $this->db_qc->where($where);
        }
        $query = $this->db_qc->get($this->table);
        if($query->num_rows()){
            return $query->result_array();
        }
        return FALSE;
    }
    
    public function get_item_detail($where){
        if(count($where)){
            $this->db_qc->where($where);
        }
        $query = $this->db_qc->get($this->table);
        if($query->num_rows()){
            return $query->row_array();
        }
        return FALSE;
    }
    
    function count_results($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db_qc->where($where);
        }
        $this->db_qc->from($this->table);
        return $this->db_qc->count_all_results();
    }
    
    public function add_item($data){
        $data['exported_from'] = "genius_insight";
        $data['date_created'] = date('Y-m-d H:i:s');
        $data['import_code'] = $this->generateKey('import_code');
        if($this->db_qc->insert($this->table, $data)){
            return $data['import_code'];
        }
        return FALSE;
    }
   

}
