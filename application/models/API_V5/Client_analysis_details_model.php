<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Client_analysis_details_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'client_analysis_details';
        $this->validate = array(
            array(
                'field' => 'record_id',
                'label' => 'Record Id',
                'rules' => 'trim',
            ),
            array(
                'field' => 'item_name',
                'label' => 'Item',
                'rules' => 'trim',
            )
        );
    }

    /**
     * @Name : get_analysis_detail()
     * @Purpose : Returns the details of analysis of the record  (saved for the Logged in user) along with the detail of record for which it is associated
     * @Call from : can be called from any controller (Detail of a Library  API )
     * @Functionality : fetch the record from the analysis_records and user_records table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    public function save($insert_data = array()) {
        if (count($insert_data)) {
            /* Process data */
            $insert_data = self::process_insert_data($insert_data);
            /* Process the data and create query for  insert and update on Duplicate key insert (record_id and item_name are composite unique key) */
            while ($rows = array_splice($insert_data, 0, 20)) {
                $sql = self::create_insert_update_query($rows);
                if ($sql) {
                    $this->db->query($sql);
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    private function process_insert_data($insert_data = array()) {
        $return_array = array();
        if (empty($insert_data)) {
            return FALSE;
        }
        foreach ($insert_data as $key => $records) {
            if (isset($records['data'])) {
                foreach ($records['data'] as $data) {
                    $return_array[] = array(
                        'record_id' => $records['record_id'],
                        'item_name' => "'" . addslashes($data['item_name']) . "'",
                        'frequency' => "'" . $data['frequency'] . "'",
                        'date_created' => "'" . date('Y-m-d H:i:s') . "'",
                        'date_updated' => "'" . NULL . "'",
                    );
                }
            }
        }
        return $return_array;
    }

    private function create_insert_update_query($insert_data) {
        if (empty($insert_data))
            return FALSE;
        $keys_array = array();
        $update_str = array();
        $insert_array = array();

        /* GET THE KEYS ARRAY */
        $keys_array = array_keys(current($insert_data));
        /* GET THE update KEY string */
        foreach ($keys_array as $key) {
            if ($key != "date_created") {
                $update_str[] = $key == "date_updated" ? $key . " = '" . date('Y-m-d H:i:s') . "'" : $key . " = VALUES(" . $key . ")";
            }
        }
        /* create insert values string */
        foreach ($insert_data as $key => $rows) {
            $valstr = array();
            foreach ($rows as $key => $col) {
                $valstr[] = $col;
            }
            $insert_array[] = '(' . implode(', ', $valstr) . ')';
        }


        $sql = 'INSERT INTO ' . $this->table . ' (' . implode(',', $keys_array) . ') VALUES ' . implode(',', $insert_array) . ''
                . ' ON DUPLICATE KEY UPDATE ' . implode(',', $update_str);
        return $sql;
    }

    /**
     * @Name : get_analysis_detail()
     * @Purpose : Returns the details of analysis of the record  (saved for the Logged in user) along with the detail of record for which it is associated
     * @Call from : can be called from any controller (Detail of a Library  API )
     * @Functionality : fetch the record from the analysis_records and user_records table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return the array of single record ,  false otherwise
     */
    function get_records($where = array()) {
        $this->db->select("record_id,item_name, frequency");
        $this->db->from($this->table);
        if (!is_null($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get();
        if ($query->num_rows()) {
            return self::client_analysis_records($query->result_array());
        } else {
            return false;
        }
    }

    private function client_analysis_records($records) {
        $return_array = array();
        foreach ($records as $key => $row) {
            $return_array[$row['record_id']][] = array(
                'item_name' => $row['item_name'],
                'frequency' => $row['frequency'],
            );
        }
        return $return_array;
    }
}
