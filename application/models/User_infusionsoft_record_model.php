<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_infusionsoft_record_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'user_infusionsoft_record';
        $this->validate = array(
            array(
                'field' => 'uuid',
                'label' => 'Device Id',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'device_description',
                'label' => 'Device Description',
                'rules' => 'trim',
            ),
            array(
                'field' => 'device_type',
                'label' => 'Device Type',
                'rules' => 'trim',
            )
        );
    }

    /**
     * @Name : get_infusionsoft_record_detail()
     * @Purpose : return single row from the user_infusionsoft_record table
     * @Call from : any controller or model
     * @Functionality : fetch the record from the user_infusionsoft_record table
     * @Receiver params : array of columns and values
     * @Return params : if match found then return a row ,  false otherwise
     */
    

    public function get_infusionsoft_record_detail($where = NULL, $user_detail_required = FALSE) {
        
        $this->db->select($this->table .'.*');
        if($user_detail_required){
            $this->db->select('users.email, users.first_name, users.last_name');
        }
        $this->db->from($this->table);
        if($user_detail_required){
            $this->db->join('users','users.id = '.$this->table.'.user_id');
        }
        if (!is_null($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->row_array();
        }
        return FALSE;
    }
    
    
    public function get_records_to_update_infusionsoft(){
        $query = $this->db->query('SELECT uir.*, users.email, users.first_name, users.last_name FROM user_infusionsoft_record uir JOIN users ON users.id = uir.user_id '
                . 'WHERE updated_on_infusionsoft="0" OR uir.platform != uir.infusionsoft_device_status OR (uir.platform IS NOT NULL AND uir.infusionsoft_device_status IS NULL) ');
        if($query->num_rows()){
            return $query->result_array();
        }
        return FALSE;
    }
    
    
    public function update_batch($update_data = array(),$key_col){
        if(count($update_data)){
            While($rows = array_splice($update_data, 0, 20)){
                $this->db->update_batch($this->table, $rows, $key_col);
            }
            
        }
    }
}
