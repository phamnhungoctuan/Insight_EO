<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$config['first_tag_open'] = $config['last_tag_open'] = $config['next_tag_open'] = $config['prev_tag_open'] = $config['num_tag_open'] = '<li>';
$config['first_tag_close'] = $config['last_tag_close'] = $config['next_tag_close'] = $config['prev_tag_close'] = $config['num_tag_close'] = '</li>';
$config['cur_tag_open'] = "<li class='disabled'><span><b>";
$config['cur_tag_close'] = "</b></span></li>";
$config['reuse_query_string'] = TRUE;
$config['page_query_string'] = TRUE;