<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


//$base_path = 'http://' . $_SERVER['HTTP_HOST'] . '/dev/QuantumGenius/';
$base_path = 'http://' . $_SERVER['HTTP_HOST'];
define('BASE_URL', $base_path);


/*===================================ADMIN PANEL RELATED CONSTANTS ENDS HERE==========================*/
/* Self defined contants */
define('ROWS_PER_PAGE', 100);
define('SYNC_RECORDS_PER_PAGE', 30);
/*===================================ADMIN PANEL RELATED CONSTANTS ENDS HERE==========================*/



/*===================================GENIUS INSIGHT APP RELATED CONSTANTS ENDS HERE==========================*/

/*===================================GENIUS INSIGHT APP RELATED CONSTANTS STARTS HERE==========================*/
define('TRIAL_PERIOD', 5);
define('IOS_TEST_URL','https://sandbox.itunes.apple.com/verifyReceipt');
define('IOS_LIVE_URL','https://buy.itunes.apple.com/verifyReceipt');
define('IOS_SHARED_KEY', '720c1574a639490cb075da8ed30c2f8b');
define('ANDROID_REFRESH_TOKEN', '1/exYSQFVOszFbBcKAot0TDNBQPRWO9398MPK9C6bGydE');
define('CLIENT_ID', '354184799898-1es6fbt27mjg8g9cnkv9l4i4qo2a9e1m.apps.googleusercontent.com');
define('CLIENT_SECRET', 'G1UB5KSZyZnbm8F-VCya1a8B');
define('REDIRECT_URI', 'http://giftsoninternet.com/all/aura_genie_v1/index.php');
define('MAX_ATTEMPTS', 3);
define('DAY_INTERVAL', "2 days");
/*======================================MAILCHIMP========================================*/
define('API_KEY', "dbd1172ad2ba9ea7ada2c080a9098ba6-us16");
define('LIST_ID', "c96b834b67");

/*===================================GENIUS INSIGHT APP RELATED CONSTANTS ENDS HERE==========================*/


/*===================================MAIL SERVER SETTINGS ENDS HERE==========================*/
define('IsSMTP', TRUE);

/*GMAIL SERVER SETTINGS */
define('smtp_Host', 'smtp.gmail.com');
define('smtp_SMTPAuth', TRUE);
define('smtp_Username', 'activation@possibilitywave.com');
define('smtp_Password', 'Tomsawyer102!');

/* BIOFEEDBACKAPPS MAIL SERVER SETTINGS */

/*define('smtp_Host', 'ptlj-7hfk.accessdomain.com');
define('smtp_SMTPAuth', TRUE);
define('smtp_Username', 'activations@biofeedbackapps.com');
define('smtp_Password', 'Activations101!');
*/

// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
define('smtp_SMTPDebug', 0);
/* NOT working on test server
 * define('smtp_Port', 465);
 * define('smtp_SMTPSecure', 'ssl'); 
 */
define('smtp_Port', 587);
define('smtp_SMTPSecure', 'tls');
define('smtp_Debugoutput', 'html');
define('admin_email','Support@quantumhealthapps.com');



/*===================================MAIL SERVER SETTINGS ENDS HERE==========================*/





/*QUANTUM OLD APPS*/
/* CRON JOBS CONSTANT*/
    /*ADD SUBSCRIBER*/
    define('ADD_SUBSCRIBER','http://biofeedbackapps.com/activation/mc/Aura/addSubscriber');
    /*list contacts*/
    define("LIST_CONTACTS", "http://biofeedbackapps.com/activation/mc/Aura/listContacts");
    /*MOVE SUSCRIBER*/
    define("MOVE_SUBSCRIBER","http://biofeedbackapps.com/activation/mc/Aura/movesubscriber");
    
    
    
/*APP IDS*/
define("GENIUS_APP_ID", "1");
define("QUANTUM_ILIFE_INFINITY_APP_ID", "2");
define("INSIGHT_ROYAL_RIFE_APP_ID", "3");
define("MIND_NRG_APP_ID", "4");
define("RELATIONSHIP_HARMONIZER_APP_ID", "5");
define("QUANTA_CAPSULE_APP_ID", "6");
define("WATER_HARMONIZER_APP_ID", "7");
define("EMOTIONAL_INSIGHT_APP_ID", "8");
define("HOLO_PHOTONIC_LIGHT_APP_ID", "9");
define("INSIGHT_WATER_HARMONIZER_APP_ID", "10");
define("INSIGHT_QUANTA_CAP_APP_ID", "11");




/*INSIGHT Quanta Cap Constants*/

define('INSIGHT_QUANTA_CAP_FREE_TRIAL','259200');