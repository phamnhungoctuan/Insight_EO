<?php

/**
 * API manager for complete solution of HTTP requests.
 * Specially baked for Codeigniter.
 * 
 * @Created By  :   Hanish Singla
 * @Created At  :   2015-07-01
 */
class API_Manager_V1 {

    public $version = '1.1';
    protected $statusCode = 200;
    protected $statusText;
    protected $parameters = array();
    protected $httpHeaders = array();
    public $requestHeaders = array();
    public $request = array();
    protected $statusTexts = array();
    private $tbl_access_token = 'auth_access_tokens';
    private $default_app_mode = 'Live';
    private $default_platform = 'ios';

    public function __construct() {
        $this->requestHeaders = getallheaders();

        $CI = & get_instance();
        // loading registration model for selecting all states from table for client registration
        $loadedmodels = array();
        $CI->load->model($loadedmodels);

        $libraries = array(
            'form_validation',
        );

        $CI->load->library($libraries);

        $CI->form_validation->set_error_delimiters('', '');
    }

    private function get_db_instance() {
        $CI = & get_instance();
        return $CI->db;
    }

    private function _fetch_from_array(&$array, $index = '', $xss_clean = FALSE) {
        $CI = & get_instance();

        if (!isset($array[$index])) {
            $array[$index] = FALSE;
        }

        if (is_object($array[$index])) {
            $array[$index] = (array) $array[$index];
        }

        if (!is_array($array[$index])) {
            if (strtolower($array[$index]) == 'null' || is_null($array[$index])) {
                $array[$index] = FALSE;
            } else if ($xss_clean === TRUE) {
                $array[$index] = $CI->security->xss_clean($array[$index]);
            }
        }
        // if current index contains an array
        if (is_array($array[$index])) {
            foreach (array_keys($array[$index]) as $key) {
                self::_fetch_from_array($array[$index], $key, $xss_clean);
            }
        }
    }

    /**
     * Handles HTTP request and put request data in global
     * variables. Makes log of request data.
     *
     */
    public function handle_request($write_log = TRUE, $dir = 'request') {
        $CI = & get_instance();
        if (!@is_dir('log')) {
            @mkdir('log');
        }
        if (strlen($dir) && !@is_dir('log/' . $dir)) {
            @mkdir('log/' . $dir);
        }
        $filepath = "log/" . (strlen($dir) ? $dir . '/' : '' ) . "/log-" . date('Y-m-d') . ".php";
        $define = "";
        if (!file_exists($filepath)) {
            $define = "<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>\n\n";
        }
        if ($CI->input->post()) {
            $this->request = $_REQUEST = $_POST;
            if ($write_log)
                error_log($define . 'POST RAW : ' . date('H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($this->request) . ' /HEADERS/ ' . json_encode($this->requestHeaders) . "<br><br>", 3, $filepath);
        } elseif ($CI->input->get()) {
            $this->request = $_POST = $_REQUEST = $_GET;
            if ($write_log)
                error_log($define . 'GET RAW : ' . date('H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($this->request) . ' /HEADERS/ ' . json_encode($this->requestHeaders) . "<br><br>", 3, $filepath);
        } else {
            $_REQUEST = json_decode(preg_replace("/\s+/", " ", file_get_contents('php://input')), TRUE);
            $this->request = $_REQUEST;
            if ($write_log)
                error_log($define . 'INPUT RAW : ' . date('H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . file_get_contents('php://input') . ' /HEADERS/ ' . json_encode($this->requestHeaders) . "<br><br>", 3, $filepath);
        }
        if (!empty($_FILES)) {
            if ($write_log)
                error_log($define . 'FILE RAW : ' . date('H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($_FILES) . "<br><br>", 3, $filepath);
        }
    }

    /**
     * Parses index data from HTTP request data.
     * Takes an index and returns data present on that index.
     *
     * @param $index string
     * Index needed to be parsed from request data.
     * 
     * @param $xss_clean Boolean
     * Clean post data for cross-server scripts.
     * 
     * @return
     * Post data variables.
     *
     */
    public function parse_request($index, $xss_clean = TRUE) {
        // Check if a field has been provided
        if ($index === NULL && !empty($_REQUEST)) {
            $post = array();
            // Loop through the full _POST array and return it
            foreach (array_keys($_REQUEST) as $key) {
                $post[$key] = $this->_fetch_from_array($_REQUEST, $key, $xss_clean);
            }

            return $post;
        }
        $this->_fetch_from_array($_REQUEST, $index, $xss_clean);
        return $_REQUEST[$index];
    }

    /**
     * Outputs data to response of HTTP request.
     *
     * @param $param
     * Response data
     * 
     */
    public function set_output($param, $log_output = FALSE, $log_dir = "request") {
        $CI = &get_instance();
        $this->setHttpHeader('Content-Type', 'application/json');
        header(sprintf('HTTP/%s %s %s', $this->version, $this->statusCode, $this->statusText));

        foreach ($this->getHttpHeaders() as $name => $header) {
            header(sprintf('%s: %s', $name, $header));
        }

        if (!$param) {
            $param = array();
        } elseif (is_string($param) || is_numeric($param)) {
            $param['data'] = $param;
        }

        array_walk_recursive($param, 'self::filter_array');
        array_walk_recursive($param, 'self::filter_output');
        if ($log_dir && $log_output) {
            if (strlen($log_dir) && !@is_dir('log/' . $log_dir)) {
                @mkdir('log/' . $log_dir);
            }
            $filepath = "log/" . ($log_dir ? $log_dir . "/" : "") . "/log-" . date('Y-m-d') . ".php";
            $define = "";
            if (!file_exists($filepath)) {
                $define = "<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>\n\n";
            }
//            error_log('OutPut : ' . date('H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($param) . ' /HEADERS/ ' . json_encode(headers_list()) . PHP_EOL.PHP_EOL, 3, 'log/' . (strlen($log_dir) ? $log_dir . '/' : '') . 'error_log_' . date('Y-m-d') . '.txt');
            error_log($define . 'OutPut : ' . date('H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($param) . "<br><br>", 3, $filepath);
        }
        echo json_encode($param);
    }

    public function format_output($param) {
        if (!$param) {
            $param = array();
        }
        array_walk_recursive($param, 'self::filter_array');
        array_walk_recursive($param, 'self::filter_output');
        return $param;
    }

    public static function filter_output(&$param, $key) {
//        if (is_numeric($param)) {
//            $param = intval($param);
//        }
        if (is_null($param)) {
            $param = '';
        }
    }

    public static function filter_array(&$param, $key) {
        if (is_object($param)) {
            $param = (array) $param;
        }
    }

    /**
     * Sets status code for a HTTP request.
     * Default status code is 200 (OK)
     *
     * @param $statusCode Int
     * HTTP status code. Default 200
     * 
     * @param $error String
     * status string. Default OK
     * 
     * @param $errorDescription String
     * Error description.
     * 
     */
    public function setError($statusCode = 200, $error = 'OK', $errorDescription = null) {
        $this->setStatusCode($statusCode, $error);
        $this->addParameters(array(
            'error' => $error,
            'error_description' => $errorDescription,
        ));

        $this->addHttpHeaders(array(
            'Cache-Control' => 'no-store'
        ));
    }

    /**
     * Sets status code for a HTTP request.
     *
     * @param $statusCode Int
     * HTTP status code. Default 200
     * 
     * @param $text String
     * status string. Default OK
     * 
     */
    private function setStatusCode($statusCode, $text = null) {
        $this->statusCode = (int) $statusCode;
        $this->statusText = (null != $text) ? $text : '';
    }

    public function addParameters(array $parameters) {
        $this->parameters = array_merge($this->parameters, $parameters);
    }

    /**
     * Get HTTP headers for response.
     *
     */
    public function getHttpHeaders() {
        return $this->httpHeaders;
    }

    /**
     * Get Request headers for response.
     *
     */
    public function getRequestHeaders($header_name = NULL, $secondary_header_name = NULL) {

        if (isset($this->requestHeaders[$header_name]) && !empty($this->requestHeaders[$header_name])) {
            return $this->requestHeaders[$header_name];
        }
        if (isset($this->requestHeaders[$secondary_header_name]) && !empty($this->requestHeaders[$secondary_header_name])) {
            return $this->requestHeaders[$secondary_header_name];
        }
    }

    /**
     * Sets HTTP headers for response.
     *
     * @param $httpHeaders Array
     * HTTP Headers
     * 
     */
    public function addHttpHeaders(array $httpHeaders) {
        $this->httpHeaders = array_merge($this->httpHeaders, $httpHeaders);
    }

    /**
     * Sets single HTTP header for response.
     *
     * @param $name String
     * HTTP Header key
     * 
     * @param $value String
     * HTTP Header value
     * 
     */
    public function setHttpHeader($name, $value) {
        $this->httpHeaders[$name] = $value;
    }

    /**
     * Gets access token for user.
     *
     * @param $user_id
     * User ID associated with the access token
     *
     */
    public function getAccessToken($user_id = null) {
        $db = $this->get_db_instance();
        if (!$db) {
            $this->setError(503, 'database_not_found', 'Error while tried to access database');
            return FALSE;
        }

        $db->select('*');
        $db->from($this->tbl_access_token);
        $db->where('user_id', $user_id);
        $db->where('active', 1);
        $query = $db->get();

        if ($query->num_rows()) {
            $row = $query->row();
//            $token = array(
//                "access_token" => $row->access_token,
//                //"request" => $this->requestHeaders,
//                "token_type" => 'Auth-Token'
//            );
            $token = $row->access_token;

            return $token;
        } else {
            return FALSE;
        }
    }

    /**
     * Handle the verification of access token.
     *
     * @param $email String
     * User Email associated with the access token
     *
     * @param $token String
     * access token
     *
     */
    public function verifyAccessToken($email = null, $token = null, $app_mode = null) {
        $CI = &get_instance();
        $db = $this->get_db_instance();
        if (!$db) {
            $data = ['access_token' => $token, "email" => $email, "query" => $db->last_query(), "error" => "Error while tried to access database"];
            error_log('RAW : ' . date('H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($this->request) . ' /HEADERS/ ' . json_encode($this->requestHeaders) . "<br/> DATA : " . json_encode($data) . "<br><br>", 3, 'log/specialcase/' . 'log-' . date('Y-m-d') . '.php');
            $this->setError(503, 'database_not_found', 'Error while tried to access database');
            return FALSE;
        }
        if (!$email) {
            $email = $this->getRequestHeaders('email', 'Email');
        }

        if (!$token) {
            $token = $this->getRequestHeaders('access_token', 'Access-Token');
        }
        if (!$app_mode) {
            if (!($app_mode = strtolower($this->getRequestHeaders('app_mode', 'App-Mode')))) {
                $app_mode = $this->default_app_mode;
            }
        }
        $db->select('*, ' . $this->tbl_access_token . '.uuid as current_uuid');
        $db->from($this->tbl_access_token);
        $db->join('users', $this->tbl_access_token . '.user_id=users.id and users.status!="0"');
        $db->where($this->tbl_access_token . '.active', 1);
        $db->where('access_token', $token);
        $db->where('email', $email);
        $db->where('build', strtolower($app_mode));
        $query = $db->get();
        if ($query->num_rows()) {
            $result = array();
            while ($row = $query->unbuffered_row('array')) {
                $result = array(
                    'user_id' => $row['user_id'],
                    'email' => $row['email'],
                    'uuid' => $row['current_uuid'],
                    'status' => $row['status'],
                    'image' => $row['image'],
                    'first_name' => $row['first_name'],
                    'last_name' => $row['last_name'],
                    'activation_key' => $row['activation_key'],
                    'date_created' => $row['date_created'],
                    'dropbox_implemented' => $row['dropbox_implemented'],
                    'user_type' => $row['user_type'],
                    'otp_date_created' => $row['otp_date_created'],
                    '3d_activation_key' => $row['3d_activation_key'],
                    '3d_activation_status' => $row['3d_activation_status'],
                    'otp_date_confirmed' => $row['otp_date_confirmed'],
                    'freetrial_start_date' => $row['freetrial_start_date'],
                    'platform' => $row['platform'],
                    'build'=> strtolower($row['build'])
                );
            }

            if (empty($result)) {
                $data = ['access_token' => $token, "email" => $email, "query" => $db->last_query(), "error" => "Empty result"];
                error_log('RAW : ' . date('H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($this->request) . ' /HEADERS/ ' . json_encode($this->requestHeaders) . "<br/> DATA : " . json_encode($data) . "<br><br>", 3, 'log/specialcase/' . 'log-' . date('Y-m-d') . '.php');
            }
            return $result;
        } else {
            $data = ['access_token' => $token, "email" => $email, "query" => $db->last_query(), "error" => "No Record found"];
            error_log('RAW : ' . date('H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($this->request) . ' /HEADERS/ ' . json_encode($this->requestHeaders) . "<br/> DATA : " . json_encode($data) . "<br><br>", 3, 'log/specialcase/' . 'log-' . date('Y-m-d') . '.php');
            /* GET THE LOGIN DEVICES OF THIS USER */
            $db->select('users.email,' . $this->tbl_access_token . '.*, ' . $this->tbl_access_token . '.uuid as current_uuid');
            $db->from($this->tbl_access_token);
            $db->join('users', $this->tbl_access_token . '.user_id=users.id and users.status!="0"');
            $db->where('email', $email);
            $query = $db->get();
            $result_array = array();
            if ($query->num_rows()) {
                $result_array = $query->result_array();
            }
            error_log('Devices: ' . date('H:i:s') . ' : ' . json_encode($result_array) . "<br><br>", 3, 'log/specialcase/' . 'log-' . date('Y-m-d') . '.php');


            $this->setError(401, 'access_denied', 'Error while tried to access user');
            return FALSE;
        }
    }

    /**
     * Handle the creation of access token.
     *
     * @param $user_id
     * User ID associated with the access token
     *
     */
    public function createAccessToken($user_id = null, $uuid = NULL) {

        if (!($app_mode = strtolower($this->getRequestHeaders('app_mode', 'App-Mode')))) {
            $app_mode = $this->default_app_mode;
        }
        if (!($platform = strtolower($this->getRequestHeaders('platform', 'Platform')))) {
            $platform = $this->default_platform;
        }
        $db = $this->get_db_instance();
        if (!$db) {
            $this->setError(503, 'database_not_found', 'Error while tried to access database');
            return FALSE;
        }
        if ($uuid) {

            /* INSERT THE ENTRIES IN `deleted_auth_access_tokens` FOR TRACKING PURPOSES */
            $this->track_deleted_tokens(array('uuid' => $uuid), "create_access_token");
            $db->delete($this->tbl_access_token, array('uuid' => $uuid));
        }
        /* FOR SINGLE LOGIN */
//        if ($user_id) {
//            $db->delete($this->tbl_access_token, array('user_id' => $user_id));
//        }
        do {
            $token_string = $this->generateAccessToken();
        } while (!$db->insert($this->tbl_access_token, array(
            'access_token' => $token_string,
            'user_id' => $user_id,
            'active' => 1,
            'uuid' => $uuid,
            'platform' => $platform,
            'build' => $app_mode
        )));

//        $token = array(
//            "access_token" => $token_string,
//            //"request" => $this->requestHeaders,
//            "token_type" => 'Auth-Token'
//        );

        $token = $token_string;

        return $token;
    }

    public function track_deleted_tokens($where = array(), $reason = "create_access_token") {


        if (is_array($where) && count($where)) {

            $db = $this->get_db_instance();
            if (!$db) {
                return FALSE;
            }
            /* INSERT THE ENTRIES IN `deleted_auth_access_tokens` FOR TRACKING PURPOSES */
            $uuid_query = $db->get_where($this->tbl_access_token, $where);
            if ($uuid_query->num_rows()) {
                /* LOG THE DEVICES IN LOG FILE */
                $result = $uuid_query->result_array();

                $CI = & get_instance();
                if (!@is_dir('log')) {
                    @mkdir('log');
                }
                if (!@is_dir('log/logout_issue')) {
                    @mkdir('log/logout_issue');
                }
                $filepath = "log/logout_issue/log-" . date('Y-m-d') . ".php";
                $define = "";
                if (!file_exists($filepath)) {
                    $define = "<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>\n\n";
                }

                error_log('RAW : ' . date('H:i:s') . ' : ' . $CI->uri->uri_string . ' ' . json_encode($this->request) . ' /HEADERS/ ' . json_encode($this->requestHeaders) . "<br/> DATA : " . json_encode($result) . "<br/>Reason : " . $reason . "<br/><br>", 3, 'log/logout_issue/' . 'log-' . date('Y-m-d') . '.php');
                foreach ($result as $row) {
                    $db->insert("deleted_auth_access_tokens", array(
                        'access_token' => $row['access_token'],
                        'user_id' => $row['user_id'],
                        'active' => $row['active'],
                        'uuid' => $row['uuid'],
                        'platform' => $row['platform'],
                        'created' => $row['created'],
                        'build' => $row['build'],
                        'deleted_time' => date('Y-m-d h:i:s')
                    ));
                }
            }
        }
    }

    /**
     * Handle the creation of access token.
     *
     * @param $user_id
     * User ID associated with the access token
     *
     */
    public function destroyAccessToken($user_id = null) {
        $CI = &get_instance();
        $db = $this->get_db_instance();

        if (!$db) {
            $this->setError(503, 'database_not_found', 'Error while tried to access database');
            return FALSE;
        }
        $token = $this->getRequestHeaders('access_token', 'Access-Token');
        $db->delete($this->tbl_access_token, array('user_id' => $user_id, 'access_token' => $token));
        return TRUE;
    }

    /**
     * Generates an unique access token.
     *
     * @return
     * An unique access token.
     *
     */
    protected function generateAccessToken() {
        $tokenLen = 40;
        $randomData = mt_rand() . mt_rand() . mt_rand() . mt_rand() . microtime(true) . uniqid(mt_rand(), true);
        return substr(hash('sha512', $randomData), 0, $tokenLen);
    }

    //send iphone push notification
    public function iphonePushNotification($deviceToken = '', $title = '', $message = '', $build = '') {
        require_once FCPATH . 'application/libraries/PushDemo/simplepush.php';
        $message = substr($message, 0, 80);
        $result = sendPushMessage($deviceToken, $title, $message, $build);
        return $result;
    }

    //send notification to players.call from  events related  api`s
    public function androidPushNotification($device_id = '', $title = '', $msg = '') {
        $apiKey = "AIzaSyCqd5Up_mt_kUmuaORrOzish1dzelg0ccg";
        $registration_id = array($device_id);
        $url = 'https://android.googleapis.com/gcm/send';

        $fields = array(
            'registration_ids' => $registration_id,
            'data' => array(
                "vibrate" => 1,
                "sound" => 1,
                "title" => $title,
                "message" => $msg
            )
        );

        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        $t = curl_setopt($ch, CURLOPT_URL, $url);
        $t = curl_setopt($ch, CURLOPT_POST, true);
        $t = curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $t = curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $t = curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $t = curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function sendNotification($notification_details = array()) {
        $CI = &get_instance();
        $db = $this->get_db_instance();
        if (!$db) {
            $this->setError(503, 'database_not_found', 'Error while tried to access database');
            return FALSE;
        }
        if (!empty($notification_details)) {
            $title = $notification_details['title'];
            $message = $notification_details['message'];

            foreach ($notification_details['device_tokens'] as $key => $device_token) {
                $result = FALSE;
                if ($device_token && strtolower($notification_details['platform'][$key]) == 'ios') {
                    $result = self::iphonePushNotification($device_token, $title, $message, $notification_details['build'][$key]);
                } else {
                    $result = self::androidPushNotification($device_token, $title, $message);
                }
                $db->insert('notifications', array('device_token' => $device_token, 'status' => $result, 'message' => $message, 'build_type' => $notification_details['build'][$key]));
            }
        }
        return TRUE;
    }

    public function get_page_from_count($count, $per_page) {
        $pages = floor($count / $per_page);
        if ($count % $per_page) {
            $pages+=1;
        }
        return $pages;
    }

}

?>