<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Name:  MY_Controller
 * 
 */
class MY_Controller extends CI_Controller {

    public $layout = 'default';
    public $data = array();

    public function __construct() {
        parent::__construct();
        
        if ($this->session->has_userdata('UserId') && !$this->input->is_ajax_request()) {
            $this->layout = 'admin';
            $this->load->theme($this->layout);
        }
        $current_uri = $this->router->fetch_class() . "/" . $this->router->fetch_method();
        if (in_array(strtolower($current_uri), $this->config->config['before_login_pages']) && $this->isLogin()) {
            redirect('Admin/genius_insight/index');
        } else if (in_array(strtolower($current_uri), $this->config->config['after_login_pages']) && !$this->isLogin()) {
            redirect('Authentication/index');
        }
    }
    
    
    
    public function get_pagination_links($per_page = ROWS_PER_PAGE, $start_rec = 1, $total_rec = 0, $url = '') {
        $this->load->library("pagination");
        $config['per_page'] = $per_page;
        $config['base_url'] = $url;
        $config['total_rows'] = $total_rec;
        /*PAGINATION CONFIGURATION IS DONE IN CONFIG FILE : \application\config\pagination.php*/
        $this->pagination->initialize($config);
        $links = $this->pagination->create_links();
        if (!empty($links)) {
            $links = '<ul class="pagination bootpag"><li class="prev" data-lp="2">' . $links . '</li></ul>';
        }
        return $links;
    }

    /**
     * @Name : format_date()
     * @Purpose : To change the format of the date.
     * @Call from : Can be called from any controller file.
     * @Functionality : change the format of the date according to the date specified.
     * @Receiver params : date string, Format of the date 
     * @Return params : Return the converted value. 
     */
    function format_date($dateString, $format) {
        if (is_null($dateString) || empty($dateString) || $dateString == "0000-00-00 00:00:00") {
            return "";
        }

        $date = date_create($dateString);
        return date_format($date, $format);
    }

    /**
     * @Name : isLogin()
     * @Purpose : To check user is logged in or not.
     * @Call from : Can be called from any controller file.
     * @Functionality : if user has already logged in or not.
     * @Receiver params : empty
     * @Return params : Return true or false. 
     */
    public function isLogin() {
        if ($this->session->has_userdata('UserId')) {
            return true;
        } else {
            return false;
        }
    }

    /*NOT USED*/    
    //send iphone push notification
    public function IphonePushNotification($deviceToken = '', $title = '', $message = '', $build = '') {
        require_once FCPATH . 'application/libraries/PushDemo/simplepush.php';
        $result = sendPushMessage($deviceToken, $title, $message, $build);
        return $result;
    }
    /*NOT USED*/    
    public function sendNotification($user_details = array(), $property_detail = array()) {
        if (!empty($user_details)) {
            $title = 'Delete notification';
            $message = 'Property at ' . $property_detail['building_street_1'] . ' ' . $property_detail['property_address_2'] . ' address is deleted.';

            foreach ($user_details as $user) {
                $result = FALSE;
                if ($user['device_token']) {
                    $result = $this->IphonePushNotification($user['device_token'], $title, $message, $user['build']);
                    $this->db->insert('notifications', array('device_token' => $user['device_token'], 'status' => $result, 'message' => $message, 'build_type' => $user['build']));
                }
            }
        }
        return TRUE;
    }
    
    /**
    * @Name : unique_key()
    * @Purpose : To generate a unique Id
    * @Call from : Can be called from any controller file.
    * @Functionality : to generate a unique id
    * @Receiver params : No parameters recieved
    * @Return params : Return a unique Key. 
    * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on September 14 2015
    * @Modified :
    */
    public function unique_key(){
        list($microseconds, $seconds) = explode(' ', microtime());
        $unique_id = md5($seconds + $microseconds . getmypid());
        return $unique_id;
    }

}
