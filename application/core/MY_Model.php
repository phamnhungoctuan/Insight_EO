<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @description	All common functions for models can be defined here and can be used in complete application.
 * 
 * @author	Hardeep <Hardeep.intersoft@gmail.com>
 * @date	12 June 2015
 * */
class MY_Model extends CI_Model {

    public $table = '';
    public $validate = array();

    /**
     * @Name : validates()
     * @Purpose : To check the validations of the form and return true if validations verified.
     * @Call from : Can be called from any controller.
     * @Functionality : Load form_validation library.
     *                  Set rules
     *                  Verify the validations 
     * @Receiver params : $data array() contains list of fields and values to validate
     * @Return params : returns true or false 
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 03 July 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 03 July 2015
     */
    public function validates($data = array()) {
        $this->load->library('form_validation');
        if (count($data) == 0)
            $data = $this->validate;
        foreach ($data as $validate)
            if (isset($validate['message']))
                $this->form_validation->set_rules($validate['field'], $validate['label'], $validate['rules'], $validate['message']);
            else
                $this->form_validation->set_rules($validate['field'], $validate['label'], $validate['rules']);
        return $this->form_validation->run();
    }

    /**
     * @Name : fetch_records()
     * @Purpose : To fetch the records from database table for pagination and listing.
     * @Call from : Can be called from any model and controller.
     * @Functionality : Simply gets the records from database tables with specified limit, conditions and starting point.
     * @Receiver params : $limit, $start, $conditions
     * @Return params : Return array of records from table 
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     */
    public function fetch_records($limit = null, $start = null, $conditions = array(), $col_to_sort = NULL, $orderBy = "DESC") {
        if (!is_null($limit) && !is_null($start)) {
            $this->db->limit($limit, $start);
        }
        if (!is_null($col_to_sort)) {
            $this->db->order_by($col_to_sort, $orderBy);
        }
        if (!empty($conditions)) {
            $this->db->where($conditions);
        }
        $query = $this->db->get($this->table);
        $query->result_array();

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    /**
     * @Name : count_results()
     * @Purpose : To count to number of rows in the database.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Simply count the results using predefined count_all_results() function.
     * @Receiver params : No parameters passed
     * @Return params : Returns the count of rows.
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 12 June 2015
     */
    function count_results($where = NULL) {
        if (!is_null($where) && !empty($where)) {
            $this->db->where($where);
        }
        $getData = $this->db->count_all_results($this->table);
        return $getData;
    }

    /**
     * @Name : save()
     * @Purpose : To save the records in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Saves the entry in specified database table.
     * @Receiver params : $data array of values to save in database.
     * @Return params : return last insert id if record is saved else return false.
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 03 July 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 03 July 2015
     */
    function save($data = array()) {
        if ($this->db->insert($this->table, $data))
            return $this->db->insert_id();
        else
            return false;
    }

    /**
     * @Name : update()
     * @Purpose : To save the records in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Saves the entry in specified database table.
     * @Receiver params : $data array of values to save in database.
     * @Return params : return true if record is saved else return false.
     * @Created : Sonia <sonia.intersoft@gmail.com> on 03 July 2015
     * @Modified : Sonia <sonia.intersoft@gmail.com>on 03 July 2015
     */
    function update($data = array(), $where = array()) {
        $query = $this->db->update($this->table, $data, $where);
        return $query;
    }

    /**
     * @Name : delete()
     * @Purpose : To delete the record in the database table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : delete the entry from the database table.
     * @Receiver params : array of values to filter the record to be deleted
     * @Return params : return true if record is deleted else return false.
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 03 July 2015
     * @Modified :
     */
    function delete($where = array()) {
        if ($this->db->delete($this->table, $where))
            return true;
        else
            return false;
    }

    function select_record($where, $table = NULL) {
        $this->db->Select('*');
        $table = is_null($table) || $table == "" ? $this->table : $table;
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else
            return false;
    }

    /**
     * @Name : hash_this()
     * @Purpose : Convert the hash value of the value passed as parameter.
     * @Call from : Authentication.php controller file.
     * @Functionality : Convert the value hash using sha1 algorithm functions.
     * @Receiver params : $value_to_hash value to be converted.
     * @Return params : return converted hash value.
     * @Created : Sonia <sonia.intersoft@gmail.com> on 03 July 2015
     * @Modified : Sonia <sonia.intersoft@gmail.com>on 03 July 2015
     */
    function hash_this($value_to_hash) {
        $security_salt = 'h!u@n#z$o%n^i&a*n';
        $hash_result = sha1(sha1($value_to_hash . $security_salt));
        return $hash_result;
    }

    public function sendMail($parms, $cc_to_admin = TRUE) {
        /* Initialize the SendMail Object */
        $this->load->library('PhpMailer');
        $this->mail = new CI_PHPMailer();
        if (IsSMTP) {
            $this->mail->IsSMTP(); // send via SMTP
        }
        $this->mail->Host = smtp_Host; // SMTP servers
        $this->mail->SMTPAuth = smtp_SMTPAuth; // turn on SMTP authentication
        $this->mail->Username = smtp_Username; // SMTP username
        $this->mail->Password = smtp_Password; // SMTP password
        $this->mail->SMTPDebug = smtp_SMTPDebug; // Enable SMTP debugging
        $this->mail->Port = smtp_Port; // Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $this->mail->SMTPSecure = smtp_SMTPSecure; // Set the encryption system to use - ssl (deprecated) or tls

        /* INITIALIZATION ENDS HERE */
        $this->mail->ContentType = isset($parms['ContentType']) ? $parms['ContentType'] : 'text/html';
        $this->mail->From = smtp_Username;
        $this->mail->FromName = 'Energy Re-Mastered Apps';
        if (!is_array($parms['To']) && strpos($parms['To'], ',') !== false || strpos($parms['To'], ';') !== false) {
            $parms['To'] = explode(",", $parms['To']);
        }
        if (is_array($parms['To'])) {
            foreach ($parms['To'] as $email) {
                $this->mail->AddAddress($email);
            }
        } else {
            $this->mail->AddAddress($parms['To']);
        }

        if ($cc_to_admin) {
            $this->mail->AddBCC("admin@quantumhealthapps.com","");
           $this->mail->AddBCC("pankaj.intersoft1@gmail.com", "");
        }

        if (isset($parms['BCC'])) {
            $this->mail->AddBCC($parms['BCC']);
        }
        if (isset($parms['attachment']) && isset($parms['attachment']['path'])) {
            $this->mail->addAttachment($parms['attachment']['path']);
        }
        $this->mail->Subject = isset($parms['Subject']) ? $parms['Subject'] : 'Aura Genie';
        $this->mail->msgHTML(isset($parms['Body']) ? $parms['Body'] : '');

        if ($this->mail->Send()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @Name : generate_key()
     * @Purpose : To generate unique 10 digit code.
     * @Call from : Can be called from any controller file.
     * @Functionality : to generate the Activation Key
     * @Receiver params : One optional Length parameter Recieved
     * @Return params : Return the code generated
     * @Created : Hardeep Kaur <hardeep.intersoft@gmail.com> on 4 July 2015
     * @Modified :
     */
    
/*NOT USED*/
    public function generate_random_string1($length = 10) {
        $key = '';
        $numbers = range(2, 9);
        $characters = array_merge(range('a', 'h'), range('p', 'z'), array('j', 'k', 'm', 'n'));
        $type = array();
        for ($i = 0; $i < length; $i++) {
            if (count($type) < 1) {
                $type = array('number', 'lowercase', 'uppercase');
            }
            $temp = array_rand[$type];
            switch ($type[$temp]) {
                case "number" :
                    $key .= $numbers[array_rand($numbers)];
                    break;
                case "lowercase" :
                    $key .= $characters[array_rand($characters)];
                    break;
                case "uppercase" :
                    $key .= strtoupper($characters[array_rand($characters)]);
                    break;
            }
            unset($type[$temp]);
        }
        return $key;
    }

    function generate_random_string($length = 10) {
        $key = '';
        $chars = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";

        $size = strlen($chars);
        for ($i = 0; $i < $length; $i++) {
            $key .= $chars[mt_rand(0, $size - 1)];
        }

        return $key;
    }

    /**
     * @Name : generateKey()
     * @Purpose : To generate a randon string unique for the specifies column in table.
     * @Call from : Can be called form any model and controller.
     * @Functionality : Generate the random String using helper method and checks the uniqueness of the string in the specified column of specified table
     * @Receiver params : Column in which to check the string uniqueness
     * @Return params : return the unique random string.
     * @Created : Hardeep <Hardeep.intersoft@gmail.com> on 05 November 2015
     * @Modified : Hardeep <Hardeep.intersoft@gmail.com> on 05 November 2015
     */
    public function generateKey($col) {
        $randomString = $this->generate_random_string();
        $where[$col] = $randomString;
        while ($this->count_results($where)) {
            $randomString = $this->generate_random_string();
            $where[$col] = $randomString;
        }
        return $randomString;
    }

}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */