<?php
if (session_status() != PHP_SESSION_ACTIVE)
    session_start();
if ((!isset($_SESSION['username'])) || (isset($_REQUEST['logout']))) {
    session_destroy();
    header("location: api_log.php");
}


/* get the directory in the log file folder */
$dirs = array_filter(glob('*'), 'is_dir');
$select_folder_html = '<select name="logtype">';
foreach ($dirs as $dir){
    $select_folder_html .= '<option value="'.$dir.'">'.$dir.'</option>';
}
$select_folder_html .= '</select>';
?>
<html>
    <head>
        <title>Log File</title>
        <style type="text/css">
            html, body {height:100%; margin:0; overflow:hidden;background-color: blanchedalmond}
            .header{display:block; background-color:black;color:#FFF; ;padding:1%}
            section {max-height:92%; display:block; overflow:auto;}
            section .filecontent {;padding:1% 1% 4%}
        </style>
    </head>
    <body>
        <div class="header">

            Select File :  <?php echo $select_folder_html?>
<!--            <select name="logtype">
                <option value="request" selected="selected">API Request</option>
                <option value="specialcase">Special Case</option>
            </select>-->
            <select name='files' onchange='getFileContent(this)' >
                <option value='' disabled='disabled' selected='selected'>Select File</option>
                <?php
                $files = array_map("htmlspecialchars", scandir("../log/request"));
                rsort($files);
                foreach ($files as $file) {
                    $startname = substr($file, 0, 4);
                    $extension = substr($file, strrpos($file, '.'));
                    if ($startname == 'log-' && $extension == '.php') {

                        echo "<option value='$file'>$file</option>";
                    }
                }
                ?>
            </select>
            <input class="reload" type="button" onclick="reload_file()" value="Reload File" style="margin:0 10px;" disabled="disabled"/>
            <input type="button" onclick="window.location = 'view_log.php?logout=1'" value="Logout" style="float: right"/>
        </div>
        <section>
            <div class="filecontent"></div>
        </section>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script>

                $(document).ready(function() {
                    $('[name="logtype"]').change(function() {
                        var folder = $(this).val();
                        if (folder) {
                            $.ajax({
                                url: "function.php",
                                method: "POST",
                                data: {logtype: folder},
                                success: function(response) {
                                    $('[name="files"]').html((response));
                                    $(".filecontent").html("");
                                    $('.reload').attr('disabled','disabled');
                                }
                            });
                        }

                    });
                });
                function getFileContent(sel) {
                    var file = $(sel).val();
                    var logtype = $('[name="logtype"]').val();
                    var content;
                    $.ajax({
                        url: "function.php",
                        method: "POST",
                        dataType: 'json',
                        data: {file: file, logtype : logtype},
                        success: function(response) {
                            $(".filecontent").html((response));
                            $('.reload').removeAttr('disabled');
                        }
                    });
                }

                function reload_file() {
                    var file = $('[name="files"]').val();
                    if (file) {
                        getFileContent($('[name="files"]'));
                    }
                }
        </script>
    </body>
</html>
