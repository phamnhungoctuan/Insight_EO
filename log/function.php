<?php

session_start();
if (!isset($_SESSION['username'])) {
    session_destroy();
    header("location: api_log.php");
}
if (isset($_POST['file'])) {
    $log_type_folder = !empty($_POST['logtype']) ? $_POST['logtype'] : "request";
    $filename = $_POST['file'];
    echo json_encode(file_get_contents('../log/' .$log_type_folder.'/'. $filename));
} else if (isset($_POST['logtype'])) {
    $log_type_folder =  $_POST['logtype'];
    $files = array_map("htmlspecialchars", scandir("../log/" . $log_type_folder));
    rsort($files);
    $html = '<option value="" disabled="disabled" selected="selected">Select File</option>';
    foreach ($files as $file) {
        $startname = substr($file, 0, 4);
        $extension = substr($file, strrpos($file, '.'));
        if ($startname == 'log-' && $extension == '.php') {

            $html .= '<option value="'.$file.'">'.$file.'</option>';
        }
    }
    echo $html;
}
?>